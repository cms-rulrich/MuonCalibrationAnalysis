#include <TLegend.h>
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TProfile.h"
#include "TGraph.h"
#include <TKey.h>
#include <TDirectory.h>
#include <TPad.h>
#include <TLine.h>

#include <TCanvas.h>
#include <TApplication.h>
#include <TMath.h>
#include <TRandom.h>

#include <TGraphErrors.h>
#include <TF1.h>
#include <TText.h>

#include "Minuit2/Minuit2Minimizer.h"
#include "Minuit2/FCNBase.h"
#include "TFitterMinuit.h"

#include <Minuit2/MnUserParameters.h>
#include <Minuit2/MnMigrad.h>
#include <Minuit2/MnSimplex.h>
#include <Minuit2/MnScan.h>
#include <Minuit2/MnPrint.h>
#include <Minuit2/MnMinos.h>
#include <Minuit2/MinosError.h>
#include <Minuit2/FCNBase.h>
#include <Minuit2/FunctionMinimum.h>
#include <Minuit2/MnMachinePrecision.h>

#include <cmath>
#include <iostream>
#include <sstream>
#include <vector>
#include <cstdlib>

#include "tdr_macro.h"


using namespace std;

//#####################################################
// simulation of mesh PMT response, loop over (1) 1st gynod gain
// and (2) probability photoelectrons to pass 1st dynod 
// program usage: pmt5Ld [<Nphe>] [QIErms]
// created by V.P.Popov 05.11.2015
//######################################################

static int printFlag = 0;
static int PhDistributionType = 1;  // 0=Poisson, 1=Landau, 2=none (e.g. Muon-energy loss -> muon signal [model] )


static double QIEsigma = 5; // in fC, noise from electronics

// static double meanPE = 1.0; // MEAN NUMBER OF PE !!!!!



 


const int NphMP = 1; // most probable # of cherenkov photons
const double LandRMS = .5;




long 
pmtResponse(const double nphe, double gain, double MissDynodProb) 
{
  const int nDyn = 15; // 15 stages ! R5505

  //gain = 2.2;
  //MissDynodProb = 0.2;
  
  //  double ne = std::max(0., nphe);
  double ne = nphe; // QE already included before
  
  for (int iDyn=0; iDyn<nDyn; ++iDyn) {
    
    const double gainDyn = (iDyn==0 ? gain*10 : gain);
    
    /*
    long prod = 0;
    for (long ie=0; ie<ne; ++ie) {
      if (gRandom->Rndm() < MissDynodProb) 
	prod += 1;
      else 
	prod += gRandom->Poisson(gain);
    }    ne = prod;

    ne = prod;
    */
    
    const double n_miss = std::max(0., ne * MissDynodProb);
    const double n_hit = std::max(0., ne - n_miss);
    
    if (n_hit<100000) 
      ne = n_miss + gRandom->Poisson( n_hit*gainDyn );
    else
      ne = n_miss + gRandom->Gaus( n_hit*gainDyn, sqrt(n_hit*gainDyn)  );    
  }
  
  return ne;
}







static int QIENbins = 100;
static int QIEstart = -100;
static int QIEend = 500; // auto-binning: 0, 1
static bool QIEbinningOverwrite = false; // overwrite with data binning

struct SIM_RESULT {
  SIM_RESULT() : spect(0), gain(0), gainErr(0), amp(0), ampErr(0) {}
  SIM_RESULT(TH1D* h, const double g, const double gE, const double a, const double aE) : spect(h), gain(g), gainErr(gE), amp(a), ampErr(aE) {}
  TH1D* spect;
  double gain;
  double gainErr;
  double amp;
  double ampErr;
};


SIM_RESULT
SIM(const double gain, const double MissDynodProb, const double meanPE, const int color)
{
  const double TransportEff = meanPE / NphMP;
  
  ostringstream name;
  name << "QIEamp_" << gain << "_" << MissDynodProb << "_" << meanPE;
  
  TH1D* hqie = new TH1D(name.str().c_str(), name.str().c_str(), QIENbins, QIEstart, QIEend);
  hqie->Sumw2();
  hqie->SetLineColor(color);
  hqie->SetLineWidth(2);
  hqie->SetXTitle("Q  (fC)");
  
  
  // initialize 
  
  double gains = 0.;
  double gainrms = 0.;
  
  double amps = 0.;
  double amprms = 0.;
  
  const long int N_Measures = 100000; //50000000;

  for(long int i=0; i<N_Measures; i++) {
    
    double nphe = 0;
    switch (PhDistributionType) {
    case 0:
      nphe = gRandom->Poisson(meanPE);
      break;
    case 1:
      nphe = gRandom->Landau(NphMP, LandRMS)* TransportEff;
      nphe = gRandom->Poisson(nphe);
      break;
    case 2:
      nphe = meanPE;
      break;
    }
    
    // nphe = 1;
    const long Ne = pmtResponse(nphe, gain, MissDynodProb);
    const double Q = Ne * 1.60217e-19;
    const double fC = Q  / 1e-15;
    
    // cout << nphe << " " << Ne << " " << Q << endl;
    
    double amp = fC;
    double qie = gRandom->Gaus() * QIEsigma; // noise

    amp += qie;
    
    hqie->Fill( amp);
    
    amps += double(amp);
    amprms += pow(double(amp), 2);
    
    if (nphe>=1) {
      gains += double(Ne) / nphe;
      gainrms += pow(double(Ne) / nphe, 2);
    }
  }
  
  //  nphemean /= N_Measures;
  gains /= N_Measures;
  gainrms /= N_Measures;
  gainrms = gainrms - gains*gains;
  gainrms = sqrt(gainrms) / sqrt(N_Measures);

  amps /= N_Measures;
  amprms /= N_Measures;
  amprms = amprms - amps*amps;
  amprms = sqrt(amprms) / sqrt(N_Measures);
  
  //hqie->Scale(1./(hqie->GetBinWidth(2)*hqie->GetEntries()));
  hqie->Scale(1./hqie->Integral("width"));
  //hqie->Scale(1./(hqie->GetEntries()));
  
  if (color>0) {
    cout << "Amplification: " << gains << " +- " << gainrms << endl; 
    hqie->Write();
  }
  
  return SIM_RESULT(hqie, gains, gainrms, amps, amprms);
}



double
CHI2(TH1D* a, TGraph* b)
{
  double chi2 = 0;
  for (int i=0; i<a->GetNbinsX(); ++i) {
    //const double err = pow(a->GetBinError(i+1) - b->GetBinError(i+1), 2);
    const double err = pow(a->GetBinError(i+1),2);
    if (err>0)
      chi2 += pow(a->GetBinContent(i+1) - b->Eval(a->GetBinCenter(i+1)), 2) / err;
  }
  return chi2;
}

double
CHI2(TH1D* a, TH1D* b)
{
  TGraph* bg = new TGraph();
  for (int i=0; i<b->GetNbinsX(); ++i)
    bg->SetPoint(bg->GetN()+1, b->GetBinCenter(i+1), b->GetBinContent(i+1));
  return CHI2(a, bg);
}

double
LOGL(TH1D* a, TGraph* b)
{
  double logL = 0;
  for (int i=0; i<a->GetNbinsX(); ++i) {
    //const double err = pow(a->GetBinError(i+1) - b->GetBinError(i+1), 2);
    const double model = b->Eval(a->GetBinCenter(i+1));
    if (model>0) {
      logL += a->GetBinContent(i+1) * log(model);
    }
  }
  return -2*logL;
}

double
LOGL(TH1D* a, TH1D* b)
{
  TGraph* bg = new TGraph();
  for (int i=0; i<b->GetNbinsX(); ++i)
    bg->SetPoint(bg->GetN()+1, b->GetBinCenter(i+1), b->GetBinContent(i+1));
  return LOGL(a, bg);
}

class MyFCN : public ROOT::Minuit2::FCNBase {
public:
  // Constructor - here you could e.g. pass in a histogram
  MyFCN(TH1D* data) : fData(data) {}
  
  double operator() (const std::vector<double> & x) const {
    TH1D* model = SIM(x[0], x[1], x[2], 0).spect;
    //double opt = CHI2(fData, model);
    double opt = LOGL(fData, model);
    delete model;
    return opt;
  }
  double Up() const { return 1; }
  // Change in function corresponding to 1 sigma error
  // - 1.0 for chi2 / 0.5 for lieklihood
  // - 1.0 for chi2 / 0.5 for likelihood
private:
  TH1D* fData;
};



void
SIM_distribution(const string& fname, const int mod, const int sec, const bool model) 
{
  // data
  //TFile* data = TFile::Open("data_Cosmics_MuonHLTSkim_2015E_0T/muons_8_9.root"); // june
  //TFile* data = TFile::Open("muon_data.root");
  
  cout << "file: " << fname.c_str() << "  " << flush;    
  TFile* file = TFile::Open(fname.c_str(), "read");
  string dir = "";
  if (!file || !file->IsOpen()) {
    cout << "Could not open file" << endl;
    exit(10);
  }
  dir = "";
  //if (!DIR.empty()) {
  //dir = DIR + "/";
  //}
  if (dir.empty()) {
    TIter next(file->GetListOfKeys());
    TKey* obj = 0;
    while (obj = (TKey*) next()) {
      if (string(obj->GetClassName()).find("TDirectory") == 0) {
        if (string(obj->GetName()) == "NoiseFits")
          continue;
        dir = obj->GetName();
        dir += "/";
        break;
      }
    }
  }
  cout << "found CFF in dir " << dir << endl;


    
  //TFile* data = TFile::Open("../../../../castor.svn/MuonIntercalibration/muons_data_PACastor_Muon_2016.root");
  //if (!data) {
  //cout << " cannot open data file" << endl;
  //exit(1);
  //}
  // 2 11
  ostringstream dataName;
  dataName << dir << "/" << "MuonSignal_sec_" << sec << "_mod_" << mod; 
  //const string dataName = "data_Cosmics_MuonHLTSkim_2015E_0T/MuonSignalSecCh_Excl8_mod_2_sec_11"; // june, old data
  //TH1D* channel = (TH1D*) data->Get("data_PACastor_Muon_2016/MuonSignalSecCh_mod_2_sec_11");
  TH1D* channel = (TH1D*) file->Get(dataName.str().c_str());
  //TH1D* channel = (TH1D*) data->Get("data_Cosmics_MuonHLTSkim_2015E_0T/MuonSignalSecCh_Excl8_mod_11_sec_2"); // june, old data
  if (!channel) {
    cout << " cannot read data" << endl;
    file->ls();
    exit(1);
  }
  channel->SetMarkerSize(1.5);
  channel->SetLineWidth(3);
  channel->Scale(1/channel->Integral("width"));
  
  // noise
  //TFile* noiseFile = TFile::Open("../../../../castor.svn/MuonIntercalibration/mean_rms.root"); // 2016 PA noise
  //TFile* noiseFile = TFile::Open("../../../.././CommonFSQFramework/Core/test/HaloMuonAna/output/mean_rms.root"); // 2015 noise
  //TFile* noiseFile = TFile::Open("mean_rms.root"); 
  //TH1D* noise = 0;
  //if (!noiseFile) {
  //cout << " cannot open noise file" << endl;
  //exit(1);
  //} else {
  ostringstream noiseHist;
  noiseHist << dir << "/" << "NoiseSignal_sec_" << sec << "_mod_" << mod;
  //const string noiseHist = "data_MinimumBias_2015_0T_June/NoiseSignal_sec_2_mod_2";
  //const string noiseHist = "data_MinimumBias_Run2015A/ChannelNoise_sec_11_mod_2");
  //noise = (TH1D*) noiseFile->Get("data_PAMinimumBias1_Noise_2016/ChannelNoise_sec_2_mod_11");
  //noise = (TH1D*) noiseFile->Get("data_MinimumBias_Run2015A/ChannelNoise_sec_2_mod_11");
  TH1D* noise = (TH1D*) file->Get(noiseHist.str().c_str());
  if (!noise) {
    cout << " cannot read noise data" << endl;
    exit(1);
  }
  //    noise->Rebin(4); // FOR **SOME** 2015 ONLY !!!!!!!!!
  QIEsigma = noise->GetRMS();
  //}
  
  channel->SetMarkerStyle(20);
  if (noise) {
    noise->Scale(1./noise->Integral("width"));
    noise->SetFillStyle(1001);
    noise->SetFillColor(kRed);
    noise->SetLineWidth(1);
    noise->SetLineStyle(3);
    noise->SetLineColor(kRed);
  }
  
  if (QIEbinningOverwrite) {
    QIENbins = channel->GetNbinsX();
    QIEstart = channel->GetXaxis()->GetXmin();
    QIEend = channel->GetXaxis()->GetXmax();
  }
  
  

  TFile* fil = new TFile("mesh.root", "RECREATE"); //create the Root file
  
 
  TCanvas* c = new TCanvas("mesh");
  //c->SetLogx(1);
  c->SetLogy(1);

  TLegend* leg = new TLegend(0.45, 0.65, 0.95, 0.92);
  leg->SetFillStyle(0);
  leg->SetBorderSize(0);
  leg->SetTextSize(0.043);
  leg->SetTextFont(42);
  ostringstream legt;
  legt << "CASTOR Tower " << sec << " Module " << mod;
  leg->SetHeader(legt.str().c_str());
  
  leg->AddEntry(channel, "Muon candidates", "p");
  
  TH2D* inputBase = (TH2D*) file->Get((dir + "/ch_input_mean_2d").c_str());
  TH2D* inputRMS = (TH2D*) file->Get((dir + "/ch_input_rms_2d").c_str());
  const double thresh = inputBase->GetBinContent(mod, sec) + 2.5*inputRMS->GetBinContent(mod, sec) ;

  if (noise) {
    noise->SetMaximum(1.5);
    noise->Draw(); // OMG
    gPad->Modified(); // OMG
    gPad->Update(); // OMG
    
    TH2D* frame = new TH2D("frame", "frame",
                           20, -50,
                           1500, // /*min(noise->GetXaxis()->GetXmin(), channel->GetXaxis()->GetXmin()),*/ max(noise->GetXaxis()->GetXmax(), channel->GetXaxis()->GetXmax()),
                           20, pow(10,gPad->GetUymin()), pow(10,gPad->GetUymax()));
                           //2, gPad->GetUymin(), gPad->GetUymax()));
    frame->SetXTitle("Charge [fC]");
    frame->SetYTitle("1/N dN/dQ [1/fC]");
    frame->GetXaxis()->SetTitleSize(0.055);
    frame->GetYaxis()->SetTitleSize(0.055);
    frame->GetXaxis()->SetLabelSize(0.045);
    frame->GetYaxis()->SetLabelSize(0.045);
    frame->GetXaxis()->SetTitleOffset(1.2);
    frame->GetYaxis()->SetTitleOffset(1.3);
    frame->Draw();

    noise->Draw("same, hist");
    channel->Draw("same, p");
    leg->AddEntry(noise, "Noise", "f");
  } else {
    channel->SetXTitle("Charge [fC]");
    channel->SetYTitle("1/N dN/dQ [1/fC]");
    channel->SetMaximum(0);
    channel->Draw("p");
  }


  TText* cms = new TText(0.21, 0.87, "CMS");
  cms->SetNDC(1);
  cms->SetTextSize(0.07);
  cms->Draw("same");
  
  
  // gain, miss, color
  /*
  double meanPE = 0.7;
  TH1D* h1 = SIM(3.5, 0.5, meanPE, 1);
  h1->Draw("same,hist,l");
  cout << "chi2=" << CHI2(h1, channel) << endl;

  TH1D* h2 = SIM(3.5, 0.51, meanPE, 2);
  h2->Draw("same,hist,l");
  cout << "chi2=" << CHI2(h2, channel) << endl;

  TH1D* h3 = SIM(3.6, 0.51, meanPE, 3);
  h3->Draw("same,hist,l");
  cout << "chi2=" << CHI2(h3, channel) << endl;
  */

  // ----------------
  // from hand scan: confirmed by MnScan ..
  //"gain per dynode" =2.6,
  //"p_miss per dynode" = 0.21, and
  //"mean_PE" = 0.5.

  //SIM(2.7, 0.31, 0.6, kCyan+1).spect->Draw("same,hist,l"); 
  //SIM(2.58, 0.2, 0.58, kAzure).spect->Draw("same,hist,l");

  if (model) {
    TH1D* hscan = SIM(2.1, 0.15, .4, kBlue).spect;   //

    //TH1D* hscan = SIM(2.45, 0.21, 0.7, kBlue).spect;  // FOR 2016 
    //TH1D* hscan = SIM(2.65, 0.21, 0.58, kBlue).spect;   // FOR 2015 !! !!!!!
    //TH1D* hscan = SIM(2.6, 0.21, 0.58, kBlue).spect;   // FOR 2015 !!
    //TH1D* hscan = SIM(2.7, 0.16, 0.3, kBlue).spect;   // FOR 2015 !!
    hscan->SetLineWidth(4);
    hscan->Draw("same,hist,l");
    //leg->AddEntry(hscan, "Model (#delta=2.65, p_{miss}=0.21, n_{PE}=0.58) ", "l");
    leg->AddEntry(hscan, "Model", "l");
    cout << "chi2=" << CHI2(hscan, channel) << endl;
  }
  
  // --- FIT
  if (false) {
   
    ROOT::Math::MinimizerOptions::SetDefaultTolerance(1); 
  
    MyFCN fcn(channel);
    
    ROOT::Minuit2::MnUserParameters parameters;
    parameters.Add("gain", 2.1, 0.1);
    parameters.Add("miss", 0.15, 0.01);
    parameters.Add("PE", 0.4, 0.1);

    //ROOT::Minuit2::MnMigrad min(fcn, parameters);
    ROOT::Minuit2::MnSimplex min(fcn, parameters);
    //ROOT::Minuit2::MnScan min(fcn, parameters);
    //min.SetTolerance(0.01);
    /*
      min.SetMaxFunctionCalls(1000);
      min.SetMaxIterations(100);
      min.SetTolerance(0.01);
    */
    
    ROOT::Minuit2::FunctionMinimum minimum0 = min(1000, 1e-2);
    // calcChi2.GetNData() - parameters.VariableParameters()  
    cout << minimum0;
    
    //min.Fix((unsigned int)0);
    //min.Fix((unsigned int)1);
    //ROOT::Minuit2::FunctionMinimum minimum = min(1000, 1e0);
    ROOT::Minuit2::FunctionMinimum minimum = minimum0;
    cout << minimum;
    
    if (!minimum.IsValid()) {
      cout << " ERROR: minimum is invalid !!!!!!!" << endl;
    }
    
    
    TH1D* hfit = SIM(min.Value((unsigned int)0), min.Value(1), min.Value(2), kBlue).spect;
    hfit->SetLineWidth(4);
    hfit->Draw("same,hist,l");
    leg->AddEntry(hfit, "PMT model", "l");
    //  cout << "chi2=" << CHI2(h3, channel) << endl;
    
  } // end fit
  

  channel->Draw("p,same");
  
  
   // ----- SCAN 
   if (false) {

     TH1D* hbest = 0;
     double chi2best = 1e30;
     
     for (int i=0; i<5; ++i) {
       double meanPE = 0.48 + 0.01*i;
       
       for (int j=0; j<5; ++j) {
	 double gain = 2.5 + 0.04*j;
	 
	 for (int k=0; k<5; ++k) {
	   double miss = 0.19 + 0.007*k;
	   
	   TH1D* hf = SIM(gain, miss, meanPE, kBlue).spect;
	   double chi2 = CHI2(hf, channel);
	   cout << "chi2=" << chi2 << endl;
	   
	   if (chi2<chi2best) {
	     cout << "BEST" << endl;
	     chi2best = chi2;
	     hbest = hf;
	   }
	 }
       }
     }
     
     if (hbest) {
       hbest->Draw("same,hist,l");
     }     

     cout << " best chi2=" << chi2best << " " << chi2best/hbest->GetNbinsX() << endl;   
   } // end scan 
   
   gPad->Modified();
   gPad->Update();
   gPad->Modified();
   
   TLine* line = new TLine(thresh, gPad->GetUymin(), thresh, gPad->GetUymax());
   line->SetLineColor(kGreen+2);
   line->SetLineStyle(9);
   line->SetLineWidth(4);
   line->Draw();
   leg->AddEntry(line, "Noise threshold", "l");

   
   leg->Draw();

   ostringstream print;
   print << "channel_fit_" << fname.substr(fname.rfind('/')+1, fname.rfind(".root")-fname.rfind('/')-1) << "_mod_" << mod << "_sec_" << sec << ".pdf";
   
   c->SaveAs(print.str().c_str());

  fil->Write();
  fil->Flush();
  //fil->Close(); 
}



void
Linearity()
{
  //"gain per dynode" =2.6,
  //"p_miss per dynode" = 0.21, and
  //"mean_PE" = 0.5.
  
  vector<double> vecPE, vecAmp, vecAmpErr;
  
  double PE = 0.1;
  
  for (int i=0; i<20; ++i) {
    
    if (i<5) 
      PE += 0.2;
    else if (i<10)
      PE += 0.5;
    else if (i<15)
      PE += 1.0;
    else 
      PE += 2.0;
    
    const SIM_RESULT result = SIM(2.6, 0.21, PE, 0);
    
    vecPE.push_back(PE);
    vecAmp.push_back(result.amp);
    vecAmpErr.push_back(result.ampErr);    
  }
  
  TCanvas* clin = new TCanvas("lineariry");
  //  clin->SetOptTitle(0);
  clin->SetGridx(1);
  clin->SetGridy(1);
  clin->SetLogx(1);
  clin->SetLogy(1);
  
  TGraphErrors* ge = new TGraphErrors(vecPE.size(), &vecPE.front(), &vecAmp.front(), 0, &vecAmpErr.front());
  ge->SetMarkerStyle(20);
  ge->Draw("ap");
  ge->GetHistogram()->SetTitle("");
  ge->GetHistogram()->SetXTitle("Number of photo-electrons");
  ge->GetHistogram()->SetYTitle("Charge  [fC]");

  TLegend* leg = new TLegend(0.2, 0.6, 0.5, 0.8);
  leg->AddEntry(ge, "PMT model (#delta=2.6, p_{miss}=0.21)", "p");

  TF1* line = new TF1("line", "pol1");
  line->SetNpx(1000);
  line->SetLineColor(kBlue);
  line->SetLineStyle(1);
  
  ge->Fit(line, "0", "0", 5, 50);
  
  line->SetRange(5,50);
  leg->AddEntry(line->DrawCopy("same,l"), "Linear fit", "l");

  line->SetLineColor(kRed);
  line->SetRange(0,5);
  line->SetLineStyle(9);
  leg->AddEntry(line->DrawCopy("same,l"), "Linear extrapolation", "l");

  leg->Draw();
  
  clin->SaveAs("Linearity.pdf");
}




int 
main(int argc, char **argv) 
{  
  //  if (argc > 1)  { meanPE = atof(argv[1]); }

  if (argc<5) {
    cout << "provide input file and module and sector and flag-show-model" << endl;
    return 0;
  }

  const string fname(argv[1]);
  const int mod = atoi(argv[2]);
  const int sec = atoi(argv[3]);
  const bool model = atoi(argv[4]);
  
  TApplication run("hold",0,0);

  setTDRStyle();
  
  //Linearity();
  SIM_distribution(fname, mod, sec, model);
  run.Run();
  

  return 0;
}
