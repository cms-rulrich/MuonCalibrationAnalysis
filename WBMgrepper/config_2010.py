Debug=False
READONLY=True

specialKeyPostfix = ""

import sys


# -------------------------------------------
# 2010 (no triggers in full year? -> there was no HLT path for castor!!!!)
# -------------------------------------------
# 146994 - 153688 -> not a single castor muon trigger !
#MIN_RUN=136672
#MAX_RUN=136974 
#MIN_RUN=146994 # first commissioning ???
#MAX_RUN=149776
#MIN_RUN=152000 # 152000 - 153688 -> not a single muon
MIN_RUN=146994 # Oct 01, first runs "special castor"
MAX_RUN=153688 # very last run 2010, Dec 08

