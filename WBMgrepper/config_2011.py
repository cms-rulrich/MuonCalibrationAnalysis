Debug=False
READONLY=True

specialKeyPostfix = ""


# -------------------------------------------
# 2011
# start beam commissioning: 2011.03.02. Roughly run 159502 
# first run with castor muon triggers: 160640
# first 3.8T, until 161248, starting from 161971: 0T, until 162580
# next run with 3.8T is 162820 (there are a few runs with 2T, too)
# another phase of 0T is in mid Mai (~13),  164813, 164807 and 164803
# MAX_RUN=164822 # last run 2011 with 0T June 13
# MIN_RUN=164865 # first run 2011 with 3.8T June 13
# end of year: castor de-installation Dec 7
#              -> NO CASTOR in 2012
# MAX_RUN=183165 # very last run 2011 with CASTOR, Dec 07
# -------------------------------------------
MIN_RUN=159502 # start 2011 
MAX_RUN=166530 # this is ~June 2011
##MIN_RUN=164856
#MAX_RUN=166436
#MAX_RUN=165533
