#!/usr/bin/python
# coding: utf-8

from subprocess import call
import sys
from collections import namedtuple
import datetime
import json
import urllib # for URL encoding (special characters)
import gzip
import shutil
from os import listdir, path
import os
import os.path


# -----------------------------------------------------------------------
# list here runs, where the wbm web interface does not seem to work for:
BadRuns = []

BadRuns.append(209948) # HLTSummary webpage does not open
BadRuns.append(159692) # HLTSummary webpage does not open
BadRuns.append(209862) # Huge rate... eLog mentions problem in CASTOR!
BadRuns.append(165590) # HLTSummary webpage does not open

# 2015 HV issues (DCS configurator) 262467-262545, 262636-262686, 262722-262723
for br in xrange(262467, 262545+1): BadRuns.append(br)
for br in xrange(262636, 262686+1): BadRuns.append(br)
for br in xrange(262722, 262723+1): BadRuns.append(br)

# all 2016 pA runs until (including) 284949 are with -16 BX trigger shift and must be discarded
for br in xrange(284367,284949): BadRuns.append(br)

# -----------------------------------------------------------------------
BadPagesFound = [] # these pages don't load properly. Don't try again


# -----------------------------------------------------------------------
# read config file 
if len(sys.argv) != 3:
    print ("usage: %s config_file beam_selection" % os.path.basename(sys.argv[0]))
    print ("beam_selection: halo, halo_450GeV, stable")
    sys.exit(2)

config_file = os.path.basename(sys.argv[1])
beam_mode = os.path.basename(sys.argv[2])
if config_file[-3:] == ".py":
    config_file = config_file[:-3]    
config = __import__(config_file, globals(), locals(), [])


if (config.specialKeyPostfix != ""):
    config.specialKeyPostfix += "_"
config.specialKeyPostfix += beam_mode


# https://lhc-commissioning.web.cern.ch/lhc-commissioning/systems/data-exchange/doc/LHC-OP-ES-0005-10-00.pdf

ExcludeLHC = [] # case insensitive

if beam_mode == "halo":
    # select all non-collisions at all beam energies
    ExcludeLHC.extend(["Squeeze", "adjust", "stable", "unstable"]) # all energies
elif beam_mode == "halo_450GeV":
    # select non-collisions only before ramp, thus at 450GeV beams
    ExcludeLHC.extend(["Ramp", "FlatTop", "Squeeze", "adjust", "stable", "unstable"]) # only 450GeV
elif beam_mode == "flattop":
    # select non-colliding FLAT TOP
    ExcludeLHC.extend(["stable", "unstable", "preramp", "Ramp", "injprob", "injphys"]) # stable beams
elif beam_mode == "stable":
    # select STABLE COLLISION 
    ExcludeLHC.extend(["adjust", "unstable", "Squeeze", "preramp", "Ramp", "FlatTop", "injprob", "injphys"]) # stable beams
else:
    print ("not a valid beam-mode choice! Select: halo, halo_450GeV, flattop, stable")
    sys.exit(3)
 
    
# these are never good
ExcludeLHC.append("Cycling") # before injection
ExcludeLHC.append("Recovery") # after quench
ExcludeLHC.append("Setup") # No beam in ring
ExcludeLHC.append("Abort") #
ExcludeLHC.append("dump")
ExcludeLHC.append("down")
ExcludeLHC.append("recover")
ExcludeLHC.append("unknown")
ExcludeLHC.append("NoBeam")
ExcludeLHC.append("InjStup")


#config.READONLY=False
#config.READONLY=True # this will never try to download anything from the web


#update this value to some sensible run number, depending on the ongoing data taking ! 
#CHECK_FOR_NEW_DATA_AFTER_RUN=263801 # right now -> End of 2015 (last run 263757)
#CHECK_FOR_NEW_DATA_AFTER_RUN=284400 # right now -> Beginning of 2016 pA run
CHECK_FOR_NEW_DATA_AFTER_RUN=325590 # Beginning of 2018 running with CASTOR










# ===================================================================


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'




shFound = True
try:
    import sh
except ImportError:
    shFound = False
    print ("Please install sh module: pip install sh")
#shFound = False


nruns = 0
ndown = 0
ncache = 0

totalRuns = 0
totalEvents = 0
totalLumi = 0
rejectedEvents = 0
rejectedRuns = 0

tableFormat = "|{run:<6}|{date:<9}|{n:<7}|{lumi:<4}|{r:<6}|{beam:<9}|{b:<3}|{mode:<60}|"

#if shFound:
#    git = sh.git.bake(_cwd='.')



# ==========================================================
# read the file BadQueries.txt and store it local memory
# ==========================================================
def read_bad_query_list(filename="BadQueries.txt"):
    f = None

    if not os.path.isfile(filename):
        return

    try:
        f = open(filename, "r")
        for line in f.readlines():
            BadPagesFound.append(line.rstrip('\n'))

    except:
        print ('could not read file ' + filename + '. FATAL!')
        sys.exit(1)    

    finally:
        if f is not None:
            f.close()


# ==========================================================
# read the file BadQueries.txt and store it local memory
# ==========================================================
def add_bad_query(query, filename="BadQueries.txt"):

    print
    print ("**************************")
    print ("INFO: adding: " + query + " to list of BAD queries: " + filename)
    print ("**************************")
    print

    sys.exit(10)
    
    f = None
    try:
        mode = "w"
        if os.path.isfile(filename):
            mode = "a"
        
        f = open(filename, mode)
        f.write(query + '\n')

    except:
        print ('could not write/append file ' + filename + '. FATAL!')
        sys.exit(1)    

    finally:
        if f is not None:
            f.close()

    BadPagesFound.append(query)


# ==========================================================
# read the file GrepperCache.txt and store it local memory
# ==========================================================

CacheEntry = namedtuple("CacheEntry", ["run", "fill", "fill_type", "triggermode", "hltmode", "trigger_name", "hltid", "ntrig", "nlumi", "lhc"])

def read_cache_file(cache_filename):

    cache = []
    list_of_hlt_wo_castor = []
    
    if not os.path.isfile(cache_filename):
        if not config.READONLY:
            f = None
            try: 
                f = open(cache_filename, "w")
            except:
                print ('could not create cache file. Disk full?')
                sys.exit(1)
            finally:
                if f is not None:
                    f.close()
    
    f = None
    try:
        f = open(cache_filename, "r")
        for line in f.readlines():
            lines = line.rstrip('\n').split(',')

            lhcModeStr = lines[9]
            fmtCache="%Y.%m.%d_%H:%M:%S" # time format
            lhc_modes_list = {}
            if (lhcModeStr!="") :
                for m in lhcModeStr.split('+'):
                    mode_name = m.split('@')[0]
                    mode_time = datetime.datetime.strptime(m.split('@')[1], fmtCache)
                    lhc_modes_list[mode_time] = mode_name

            fill_type = lines[2]
            if "<I>" in fill_type:
                fill_type = fill_type.split('<I>')[1].split('</I>')[0]
            # format of type: runno, fill, fill_type, triggermode, hltmode, castor-trigger, numtrig, nlumi

            cache.append( CacheEntry(run=int(lines[0]), fill=int(lines[1]), fill_type=fill_type, triggermode=lines[3], hltmode=lines[4], trigger_name=lines[5], hltid=int(lines[6]), ntrig=int(lines[7]), nlumi=int(lines[8]), lhc=lhc_modes_list) )

            if lines[5] == "n/a":
                if (config.Debug):
                    print ("Add " + str(lines[4]) + " to list_of_hlt_wo_castor due to " + str(lines[5]) + " in run " + str(lines[0])) 
                list_of_hlt_wo_castor.append(lines[4])

    except:
#    except Exception as exception:
 #       assert type(exception).__name__ == 'NameError'
  #      assert exception.__class__.__name__ == 'NameError'
#        print str(sys.exc_info())
 #       print str(type(exception))
  #      print exception.__class__.__name__
        print ('could not read cache file. FATAL!')
        sys.exit(1)    


    finally:
        if f is not None:
            f.close()

    #print str(cache)
    #print str(list_of_hlt_wo_castor)

    return (cache, list_of_hlt_wo_castor)



# ================================================================
# check the structure and completeness of one a html file on disk
# ================================================================
def check_open_file(file, requires, alsoExit=True) :
    ok = False
    for line in file.readlines():

        #print (line)
        
        if "Cern Authentication" in line:
            print
            print "************************************"
            print ("NO CERN AUTHENTIFICATION. LOGIN AND SAVE cookies.txt with \'Cookie Exporter\' in firefox ")
            print "************************************"
            print
            print ("removing: " + str(file))
            try:
                os.remove(file.name)
            except:
                print("Unexpected error:", sys.exc_info()[0])
            if alsoExit:
                raise SystemExit(1)
            
        if "cmswbm.web.cern.ch/Shibboleth.sso/ADFS" in line:
            print
            print "************************************"
            print ("NO CERN AUTHENTIFICATION/shibboleth. LOGIN AND SAVE cookies.txt with \'Cookie Exporter\' in firefox ")
            print "************************************"
            print
            print ("removing: " + str(file))
            try:
                os.remove(file.name)
            except:
                print("Unexpected error:", sys.exc_info()[0])
            if alsoExit:
                raise SystemExit(1)
            
        if "HTTP Status 500" in line:
            print
            print "************************************"
            print("Page cannot be loaded")
            print "************************************"
            print
            ok = False
            break
            
        for test in requires:
            if test in line:
                requires.remove(test)

        if len(requires) == 0:
            ok = True
            break

    if not ok:
        print
        print ("**************************")
        print ("INFO: check file: " + file.name + " -> not ok. ")
        print ("      did not find: " + str(requires))
        print ("**************************")
        print

    return ok


# ================================================================
# check the structure and completeness of one a html file on disk
# ================================================================
def check_html_file(filename, requires, gz=True, alsoExit=True) :
    ok = False
    if os.path.isfile(filename):
        file = None
        try:
            if gz:
                file = gzip.open(filename, 'rb')
            else:
                file = open(filename, "r")
        
            ok = check_open_file(file, requires, alsoExit)
        
        except SystemExit:
            sys.exit(1) # cern authentification error
            raise

        except:
            print
            print ("**************************")
            print ("ERROR in file " + filename)
            print ("**************************")
            print
            ok = False
        
        finally:
            if file is not None:
                file.close()


        if not ok:
            print
            print ("**************************")
            print ("INFO: check file: " + filename + " -> not ok. ")
            print ("      missing: " + str(requires))
            print ("**************************")
            print

    else:
        print
        print ("**************************")
        print ("INFO: check file: " + filename + " -> does not (yet) exist. ")
        print ("**************************")
        print
    
    return ok


# ==========================================================
# check if cmswbm.web.cern.ch page corresponding to "query" exists on disk, 
# and that it contains the information "requires" and that it does not
# contain a "CERN Authorization" error. Download the page with wget if 
# necessary.
# ==========================================================
def get_WBM_page(query, requires, file_name=None):

    # error status
    isgood = True
    
    # this is the local path of the wget data
    theWbmPath = path.join('cmswbm.web.cern.ch','cmswbm','cmsdb','servlet')

    # some of the very complex queries, that contain slashes, 
    # must be handled with care:
    if file_name == None:
        file_name = query
    
    file_path = path.join(theWbmPath, file_name).strip()
    file_path_gz = file_path + '.gz'
    
    need_wget = False
    need_gz = True

    if config.Debug:
        print ("get_WBM_page: " + query)
        print ("get_WBM_page: " + file_path_gz)
    
    if not check_html_file(file_path_gz, requires, alsoExit=True):
        if not check_html_file(file_path, requires, gz=False, alsoExit=True):
            need_wget = True
    else:
        need_gz = False

    if need_wget:
        if config.READONLY:
            print ("cannot download WBM page in read-only mode")
            sys.exit(1)
        ret = call(["wget", "--no-check-certificate", "--load-cookies", "cookies.txt", "-p", "https://cmswbm.web.cern.ch/cmswbm/cmsdb/servlet/" + query]) 
        if not check_html_file(file_path, requires, gz=False):
            print
            print "***********************************"
            print ("ERRORL: HTML file is still corrupt, also after wget")
            print "***********************************"
            print
            isgood = False
    
    if need_gz:
        with open(file_path, 'rb') as f_in, gzip.open(file_path_gz, 'wb', compresslevel=9) as f_out:
            shutil.copyfileobj(f_in, f_out)
        print ("removing unzipped: " + str(file_path))
        os.remove(file_path)
        
    if not check_html_file(file_path_gz, requires):
        print
        print "***********************************"
        print ("ERRORL:  HTML file is corrupt after gzip")
        print "***********************************"
        print
        isgood = False
        
    if need_gz:
        try:
#            if shFound:
#                git.add(file_path_gz)
#            else:
            call(["git", "add", str(file_path_gz)])            
        except:
            print
            print "***********************************"
            print ("WARNING:  git did not work")
            print "***********************************"
            print            
    
    return gzip.open(file_path_gz, "r"), isgood



# ==========================================================
# analyse the RunSummary page for one run
# ==========================================================
def check_runsummary(run):

    query = "RunSummary?RUN="+str(run)+"&DB=default"

    fill = 0
    l1trigger = long(0)

    if query in BadPagesFound:
        return fill, l1trigger
    
    file = None
    try:
        file, isgood = get_WBM_page(query, ["<A HREF=RunSummary>Run Summary</A>", "<TITLE>CMS RunSummary Run "+str(run)+"</TITLE>"])
        
        if isgood:

            for line in file.readlines():

                if "<TR><TH ALIGN=RIGHT>LHC Fill" in line:
                    snip = ""
                    if "<A HREF=FillReport?FILL=" in line:
                        snip = line.split('>')[5].split('<')[0]
                    else:
                        snip = line.split('>')[4].split('<')[0]
                    if snip =="n/a":
                        fill = 0
                    else:
                        fill = int(snip)

                if "<TR><TH ALIGN=RIGHT>L1 Triggers</TH>" in line:
                    snip = line.split('<TD>')[1].split('</TD>')[0]
                    if (snip=="n/a"):
                        l1trigger = 0
                    else:
                        l1trigger = long(snip)

        else:
            print ("ERROR in downloaded data (runsummary)!! " + query)
            add_bad_query(query)

    except SystemExit:
        sys.exit(1) # pass on fatal errors
    
    except:
        print ("ERROR in downloaded data (runsummary)!! " + query)
        add_bad_query(query)

    finally:
        if file is not None:
            file.close()

    return fill, l1trigger



# ==========================================================
# Analyse the FillReport page for one fill
# ==========================================================
def check_filltype(fill):

    fill_type = "unkown"
    
    if fill == 0:
        return fill_type

    query="FillReport?FILL="+str(fill)

    if query in BadPagesFound:
        return fill_type

    file = None
    try:
        file,isgood = get_WBM_page(query, ["CMS Fill "+str(fill)+" Report</B></FONT></TD>"]) #, "</BODY></HTML>"]) sometimes web pages are broken...
                
        if isgood:

            for line in file.readlines():

                # looks like: "<TH ALIGN=RIGHT>Type</TH><TD><I>Ion</I> &minus; PROTON vs PB82</TD></TR>"
                if "<TH ALIGN=RIGHT>Type</TH>" in line:                
    #                fill_type = line.split('<TD>')[1].split('</TD>')[0]
                    fill_type = line.split('<I>')[1].split('</I>')[0]

        else:

            print ("ERROR in downloaded data!! FindFile " + query)
            add_bad_query(query)
            

    except SystemExit:
        sys.exit(1)  # pass on fatal errors

    except:
        print ("ERROR in downloaded data!! FindFile " + query)
        add_bad_query(query)

    finally:
        if file is not None:
            file.close()

    return (fill_type)


# ==========================================================
# Analyse the LhcEvents page for a time period
# ==========================================================
def check_LHCEvents(startTime, endTime,
                    extraCheckPeriod=0) : # extended search window before time in case no events are found
    
    allModes = {}

    if extraCheckPeriod>75 : # prevent invinite backward loop
        print ("check_LHCEvents: Did NOT find any LHC event before start of run. Check if needed.")
        return allModes
    
    fmt="%Y.%m.%d %H:%M:%S"

    # use an +-1 h extended search window for LHC events
    startTimeQuery = (startTime + datetime.timedelta(hours=-(1+extraCheckPeriod))).strftime(fmt)
    endTimeQuery = (endTime + datetime.timedelta(hours=1)).strftime(fmt)
    
    # need this format:  2015.06.13+03%3A00%3A00
    query="LhcEvents?TIME_BEGIN="+startTimeQuery+"&TIME_END="+endTimeQuery+"&SUBMIT=Submit&SOURCE=0x140A&VERSION=3"

    if query in BadPagesFound:
        return allModes

    prev_count = 0 # also remember the entry just BEFORE the run started

    file = None
    try:
        file,isgood = get_WBM_page(query, ["<TITLE>LhcEvents</TITLE>", "</TBODY></TABLE>"])
                
        if isgood: 

            count_lines = -1

            for line in file.readlines():

                if count_lines>=0:
                    count_lines += 1

                if line.find("<TR>")==0: # next table row
                    count_lines = 0
                    continue

                if count_lines == 1:
                    timeStr = line.split('>')[1].split('&')[0]
                    timeStr = timeStr[0:timeStr.rfind('.')]
                    time = datetime.datetime.strptime(timeStr, fmt)

                if count_lines == 5:
                    if time>endTime:
                        continue
                    
                    if time<startTime:
                        prev_count += 1
                    
                    if prev_count>1:
                        continue
                    
                    what = line.split('>')[1].split('&')[0]
                    allModes[time] = what

        else:
            print ("ERROR in downloaded data!! FindFile " + query)
            add_bad_query(query)
 
    except SystemExit:
        sys.exit(1)  # pass on fatatl errors

    except:
        print ("ERROR in downloaded data!! FindFile " + query)
        add_bad_query(query)

    finally:
        if file is not None:
            file.close()

    if prev_count == 0: # not yet one entry before run start
        if config.Debug:
            print ("check_LHCEvents: did not find LHC event. Extended search period +24h")
        allModes.update(check_LHCEvents(startTime, endTime, extraCheckPeriod+24))
            
    return allModes



# ==========================================================
# Analyse the HLTSummary page for one run
# ==========================================================
def check_hlt_of_run(run, triggermode, hltmode):

    query="HLTSummary?RUN=" + str(run) + "&NAME=" + hltmode
    file_name="HLTSummary?RUN=" + str(run) + "&NAME=" + urllib.quote(hltmode, safe='')

    if query in BadPagesFound:
        return "n/a", 0, 0, 0
        
    trig_name = ""
    hltid = 0
    num_trig = 0
    nlumi = 0

    found_any_trigger = False
    
    # check downloaded data
    file = None
    try:
        file,isgood = get_WBM_page(query, ["HLTSummary Run " + str(run), "<HR><H3>Services</H3>"], file_name=file_name)

        if isgood: # page is good

            # now analyse HTLM page data
            hltOk = False

            for line in file.readlines():

                if ("HLTSummary Run " + str(run) in line):
                    hltOk = True
                    continue

                
                if ("</TD><TD><A HREF=HLTPath?PATHID=" in line and # this is a line that discusses one HLT path
                    "ChartHLTTriggerRates" in line):               # protection against runs not long enough to provide numbers)

                    if not hltOk:
                        print ("HLTSummary page must be corrupt. FIX.")
                        sys.exit(1)

                    ll = line.lower()
                    found_any_trigger = True
                    #                print "line  ", line

                    if ("castor" in ll and "muon" in ll): # now we found a castor halo muon trigger path                    
                        this_trig_name = line.split('>')[5].split(' ')[0]
                        one_hltid = int(line.split('>')[5].split('(')[1].split(')')[0])
                        one_nlumi = int(line.split('>')[8].split('(')[1].split(')')[0])
                        one_num_trig = int(line.split('>')[14].split('<')[0])

                        # only pick most efficient trigger, if multiple are found (sometimes there are inactive versions!)
                        if (one_num_trig >= num_trig) : 
                        
                            num_trig = one_num_trig
                            nlumi = one_nlumi
                            hltid = one_hltid
                        
                            if (this_trig_name not in trig_name):                            
                                if (trig_name != ""):                                
                                    trig_name += "+"
                                trig_name += this_trig_name
                                                    
                            if (config.Debug):
                                print ("HLTSummary, found trigger: " + str(this_trig_name) + " " + str(one_nlumi) + " " + str(one_num_trig))

                        else :
                            if (config.Debug):
                                print ("Skipping " + str(this_trig_name) + " due to nlumi=" + str(one_nlumi) + " num_trig=" + str(one_num_trig))
                            


        else:
            print ("ERROR in downloaded data checkhlt!! " + query)
            add_bad_query(query)
                    
    except SystemExit:
        sys.exit(1)  # pass on fatatl errors

    except:
        print ("ERROR in downloaded data checkhlt!! " + query)
        add_bad_query(query)

    finally:
        if (file is not None):
            file.close()

    if (trig_name == ""):
        if (found_any_trigger):
            trig_name = "n/a"

    return (trig_name, num_trig, nlumi, hltid)
    




# ==========================================================
# Analyse the lumi-section dependence of the trigger path for this run
# ==========================================================
#last_good_LS, num_trig_good, num_trig_bad = check_ChartHLTTriggerRates(run, lhc_modes_list_time)
def check_ChartHLTTriggerRates(run, start_time, end_time, hltid):

    if config.Debug:
        print ("check_ChartHLTTriggerRates: start_time=" + str(start_time) + ", end_time=" + str(end_time))
    
    fmt="%Y.%m.%d %H:%M:%S" # time format

    # query="ChartHLTTriggerRates?RUNID="+run+"&PATHID=2021967&LSLENGTH=23.310409580838325&TRIGGER_PATH=HLT_L1Tech59_CastorMuon_v1"
    query="ChartHLTTriggerRates?RUNID="+str(run)+"&PATHID="+str(hltid)+"&LSLENGTH=23.310409580838325"
    
    first_LS = -1
    last_LS = -1
    num_trig = 0
    
    if query in BadPagesFound:
        return 0, 0, 0
#        return first_LS, last_LS, num_trig
    
    file = None
    try:
        file,isgood = get_WBM_page(query, ["<title>HLT trigger rates, run "+str(run)+", path "+str(hltid)+"</title>", "</html>"])

        if isgood:
            
            list_started = False
            count_lines = 0

            curr_LS = 0

            for line in file.readlines():

                # looks like: "<TH ALIGN=RIGHT>Type</TH><TD><I>Ion</I> &minus; PROTON vs PB82</TD></TR>"
                if "<map id=\"RATES_BY_TIME\" name=\"RATES_BY_TIME\">" in line:
                    list_started = True
                    count_lines = 0

                if list_started and "<td>" in line:
                    count_lines += 1

                    # <td>1</td>
                    # <td>2015.11.24 00:33:05</td>
                    # <td>229.0000</td>
                    # <td>4391</td>
 
                    if count_lines == 1: # LS
                        curr_LS = int(line.split('>')[1].split('<')[0])

                    if count_lines == 2: # time
                        curr_Time = datetime.datetime.strptime(line.split('>')[1].split('<')[0], fmt)

                    if count_lines == 3: # don't need the rate
                        pass

                    if count_lines == 4: # number
                        numTrig = int(line.split('>')[1].split('<')[0])
                        count_lines = 0 # next TR

#                        print str( (curr_LS, curr_Time, numTrig) )

                        if curr_Time>start_time and curr_Time<=end_time:
                            if first_LS < 0:
                                first_LS = curr_LS
                            
                            if curr_Time<=end_time:
                                last_LS = curr_LS
                                num_trig = num_trig + numTrig
                                
        else:

            print ("ERROR in downloaded data!! FindFile " + query)
            add_bad_query(query)
            
    
    except SystemExit:
        sys.exit(1)  # pass on fatal errors
    
    except:
        print ("ERROR in downloaded data!! FindFile " + query)
        add_bad_query(query)
    
    finally:
        if file is not None:
            file.close()

    return first_LS, last_LS, num_trig






# DONT CHANGE THESE TWO PARAMETERS ---------
cache_filename='GrepperCache.txt' # very important output file !!!
minEvents=100 # per run, otherwise it is maybe just noise
# ---------------------------




assert config.MAX_RUN>config.MIN_RUN

# the main output is provided separately for different magnetic field settings:
class MuonOutput:    
    def __init__(self): 
        self.JSONoutput = dict()
        self.all_trig_names = set() 
        self.all_modes = set() 
        self.totalRuns = 0 
        self.totalEvents = 0
        self.totalLumi = 0

AllMuonOutput = {}

# work in blocks of 100 runs. DON'T CHANGE THIS !
minRunInterval = config.MIN_RUN/100
maxRunInterval = config.MAX_RUN/100



# =================================================================
# Start main program 
# =================================================================

if config.READONLY:
    print ("**********************************")
    print ("         read only                ")
    print ("**********************************")


# read previous cache file
read_bad_query_list()
cache, list_of_hlt_wo_castor = read_cache_file(cache_filename)

logfile = "muon_" + str(config.MIN_RUN) + "_" + str(config.MAX_RUN) + "_" + config.specialKeyPostfix + ".log"
logout = open(logfile, "w")

print tableFormat.format(run="Run",date="Date",n="Muons",lumi="LS",r="Mu/LS",beam="Beam",b="B/T",mode="Mode") # ,rate="Rate")
print tableFormat.format(run="",date="",n="",lumi="",r="",beam="",b="", mode="") # ,rate="")

logout.write(tableFormat.format(run="Run",date="Date",n="Muons",lumi="LS",r="Mu/LS",beam="Beam",b="B/T",mode="Mode") + "\n")
logout.write(tableFormat.format(run="",date="",n="",lumi="",r="",beam="",b="", mode="") + "\n")

MagicRun=263280 # this is the first run hin Dec 2015 when the muon HV was set to be the physics HV (pp)
beforeMagicRun=False
afterMagicRun=False

thisFileName = ""
previousFileName = ""

for runInterval in xrange(minRunInterval, maxRunInterval+1):
    
    thisMinRun = max(runInterval*100, config.MIN_RUN)
    thisMaxRun = min((runInterval+1)*100-1, config.MAX_RUN)

    if thisMaxRun == thisMinRun:
        continue

    # ---------------------------------------------------------
    # The first HTTP query is to get a list of runs
    # 
    query="RunSummary?DB=default&SUBMIT_TOP=SubmitQuery&RUN_BEGIN="+str(thisMinRun)+"&RUN_END="+str(thisMaxRun)+"&STATUS_CASTOR=on"
    
    
    print "query " + query

    # unfortunately the order of runs in RunSummary is from late-to-early
    # I want to reverse this for optimal time-ordering
    # need to first read the content of RunSummary and the
    # process it in reverse order
    
    cache_run = []
    cache_triggermode = []
    cache_hltmode = []
    cache_startTime = []
    cache_endTime = []
    cache_bfield = []

    noMoreData = False
    
    file = None
    try:
        file,isgood = get_WBM_page(query, ["<A HREF=RunSummary>Run Summary</A>", "</TBODY></TABLE>"])
        
        previousFileName = thisFileName
        thisFileName = file.name

        if thisMinRun>CHECK_FOR_NEW_DATA_AFTER_RUN and check_open_file(file, ["There is no information in database"]):
            noMoreData = True
            isgood = False

        print ("--> Processing RUNs from: " + str(file.name))
            
        file.seek(0,0) # move back to beginning

        if isgood:

            run = 0
            triggermode = "none"
            hltmode = "none"
            startTime = ""
            endTime = ""
            bfield = -1.0

            count_lines_after_HLTSummary = 0
            badRun = False
            finished = False

            for line in file.readlines():

                count_lines_after_HLTSummary += 1 # because times are one and two lines after the HLTsummary...
                
                if not "<TD" in line:
                    continue

                if "RunSummary" in line: 
                    if run !=0 or triggermode != "none" or hltmode != "none":
                        print ("error while parsing run summary 1")
                        print run, triggermode, hltmode
                        sys.exit(1)
                    run = int(line.split('>')[2].split('<')[0])                
                    if run in BadRuns:
                        print ("Identify bad run: " + str(run))
                        badRun = True

                if "TriggerMode" in line:
                    if run == 0 or triggermode != "none" or hltmode != "none":
                        print ("error while parsing run summary 2")
                        print run, triggermode, hltmode
                        sys.exit(1)
                    triggermode = line.split('>')[2].split('<')[0]

                if hltmode != "none" and count_lines_after_HLTSummary == 1:
                    startTime = line.split('>')[1].split('&')[0]

                if hltmode != "none" and count_lines_after_HLTSummary == 2:
                    endTime = line.split('>')[1].split('&')[0]

                if hltmode != "none" and count_lines_after_HLTSummary == 4:
                    bfield = float(line.split('>')[1].split('&')[0].strip())
                    finished = True

                if "HLTSummary" in line:
                    if run == 0 or triggermode == "none" and hltmode != "none":
                        print ("error while parsing run summary 3")
                        print run, triggermode, hltmode
                        sys.exit(1)

                    count_lines_after_HLTSummary = 0
                    nruns += 1

                    hltmode = line.split('>')[2].split('<')[0]


                if finished:

                    if not badRun:
                        cache_run.append(run)
                        cache_triggermode.append(triggermode)
                        cache_hltmode.append(hltmode)
                        cache_startTime.append(startTime)
                        cache_endTime.append(endTime)
                        cache_bfield.append(bfield)

                    # next run
                    run = 0
                    triggermode = "none"
                    hltmode = "none"
                    startTime = ""
                    endTime = ""
                    bfield=-1.0

                    count_lines_after_HLTSummary = 0
                    badRun = False
                    finished = False

        else:
            if not noMoreData:
                print ("ERROR in downloaded data run-list!! query: " + query)
                add_bad_query(query)

    except SystemExit:
        sys.exit(1)  # pass on fatatl errors

    except Exception as ex:
        print "exception: " + type(ex).__name__
        print ex
        if not noMoreData:
            print ("ERROR in downloaded data run-list (exception)!! query: " + query)
            add_bad_query(query)



    finally:
        if file is not None:
            file.close()

            
    if noMoreData:
        print
        print
        print "               *********  no more data between run="+ str(thisMinRun) + " and run=" + str(thisMaxRun) + " ************ "
        print
        print
        # remove this and previous RunSummary pages, so that they are re-loaded next time to search new data
        if thisFileName != "": 
           print ("removing incomplete: " + thisFileName)
           os.remove(thisFileName)
        if previousFileName != "":
            print ("removing previous: " + previousFileName)
            os.remove(previousFileName)
        break






    # ================================================================ 
    # now go through the list of runs, reverse order
    # ================================================================     

    for iRunRev in xrange(0, len(cache_run)):
        
        iRun = len(cache_run) - iRunRev - 1
        
        run = cache_run[iRun]
        triggermode = cache_triggermode[iRun]
        hltmode = cache_hltmode[iRun]
        startTimeStr = cache_startTime[iRun]
        endTimeStr = cache_endTime[iRun]
        bfield = cache_bfield[iRun]        

        if config.Debug:
            print ("------------")
            print ("run: " + str(run))
            print ("triggermode: " + str(triggermode))
            print ("hltmode: " + str(hltmode))
            print ("startTimeStr: " + str(startTimeStr))
            print ("endTimeStr: " + str(endTimeStr))
            print ("bfield: " + str(bfield))
        
        fmt="%Y.%m.%d %H:%M:%S"
        if startTimeStr == "null" or endTimeStr == "null": # Still ongoing ????
            print ("CHECK THIS RUN: " + str(run))
            sys.exit(1)
            continue

        startTime = datetime.datetime.strptime(startTimeStr, fmt)
        endTime = datetime.datetime.strptime(endTimeStr, fmt)

        
        if run<MagicRun:
            beforeMagicRun = True
            afterMagicRun = False

        # warning: THIS SHOULD BE DONE WITH run-range and without "MagicRun"
        if beforeMagicRun and not afterMagicRun and run>=MagicRun:
            print ("================= switch to physics HV ================")
            afterMagicRun = True
            beforeMagicRun = False
        
        dateStr = startTime.strftime("%Y%b%d")
        yearStr = startTime.strftime("%Y")

        if hltmode in list_of_hlt_wo_castor:
            if (config.Debug):
                print ("Skipping due to hltmode=" + str(hltmode))
         
        else:

            # first check, if we already have the page in cache "GrepperCache.txt"
            in_cache = False
            for row in cache: 
                if row.run == run:
                    # 
                    # ------------------- found in CACHE ---------------------
                    # 
                    ncache += 1
                    beam = row.fill_type
                    triggermode = row.triggermode
                    hltmode = row.hltmode
                    trig_name = row.trigger_name
                    hltid = row.hltid
                    lhc_modes_list = row.lhc
                    num_trig = row.ntrig
                    nlumi = row.nlumi                    
                    
                    num_trig_good = 0        
                    num_trig_bad = 0         
                    
                    if trig_name != 'n/a' and trig_name != '' and num_trig>minEvents:
                    
                        # check LHC modes and LS range
                        lhc_modes = ""
                        good_Lumis = [] # default json, empty
                        sorted_LHCEvents = sorted(lhc_modes_list)
                        for lhc_modes_list_time in sorted_LHCEvents:
                            
                            # range of interval
                            start_interval = lhc_modes_list_time
                            end_interval = endTime
                            if start_interval < startTime:
                                start_interval = startTime
                            if sorted_LHCEvents.index(lhc_modes_list_time) < len(sorted_LHCEvents)-1:
                                end_interval = sorted_LHCEvents[sorted_LHCEvents.index(lhc_modes_list_time)+1] 
                            if end_interval > endTime:
                                end_interval = endTime
                            
                            if end_interval<startTime:
                                print (" THIS IS NOT GOOD! ")
                                sys.exit(1)

                            # now check if this interval is good or bad
                            good_interval=True
                            for m in ExcludeLHC:
                                if m.lower() in lhc_modes_list[lhc_modes_list_time].lower():
                                    good_interval=False
                            
                            # now determine LS range and count events
                            # go to LumiSections?RUN= page and identify LS range
                            first_LS_interval, last_LS_interval, num_trig_interval = check_ChartHLTTriggerRates(run, start_interval, end_interval, hltid)
                            
                            if config.Debug:
                                print ("LHC Events: " + str(lhc_modes_list[lhc_modes_list_time]) + " from " + str(lhc_modes_list_time) + " good: " + str(good_interval) + " LS: [" + str(first_LS_interval) + "," + str(last_LS_interval) + "], ntrig: " + str(num_trig_interval) ) + ", nlumi: " + str(nlumi)

                            if first_LS_interval == last_LS_interval: # found nothing
                                continue
                                
                            if first_LS_interval>nlumi:
                                first_LS_interval = nlumi
                            
                            if last_LS_interval>nlumi:
                                last_LS_interval = nlumi
                            
                            if good_interval:
                                if (lhc_modes != ""):
                                    lhc_modes = lhc_modes + "+"                            
                                lhc_modes = lhc_modes + lhc_modes_list[lhc_modes_list_time]

                                num_trig_good = num_trig_good + num_trig_interval
                                extended_lumi = False
                                for i_good_Lumis in good_Lumis:
                                    if first_LS_interval >= i_good_Lumis[0] and first_LS_interval-1 <= i_good_Lumis[1]:
                                        i_good_Lumis[1] = last_LS_interval
                                        extended_lumi = True
                                        break
                                    if last_LS_interval+1 >= i_good_Lumis[0] and last_LS_interval <= i_good_Lumis[1]:
                                        i_good_Lumis[0] = first_LS_interval
                                        extended_lumi = True
                                        break
                                if not extended_lumi:
                                    good_Lumis.append([first_LS_interval,last_LS_interval])                        
                        
                        if (lhc_modes == ""): # found nothing, e.g. no Fill data
                            num_trig_good = 0
                            num_trig_bad = num_trig
                            
                            
                        num_trig_bad = num_trig - num_trig_good
                    
                        num_good_LS = 0
                        for i_good_Lumis in good_Lumis:
                            num_good_LS = num_good_LS +i_good_Lumis[1]-i_good_Lumis[0]

                        if config.Debug:
                            print ("LHCEvents-summary: lhc_modes=" +str(lhc_modes) + ", num_trig_good=" + str(num_trig_good) + " num_good_LS=" + str(num_good_LS)) + " json=" + str(good_Lumis)
                            
                        if (num_trig_good>1 and num_good_LS>1):

                            print (tableFormat.format(run=str(run),date=dateStr,n=str(num_trig_good), lumi=str(num_good_LS),r=int(num_trig_good/num_good_LS),beam=beam,b=str(round(bfield,1)),mode=lhc_modes))
                            logout.write(tableFormat.format(run=str(run),date=dateStr,n=str(num_trig_good), lumi=str(num_good_LS),r=int(num_trig_good/num_good_LS),beam=beam,b=str(round(bfield,1)),mode=lhc_modes) + "\n")

                            theKey = yearStr + "_" + str(round(bfield,1)) + "T" 
                            if (config.specialKeyPostfix!=""):
                                theKey += str("_" + config.specialKeyPostfix)
                            if (afterMagicRun):
                                theKey += "_ppHV"
                                
                            if not theKey in AllMuonOutput:
                                AllMuonOutput[theKey] = MuonOutput() 
                                
                            if str(run) in AllMuonOutput[theKey].JSONoutput:
                                print ("Trying to add run {run} multiple times to json...".format(run=run))
                                sys.exit(1)

                            AllMuonOutput[theKey].JSONoutput[str(run)] = good_Lumis
                            for tn in trig_name.split('+'):
                                AllMuonOutput[theKey].all_trig_names.add(tn)
                            AllMuonOutput[theKey].all_modes.add(hltmode)

                            AllMuonOutput[theKey].totalEvents = AllMuonOutput[theKey].totalEvents + num_trig_good
                            AllMuonOutput[theKey].totalRuns += 1
                            AllMuonOutput[theKey].totalLumi += num_good_LS

                            totalEvents += num_trig_good
                            totalRuns += 1
                            totalLumi += num_good_LS
                            
                        else:
                            rejectedRuns += 1

                    else:
                        num_trig_bad = num_trig # discard all
                        if config.Debug:
                            print ("Do not consider run: " + str(run) + ", trigger: " + trig_name + ", ntrig: " + str(num_trig))
                            
                    rejectedEvents += num_trig_bad

                    in_cache = True
                    break

            ##########################################################
            # ------------------- not yey in cache -------------------
            ##########################################################
            
            if not in_cache:

                fill, l1trig = check_runsummary(run)

                if run in BadPagesFound:
                    continue

                # -------------------------------
                # * analyse FILL
                # * check trigger menu. Search for CASTOR HLTs!
                # * chekc LHC beam status
                # -------------------------------                
                fill_type = check_filltype(fill)
                trig_name, num_trig, nlumi, hltid = check_hlt_of_run(run, triggermode, hltmode)
                lhc_modes_list = check_LHCEvents(startTime, endTime)

                if (config.Debug):
                    print ("Not in cache: " + str(run) + " " + str(trig_name) + " nTrig=" + str(num_trig) + " nLS=" + str(nlumi) + " hltid=" + str(hltid) + ", LHC=" + str(lhc_modes_list) )
                
                
                # count number of triggers in run
                num_trig_good = 0 
#                num_trig_bad = 0         # default -> none
                
                if trig_name != 'n/a' and num_trig>minEvents:
                    
                    # check LHC modes and LS range
                    lhc_modes = ""
                    good_Lumis = [] # default json, empty
                    sorted_LHCEvents = sorted(lhc_modes_list)
                    for lhc_modes_list_time in sorted_LHCEvents: # sort by time
                        
                        # range of interval
                        start_interval = lhc_modes_list_time
                        end_interval = endTime
                        if start_interval < startTime:
                            start_interval = startTime
                        if sorted_LHCEvents.index(lhc_modes_list_time) < len(sorted_LHCEvents)-1:
                            end_interval = sorted_LHCEvents[sorted_LHCEvents.index(lhc_modes_list_time)+1] 

                        if end_interval<startTime:
                            print (" THIS IS NOT GOOD! ")
                            sys.exit(1)

                        # now check if this interval is good or bad
                        good_interval=True
                        for m in ExcludeLHC:
                            if m.lower() in lhc_modes_list[lhc_modes_list_time].lower() :
                                good_interval=False


                        # now determine LS range and count events
                        # go to LumiSections?RUN= page and identify LS range
                        first_LS_interval, last_LS_interval, num_trig_interval = check_ChartHLTTriggerRates(run, start_interval, end_interval, hltid)

                        if config.Debug:
                            print ("LHC Events: " + str(lhc_modes_list[lhc_modes_list_time]) + " from " + str(lhc_modes_list_time) + " good: " + str(good_interval) + " LS: [" + str(first_LS_interval) + "," + str(last_LS_interval) + "], ntrig: " + str(num_trig_interval) ) + ", nlumi: " + str(nlumi)

                        if first_LS_interval == last_LS_interval: # found nothing
                            continue
                                                    
                        if first_LS_interval>nlumi:
                            first_LS_interval = nlumi
                            
                        if last_LS_interval>nlumi:
                            last_LS_interval = nlumi
                        
                        if good_interval:
                            if (lhc_modes != ""):
                                lhc_modes += "+"                            
                            lhc_modes += lhc_modes_list[lhc_modes_list_time]
                            
                            num_trig_good += num_trig_interval
                            extended_lumi = False
                            for i_good_Lumis in good_Lumis:
                                if first_LS_interval >= i_good_Lumis[0] and first_LS_interval-1 <= i_good_Lumis[1]:
                                    i_good_Lumis[1] = last_LS_interval
                                    extended_lumi = True
                                    break
                                if last_LS_interval+1 >= i_good_Lumis[0] and last_LS_interval <= i_good_Lumis[1]:
                                    i_good_Lumis[0] = first_LS_interval
                                    extended_lumi = True
                                    break
                            if not extended_lumi:
                                good_Lumis.append([first_LS_interval,last_LS_interval])
                        
                                # put a marker in output to indicate truncation of run
#                                if (lhc_modes != ""):
#                                    lhc_modes += " (CUT)"                            



                    if (lhc_modes == ""): # found nothing, e.g. no FILL data 
                        num_trig_good = 0
                        num_trig_bad = num_trig

                    num_trig_bad = num_trig - num_trig_good
                    
                    num_good_LS = 0
                    for i_good_Lumis in good_Lumis:
                        num_good_LS = num_good_LS +i_good_Lumis[1]-i_good_Lumis[0]+1

                    if config.Debug:
                        print ("LHCEvents-summary: lhc_modes=" +str(lhc_modes) + ", num_trig_good=" + str(num_trig_good) + " num_good_LS=" + str(num_good_LS)) + " json=" + str(good_Lumis)

                    if (num_trig_good>1 and num_good_LS>1):
                        
                        print (tableFormat.format(run=str(run),date=dateStr,n=str(num_trig_good), lumi=str(num_good_LS), r=int(num_trig_good/num_good_LS),beam=fill_type,b=str(round(bfield,1)),mode=lhc_modes))
                        logout.write(tableFormat.format(run=str(run),date=dateStr,n=str(num_trig_good), lumi=str(num_good_LS), r=int(num_trig_good/num_good_LS),beam=fill_type,b=str(round(bfield,1)),mode=lhc_modes) + "\n")

                        theKey = yearStr + "_" + str(round(bfield,1)) + "T" 
                        if (config.specialKeyPostfix!=""):
                            theKey += str("_" + config.specialKeyPostfix)
                        if (afterMagicRun):
                            theKey += "_ppHV"

                        if not theKey in AllMuonOutput:                            
                            AllMuonOutput[theKey] = MuonOutput() 

                        if str(run) in AllMuonOutput[theKey].JSONoutput:
                            print ("Trying to add run {run} multiple times to json...".format(run=run))
                            sys.exit(1)

                        AllMuonOutput[theKey].JSONoutput[str(run)] = good_Lumis
                        for tn in trig_name.split('+'):
                            AllMuonOutput[theKey].all_trig_names.add(tn)
                        AllMuonOutput[theKey].all_modes.add(hltmode)

                        AllMuonOutput[theKey].totalEvents += num_trig_good
                        AllMuonOutput[theKey].totalRuns += 1
                        AllMuonOutput[theKey].totalLumi += num_good_LS

                        totalEvents += num_trig_good
                        totalRuns += 1
                        totalLumi += num_good_LS

                    else:
                        rejectedRuns += 1                    

                    rejectedEvents += num_trig_bad


                # add run number to cache file  

                fmtCache="%Y.%m.%d_%H:%M:%S" # time format

                lhc_modes_list_str=""
                for lhc_modes_list_time in sorted(lhc_modes_list):
                    if (lhc_modes_list_str!=""):
                        lhc_modes_list_str += "+"
                    lhc_modes_list_str += lhc_modes_list[lhc_modes_list_time] + "@" + datetime.datetime.strftime(lhc_modes_list_time, fmtCache)
                  
                text = str(run)+','+str(fill)+','+fill_type+','+triggermode+','+hltmode+','+trig_name+','+str(hltid)+','+str(num_trig)+','+str(nlumi)+','+lhc_modes_list_str

                if not config.READONLY:
                    fout = None
                    try:
                        fout = open(cache_filename, "a")
                        fout.write(text+'\n')
                    except:
                        print ('could not write to cache file. FATAL! Disk full?')
                        print (text)
                        sys.exit(1)
                    finally:
                        if fout is not None:
                            fout.close()
                
                # also add to local list in memory
                cache.append( CacheEntry(run=run, fill=fill, fill_type=fill_type, triggermode=triggermode, hltmode=hltmode, trigger_name=trig_name, hltid=hltid, ntrig=num_trig, nlumi=nlumi, lhc=lhc_modes_list) )

                if (trig_name == "n/a"):
                    if hltmode not in list_of_hlt_wo_castor:
                        list_of_hlt_wo_castor.append(hltmode)
                        if (config.Debug):
                            print ("Adding " + str(hltmode) + " to list_of_hlt_wo_castor")


# print "Bad queries found: " + str(BadPagesFound)


print
print "=================================================================="
print
print "Done browsing WBM. Downloaded runs: " + str(ndown) + ",  Cached: " + str(ncache) 
print "Run interval: " + str(config.MIN_RUN) + " - " + str(config.MAX_RUN) + "  ->  " + str(config.MAX_RUN-config.MIN_RUN+1) + " runs"
print "Total number of runs (with CASTOR) analysed: " + str(nruns) 
print "Rejected events: " + str(rejectedEvents) + ', and ' + str(rejectedRuns) + ' entire runs'
print "In total found {n} good runs with {e} events and {lumi} lumisections".format(n=int(totalRuns),e=int(totalEvents),lumi=int(totalLumi))

for key, output in AllMuonOutput.iteritems() :

    print 
    print "--------------------------------------------------------------"
    print "Output for: " + bcolors.OKGREEN + key + bcolors.ENDC
    print 
    print "Found {n} good runs with {e} events and {lumi} lumisections".format(n=int(output.totalRuns),e=int(output.totalEvents),lumi=int(output.totalLumi))
    print 
    print "creating  muon_" + key + ".json  file"
    print 
    print json.dumps(output.JSONoutput, sort_keys=True)
    print 
    print "all trigger names : " + str(output.all_trig_names)
    print "all trigger menues: " + str(output.all_modes)

    logout.write("\n") 
    logout.write( "--------------------------------------------------------------\n") 
    logout.write( "Output for: " + bcolors.OKGREEN + key + bcolors.ENDC + "\n")
    logout.write( "\n")
    logout.write( "Found {n} good runs with {e} events and {lumi} lumisections".format(n=int(output.totalRuns),e=int(output.totalEvents),lumi=int(output.totalLumi)) + "\n")
    logout.write( "\n")
    logout.write( "creating  muon_" + key + ".json  file \n")
    logout.write( "\n")
    logout.write( json.dumps(output.JSONoutput, sort_keys=True) + "\n")
    logout.write( "\n")
    logout.write( "all trigger names : " + str(output.all_trig_names) + "\n")
    logout.write( "all trigger menues: " + str(output.all_modes) + "\n")

    if not config.READONLY:
        jsonout = open('muon_' + key + '.json', 'w')
        #jsonout.write(str(JSONoutput))
        jsonout.write(json.dumps(output.JSONoutput, sort_keys=True))
        jsonout.close()

print
print " NOTE: from this json file also exclude all periods where the detector "
print "       CASTOR is known to have been operating in non-standard conditions. "
print "       (check elog for HV, timing, other problems and settings)" 
print
print " PLEASE always update the HTTP cache directory ./cmswbm.web.cern.ch also in git."
print "        This is very important for other users and the future. Run \"git push origin master\" if needed!"
print

logout.close()

try:   
#    if shFound: 
#        git.commit('-m=\"adding new WBM data by: ' + os.getlogin() + '\"')
#    else:
#    call(["git", "commit", "-m \"adding new WBM data by: " + os.getlogin() + "\""])
    print ("check git status; run git commit/push")

except:
    print
    print "***********************************"
    print "git commit did not work, probably it was not needed."
    print "***********************************"
