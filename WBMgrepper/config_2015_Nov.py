Debug=False
READONLY=True

specialKeyPostfix = "Nov"

# ---------------------
# 2015, 5TeV (pp-reference) and HI (5TeV)
# first run 2015 3.8T: 262157 (261700 is well before TS3)
# last run 2015 3.8T:  262201 (around)
# first run 2015 HI:   
# last run 2015 HI:    
# first run with PHYSICS-HV muons: 263280
# ---------------------
MIN_RUN=260858 # 260858 nothing before this one 
MAX_RUN=263800 # LAST RUN ON:  Mi 2. Dez 16:49:46 CET 2015
# ---------------------

