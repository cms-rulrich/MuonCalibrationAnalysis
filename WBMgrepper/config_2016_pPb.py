Debug=False
READONLY=True

specialKeyPostfix = ""

# ---------------------
# 2016, 5TeV pA run
# ---------------------
MIN_RUN=284000 # there are maybe a few final high-PU pp runs still included 
#MAX_RUN=287000 # ongoing (Mo 5. Dez 08:52:45 CET 2016: 286529)
MAX_RUN=286495 # ongoing (Mo 5. Dez 08:52:45 CET 2016: 286529)

# there are just three more circulating runs afterwards in 2016:
#286509
#286511
#286512
