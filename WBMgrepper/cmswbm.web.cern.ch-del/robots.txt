#
# Robots.txt file for http://webafsNNN.cern.ch
#
# Same file for all servers

User-agent: *
Disallow: /
Allow: /fccr/ 
Allow: /FCCr/
Allow: /geant4/
Allow: /mad/
Allow: /cms-tracker/
Allow: /droussea/
Allow: /CLICr/
Allow: /rtomas/
Allow: /theofil/
Allow: /siodmok/
Allow: /gfasanel/
Allow: /mig/
Allow: /mcfayden/
Allow: /sviel/
Allow: /pmarino/
Allow: /cnellist/
Allow: /swedish/
Allow: /grid-deployment/
Allow: /davidc/
Allow: /glite/
Allow: /club-badminton-bst2016/
Allow: /spectrum/

User-agent: FAST Enterprise Crawler 6 used by CERN (project-search@cern.ch)
Disallow:

User-agent: FAST Enterprise Crawler for Sharepoint used by CERN (project-search@cern.ch)
Disallow:

