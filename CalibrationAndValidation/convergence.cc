#include <TProfile2D.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TH1D.h>
#include <TFile.h>
#include <TTree.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TH2D.h>
#include <TApplication.h>
#include <TDirectory.h>
#include <TKey.h>
#include <TLatex.h>
#include <TList.h>
#include <TGaxis.h>
#include <TMath.h>

#include <TGraph.h>
#include <TGraphErrors.h>

#include <iostream>
#include <string>
#include <sstream>
#include <iostream>

using namespace std;


TH1D*
spread(TGraphErrors* g1, TGraphErrors* g2)
{
  static int iSpread = 0;
  iSpread++;
  ostringstream name;
  name << "spread_" << iSpread;
  TH1D* h = new TH1D(name.str().c_str(), name.str().c_str(), 100, -100, 100);
  for (int i=0; i<224; ++i) {
    if (g2->GetY()[i]!=0) {
      h->Fill(((g1->GetY()[i] / g2->GetY()[i]) - 1) * 100);
    }
  }
  return h;
}



TGraphErrors*
makeCal(TH2D* h)
{
  const int refMod = 4; // absolute numbering
  const int refSec = 9; // absolute numbering
  // const int refChannel = (9-1)*14 + (4-1);

  const double ref = h->GetBinContent(refMod, refSec);
  const double refErr = h->GetBinError(refMod, refSec);

  vector<double> vecx, vecy, vecerr;
  
  for(int iMod=0; iMod<14; ++iMod) {
    for(int iSec=0; iSec<16; ++iSec) {
      
      const int iCh = iSec*14 + iMod;
      
      const double v = h->GetBinContent(iMod+1, iSec+1);
      const double verr = h->GetBinError(iMod+1, iSec+1);
      
      vecx.push_back(iCh+1);
      vecy.push_back(v/ref);
      vecerr.push_back(sqrt(refErr*refErr + verr*verr));  
    }
  }

  return new TGraphErrors(224, &vecx.front(), &vecy.front(), 0, &vecerr.front());
}


TGraphErrors*
Scale(const TGraphErrors* in, const double min, const double max)
{ 
  const double min_in = in->GetMinimum();
  const double max_in = in->GetMaximum();
  
  cout << min << " " << max << " " << min_in << "  " << max_in << endl;
  
  TGraphErrors* out = new TGraphErrors();
  out->SetTitle(in->GetTitle());
  out->SetMarkerStyle(in->GetMarkerStyle());
  out->SetMarkerSize(in->GetMarkerSize());
  out->SetMarkerColor(in->GetMarkerColor());
  out->SetLineColor(in->GetLineColor());
  out->SetLineWidth(in->GetLineWidth());
  for (int i=0; i<in->GetN(); ++i) {    
    out->SetPoint(i, in->GetX()[i], min+(in->GetY()[i]-min_in)/(max_in-min_in)*(max-min));
    //out->SetPointError(i, in->GetErrorX(i), (in->GetErrorY(i))/(max_in-min_in)*(max-min));
  }
  return out;
}


void
convergence(const string& name)
{
  const string trunk = name.substr(0, name.rfind("_"));

  vector<double> vecIter;
  vector<double> vecNBad;
  vector<double> vecNCand;
  vector<double> vecSigma;
  vector<double> vecAvgBaseline;
  vector<double> vecAvgNoiseRms;
  vector<double> vecAvgMuSignal;
  vector<double> vecAvgMuRms;
  vector<double> vecMaxZSdeviation;

  vector<double> vecNBadErr;
  vector<double> vecNCandErr;
  vector<double> vecSigmaErr;
  vector<double> vecAvgBaselineErr;
  vector<double> vecAvgNoiseRmsErr;
  vector<double> vecAvgMuSignalErr;
  vector<double> vecAvgMuRmsErr;
  vector<double> vecMaxZSdeviationErr;

  int iteration = 0;
  while(true) {

    ostringstream fname;
    fname << trunk << "_" << iteration << ".root";
    ++iteration;

    cout << "file: " << fname.str() << "  " << flush;
    
    TFile* file = TFile::Open(fname.str().c_str(), "read");
    if (!file || !file->IsOpen()) {      
      cout << endl;
      break;
    }
    
    string dirname = "none";
    {
      TIter next(file->GetListOfKeys());
      TKey* obj = 0;
      while (obj = (TKey*) next()) {
        if (string(obj->GetClassName()).find("TDirectory") == 0) {
          if (string(obj->GetName()) != "NoiseFits") 
            dirname = obj->GetName();
        }
      }
    }
    if (dirname == "none")
      break;
    cout << "found CFF in dir " << dirname << flush;
    
    // EventCoutner
    TH1D* hCounter = (TH1D*) file->Get((dirname+"/EventCounter").c_str());
    if (!hCounter) {
      cout << "No EventCounter" << endl;
      continue;
    }
    
    // pixel sigma
    TH1D* hSigma = (TH1D*) file->Get((dirname+"/ch_sigma").c_str());
    if (!hSigma) {
      cout << "No ch-sigma" << endl;
      continue;
    }
    
    // get pixel ZS count
    TH2D* hChZS = (TH2D*) file->Get((dirname+"/ch_zs_2d").c_str());
    if (!hChZS) {
      cout << "No ZS counts" << endl;
      continue;
    }

    // get bad channels
    TH2D* hBadCh = (TH2D*) file->Get((dirname+"/ch_bad_2d").c_str());
    if (!hBadCh) {
      cout << "No bad-ch" << endl;
      continue;
    }
    
    // get muon response
    TProfile2D* hNoiseSignals = (TProfile2D*) file->Get((dirname+"/ch_bkg_signals_2d").c_str());
    if (!hNoiseSignals) {
      cout << "No noise signals" << endl;
      continue;
    }

    
    // get background response
    double noise_avg = 0;
    double noise_rms = 0;
    double noise_n = 0;
    for (int iSec=0; iSec<16; ++iSec) {
      for (int iMod=0; iMod<14; ++iMod) {
        if (hBadCh->GetBinContent(iMod+1, iSec+1) == 0) {
          noise_avg += hNoiseSignals->GetBinContent(iMod+1, iSec+1);
          noise_rms += pow(hNoiseSignals->GetBinError(iMod+1, iSec+1), 2);
          noise_n++;
        }
      }
    }
    if (!noise_n) {
      cout << "noise_n is zero" << endl;
      continue;
    }
    noise_avg /= noise_n;
    //noise_rms /= noise_n;
    noise_rms = sqrt(noise_rms) / noise_n;
    
    double noise_avg_err = noise_rms; // / sqrt(noise_n);
    double noise_rms_err = noise_rms / sqrt(noise_n) / 2;

    
    // get muon response
    TProfile2D* hMuonSignals = (TProfile2D*) file->Get((dirname+"/ch_mu_signals_2d").c_str());
    if (!hMuonSignals) {
      cout << "No nuon signals" << endl;
      continue;
    }
    double muon_avg = 0;
    double muon_rms = 0;
    double muon_n = 0;
    for (int iSec=0; iSec<16; ++iSec) {
      for (int iMod=0; iMod<14; ++iMod) {
        if (hBadCh->GetBinContent(iMod+1, iSec+1) == 0) {
          muon_avg += hMuonSignals->GetBinContent(iMod+1, iSec+1);
          muon_rms += pow(hMuonSignals->GetBinError(iMod+1, iSec+1),2); 
          muon_n++;
        }
      }
    }
    if (!muon_n) {
      cout << " muon_n is zero" << endl;
      continue;
    }
    muon_avg /= muon_n;
    // muon_rms /= muon_n;
    muon_rms = sqrt(muon_rms) / muon_n;
    
    double muon_avg_err = muon_rms; // / sqrt(muon_n);
    double muon_rms_err = muon_rms / sqrt(muon_n*2);


    // study ZS counts
    double countZSgood = 0; // total number of ZS channels
    double countGoodCh = 0; // total number of good PMTs
    for (int iSec=0; iSec<16; ++iSec) {
      for (int iMod=0; iMod<14; ++iMod) {
        if (hBadCh->GetBinContent(iMod+1, iSec+1) == 0) {
          countZSgood += hChZS->GetBinContent(iMod+1, iSec+1);
          countGoodCh += 1;
        }
      }
    }
    const double expectedZS = countZSgood/countGoodCh;
    double maxZSdeviation = 0;
    int iSecMax = 0;
    int iModMax = 0;
    for (int iSec=0; iSec<16; ++iSec) {
      for (int iMod=0; iMod<14; ++iMod) {
        if (hBadCh->GetBinContent(iMod+1, iSec+1) == 0) {
          if ((hChZS->GetBinContent(iMod+1, iSec+1)-expectedZS) / sqrt(expectedZS) > maxZSdeviation) {
            maxZSdeviation = (hChZS->GetBinContent(iMod+1, iSec+1)-expectedZS) / sqrt(expectedZS);
            iSecMax = iSec;
            iModMax = iMod;
          }
        }
      }
    }
    cout << " (Maximum ZS deviation in channel: sec=" << iSecMax+1 << " mod=" << iModMax+1 << " with " << maxZSdeviation << ")" <<flush;
      
    
    vecIter.push_back(iteration);
    vecNBad.push_back(hBadCh->GetEntries());
    vecNBadErr.push_back(sqrt(hBadCh->GetEntries()));
    
    vecNCand.push_back(hCounter->GetBinContent(hCounter->GetXaxis()->FindBin("Muon cand")));
    vecNCandErr.push_back(sqrt(hCounter->GetBinContent(hCounter->GetXaxis()->FindBin("Muon cand"))));

    vecSigma.push_back(hSigma->GetRMS());
    vecSigmaErr.push_back(hSigma->GetRMSError());
    
    vecAvgBaseline.push_back(noise_avg);
    vecAvgNoiseRms.push_back(noise_rms);
    vecAvgMuSignal.push_back(muon_avg);
    vecAvgMuRms.push_back(muon_rms);    

    vecAvgBaselineErr.push_back(noise_avg_err);
    vecAvgNoiseRmsErr.push_back(noise_rms_err);
    vecAvgMuSignalErr.push_back(muon_avg_err);
    vecAvgMuRmsErr.push_back(muon_rms_err);    

    vecMaxZSdeviation.push_back(maxZSdeviation);
    vecMaxZSdeviationErr.push_back(sqrt(maxZSdeviation));
        
    cout << " done" << endl;
    file->Close();
  }

  
  TCanvas* c1 = new TCanvas("c1", "n-bad");
  TGraphErrors* gNBad = new TGraphErrors(vecIter.size(), &vecIter.front(), &vecNBad.front(), 0, 0);
  gNBad->SetTitle("Bad channels; Iteration; N_{bad}");
  gNBad->SetMarkerStyle(20);
  gNBad->Draw("alp");

  
  TCanvas* c2 = new TCanvas("c2", "n-cand");
  TGraphErrors* gNCand = new TGraphErrors(vecIter.size(), &vecIter.front(), &vecNCand.front(), 0, &vecNCandErr.front());
  gNCand->SetTitle("Muon candidates; Iteration; N_{mu-cand}");
  gNCand->SetMarkerStyle(20);
  gNCand->Draw("alp");

  
  TCanvas* c3 = new TCanvas("c3", "PMT sigma");
  TGraphErrors* gSigma = new TGraphErrors(vecIter.size(), &vecIter.front(), &vecSigma.front(), 0, &vecSigmaErr.front());
  gSigma->SetTitle("Channel sigma; Iteration; #sigma_{channel}=(S-S_{base})/RMS(noise)");
  gSigma->SetMarkerStyle(20);
  gSigma->Draw("alp");

  
  TCanvas* c4 = new TCanvas("c4", "avg baseline");
  TGraphErrors* gAvgBaseline = new TGraphErrors(vecIter.size(), &vecIter.front(), &vecAvgBaseline.front(), 0, &vecAvgBaselineErr.front());
  gAvgBaseline->SetTitle("Avg baseline; Iteration; <baseline>");
  gAvgBaseline->SetMarkerStyle(20);
  gAvgBaseline->Draw("alp");

  
  TCanvas* c5 = new TCanvas("c5", "avg noise");
  TGraphErrors* gAvgNoiseRms = new TGraphErrors(vecIter.size(), &vecIter.front(), &vecAvgNoiseRms.front(), 0, &vecAvgNoiseRmsErr.front());
  gAvgNoiseRms->SetTitle("Avg noise RMS; Iteration; <RMS_{noise}>");
  gAvgNoiseRms->SetMarkerStyle(20);
  gAvgNoiseRms->Draw("alp");

  
  TCanvas* c6 = new TCanvas("c6", "avg mu");
  TGraphErrors* gAvgMuSignal = new TGraphErrors(vecIter.size(), &vecIter.front(), &vecAvgMuSignal.front(), 0, &vecAvgMuSignalErr.front());
  gAvgMuSignal->SetTitle("Avg muon signal; Iteration; <S_{muon}>");
  gAvgMuSignal->SetMarkerStyle(20);
  gAvgMuSignal->Draw("alp");

  
  TCanvas* c7 = new TCanvas("c7", "avg mu rms");
  TGraphErrors* gAvgMuRms = new TGraphErrors(vecIter.size(), &vecIter.front(), &vecAvgMuRms.front(), 0, &vecAvgMuRmsErr.front());
  gAvgMuRms->SetTitle("Avg muon RMS; Iteration; <RMS_{muon}>");
  gAvgMuRms->SetMarkerStyle(20);
  gAvgMuRms->Draw("alp");

  TCanvas* c8 = new TCanvas("c8", "max ZS deviation");
  TGraphErrors* gMaxZS = new TGraphErrors(vecIter.size(), &vecIter.front(), &vecMaxZSdeviation.front(), 0, &vecMaxZSdeviationErr.front());
  gMaxZS->SetTitle("Maximum ZS deviation; Iteration; #sigma_{max}(N_{ZS})");
  gMaxZS->SetMarkerStyle(20);
  gMaxZS->Draw("alp");


  

  TCanvas* cSummary = new TCanvas("cSummary", "Muon selection convergence", 1500, 700);
  gPad->SetLeftMargin(0.2);
  gPad->SetRightMargin(0.2);

  const int lineWidth = 2;
  
  gAvgMuSignal->SetTitle(";Iteration;Average muon signal / channel [fC];");
  gAvgMuSignal->SetMarkerStyle(20);
  gAvgMuSignal->SetMarkerColor(kBlack);
  gAvgMuSignal->SetMarkerSize(1.5);
  gAvgMuSignal->SetLineColor(kBlack);
  gAvgMuSignal->SetLineWidth(lineWidth);
  gAvgMuSignal->Draw("alp");
  gAvgMuSignal->GetHistogram()->GetYaxis()->SetLabelFont(42);
  gAvgMuSignal->GetHistogram()->GetYaxis()->SetLabelSize(0.035);
  gAvgMuSignal->GetHistogram()->GetYaxis()->SetTitleFont(42);
  gAvgMuSignal->GetHistogram()->GetYaxis()->SetTitleSize(0.045);
  gAvgMuSignal->GetHistogram()->GetYaxis()->SetTitleOffset(0.7);
  gAvgMuSignal->GetHistogram()->GetYaxis()->CenterTitle();
  gAvgMuSignal->GetHistogram()->GetXaxis()->CenterTitle();
  gAvgMuSignal->GetHistogram()->GetXaxis()->SetTitleSize(0.05);
  gAvgMuSignal->GetHistogram()->GetXaxis()->SetTitleOffset(.8);

  
  {
    TPad *overlay = new TPad("overlay","",0,0,1,1);    
    overlay->SetFillStyle(4000);
    overlay->SetFillColor(0);
    overlay->SetFrameFillStyle(4000);
    overlay->Draw();
    overlay->cd();
    gPad->SetLeftMargin(0.2);
    gPad->SetRightMargin(0.2);
    gSigma->SetTitle("");
    gSigma->GetHistogram()->GetYaxis()->SetLabelSize(0);
    gSigma->GetHistogram()->GetYaxis()->SetTitleSize(0);
    gSigma->GetHistogram()->GetYaxis()->SetTickSize(0);
    gSigma->GetHistogram()->GetXaxis()->SetLabelSize(0);
    gSigma->GetHistogram()->GetXaxis()->SetTitleSize(0);
    gSigma->SetMarkerStyle(21);
    gSigma->SetMarkerColor(kCyan+2);
    gSigma->SetMarkerSize(1.5);
    gSigma->SetLineColor(kCyan+2);
    gSigma->SetLineWidth(lineWidth);
    gSigma->Draw("alp");

    gPad->Modified();
    gPad->Update();
    const double pos = gPad->GetUxmin() - (gPad->GetUxmax()-gPad->GetUxmin()) / 0.6 * 0.1;
    TGaxis* ax = new TGaxis(pos, gPad->GetUymin(), pos, gPad->GetUymax(), gPad->GetUymin(), gPad->GetUymax(), 510, "br");
    ax->SetLabelFont(42);
    ax->SetLabelSize(0.035);
    ax->SetTitleFont(42);
    ax->SetTitleSize(0.045);
    ax->SetTitleOffset(0.7);
    ax->CenterTitle();
    ax->SetLineColor(kCyan+3);
    ax->SetTextColor(kCyan+3);
    ax->SetLabelColor(kCyan+3);
    ax->SetTitle("Average noise width / channel [fC]");
    ax->Draw();
  }

  
  {
    TPad *overlay = new TPad("overlay","",0,0,1,1);
    overlay->SetFillStyle(4000);
    overlay->SetFillColor(0);
    overlay->SetFrameFillStyle(4000);
    overlay->Draw();
    overlay->cd();
    gPad->SetLeftMargin(0.2);
    gPad->SetRightMargin(0.2);
    gNCand->SetTitle("");
    gNCand->GetHistogram()->GetYaxis()->SetLabelSize(0);
    gNCand->GetHistogram()->GetYaxis()->SetTitleSize(0);
    gNCand->GetHistogram()->GetYaxis()->SetTickSize(0);
    gNCand->GetHistogram()->GetXaxis()->SetLabelSize(0);
    gNCand->GetHistogram()->GetXaxis()->SetTitleSize(0);
    gNCand->SetMarkerStyle(22);
    gNCand->SetMarkerColor(kBlue+1);
    gNCand->SetMarkerSize(1.5);
    gNCand->SetLineColor(kBlue+1);
    gNCand->SetLineWidth(lineWidth);
    gNCand->Draw("alp");
    
    gPad->Modified();
    gPad->Update();
    const double pos = gPad->GetUxmax() + (gPad->GetUxmax()-gPad->GetUxmin()) / 0.6 * 0.0;
    TGaxis* ax = new TGaxis(pos, gPad->GetUymin(), pos, gPad->GetUymax(), gPad->GetUymin(), gPad->GetUymax(), 510, "+L");
    ax->SetLabelFont(42);
    ax->SetLabelSize(0.035);
    ax->SetTitleFont(42);
    ax->SetTitleSize(0.045);
    ax->SetTitleOffset(0.8);
    ax->CenterTitle();
    ax->SetLineColor(kBlue+2);
    ax->SetTextColor(kBlue+2);
    ax->SetLabelColor(kBlue+2);
    ax->SetTitle("Number of muon candidates");
    ax->Draw();
  }

  
  {
    TPad *overlay = new TPad("overlay","",0,0,1,1);
    overlay->SetFillStyle(4000);
    overlay->SetFillColor(0);
    overlay->SetFrameFillStyle(4000);
    overlay->Draw();
    overlay->cd();
    gPad->SetLeftMargin(0.2);
    gPad->SetRightMargin(0.2);
    gNBad->SetTitle("");
    gNBad->GetHistogram()->GetYaxis()->SetLabelSize(0);
    gNBad->GetHistogram()->GetYaxis()->SetTitleSize(0);
    gNBad->GetHistogram()->GetYaxis()->SetTickSize(0);
    gNBad->GetHistogram()->GetXaxis()->SetLabelSize(0);
    gNBad->GetHistogram()->GetXaxis()->SetTitleSize(0);
    gNBad->SetMarkerStyle(23);
    gNBad->SetMarkerColor(kRed+1);
    gNBad->SetMarkerSize(1.5);
    gNBad->SetLineColor(kRed+1);
    gNBad->SetLineWidth(lineWidth);
    gNBad->Draw("alp");

    gPad->Modified();
    gPad->Update();
    const double pos = gPad->GetUxmax() + (gPad->GetUxmax()-gPad->GetUxmin()) / 0.6 * 0.1;
    TGaxis* ax = new TGaxis(pos, gPad->GetUymin(), pos, gPad->GetUymax(), gPad->GetUymin(), gPad->GetUymax(), 510, "+L");
    ax->SetLabelFont(42);
    ax->SetLabelSize(0.035);
    ax->SetTitleFont(42);
    ax->SetTitleSize(0.045);
    ax->SetTitleOffset(0.5);
    ax->CenterTitle();
    ax->SetLineColor(kRed+1);
    ax->SetTextColor(kRed+1);
    ax->SetLabelColor(kRed+1);
    ax->SetTitle("Number of bad channels");
    ax->Draw();
  }
  
  TLatex* cms = new TLatex(0.2, 0.925, "CMS, interfill data 2015/June");
  cms->SetNDC();
  cms->SetTextFont(62);
  cms->SetTextSize(0.065);
  cms->Draw("same");
  
  cSummary->SaveAs("CastorCal_MuonSelectionSummary.pdf");
  
}




int
main(int argc, char** argv)
{
  if (argc<2) {
    cout << "Please use: " << argv[0] << " input-file " << endl;
    return 1;
  }
  
  TApplication app("runner", 0, 0);    
  convergence(argv[1]);
  app.Run();
  
  return 0;
}
