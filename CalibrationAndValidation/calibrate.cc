#include <TLatex.h>
#include <TProfile2D.h>
#include <TText.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TH1D.h>
#include <TFile.h>
#include <TTree.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TH2D.h>
#include <TApplication.h>
#include <TMath.h>
#include <TDirectory.h>
#include <TKey.h>
#include <TList.h>
#include <TGaxis.h>

#include <fitNoiseAna.h>

#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include <TGraph.h>
#include <TGraphErrors.h>

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <algorithm>

#include "tdr_macro.h"

using namespace std;


const string dirData = "../AnalyseCFF";


// HV correction factors
struct CorrectionArray {
  double v[16][14];
  CorrectionArray() {for (int i=0; i<16; i++) for (int j=0; j<14; j++) v[i][j] = 1.0; }
  void Load(TH2D* h) {for (int i=0; i<16; i++) for (int j=0; j<14; j++) v[i][j] = h->GetBinContent(j+1, i+1);}
  void Load(const string& name) {
    {
      /*vector<vector<double> > factors;
        for (int i=0; i<16; ++i) {
        vector<double> j(14, 0);
        factors.push_back( j );
        }*/
      // https://twiki.cern.ch/twiki/bin/view/CMS/CASTORR2cal
      //  - For the V1 recipe (which was used for the LHCf run and higher) the Transposition factors are in: 
      //       CorrectionFactorsTestData_Stragegy6_RecipeV0.txt
      //  - For the V0 recipe (which was used befor the LHCf run) the Transposition factors are in: 
      //       CorrectionFactorsTestData_Stragegy6.txt
      ifstream data(name.c_str());
      if (!data.good()) {
        cerr << "could not open HV correction factors file: " << name << endl;
        exit(1);
      }
      string line;
      getline(data,line);
      while (getline(data,line)) {
        istringstream ss(line);
        int sec = 0, mod = 0;
        double T = 0, err = 0;
        ss >> sec >> mod >> T >> err;
        // cout << sec << " " << mod << " " << T << endl;
        // T is g_mu/g_phy: muon HV -> physics HV,  S_phy = S_mu * g_phy/g_mu
        if (T>0) {
          v[sec-1][mod-1] = 1./T;
          // cout << " corrections " << sec << " " << mod << " " << 1./T << endl;
        } else {
          v[sec-1][mod-1] = 0;
        }
      }
    }
  }
  TH2D* GetHist(const string& name) {
    TH2D* h = new TH2D(name.c_str(), name.c_str(), 14, 0.5, 14.5, 16, 0.5, 16.5);
    h->Sumw2();
    for (int i=0; i<16; i++) {
      for (int j=0; j<14; j++) {
        h->SetBinContent(j+1, i+1, v[i][j]);
      }
    }
    h->SetMaximum(3);
    h->SetMinimum(0.01);
    return h;
  }
};


struct BadChannelList {
  int v[16][14];
  BadChannelList() {for (int i=0; i<16; i++) for (int j=0; j<14; j++) v[i][j] = 0; }
  void Combine(const BadChannelList& l) {for (int i=0; i<16; i++) for (int j=0; j<14; j++) if (l.Is(i+1,j+1)) v[i][j] = 1; }
  void Set(const int sec, const int mod) { v[sec-1][mod-1] = 1;}
  void Unset(const int sec, const int mod) { v[sec-1][mod-1] = 0;}
  void Remove(TH2* h) {for (int i=0; i<16; i++) for (int j=0; j<14; j++) if (v[i][j]) h->SetBinContent(j+1, i+1, 0); }
  bool Is(const int sec, const int mod) const { return v[sec-1][mod-1]; }
  TH2D* GetHist(const string& name) {
    TH2D* h = new TH2D(name.c_str(), name.c_str(), 14, 0.5, 14.5, 16, 0.5, 16.5);
    h->Sumw2();
    for (int i=0; i<16; i++) {
      for (int j=0; j<14; j++) {
        if (Is(i+1, j+1)) 
          h->SetBinContent(j+1, i+1, 1);
      }
    }
    h->SetMaximum(31);
    h->SetMinimum(0);
    return h;
  }
  void Load(const string& name) {
    ifstream input(name.c_str());
    if (!input.good()) {
      cerr << "could not open bad channel quality file" << endl;
      exit(10);
    }
    int nBad = 0;
    while (input.good()) {
      if (input.peek() == '#') { input.ignore(999, '\n');}
      int auxI = 0;
      int isec =0, imod = 0;
      string name = "";
      int value = 0;
      string ID = "";
      // eta phi depth name value id
      input >> auxI >> isec >> imod >> name >> value >> ID;
      if (!input.good()) break;
      Set(isec, imod);
      nBad++;
      //    cout << isec << " " << imod << " " << ID << endl;      
    }
    input.close();
    cout << "Read " << nBad << " bad channels from " << name << endl;
  }
  void Save(const string& fname) {
    ifstream cal_template("intercalibration_template.txt");  
    ofstream cal(fname);
    // title comment, header
    cal << "# eta phi dep det value DetId" << endl;
    while (cal_template.good()) {
      if (cal_template.peek() == '#') { cal_template.ignore(999, '\n');}
      int auxI = 0;
      int isec =0, imod = 0;
      string name = "";
      double auxD = 0;
      string ID = "";      
      cal_template >> auxI >> isec >> imod >> name >> auxD >> auxD >> auxD >> auxD >> ID;
      if (!cal_template.good()) break;
      
      /*
        #SWAP THE CORRECTIONS for the channels that were found to be swapped
        s5m10 = corr_val[('5','10')]
        s5m12 = corr_val[('5','12')]
        s6m10 = corr_val[('6','10')]
        s6m12 = corr_val[('6','12')] 
        s7m10 = corr_val[('7','10')]
        s7m12 = corr_val[('7','12')]
        corr_val[('5','10')] = s5m12
        corr_val[('5','12')] = s5m10
        corr_val[('6','10')] = s6m12
        corr_val[('6','12')] = s6m10
        corr_val[('7','10')] = s7m12
        corr_val[('7','12')] = s7m10
      */

      if (!Is(isec, imod))
        continue;
      
      cal << setw(5) << -1 << " "
          << setw(4) << isec << " "
          << setw(4) << imod << " "
          << setw(12) << name << " "
          << setw(12) << 1 << " "
          << "  " << ID << endl;
    }
    cal.close();
    cal_template.close();
    cout << "Write: " << fname << endl;
  }
  enum eBadChannelList {kBad2013, kBad2015, kBad2016 };  
  void SetFix(eBadChannelList BadList) {   
    // int badChannelsSecMod[16][2] = {{2,10},{3,11},{12,12},{5,4},{3,8},{7,5},{8,8},{4,11} ,{9,4}, {16,6},{16,4},{15,4}, {15,12},{16,6},{3,11},{3,8}};//2015 // {9,4} is bad channel
    const int badChannelsSecMod_2013[9][2] = {{2,10},{3,11},{12,12},{5,4},{3,8},{7,5},{8,8},{4,11} ,{9,4}} ;//2013
    const int badChannelsSecMod_2015[6][2] = {{2,5},{2,10},{3,8},{4,9},{5,9}}; // 2015 // {9,4} is bad channel
    const int badChannelsSecMod_2016[3][2] = {{7,14},{4,9},{5,9}}; // 2016 
    if(BadList == kBad2016) {
      for(int ipair=0; ipair<3; ipair++) {
        v[badChannelsSecMod_2016[ipair][0]-1][badChannelsSecMod_2016[ipair][1]-1] = 1;
      }
    } else if(BadList == kBad2015) {
      for(int ipair=0; ipair<6; ipair++) {
        v[badChannelsSecMod_2015[ipair][0]-1][badChannelsSecMod_2015[ipair][1]-1] = 1;
      }      
    } else if(BadList == kBad2013) {
      for(int ipair=0; ipair<9; ipair++) {
        v[badChannelsSecMod_2013[ipair][0]-1][badChannelsSecMod_2013[ipair][1]-1] = 1;
      }
    }
  }
};


TH2D*
HRotate(const TProfile2D* h)
{
  TH2D* h2 = new TH2D((string(h->GetName())+"_rot").c_str(), (string(h->GetName())+"_rot").c_str(), 14, 0.5, 14.5, 16, 0.5, 16.5);
  h2->Sumw2();
  h2->SetLineColor(h->GetLineColor());
  h2->SetLineStyle(h->GetLineStyle());
  h2->SetLineWidth(h->GetLineWidth());
  h2->SetMarkerStyle(h->GetMarkerStyle());
  h2->SetMarkerColor(h->GetMarkerColor());
  h2->SetMarkerSize(h->GetMarkerSize());
  for (int isec=0; isec<16; ++isec) {
    for (int imod=0; imod<14; ++imod) {
      h2->SetBinContent(imod+1, isec+1, h->GetBinContent(isec+1, imod+1));
      h2->SetBinError(imod+1, isec+1, h->GetBinError(isec+1, imod+1) );
      //h2->SetBinError(imod+1, isec+1, h->GetBinError(isec+1, imod+1) / sqrt(h->GetEntries()));
      //h2->SetBinEntries(imod+1, isec+1, h->GetBinEntries(isec+1, imod+1));
      //cout << "HRot " << imod+1 << " " << isec+1 << " " << h2->GetBinContent(imod+1, isec+1) << " " << h2->GetBinError(imod+1, isec+1) << " " << h2->GetBinError(imod+1, isec+1)/sqrt(h->GetEntries()) << endl;
    }
  }
  return h2;
}


TGraphErrors*
ForLegend(const int mst, const double msi, const int lwi, const int col)
{
  TGraphErrors* g = new TGraphErrors(0);
  g->SetMarkerStyle(mst);
  g->SetMarkerSize(msi);
  g->SetLineWidth(lwi);
  g->SetMarkerColor(col);
  g->SetLineColor(col);
  g->SetFillColor(col);
  return g;
}



struct MinimumBiasResponse {
private:
  MinimumBiasResponse(const MinimumBiasResponse&);
  MinimumBiasResponse& operator=(const MinimumBiasResponse&);
public:
  TFile* file = 0;
  string dir = "";
  int color = kBlack;
  MinimumBiasResponse(const string& fname, const int c) { file=0; color=c; Load(fname); }
  MinimumBiasResponse(const string& fname, const string& DIR, const int c) { file=0; color=c; Load(fname, DIR); }
  // ~MinimumBiasResponse() { delete file; }
  TProfile2D* GetP2D(const string& hname, const string nname="") {
    TProfile2D* h = (TProfile2D*) file->Get((dir+hname).c_str());
    if (!h) {
      cout << "could not read " << hname << " from " << file->GetName() << endl;
      exit(2);
    }
    if (!nname.empty())
      h->SetName(nname.c_str());
    h->SetLineColor(color);
    h->SetMarkerColor(color);
    return h;
  }
  TH2D* Get2D(const string& hname) {
    TH2D* h = (TH2D*) file->Get((dir+hname).c_str());
    if (!h) {
      cout << "could not read " << hname << " from " << file->GetName() << endl;
      exit(2);
    }
    //h->Scale(1./h->GetEntries());
    h->SetLineColor(color);
    h->SetMarkerColor(color);
    return h;
  }
  TH1D* Get1D(const string& hname) {
    TH1D* h = (TH1D*) file->Get((dir+hname).c_str());
    if (!h) {
      cout << "could not read " << hname << " from " << file->GetName() << endl;
      exit(2);
    }
    //h->Scale(1./h->GetEntries());
    h->SetLineColor(color);
    h->SetMarkerColor(color);
    return h;
  }
  void Load(const string& fname, const string& DIR="") {
    cout << "file: " << fname.c_str() << "  " << flush;    
    file = TFile::Open(fname.c_str(), "read");
    if (!file || !file->IsOpen()) {
      cout << "Could not open file" << endl;
      exit(10);
    }
    dir = "";
    if (!DIR.empty()) {
      dir = DIR + "/";
    }
    if (dir.empty()) {
      TIter next(file->GetListOfKeys());
      TKey* obj = 0;
      while (obj = (TKey*) next()) {
        if (string(obj->GetClassName()).find("TDirectory") == 0) {
          if (string(obj->GetName()) == "NoiseFits")
            continue;
          dir = obj->GetName();
          dir += "/";
          break;
        }
      }
    }
    cout << "found CFF in dir " << dir << endl;
    //MB = (TH2D*) file->Get((dir + "/castor_channel_2d").c_str());
  }
};


double
DrawErrorBand(const TH1* h, const double errin=0.16, const TH1* ref=0, const bool fit=false)
{
  double err = errin; // default
  if (fit) {
    bool dumm = true;
    double steperr = 0.01;
    bool direction = false; // make errors smaller
    const int niter = 1000;
    double chi2 = 0;    
    for (int iiter=0; iiter<niter; ++iiter) {
      chi2 = 0;
      const int ndf = h->GetNbinsX();
      for (int i=0; i<ndf; ++i) {
        // chi2 += pow((h->GetBinContent(i+1)-ref->GetBinContent(i+1))/(err*h->GetBinContent(i+1)), 2);
        chi2 += pow((h->GetBinContent(i+1)-ref->GetBinContent(i+1)) /
                    sqrt( pow(h->GetBinError(i+1),2) + pow(err*h->GetBinContent(i+1),2) ), 2);
        if (dumm) {
          //cout << i << " " << h->GetBinContent(i+1) << " " << ref->GetBinContent(i+1) << " " << h->GetBinError(i+1) << endl;
        }
      }
      dumm = false;
      chi2 /= ndf;
      if (chi2>1) {
        if (direction) {
          direction = false;
          steperr /= 2;
        }
        err += steperr;
      } else {
        if (!direction) {
          direction = true;
          steperr /= 2;
        }
        err -= steperr;
      }
    }
    cout << "Best relative error is : " << err << " chi2=" << chi2 << " " << steperr << endl;
  }
  
  TGraphErrors* band = new TGraphErrors();
  band->SetMarkerStyle(0);
  band->SetLineWidth(3);
  band->SetLineColor(h->GetLineColor());
  band->SetFillStyle(kSolid);
  band->SetFillColor(kYellow+1);
  for (int i=0; i<h->GetNbinsX(); ++i) {
    band->SetPoint(i, h->GetBinCenter(i+1), h->GetBinContent(i+1));
    band->SetPointError(i, 0, h->GetBinContent(i+1)*err); // h->GetBinContent(i+1)*0.016);
  }
  band->Draw("p");
  return err;
}


TH1D*
ProjectPhi(TH2D* h, const bool norm)
{
  TH1D* p = new TH1D((string(h->GetName())+"_phi").c_str(), (string(h->GetName())+"_phi;Phi;").c_str(), 16, 0.5, 16.5);
  p->Sumw2();
  p->SetXTitle("#phi / Tower");
  //p->GetXaxis()->SetTitleSize(0.05);
  //p->GetYaxis()->SetTitleSize(0.05);
  p->SetLineColor(h->GetLineColor());
  p->SetLineStyle(h->GetLineStyle());
  p->SetLineWidth(h->GetLineWidth());
  p->SetMarkerStyle(h->GetMarkerStyle());
  p->SetMarkerColor(h->GetMarkerColor());
  p->SetMarkerSize(h->GetMarkerSize());
  //cout << "ProjectPhi: " << h->GetName() << endl;
  for (int isec=0; isec<16; ++isec) {
    double v = 0;
    double v2 = 0;
    int n = 0;
    for (int imod=0; imod<14; ++imod) {
      //p->Fill(isec+1, h->GetBinContent(imod+1, isec+1));
      if (h->GetBinContent(imod+1, isec+1)) {
        v += h->GetBinContent(imod+1, isec+1);
        v2 += pow(h->GetBinError(imod+1, isec+1), 2);
        //cout << "test: " << imod+1 << " " << isec+1 << " " << h->GetBinContent(imod+1,isec+1) << " " << h->GetBinError(imod+1,isec+1) << endl;
        n++;
      }
    }
    if (n>0) {
      p->SetBinContent(isec+1, v/n);
      //p->SetBinError(isec+1, sqrt(v2/n - pow(v/n,2))/sqrt(n));
      p->SetBinError(isec+1, sqrt(v2)/n);
      //cout << " sec " << isec+1 << " " << v/n << " " << sqrt(v2)/n << " " << n << endl;
    }
  }
  p->SetYTitle("Signal");
  if (norm) {
    p->SetYTitle("Normalized Signal");
    p->Scale(1./p->Integral());
    p->SetMaximum(0.15);
  }
  p->SetMinimum(0);
  return p;
}


TH1D*
ProjectDepth(TH2D* h, const bool norm)
{
  TH1D* p = new TH1D((string(h->GetName())+"_z").c_str(), (string(h->GetName())+"_z;z;").c_str(), 14, 0.5, 14.5);
  p->Sumw2();
  p->SetXTitle("Depth / Module");
  //p->GetXaxis()->SetTitleSize(0.05);
  //p->GetYaxis()->SetTitleSize(0.05);
  p->SetLineColor(h->GetLineColor());
  p->SetLineStyle(h->GetLineStyle());
  p->SetLineWidth(h->GetLineWidth());
  p->SetMarkerStyle(h->GetMarkerStyle());
  p->SetMarkerColor(h->GetMarkerColor());
  p->SetMarkerSize(h->GetMarkerSize());
  double etot = 0;
  double etotFront = 0;
  for (int imod=0; imod<14; ++imod) {
    double v = 0;
    double v2 = 0;
    int n = 0;
    for (int isec=0; isec<16; ++isec) {
      //p->Fill(imod+1, h->GetBinContent(imod+1, isec+1));
      if (h->GetBinContent(imod+1, isec+1)>0) {
        v += h->GetBinContent(imod+1, isec+1);
        //v2 += pow(h->GetBinContent(imod+1, isec+1),2);
        v2 += pow(h->GetBinError(imod+1, isec+1),2);
        n++;
      }
      etot += h->GetBinContent(imod+1, isec+1);
      if (imod<6) {
        etotFront += h->GetBinContent(imod+1, isec+1);
      }
    }
    if (n>0) {
      p->SetBinContent(imod+1, v/n);
      //p->SetBinError(imod+1, sqrt(v2/n - pow(v/n,2))/sqrt(n));
      p->SetBinError(imod+1, sqrt(v2)/n);
    }
  }
  p->SetYTitle("Signal");
  if (norm) {
    p->SetYTitle("Normalized Signal");
    p->Scale(1./p->Integral());
    p->SetMaximum(0.95);
  }
  cout << " Total CASTOR energy in: " << h->GetName() << " is " << etot << " and in front " << etotFront << endl;
  p->SetMinimum(0.005);
  return p;
}



const double gain_HIHV_2015[16][14] =
  { {1386.0, 551.902, 365.181, 0.0, 0.0, 0.0, 0.0, 0.0, 42.364, 37.9622, 38.5458, 43.0751, 42.5531, 53.2861}, 
    {321.666, 533.041, 309.6, 484.069, 0.0, 0.0, 0.0, 0.0, 41.6494, 0.0, 37.0036, 41.5091, 44.851, 56.9927}, 
    {826.955, 2885.54, 323.043, 401.205, 83.0216, 0.0, 0.0, 0.0, 0.0, 45.9057, 7.65391, 42.1961, 42.0449, 63.0794}, 
    {1167.74, 839.416, 304.742, 469.524, 61.8803, 0.0, 0.0, 0.0, 0.0, 40.8239, 53.9855, 45.3609, 64.3617, 56.4184}, 
    {1037.35, 0.0, 327.339, 393.639, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 47.4305, 41.8222, 44.9322, 74.2855}, 
    {323.949, 496.119, 294.906, 371.519, 66.4596, 0.0, 0.0, 0.0, 62.069, 45.3596, 49.1401, 44.4375, 49.638, 87.3435}, 
    {572.743, 486.363, 333.928, 391.927, 47.9869, 0.0, 0.0, 0.0, 41.3919, 53.1017, 49.8, 50.7194, 180.078, 45.1724}, 
    {607.352, 453.404, 304.14, 404.97, 130.822, 0.0, 0.0, 27.3106, 42.7855, 45.2685, 45.8772, 73.0081, 47.0732, 47.9624}, 
    {405.338, 379.321, 242.022, 309.805, 39.094, 0.0, 0.0, 21.7232, 45.539, 43.6106, 43.5862, 46.6239, 19.2105, 54.6698}, 
    {327.954, 352.009, 279.514, 297.242, 50.9709, 0.0, 0.0, 62.3288, 46.508, 40.2053, 43.4375, 45.636, 52.792, 50.8847}, 
    {281.161, 301.095, 284.94, 324.326, 58.546, 0.0, 0.0, 0.0, 49.4448, 56.813, 45.1069, 44.8187, 40.44, 19.2216}, 
    {200.912, 297.141, 257.308, 337.746, 73.5728, 0.0, 0.0, 0.0, 60.7857, 42.8023, 49.7674, 50.2173, 49.9846, 56.4612}, 
    {251.493, 271.795, 262.638, 346.243, 82.0188, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 44.7661, 0.0, 64.734}, 
    {275.235, 270.651, 246.194, 322.654, 0.0, 0.0, 0.0, 0.0, 43.0853, 43.8883, 0.0, 51.5477, 43.2069, 0.0}, 
    {348.007, 327.59, 276.926, 360.177, 63.9612, 0.0, 0.0, 0.0, 38.9483, 42.1936, 41.6949, 51.9858, 46.6616, 55.1977}, 
    {404.932, 306.405, 292.38, 309.399, 63.6149, 0.648515, 0.0, 24.8322, 41.0296, 40.933, 43.0098, 45.6658, 46.3224, 55.1108}};


const double gain_ppHV_2015[16][14] = 
  {  {34.3701, 18.6152, 15.3745, 0.0, 0.0, 0.0, 0.0, 0.114212, 2.11848, 2.07242, 2.07876, 2.12556, 2.12037, 2.21812}, 
     {14.4971, 18.3178, 14.2427, 17.5182, 0.0, 0.0, 0.0, 0.0, 2.11127, 0.0, 2.06182, 2.10984, 2.14283, 2.24822}, 
     {26.7003, 26.3912, 14.5258, 16.0592, 2.42424, 4.2115, 0.460481, 0.0, 0.0, 2.15284, 1.88525, 2.11679, 2.11527, 2.2944}, 
     {37.6205, 20.0698, 14.1387, 17.2724, 2.58571, 4.77444, 0.0, 0.0, 0.0, 2.10282, 2.23724, 2.14769, 5.28384, 2.24366}, 
     {35.4008, 0.0, 14.615, 15.9183, 0.0, 3.3105, 1.36778, 0.0, 0.0, 0.0, 2.16698, 1.85968, 2.14361, 2.37082}, 
     {14.5447, 17.7188, 13.9255, 15.4975, 6.54835, 4.94565, 0.7075, 0.0, 6.36364, 2.31361, 2.18241, 1.82776, 2.18683, 2.44902}, 
     {18.9375, 17.5566, 14.7505, 15.8861, 2.53448, 3.45349, 0.0, 0.0, 2.10865, 2.64198, 2.63492, 2.28155, 2.99351, 2.19247}, 
     {19.4592, 16.9952, 14.1258, 16.1289, 5.22572, 0.817647, 0.0, 1.05564, 2.12269, 2.64179, 5.72837, 3.32593, 2.25731, 2.54576}, 
     {16.1356, 15.6474, 12.7075, 14.247, 2.21905, 1.03268, 0.0, 1.06657, 2.14938, 2.13083, 2.13059, 2.15955, 3.17391, 2.22955}, 
     {14.6277, 15.1151, 13.584, 13.9765, 0.0, 1.024, 0.0, 2.86464, 2.15847, 2.09639, 2.12913, 2.1503, 2.21399, 2.19772}, 
     {13.621, 14.0601, 13.7055, 14.5525, 2.26036, 2.09868, 0.0, 1.7451, 2.18512, 2.24679, 2.05641, 2.14253, 2.09884, 0.8025}, 
     {11.6578, 13.9743, 13.0731, 14.8284, 2.36625, 1.83051, 0.0, 0.0, 2.27743, 2.12285, 2.568, 2.19192, 2.18988, 2.244}, 
     {12.9354, 13.409, 13.1978, 15.0, 2.41834, 0.0, 0.0, 0.0, 0.0, 5.32951, 6.94352, 2.14202, 2.65789, 2.30633}, 
     {13.4873, 13.3828, 12.8085, 14.5177, 3.52349, 0.764706, 0.0, 0.0, 2.12566, 2.13354, 3.52071, 2.20343, 2.12686, 0.0}, 
     {15.0353, 14.6201, 13.5256, 25.5975, 2.30079, 0.780401, 0.0, 0.353795, 2.08309, 4.27451, 2.11173, 2.41118, 2.1599, 2.23385}, 
     {16.1281, 14.1744, 13.8701, 14.2384, 2.29829, 1.56138, 0.389796, 2.27926, 2.10494, 2.10394, 2.12491, 2.15058, 2.15674, 2.23314}};



void
GetCorrectionsPP(CorrectionArray& factors)
{
  for(int isec=0; isec<16; isec++) {
    for(int imod=0; imod<14; imod++) {
      const double T = gain_ppHV_2015[isec][imod];
      if (T>0)
        factors.v[isec][imod] = 1./T;
      else
        factors.v[isec][imod] = 0;
    }
  }
}



TH1D*
Relative(const TH2* h, const TH2* ref, const BadChannelList& bad)
{
  TH1D* hRel = new TH1D((string(h->GetName())+"_relative").c_str(),
                        (string(h->GetName())+"_relative").c_str(),
                        30, -1, 3);
  hRel->Sumw2();
  hRel->SetXTitle("h/href");
  hRel->SetLineWidth(h->GetLineWidth());
  hRel->SetLineStyle(h->GetLineStyle());
  hRel->SetLineColor(h->GetLineColor());
  double m = 0;
  double m2 = 0;
  int n = 0;
  for (int isec=0; isec<16; isec++) {
    for (int imod=0; imod<14; imod++) {
      //for (int imod=10; imod<14; imod++) {
      //for (int imod=0; imod<5; imod++) {
      
      // check bad channels !
      if (bad.Is(isec+1, imod+1))
        continue;
      
      if (ref->GetBinContent(imod+1, isec+1)<=0 ||
          h  ->GetBinContent(imod+1, isec+1)<=0)
        continue;
      
      const double r = h->GetBinContent(imod+1, isec+1) / ref->GetBinContent(imod+1, isec+1);
      hRel->Fill(r);
      m += r;
      m2 += r*r;
      n++;
    }
  }
  m /= n;
  m2 /= n;
  cout << setw(50) << hRel->GetName() << " avg=" << setw(15) << m << " rms=" << sqrt(m2-m*m)
       << " for n=" << n
       << endl;
  return hRel;
}




double
GainCorrectionFactors_HI(int isec, int imod)
{
  return gain_HIHV_2015[isec][imod];
}




double
GainCorrectionFactors_pp(int isec, int imod)
{
  return gain_ppHV_2015[isec][imod];
}




double
GetKaterina2013(int isec, int imod) 
{
  const double
    gain[16][14] = {{0.7510, 0.8700, 2.7370, 0.0000, 0.3630, 0.6430, 0.0000, 0.3100, 0.2120, 0.2740, 0.3030, 0.1690, 0.2650, 0.1550}, 
                    {0.6190, 0.6160, 1.8130, 0.8690, 0.1820, 0.6280, 0.0000, 0.5070, 0.1680, 0.2910, 0.3380, 0.1460, 0.2490, 0.1250}, 
                    {1.0700, 0.6510, 1.4250, 0.7660, 0.3040, 0.1930, 8.2170, 13.2900, 0.4650, 0.2350, 0.0000, 0.2950, 0.3430, 0.3510}, 
                    {0.5310, 0.3300, 0.8910, 0.8260, 0.1170, 0.3300, 0.0000, 0.0000, 0.0000, 0.6390, 0.0000, 0.3170, 0.0000, 0.4580}, 
                    {0.6120, 0.0000, 1.3410, 0.7020, 0.1560, 0.5690, 0.8360, 0.0000, 0.0000, 0.5230, 0.2360, 0.3290, 0.3990, 0.3420}, 
                    {1.3130, 0.4870, 1.4000, 0.6320, 0.1990, 0.7950, 1.2090, 0.0000, 0.5100, 0.7060, 0.2330, 0.2800, 0.4830, 0.4410}, 
                    {0.4160, 0.2820, 1.0430, 0.3130, 0.1140, 0.0860, 250.6690, 0.1950, 0.4200, 6.9160, 3.4790, 1.5110, 4.8590, 3.5340}, 
                    {0.3420, 0.2950, 1.1980, 1.4030, 0.2130, 1.0730, 0.0000, 0.2060, 0.6350, 27.2690, 9.4210, 3.3400, 3.4880, 1.0100}, 
                    {0.3030, 0.8460, 1.4120, 1.0000, 0.2180, 0.8830, 0.0000, 0.1320, 0.1950, 0.2490, 0.2250, 0.2270, 0.2990, 0.2780}, 
                    {0.9040, 1.4030, 2.6580, 1.1900, 0.2350, 1.5570, 0.0000, 0.3160, 0.1990, 0.3100, 0.1790, 0.2510, 0.2510, 0.2520}, 
                    {1.0160, 0.9930, 1.6950, 0.8870, 0.2850, 0.6230, 0.0000, 10.0790, 0.3730, 0.2440, 9.6350, 0.5240, 0.6990, 0.3790}, 
                    {1.1690, 1.1300, 2.1400, 1.3920, 0.2630, 1.2470, 0.0000, 0.0000, 0.5670, 0.3030, 99.3510, 0.3510, 0.1980, 0.3560}, 
                    {0.9160, 1.2700, 1.6430, 0.8070, 0.2310, 2.3020, 0.0000, 0.0000, 0.3230, 0.2910, 0.0000, 0.3430, 0.1280, 0.3080}, 
                    {0.6010, 0.9840, 2.1400, 0.8210, 0.1770, 1.0970, 0.0000, 0.0000, 0.2030, 0.2920, 16.6350, 0.3020, 0.3510, 0.3680}, 
                    {0.7590, 1.3650, 2.9620, 1.1740, 0.3800, 2.3370, 0.0000, 517.2540, 0.2690, 0.0000, 0.1940, 0.2740, 0.2800, 0.4100}, 
                    {0.7420, 0.9720, 2.4600, 0.9240, 0.2200, 0.1630, 3.9070, 0.1970, 0.2700, 0.2580, 0.1510, 0.1340, 0.2790, 0.2620}}; 
  return gain[isec][imod];
}



// magnetic field correction 2013 katerinas values//LED data, r238434(B=3.8T) to r238342(B=0T), HV ~ 1800V, average amplitude w/o ZS)
double
Getcorr4Tto0TKaterina2013(int isec, int imod) 
{
  const double gain[16][14] =  
  //mod 1          2              3          4           5               6          7               8          9          10              11          12        13           14
    {{     1.48512,     1.39079,     1.23936,     1.20485,     1.53704,     0.53953,     0.00404,     0.73703,     0.99343,     0.97474,     1.07208,     1.00473,     1.21792,     1.38684}, // s 1
     {     1.34932,     1.31645,     1.16597,     1.07258,     1.10938,     0.91177,     0.00558,     0.06076,     0.94764,     0.92360,     0.96920,     1.16183,     1.18510,     1.23530}, // s 2
     {     1.19602,     1.46404,     1.26956,     1.27067,     1.55640,     0.28252,     0.00494,    -0.00000,     0.04254,     0.97351,     1.00799,     1.01488,     1.17762,     1.33190}, // s 3
     {     1.28421,     1.54017,     1.14003,     1.21478,     1.62988,     0.52778,     0.00217,    -0.00000,    -0.00000,     0.85758,     1.01701,     1.11001,     1.15505,     1.45989}, // s 4
     {     1.08401,     1.27912,     1.15338,     1.02110,     1.48480,     0.18463,     0.04877,    -0.00000,     0.00035,     0.93851,     0.91677,     1.22820,     1.04381,     1.25080}, // s 5
     {     1.09123,     1.19290,     1.02762,     1.15109,     1.01973,     0.27162,     0.01023,     0.00001,     0.22811,     0.95087,     1.02469,     0.94455,     1.14685,     1.19158}, // s 6
     {     1.35555,     1.28333,     0.80874,     1.08100,     0.98072,     0.39716,     0.00095,     0.29955,     1.01768,     0.94803,     0.88673,     1.02905,     1.02306,     1.40891}, // s 7
     {     1.40568,     1.28872,     1.01763,     1.12056,     1.12694,     0.05847,     0.00022,     0.90227,     1.01943,     1.14345,     1.04596,     0.96388,     1.15198,     1.29044}, // s 8
     {     1.06251,     1.14400,     1.01962,     1.03241,     1.02967,     0.18843,     0.00002,     0.80946,     0.87329,     0.88913,     0.86814,     1.13983,     1.05866,     1.04589}, // s 9
     {     1.01657,     1.16994,     1.03485,     1.06841,     1.12475,     0.21338,     0.00008,     0.37391,     0.92616,     0.95543,     0.95621,     1.01440,     0.86022,     1.37397}, // s10
     {     0.98176,     0.98911,     0.99062,     1.03866,     1.21427,     0.10842,     0.00007,     0.05552,     0.71987,     0.87018,     0.89760,     0.95953,     1.19473,     1.22346}, // s11
     {     0.98305,     0.99199,     0.99141,     1.08809,     1.13973,     0.14923,     0.00018,     0.00005,     0.36019,     0.86800,     0.93984,     1.06136,     1.08128,     1.07615}, // s12
     {     0.99641,     0.99703,     0.71680,     1.00467,     1.24617,     0.03526,     0.00227,    -0.00000,     0.36686,     0.68199,     0.98805,     1.09305,     1.19635,     1.33507}, // s13
     {     1.00974,     1.00226,     0.98903,     1.03647,     1.27846,     0.39654,     0.00116,     0.12479,     0.66424,     1.14252,     1.01872,     1.12014,     1.26828,     0.77948}, // s14
     {     1.17599,     1.08516,     1.01189,    -0.00000,     1.36638,     0.07736,    -0.00000,     0.00148,     0.95000,     0.86826,     0.97357,     1.04934,     1.09820,     1.07579}, // s15
     {     1.05973,     1.13987,     1.05313,     0.83706,     1.31051,     0.33701,     0.00358,     0.74754,     0.97755,     0.87077,     1.01832,     1.10647,     1.15042,     1.08966}};// s16
  return gain[isec][imod];
}



void
WriteIntercal(TH2* h, const char* fname)
{
  ifstream cal_template("intercalibration_template.txt");
  if (!cal_template.is_open() || !cal_template.good()) {
    cout << "\nERROR: intercalibration_template.txt is missing -> cannot proceed\n" << endl;
    exit(1);
  }
  ofstream cal(fname);
  // title comment, header
  cal << "#             eta             phi             dep             det     cap0     cap1     cap2     cap3      DetId" << endl;
  while (cal_template.good()) {
    if (cal_template.peek() == '#') { cal_template.ignore(999, '\n');}
    int auxI = 0;
    int isec =0, imod = 0;
    string name = "";
    double auxD = 0;
    string ID = "";

    cal_template >> auxI >> isec >> imod >> name >> auxD >> auxD >> auxD >> auxD >> ID;
    if (!cal_template.good()) break;
    
    //    cout << isec << " " << imod << " " << ID << endl;

    /*
      #SWAP THE CORRECTIONS for the channels that were found to be swapped
      s5m10 = corr_val[('5','10')]
      s5m12 = corr_val[('5','12')]
      s6m10 = corr_val[('6','10')]
      s6m12 = corr_val[('6','12')] 
      s7m10 = corr_val[('7','10')]
      s7m12 = corr_val[('7','12')]
      corr_val[('5','10')] = s5m12
      corr_val[('5','12')] = s5m10
      corr_val[('6','10')] = s6m12
      corr_val[('6','12')] = s6m10
      corr_val[('7','10')] = s7m12
      corr_val[('7','12')] = s7m10
    */
    
    const double val = h->GetBinContent(imod, isec);
    cal << setw(5) << -1 << " "
        << setw(4) << isec << " "
        << setw(4) << imod << " "
        << setw(12) << name << " "
        << setw(12) << val << " "
        << setw(12) << val << " "
        << setw(12) << val << " "
        << setw(12) << val << " "
        << "  " << ID << endl;
  }
  cal.close();
  cal_template.close();
  cout << "Write: " << fname << endl;
}


void
ReadIntercal(TH2* h, const char* fname)
{
  ifstream cal(fname);
  int n = 0;
  while (cal.good()) {
    if (cal.peek() == '#') { cal.ignore(999, '\n');}
    int auxI = 0;
    int isec =0, imod = 0;
    string name = "";
    double auxD = 0;
    string ID = "";
    cal >> auxI >> isec >> imod >> name >> auxD >> auxD >> auxD >> auxD >> ID;
    if (!cal.good()) break;
    h->SetBinContent(imod, isec, auxD);
    h->SetBinError(imod, isec, 0);
    n++;
  }
  cal.close();
  cout << "Read " << n << " entries from " << fname << endl;
}


enum MResponseMode {
  eAverage,
  eRMS,
  eRMSmNoise,
  eRMStruncated,
  eNpe
};


const TH2D *
ReadMuonResponse(const string& name, const string& hname,
                 const int color, const int style, const int width)
{
  const MResponseMode mode = eRMStruncated;
  //const MResponseMode mode = eAverage;
  //const MResponseMode mode = eRMSmNoise;
  //const MResponseMode mode = eNpe;
  //const MResponseMode mode = eRMS;

  TH2D* h = new TH2D(hname.c_str(), hname.c_str(), 14,0.5,14.5, 16,0.5,16.5);
  h->Sumw2();
  h->SetDirectory(0);
  
  TFile* file = TFile::Open(name.c_str(), "read");
  if (!file || !file->IsOpen()) {
    cout << "Could not open file" << endl;
    exit(10);
  }
  string dir = "";
  {
    TIter next(file->GetListOfKeys());
    TKey* obj = 0;
    while (obj = (TKey*) next()) {
      if (string(obj->GetClassName()).find("TDirectory") == 0) {
        if (string(obj->GetName()) == "NoiseFits")
          continue;
        dir = obj->GetName();
      }      
    }
  }
  cout << "found CFF in dir " << dir << endl;
  bool readHists = false;

  if (mode != eNpe) {
  
    for (int isec=1; !readHists&&(isec<=16); ++isec) {
      for (int imod=1; !readHists&&(imod<=14); ++imod) {
        TH2D* noiseRMS = (TH2D*) file->Get((dir + "/ch_input_rms_2d").c_str());
        ostringstream ssTreeName;
        ssTreeName << dir << "/ChannelTree_sec_" << isec << "_mod_" << imod;
        TTree* data = (TTree*) file->Get(ssTreeName.str().c_str());
        if (!data) {
          cerr << " ****************************** " << endl;
          cerr << " could not read muon data ouput: " << ssTreeName.str() << " from " << name << endl;
          readHists = true;
          continue;
        }
        double chE = 0;
        data->SetBranchAddress("energy", &chE);
        vector<double> vecE;
        for (int iM=0; iM<data->GetEntries(); ++iM) {
          data->GetEntry(iM);
          vecE.push_back(chE);
        }
        delete data;
        std::sort(vecE.begin(), vecE.end());
        double avgE = 0, avgE2 = 0;
        int nE = 0;
        const int nV = (mode==eRMStruncated)||(mode==eRMSmNoise) ? vecE.size()*0.97 : vecE.size();
        for (int iV=0; iV<nV; ++iV) {
          // for (int iV=vecE.size()*0.01; iV<vecE.size()*0.99; ++iV) {
          avgE += vecE[iV];
          avgE2 += pow(vecE[iV], 2);
          nE ++;
        }
        double value = 0;
        double error = 0;
        if (nE>0) {
          if (mode==eAverage) {
            value = avgE/nE;
            const double rms = sqrt(avgE2/nE - value*value);
            error = rms / sqrt(nE);
          } else if (mode==eRMS || mode==eRMStruncated || mode==eRMSmNoise) {
            value = sqrt(max(0.0, avgE2/nE - pow(avgE/nE,2)));
            error = value / sqrt(2*nE);
          }
          if (mode == eRMSmNoise) {
            value = sqrt(value*value - pow(noiseRMS->GetBinContent(imod,isec),2));
            error = sqrt(error*error + pow(noiseRMS->GetBinError(imod,isec),2));
          }
        }
        if (imod <= 2)
          value *= 2; // EM channels are only halve signal
        if (TMath::IsNaN(value)) {
          cerr << "..................................................... nan" << endl;
        }
        h->SetBinContent(imod, isec, value);
        h->SetBinError(imod, isec, error);
      }
    }
  }
  if (readHists) {
    if (mode == eNpe) {
      NoiseFitHelper nfh(file);
      for (int isec=1; isec<=16; ++isec) {
        for (int imod=1; imod<=14; ++imod) {
          double value = nfh.h2Zprob->GetBinContent(imod, isec);
          //double value = p->GetBinContent(imod, isec);
          //double value = max(0.0, pow(p->GetBinContent(imod,isec),2) - pow(b->GetBinContent(imod,isec),2));
          if (imod <= 2)
            value *= 2; // EM channels are only halve signal
          h->SetBinContent(imod, isec, value);
        }
      }
    } else {
      TH2* p = 0;
      if (mode==eAverage) {
        //p = (TProfile2D*) file->Get((dir + "/ch_mu_signals_2d").c_str());
        p = (TH2D*) file->Get((dir + "/ch_mu_mean_2d").c_str());
      } else {
        p = (TH2D*) file->Get((dir + "/ch_mu_rms_2d").c_str());
      }
      //TH2D* noiseRMS = (TH2D*) file->Get((dir + "/ch_bkg_rms_2d").c_str());
      TH2D* noiseRMS = (TH2D*) file->Get((dir + "/ch_input_rms_2d").c_str());
      for (int isec=1; isec<=16; ++isec) {
        for (int imod=1; imod<=14; ++imod) {
          //double value = nfh.h2Zprob->GetBinContent(imod, isec);
          double value = p->GetBinContent(imod, isec);
          //double value = max(0.0, pow(p->GetBinContent(imod,isec),2) - pow(b->GetBinContent(imod,isec),2));
          if (imod <= 2)
            value *= 2; // EM channels are only halve signal
          h->SetBinContent(imod, isec, value);
        }
      }
    }
  }
  file->Close();
  h->SetLineWidth(width);
  h->SetLineColor(color);
  h->SetLineStyle(style);
  h->SetMaximum(1000);
  h->SetMinimum(0.01);
  return h;
}



TH2D*
CorrectionHV(const TH2D* h, const CorrectionArray& factor)
{
  TH2D* output = new TH2D((string(h->GetName())+"_HV").c_str(), "phys_HV", 14, 0.5, 14.5, 16, 0.5, 16.5);
  output->Sumw2();
  for (int isec = 0; isec < 16; isec++){
    for (int imod = 0; imod < 14; imod++) {
      output->SetBinContent(imod+1, isec+1, h->GetBinContent(imod+1, isec+1)*factor.v[isec][imod]);
      output->SetBinError(imod+1, isec+1, h->GetBinError(imod+1, isec+1)*factor.v[isec][imod]);
    }
  }
  output->SetMaximum(1000);
  output->SetMinimum(0.01);
  return output;
}


TH2D*
InverseCorrectionHV(const TH2D* h, const CorrectionArray& factor)
{
  TH2D* output = new TH2D((string(h->GetName())+"_HV").c_str(), "muons_HV", 14, 0.5, 14.5, 16, 0.5, 16.5);
  output->Sumw2();
  for (int isec = 0; isec < 16; isec++){
    for (int imod = 0; imod < 14; imod++) {
      if (factor.v[isec][imod]>0) {
        output->SetBinContent(imod+1, isec+1, h->GetBinContent(imod+1, isec+1)/factor.v[isec][imod]);
        output->SetBinError(imod+1, isec+1, h->GetBinError(imod+1, isec+1)/factor.v[isec][imod]);
      } else
        output->SetBinContent(imod+1, isec+1, 0);
    }
  }
  output->SetMaximum(1000);
  output->SetMinimum(0.01);
  return output;
}


TH2D* 
MakeCalibration(const TH2D* h)
{ 
  TH2D* output = new TH2D((string(h->GetName())+"_calib").c_str(), "cal", 14, 0.5, 14.5, 16, 0.5, 16.5);
  output->Sumw2();
  for (int isec = 0; isec < 16; isec++){
    for (int imod = 0; imod < 14; imod++) {
      const double zahl25 = h->GetBinContent(imod+1, isec+1);
      double zahl2 = 0;
      double zahl3 = 0;
      if (zahl25>0) {
        zahl2 = 1. / zahl25;
        zahl3 = sqrt( pow( pow(1/zahl25, 2) * h->GetBinError(imod+1, isec+1), 2) );
      }
      output->SetBinContent(imod+1, isec+1, zahl2);
      output->SetBinError(imod+1, isec+1, zahl3);
      //output->SetBinError(imod+1, isec+1, 0);
      //output->SetBinError(imod+1, isec+1, zahl2*0.16);
    }
  }
  return output;
} 



enum ScaleMode { eIgorKatarina, eRalf }; // igor: use Katerina 2013 results and LED corrections, ralf: consider muons response stable
TH2D* gReferenceMuonResponse = 0;

void
EnergyScale(TH2D* h, const BadChannelList& bc, const ScaleMode mode = eIgorKatarina)
{
  BadChannelList bc2013;
  bc2013.SetFix(BadChannelList::kBad2013);
  double scale_2013 = 0;
  double scale_h = 0;
  int n_scale = 0;
  for (int isec = 0; isec < 16; isec++){
    for (int imod = 0; imod < 14; imod++){
      
      if (imod >= 5) continue;
      
      // const double inv_trpval = GainCorrectionFactors_pp(isec, imod);
      
      if ( bc.Is(isec+1, imod+1) ) continue;      
      if ( bc2013.Is(isec+1, imod+1) ) continue; 
      //if ( inv_trpval <= 0 ) continue;
      
      const double Kat2013 = Getcorr4Tto0TKaterina2013(isec, imod) *
        GetKaterina2013(isec, imod);
      
      const double Response2013 = (mode==eIgorKatarina ? Kat2013 : gReferenceMuonResponse->GetBinContent(imod+1,isec+1));
      
      if (Response2013 <= 0) continue;      
      if (h->GetBinContent(imod+1, isec+1) <= 0) continue;
      
      scale_2013 += Response2013;
      scale_h += h->GetBinContent(imod+1, isec+1);
      n_scale ++;
    }
  }
  cout << "scale_2013=" << scale_2013/n_scale << " scale_h=" << scale_h/n_scale << endl;  
  const double scale = scale_2013 / scale_h * 0.020;
  cout << "scale=" << scale << endl;
  h->Scale(scale);  
}








void
calibrate() 
{         
  gStyle->SetOptStat("");
  gStyle->SetPaintTextFormat("4.2f");
  TGaxis::SetMaxDigits(3);
  //setTDRStyle();
  //tdrStyle->SetPadRightMargin(0.04);
  //tdrStyle->SetPadLeftMargin(0.165);
  //tdrStyle->SetOptFit(0);        
  gStyle->SetOptStat(1);  


  BadChannelList bc2015;
  bc2015.SetFix(BadChannelList::kBad2015);
  
  BadChannelList bc2016;
  bc2016.SetFix(BadChannelList::kBad2016);




  const TH2D* hCalib_2013_4T = ReadMuonResponse(//"MuonSelectionOutput_data_PAMinBiasUPC_Run2013_iteration_5.root",
                                                //dirData + "/MuonSelectionOutput_data_PAMinBiasUPC_Run2013_iteration_19.root",
                                                //dirData + "/MuonSelectionOutput_data_PAMinBiasUPC_Run2013_iteration_20.root",
                                                dirData + "/MuonSelectionOutput_data_PAMinBiasUPC_Run2013_iteration_21.root",
                                                "muon_response_2013_4T",
                                                kGreen+1, 1, 2);
  

  const TH2D* hCalib_2015_0T = ReadMuonResponse(//dirData + "/MuonSelectionOutput_data_MinimumBias_2015_0T_June_iteration_21.root",
                                                //dirData + "/MuonSelectionOutput_data_MinimumBias_2015_0T_June_iteration_22.root",
                                                dirData + "/MuonSelectionOutput_data_MinimumBias_2015_0T_June_iteration_23.root",
                                                "muon_response_2015_0T",
                                                kBlue, 1, 2);

  const TH2D* hCalib_2015_Nov_0T = ReadMuonResponse(//dirData + "/MuonSelectionOutput_data_MinimumBias_2015_0T_Nov_iteration_17.root",
                                                    //dirData + "/MuonSelectionOutput_data_MinimumBias_2015_0T_Nov_iteration_18.root",
                                                    dirData + "/MuonSelectionOutput_data_MinimumBias_2015_0T_Nov_iteration_19.root",
                                                    "muon_response_2015_Nov_0T",
                                                    kBlue-2, 1, 2);
  
  const TH2D* hCalib_2015_Nov_4T = ReadMuonResponse(//dirData + "/MuonSelectionOutput_data_MinimumBias_2015_38T_Nov_iteration_16.root",
                                                    //dirData + "/MuonSelectionOutput_data_MinimumBias_2015_38T_Nov_iteration_17.root",
                                                    dirData + "/MuonSelectionOutput_data_MinimumBias_2015_38T_Nov_iteration_18.root",
                                                    "muon_response_2015_Nov_4T",
                                                    kOrange+1, 1, 2);
  
  const TH2D* hCalib_2015_HI_4T = ReadMuonResponse(//dirData + "/MuonSelectionOutput_data_MinimumBias_2015_38T_HIRunNov_iteration_18.root",
                                                   //dirData + "/MuonSelectionOutput_data_MinimumBias_2015_38T_HIRunNov_iteration_19.root",
                                                   dirData + "/MuonSelectionOutput_data_MinimumBias_2015_38T_HIRunNov_iteration_20.root",
                                                   "muon_response_2015_HI_4T", 
                                                   kRed-2, 1, 2);
  
  const TH2D* hCalib_2015_Nov_ppHV = ReadMuonResponse(//dirData + "/MuonSelectionOutput_data_MinimumBias_2015_38T_ppHVNov_iteration_16.root",
                                                      //dirData + "/MuonSelectionOutput_data_MinimumBias_2015_38T_ppHVNov_iteration_17.root",
                                                      dirData + "/MuonSelectionOutput_data_MinimumBias_2015_38T_ppHVNov_iteration_18.root",
                                                      "muon_response_2015_Nov_ppHV", 
                                                      kRed, 1, 2);
  
  const TH2D* hCalib_2016C_4T = ReadMuonResponse(//dirData + "/MuonSelectionOutput_data_PACastor_Muon_2016C_iteration_17.root",
                                                 //dirData + "/MuonSelectionOutput_data_PACastor_Muon_2016C_iteration_18.root",
                                                 dirData + "/MuonSelectionOutput_data_PACastor_Muon_2016C_iteration_19.root",
                                                 "muon_response_2016C_4T",
                                                 kYellow+2, 1, 2);
  
  const TH2D* hCalib_2016B_4T = ReadMuonResponse(//dirData + "/MuonSelectionOutput_data_PACastor_Muon_2016B_iteration_15.root",
                                                 //dirData + "/MuonSelectionOutput_data_PACastor_Muon_2016B_iteration_16.root",
                                                 dirData + "/MuonSelectionOutput_data_PACastor_Muon_2016B_iteration_17.root",
                                                 "muon_response_2016B_4T",
                                                 kMagenta, 1, 2);

  // const bool do2011_magnet = false;
  /*const TH2D* hCalib_2011_0T = ReadMuonResponse(dirData + "/MuonSelectionOutput_data_ForwardTriggers_2011_0T_iteration_10.root",
                                                "muon_response_2011_0T",
                                                kAzure+2, 9, 2);
  
  const TH2D* hCalib_2011_2T = ReadMuonResponse(dirData + "/MuonSelectionOutput_data_ForwardTriggers_2011_2T_iteration_19.root",
                                                "muon_response_2011_2T",
                                                kCyan+3, 9, 2);
  
  const TH2D* hCalib_2011_4T = ReadMuonResponse(dirData + "/MuonSelectionOutput_data_ForwardTriggers_2011_iteration_3.root",
                                                "muon_response_2011_4T",
                                                kRed+4, 9, 2);
  */

  /*
  const TH2D* hCalib_2011_4T_Mar = ReadMuonResponse(dirData + "/MuonSelectionOutput_data_ForwardTriggers_2011_Mar_iteration_13.root",
                                                    "muon_response_2011_4T_Mar",
                                                    kOrange+2, 9, 2);
  */
  
  const TH2D* hCalib_2011_4T_Apr = ReadMuonResponse(//dirData + "/MuonSelectionOutput_data_ForwardTriggers_2011_Apr_iteration_4.root",
                                                    //dirData + "/MuonSelectionOutput_data_ForwardTriggers_2011_Apr_iteration_5.root",
                                                    dirData + "/MuonSelectionOutput_data_ForwardTriggers_2011_Apr_iteration_6.root",
                                                    "muon_response_2011_4T_Apr",
                                                    kOrange+2, 9, 2);

  const TH2D* hCalib_2011_4T_May = ReadMuonResponse(//dirData + "/MuonSelectionOutput_data_ForwardTriggers_2011_May_iteration_5.root",
                                                    //dirData + "/MuonSelectionOutput_data_ForwardTriggers_2011_May_iteration_6.root",
                                                    dirData + "/MuonSelectionOutput_data_ForwardTriggers_2011_May_iteration_7.root",
                                                    "muon_response_2011_4T_May",
                                                    kOrange+2, 9, 2);

  const TH2D* hCalib_2011_4T_June = ReadMuonResponse(//dirData + "/MuonSelectionOutput_data_ForwardTriggers_2011_Jun_iteration_2.root",
                                                     //dirData + "/MuonSelectionOutput_data_ForwardTriggers_2011_Jun_iteration_3.root",
                                                     dirData + "/MuonSelectionOutput_data_ForwardTriggers_2011_Jun_iteration_4.root",
                                                    "muon_response_2011_4T_June",
                                                    kOrange+2, 9, 2);
  
  
  


  


  BadChannelList bad2016;
  bad2016.Set(11, 11);
  bad2016.Set(12, 11);
  bad2016.Set(13, 11);
  bad2016.Set(14, 11);

  bad2016.Set(3, 11); // noisy as also found by Hans ! 
  bad2016.Set(7, 14);
  bad2016.Set(16, 6);

  {
    TCanvas c;
    bad2016.GetHist("BadChannels2016;module;sector")->Draw("colz");
    c.SaveAs("BadChannels2016.png");  
    bad2016.Save("BadChannels2016.txt");
  }

  
  
  BadChannelList bad2013;
  bad2013.Load("../AnalyseCFF/CastorChannelQuality_v2.2_offline.txt");
  bad2013.Set(11, 11);
  bad2013.Set(12, 11);
  bad2013.Set(13, 11);
  bad2013.Set(14, 11);

  bad2013.Set(8,10);
  bad2013.Set(8,11);
  bad2013.Set(8,12);

  //bad2013.Set(5,1);
  //bad2013.Set(5,2);

  bad2013.Set(3,11);
  bad2013.Set(4,13);
  bad2013.Set(15,10);

  {
    TCanvas c;
    bad2013.GetHist("BadChannels2013;module;sector")->Draw("colz");
    c.SaveAs("BadChannels2013.png");  
    bad2013.Save("BadChannels2013.txt");
  }

  
  /*
    35 *         3 *         8 *         1 *      
    105 *         8 *         8 *         1 *      
    215 *        16 *         6 *         1 *      
  */
  
  BadChannelList bad2015_0T;
  bad2015_0T.Load("../AnalyseCFF/CastorChannelQuality_v3.00_offline.txt");

  bad2015_0T.Set(11, 11);
  bad2015_0T.Set(12, 11);
  bad2015_0T.Set(13, 11);
  bad2015_0T.Set(14, 11);

  //bad2015_0T.Set(7, 10);
  //bad2015_0T.Set(7, 11);
  bad2015_0T.Set(7, 12);
  bad2015_0T.Set(8, 10);
  bad2015_0T.Set(8, 11);
  bad2015_0T.Set(8, 12);
  
  bad2015_0T.Set(14, 14);
  bad2015_0T.Set(6, 12);

  {
    TCanvas c;
    bad2015_0T.GetHist("BadChannels2015June;module;sector")->Draw("colz");
    c.SaveAs("BadChannels2015June.png");  
    bad2015_0T.Save("BadChannels2015June.txt");
  }

  
  
  BadChannelList bad2015_4T;
  bad2015_4T.Load("../AnalyseCFF/CastorChannelQuality_v3.00_offline.txt");
  bad2015_4T.Set(11, 11);
  bad2015_4T.Set(12, 11);
  bad2015_4T.Set(13, 11);
  bad2015_4T.Set(14, 11);
  for (int i=7; i<=9; ++i) for (int j=1; j<=16; ++j) bad2015_4T.Set(j, i);

  {
    TCanvas c;
    bad2015_4T.GetHist("BadChannels2015Nov;module;sector")->Draw("colz");
    c.SaveAs("BadChannels2015Nov.png");  
    bad2015_4T.Save("BadChannels2015Nov.txt");
  }
  
  
  BadChannelList bad2011;
  bad2011.Load("../AnalyseCFF/castor_channelquality_v1.0_hlt.txt");
  
  {
    TCanvas c;
    bad2011.GetHist("BadChannels2011;module;sector")->Draw("colz");
    c.SaveAs("BadChannels2011.png");  
    bad2011.Save("BadChannels2011.txt");
  }
  
  
  
  
  // -------------------------------------------------
  
  TCanvas* cHV_from_muons = new TCanvas("cHV_from_muons");
  cHV_from_muons->SetLogz(1);
  
  TH2D* hHV = (TH2D*) hCalib_2015_Nov_ppHV->Clone("hv_from_muons");
  //hHV->Divide(hCalib_2015_Nov_0T);
  //hHV->Divide(hCalib_2015_Nov_4T);
  hHV->Divide(hCalib_2015_HI_4T);
  
  hHV->Draw("colz");
  
  
  
  
  
  // -------------------------------------------------

  CorrectionArray factors_2015_muons;
  factors_2015_muons.Load(hHV);
  TH2D* hCorr_2015_muons = factors_2015_muons.GetHist("correction_factors_2015_muons");
  
  CorrectionArray factors_recipeV0;
  factors_recipeV0.Load("../AnalyseCFF/CorrectionFactorsTestData_Stragegy6_RecipeV0.txt");
  TH2D* hCorr_recipeV0 = factors_recipeV0.GetHist("correction_factors_recipeV0");
  
  CorrectionArray factors;
  factors.Load("../AnalyseCFF/CorrectionFactorsTestData_Stragegy6.txt");
  TH2D* hCorr = factors.GetHist("correction_factors");
  
  CorrectionArray factorsPP;
  GetCorrectionsPP(factorsPP);
  TH2D* hCorrPP = factorsPP.GetHist("correction_factors_PP");
  
  //CorrectionArray factors_2015_Nov_0T;
  //factors_2015_Nov_0T.Load("CorrectionFactorsTestData_Stragegy6.txt");

  CorrectionArray factors_2011; // unity
  factors_2011 = factors_recipeV0;
  
  TH2D* hCorrectionRecipe = (TH2D*) hCorr_recipeV0->Clone("ratio_v0_v1");
  hCorrectionRecipe->Divide(hCorr);
  
  CorrectionArray factors_2011_muons; // 2011 muHV -> 2015 muHV
  factors_2011_muons.Load("CorrectionFactors_hvhistory2011.txt");
  TH2D* hCorr_2011_muons = factors_2011_muons.GetHist("correction_factors_2011_muons2muons");
  
  
  TCanvas* cCorrections = new TCanvas("cCorrections", "cCorrections", 1500, 300);
  cCorrections->Divide(6);
  
  cCorrections->cd(1);
  gPad->SetLogz(1);
  hCorr_2015_muons->Draw("colz");
  
  cCorrections->cd(2);
  gPad->SetLogz(1);
  hCorr_recipeV0->Draw("colz");

  cCorrections->cd(3);
  gPad->SetLogz(1);
  hCorr->Draw("colz");
  
  cCorrections->cd(4);
  gPad->SetLogz(1);
  hCorrPP->Draw("colz");

  cCorrections->cd(5);
  gPad->SetLogz(1);
  hCorrectionRecipe->Draw("colz");

  cCorrections->cd(6);
  gPad->SetLogz(1);
  hCorr_2011_muons->Draw("colz");




  // -------------------------------------------------
  
  
  // NOTE: the energy scale reference data must be processed first !!!!
  //       for ScaleMode = eRalf
  
  
  // --------------------------------------------------------------------
  //
  // 2013 calibration, B=3.8T   phys-HV 
  
  const TH2D* hCalib_2013_4T_muHV = InverseCorrectionHV(hCalib_2013_4T, factors);
  //const TH2D* hCalib_2013_4T_muHV = CorrectionHV(hCalib_2013_4T, factors);
  //const TH2D* hCalib_2013_4T_muHV = InverseCorrectionHV(hCalib_2013_4T, factors_recipeV0);
  TH2D* hCalib_2013_4T_HV = (TH2D*) hCalib_2013_4T->Clone("hCalib_2013_4T_HV"); 
  gReferenceMuonResponse = hCalib_2013_4T_HV;
  TH2D* hCalib_2013_4T_cal = MakeCalibration(hCalib_2013_4T_HV); // inverse of muon signals
  EnergyScale(hCalib_2013_4T_cal, bc2015);  // energy scale (to 2013)
  WriteIntercal(hCalib_2013_4T_cal, "intercalibration_2013_4T_Nov.txt");
  {
    TCanvas c;
    c.SetLogz(1);
    hCalib_2013_4T_cal->Draw("colz");
    c.SaveAs("intercalibration_2013_4T_Nov.png");
  }
  
  TCanvas* calib_2013_4T = new TCanvas("calib_2013_4T", "calib_2013_4T");
  calib_2013_4T->Divide(2,2);
  
  calib_2013_4T->cd(1);
  gPad->SetLogz(1);
  //hCalib_2013_4T->SetMaximum(400);
  //hCalib_2013_4T->SetMinimum(0);
  const_cast<TH2D*>(hCalib_2013_4T_muHV)->Draw("colz");
  
  calib_2013_4T->cd(2);
  gPad->SetLogz(1);
  //hCalib_2013_4T_muHV->SetMaximum(400);
  //hCalib_2013_4T_muHV->SetMinimum(0);
  const_cast<TH2D*>(hCalib_2013_4T_HV)->Draw("colz");
  //hCorr->Draw("colz");
  
  calib_2013_4T->cd(3);
  gPad->SetLogz(1);
  hCalib_2013_4T_HV->Draw("colz");
  
  calib_2013_4T->cd(4);
  gPad->SetLogz(1);
  //hCalib_2013_4T_cal->SetMinimum(0.001);
  //hCalib_2013_4T_cal->SetMaximum(0.1);
  hCalib_2013_4T_cal->Draw("colz");

  

  
  // --------------------------------------------------------------------
  //
  // 2015 June calibration, B=0T
  
  const TH2D* hCalib_2015_0T_muHV = (TH2D*) hCalib_2015_0T->Clone("hCalib_2015_0T_muHV");
  TH2D* hCalib_2015_0T_HV = CorrectionHV(hCalib_2015_0T, factors); 
  //TH2D* hCalib_2015_0T_HV = CorrectionHV(hCalib_2015_0T, factors_2015_muons); 
  TH2D* hCalib_2015_0T_cal = MakeCalibration(hCalib_2015_0T_HV); // inverse of muon signals  
  EnergyScale(hCalib_2015_0T_cal, bc2015, eIgorKatarina);  // energy scale (to 2013)
  // EnergyScale(hCalib_2015_0T_cal, bc2015, eRalf);  // energy scale (to 2013)
  WriteIntercal(hCalib_2015_0T_cal, "intercalibration_2015_0T_June.txt");
  {
    TCanvas c;
    c.SetLogz(1);
    hCalib_2015_0T_cal->Draw("colz");
    c.SaveAs("intercalibration_2015_0T_June.png");
  }

  TCanvas* calib_2015_0T = new TCanvas("calib_2015_0T", "calib_2015_0T");
  calib_2015_0T->Divide(2,2);
  
  calib_2015_0T->cd(1);
  gPad->SetLogz(1);
  // hCalib_2015_0T->SetMaximum(400);
  // hCalib_2015_0T->SetMinimum(0);
  const_cast<TH2D*>(hCalib_2015_0T_muHV)->Draw("colz");
  
  calib_2015_0T->cd(2);
  gPad->SetLogz(1);
  //hCalib_2015_0T->Draw("colz");
  hCorr->Draw("colz");
  
  calib_2015_0T->cd(3);
  gPad->SetLogz(1);
  hCalib_2015_0T_HV->Draw("colz");
  
  calib_2015_0T->cd(4);
  gPad->SetLogz(1);
  //hCalib_2015_0T_cal->SetMinimum(0.001);
  //hCalib_2015_0T_cal->SetMaximum(0.1);
  hCalib_2015_0T_cal->Draw("colz");
  
  
  
  // --------------------------------------------------------------------
  //
  // 2015 Nov calibration, B=0T
  
  
  TH2D* hCalib_2015_Nov_0T_HV = CorrectionHV(hCalib_2015_Nov_0T, factors); 
  TH2D* hCalib_2015_Nov_0T_cal = MakeCalibration(hCalib_2015_Nov_0T_HV); 
  EnergyScale(hCalib_2015_Nov_0T_cal, bc2015);  // energy scale (to 2013)
  WriteIntercal(hCalib_2015_Nov_0T_cal, "intercalibration_2015_Nov_0T.txt");
  {
    TCanvas c;
    c.SetLogz(1);
    hCalib_2015_Nov_0T_cal->Draw("colz");
    c.SaveAs("intercalibration_2015_Nov_0T.png");
  }

  TCanvas* calib_2015_Nov_0T = new TCanvas("calib_2015_Nov_0T", "calib_2015_Nov_0T");
  calib_2015_Nov_0T->Divide(2,2);
  
  calib_2015_Nov_0T->cd(1);
  gPad->SetLogz(1);
  //hCalib_2015_Nov_0T->SetMaximum(400);
  //hCalib_2015_Nov_0T->SetMinimum(0);
  //const_cast<TH2D*>(hCalib_2015_Nov_0T_muHV)->Draw("colz");
  
  calib_2015_Nov_0T->cd(2);
  gPad->SetLogz(1);
  const_cast<TH2D*>(hCalib_2015_Nov_0T)->Draw("colz");
  
  calib_2015_Nov_0T->cd(3);
  gPad->SetLogz(1);
  hCalib_2015_Nov_0T_HV->Draw("colz");
  
  calib_2015_Nov_0T->cd(4);
  gPad->SetLogz(1);
  //hCalib_2015_Nov_0T_cal->SetMinimum(0.001);
  //hCalib_2015_Nov_0T_cal->SetMaximum(0.1);
  hCalib_2015_Nov_0T_cal->Draw("colz");
  
  
  
  // --------------------------------------------------------------------
  //
  // 2015 Nov calibration, B=4T
  
  const TH2D* hCalib_2015_Nov_4T_muHV = (TH2D*) hCalib_2015_Nov_4T->Clone("hCalib_2015_Nov_4T_muHV");
  TH2D* hCalib_2015_Nov_4T_HV = CorrectionHV(hCalib_2015_Nov_4T, factors); 
  TH2D* hCalib_2015_Nov_4T_cal = MakeCalibration(hCalib_2015_Nov_4T_HV); // inverse of muon signals
  EnergyScale(hCalib_2015_Nov_4T_cal, bc2015);  // energy scale (to 2013)
  WriteIntercal(hCalib_2015_Nov_4T_cal, "intercalibration_2015_Nov_4T.txt");
  {
    TCanvas c;
    c.SetLogz(1);
    hCalib_2015_Nov_4T_cal->Draw("colz");
    c.SaveAs("intercalibration_2015_Nov_4T.png");
  }

  TCanvas* calib_2015_Nov_4T = new TCanvas("calib_2015_Nov_4T", "calib_2015_Nov_4T");
  calib_2015_Nov_4T->Divide(2,2);
  
  calib_2015_Nov_4T->cd(1);
  gPad->SetLogz(1);
  //hCalib_2015_Nov_4T->SetMaximum(400);
  //hCalib_2015_Nov_4T->SetMinimum(0);
  const_cast<TH2D*>(hCalib_2015_Nov_4T_muHV)->Draw("colz");
  
  calib_2015_Nov_4T->cd(2);
  gPad->SetLogz(1);
  const_cast<TH2D*>(hCalib_2015_Nov_4T)->Draw("colz");
  
  calib_2015_Nov_4T->cd(3);
  gPad->SetLogz(1);
  hCalib_2015_Nov_4T_HV->Draw("colz");
  
  calib_2015_Nov_4T->cd(4);
  gPad->SetLogz(1);
  //hCalib_2015_Nov_4T_cal->SetMinimum(0.001);
  //hCalib_2015_Nov_4T_cal->SetMaximum(0.1);
  hCalib_2015_Nov_4T_cal->Draw("colz");

  
  
  // --------------------------------------------------------------------
  //
  // 2015 Nov HIRun, B=3.8T
  
  const TH2D* hCalib_2015_HI_4T_muHV = (TH2D*) hCalib_2015_HI_4T->Clone("hCalib_2015_HI_4T_muHV");
  TH2D* hCalib_2015_HI_4T_HV = CorrectionHV(hCalib_2015_HI_4T, factors); 
  TH2D* hCalib_2015_HI_4T_cal = MakeCalibration(hCalib_2015_HI_4T_HV); // inverse of muon signals
  EnergyScale(hCalib_2015_HI_4T_cal, bc2015);  // energy scale (to 2013)
  WriteIntercal(hCalib_2015_HI_4T_cal, "intercalibration_2015_HI_4T_Nov.txt");
  {
    TCanvas c;
    c.SetLogz(1);
    hCalib_2015_HI_4T_cal->Draw("colz");
    c.SaveAs("intercalibration_2015_HI_4T_Nov.png");
  }

  TCanvas* calib_2015_HI_4T = new TCanvas("calib_2015_HI_4T", "calib_2015_HI_4T");
  calib_2015_HI_4T->Divide(2,2);
  
  calib_2015_HI_4T->cd(1);
  gPad->SetLogz(1);
  //hCalib_2015_HI_4T->SetMaximum(400);
  //hCalib_2015_HI_4T->SetMinimum(0);
  const_cast<TH2D*>(hCalib_2015_HI_4T_muHV)->Draw("colz");
  
  calib_2015_HI_4T->cd(2);
  gPad->SetLogz(1);
  const_cast<TH2D*>(hCalib_2015_HI_4T)->Draw("colz");
  
  calib_2015_HI_4T->cd(3);
  gPad->SetLogz(1);
  hCalib_2015_HI_4T_HV->Draw("colz");
  
  calib_2015_HI_4T->cd(4);
  gPad->SetLogz(1);
  //hCalib_2015_HI_4T_cal->SetMinimum(0.001);
  //hCalib_2015_HI_4T_cal->SetMaximum(0.1);
  hCalib_2015_HI_4T_cal->Draw("colz");
  
  
  
  
  // --------------------------------------------------------------------
  //
  // 2015 Nov calibration, B=3.8T, pp-physHV
  
  const TH2D* hCalib_2015_Nov_ppHV_muHV = InverseCorrectionHV(hCalib_2015_Nov_ppHV, factors);
  TH2D* hCalib_2015_Nov_ppHV_HV = (TH2D*)hCalib_2015_Nov_ppHV->Clone();
  TH2D* hCalib_2015_Nov_ppHV_cal = MakeCalibration(hCalib_2015_Nov_ppHV_HV); // inverse of muon signals
  EnergyScale(hCalib_2015_Nov_ppHV_cal, bc2015);  // energy scale (to 2013)
  WriteIntercal(hCalib_2015_Nov_ppHV_cal, "intercalibration_2015_Nov_ppHV_Nov.txt");
  {
    TCanvas c;
    c.SetLogz(1);
    hCalib_2015_Nov_ppHV_cal->Draw("colz");
    c.SaveAs("intercalibration_2015_Nov_ppHV_Nov.png");
  }

  TCanvas* calib_2015_Nov_ppHV = new TCanvas("calib_2015_Nov_ppHV", "calib_2015_Nov_ppHV");
  calib_2015_Nov_ppHV->Divide(2,2);
  
  calib_2015_Nov_ppHV->cd(1);
  gPad->SetLogz(1);
  //hCalib_2015_Nov_ppHV->SetMaximum(400);
  //hCalib_2015_Nov_ppHV->SetMinimum(0);
  const_cast<TH2D*>(hCalib_2015_Nov_ppHV_muHV)->Draw("colz");
  
  calib_2015_Nov_ppHV->cd(2);
  gPad->SetLogz(1);
  //hCalib_2015_Nov_ppHV_muHV->SetMaximum(400);
  //hCalib_2015_Nov_ppHV_muHV->SetMinimum(0);
  const_cast<TH2D*>(hCalib_2015_Nov_ppHV_muHV)->Draw("colz");
  
  calib_2015_Nov_ppHV->cd(3);
  gPad->SetLogz(1);
  hCalib_2015_Nov_ppHV_HV->Draw("colz");
  
  calib_2015_Nov_ppHV->cd(4);
  gPad->SetLogz(1);
  //hCalib_2015_Nov_ppHV_cal->SetMinimum(0.001);
  //hCalib_2015_Nov_ppHV_cal->SetMaximum(0.1);
  hCalib_2015_Nov_ppHV_cal->Draw("colz");
  
  
  
  
  
  // --------------------------------------------------------------------
  //
  // 2016 Nov calibration, B=3.8T,  2016C
  
  const TH2D* hCalib_2016C_4T_muHV = InverseCorrectionHV(hCalib_2016C_4T, factors); 
  TH2D* hCalib_2016C_4T_HV = (TH2D*)hCalib_2016C_4T->Clone(); 
  TH2D* hCalib_2016C_4T_cal = MakeCalibration(hCalib_2016C_4T_HV); // inverse of muon signals
  EnergyScale(hCalib_2016C_4T_cal, bc2016);  // energy scale (to 2013)
  WriteIntercal(hCalib_2016C_4T_cal, "intercalibration_2016C_4T_Nov.txt");
  {
    TCanvas c;
    c.SetLogz(1);
    hCalib_2016C_4T_cal->Draw("colz");
    c.SaveAs("intercalibration_2016C_4T_Nov.png");
  }
  
  TCanvas* calib_2016C_4T = new TCanvas("calib_2016C_4T", "calib_2016C_4T");
  calib_2016C_4T->Divide(2,2);
  
  calib_2016C_4T->cd(1);
  gPad->SetLogz(1);
  //hCalib_2016C_4T->SetMaximum(400);
  //hCalib_2016C_4T->SetMinimum(0);
  const_cast<TH2D*>(hCalib_2016C_4T)->Draw("colz");
  
  calib_2016C_4T->cd(2);
  gPad->SetLogz(1);
  //hCalib_2016C_4T_muHV->SetMaximum(400);
  //hCalib_2016C_4T_muHV->SetMinimum(0);      
  const_cast<TH2D*>(hCalib_2016C_4T_muHV)->Draw("colz");
  
  calib_2016C_4T->cd(3);
  gPad->SetLogz(1);
  hCalib_2016C_4T_HV->Draw("colz");
  
  calib_2016C_4T->cd(4);
  gPad->SetLogz(1);
  //hCalib_2016C_4T_cal->SetMinimum(0.001);
  //hCalib_2016C_4T_cal->SetMaximum(0.1);
  hCalib_2016C_4T_cal->Draw("colz");
  

  

  // --------------------------------------------------------------------
  //
  // 2016 Nov calibration, B=3.8T   2016B
  
  const TH2D* hCalib_2016B_4T_muHV = InverseCorrectionHV(hCalib_2016B_4T, factors);
  TH2D* hCalib_2016B_4T_HV = (TH2D*)hCalib_2016B_4T->Clone("hCalib_2016B_4T_HV"); 
  TH2D* hCalib_2016B_4T_cal = MakeCalibration(hCalib_2016B_4T_HV); // inverse of muon signals
  EnergyScale(hCalib_2016B_4T_cal, bc2016);  // energy scale (to 2013)
  WriteIntercal(hCalib_2016B_4T_cal, "intercalibration_2016B_4T_Nov.txt");
  {
    TCanvas c;
    c.SetLogz(1);
    hCalib_2016B_4T_cal->Draw("colz");
    c.SaveAs("intercalibration_2016B_4T_Nov.png");
  }

  TCanvas* calib_2016B_4T = new TCanvas("calib_2016B_4T", "calib_2016B_4T");
  calib_2016B_4T->Divide(2,2);
  
  calib_2016B_4T->cd(1);
  gPad->SetLogz(1);
  //hCalib_2016B_4T->SetMaximum(400);
  //hCalib_2016B_4T->SetMinimum(0);
  const_cast<TH2D*>(hCalib_2016B_4T)->Draw("colz");
  
  calib_2016B_4T->cd(2);
  gPad->SetLogz(1);
  //hCalib_2016B_4T_muHV->SetMaximum(400);
  //hCalib_2016B_4T_muHV->SetMinimum(0);
  const_cast<TH2D*>(hCalib_2016B_4T_muHV)->Draw("colz");
  
  calib_2016B_4T->cd(3);
  gPad->SetLogz(1);
  hCalib_2016B_4T_HV->Draw("colz");
  
  calib_2016B_4T->cd(4);
  gPad->SetLogz(1);
  //hCalib_2016B_4T_cal->SetMinimum(0.001);
  //hCalib_2016B_4T_cal->SetMaximum(0.1);
  hCalib_2016B_4T_cal->Draw("colz");
   
  
  
  
  
  // --------------------------------------------------------------------
  //
  // 2011 calibration, B=0T   muon-HV 
  /*
  TH2D* hCalib_2011_0T_HV = CorrectionHV(hCalib_2011_0T, factors_2011); 
  const TH2D* hCalib_2011_0T_muHV = (TH2D*)hCalib_2011_0T->Clone("hCalib_2011_0T_muHV");
  TH2D* hCalib_2011_0T_cal = MakeCalibration(hCalib_2011_0T_HV); // inverse of muon signals
  EnergyScale(hCalib_2011_0T_cal, bc2015);  // energy scale (to 2013)
  WriteIntercal(hCalib_2011_0T_cal, "intercalibration_2011_0T_Nov.txt");
  
  TCanvas* calib_2011_0T = new TCanvas("calib_2011_0T", "calib_2011_0T");
  calib_2011_0T->Divide(2,2);
  
  calib_2011_0T->cd(1);
  gPad->SetLogz(1);
  //hCalib_2011_0T->SetMaximum(400);
  //hCalib_2011_0T->SetMinimum(0);
  const_cast<TH2D*>(hCalib_2011_0T)->Draw("colz");
  
  calib_2011_0T->cd(2);
  gPad->SetLogz(1);
  //hCorr_2011_0T->Draw("colz");
  
  calib_2011_0T->cd(3);
  gPad->SetLogz(1);
  hCalib_2011_0T_HV->Draw("colz");
  
  calib_2011_0T->cd(4);
  gPad->SetLogz(1);
  //hCalib_2011_0T_cal->SetMinimum(0.001);
  //hCalib_2011_0T_cal->SetMaximum(0.1);
  hCalib_2011_0T_cal->Draw("colz");
  
  
  
  
  
  // --------------------------------------------------------------------
  //
  // 2011 calibration, B=2T   muon-HV 
  
  TH2D* hCalib_2011_2T_HV = CorrectionHV(hCalib_2011_2T, factors_2011);
  const TH2D* hCalib_2011_2T_muHV = (TH2D*)hCalib_2011_2T->Clone("hCalib_2011_2T_muHV");
  TH2D* hCalib_2011_2T_cal = MakeCalibration(hCalib_2011_2T_HV); // inverse of muon signals
  EnergyScale(hCalib_2011_2T_cal, bc2015);  // energy scale (to 2013)
  WriteIntercal(hCalib_2011_2T_cal, "intercalibration_2011_2T_Nov.txt");
  
  TCanvas* calib_2011_2T = new TCanvas("calib_2011_2T", "calib_2011_2T");
  calib_2011_2T->Divide(2,2);
  
  calib_2011_2T->cd(1);
  gPad->SetLogz(1);
  //hCalib_2011_2T->SetMaximum(400);
  //hCalib_2011_2T->SetMinimum(0);
  const_cast<TH2D*>(hCalib_2011_2T)->Draw("colz");
  
  calib_2011_2T->cd(2);
  gPad->SetLogz(1);
  //hCorr_2011_2T->Draw("colz");
  
  calib_2011_2T->cd(3);
  gPad->SetLogz(1);
  hCalib_2011_2T_HV->Draw("colz");
  
  calib_2011_2T->cd(4);
  gPad->SetLogz(1);
  //hCalib_2011_2T_cal->SetMinimum(0.001);
  //hCalib_2011_2T_cal->SetMaximum(0.1);
  hCalib_2011_2T_cal->Draw("colz");
  
  
  
  // --------------------------------------------------------------------
  //
  // 2011 calibration, B=4T   muon-HV 
  
  TH2D* hCalib_2011_4T_HV = CorrectionHV(hCalib_2011_4T, factors_2011);
  const TH2D* hCalib_2011_4T_muHV = (TH2D*)hCalib_2011_4T->Clone("hCalib_2011_4T_muHV");
  TH2D* hCalib_2011_4T_cal = MakeCalibration(hCalib_2011_4T_HV); // inverse of muon signals
  EnergyScale(hCalib_2011_4T_cal, bc2015);  // energy scale (to 2013)
  WriteIntercal(hCalib_2011_4T_cal, "intercalibration_2011_4T_Nov.txt");
  
  TCanvas* calib_2011_4T = new TCanvas("calib_2011_4T", "calib_2011_4T");
  calib_2011_4T->Divide(2,2);
  
  calib_2011_4T->cd(1);
  gPad->SetLogz(1);
  //hCalib_2011_4T->SetMaximum(400);
  //hCalib_2011_4T->SetMinimum(0);
  const_cast<TH2D*>(hCalib_2011_4T)->Draw("colz");
  
  calib_2011_4T->cd(2);
  gPad->SetLogz(1);
  //hCorr_2011_4T->Draw("colz");
  
  calib_2011_4T->cd(3);
  gPad->SetLogz(1);
  hCalib_2011_4T_HV->Draw("colz");
  
  calib_2011_4T->cd(4);
  gPad->SetLogz(1);
  //hCalib_2011_4T_cal->SetMinimum(0.001);
  //hCalib_2011_4T_cal->SetMaximum(0.1);
  hCalib_2011_4T_cal->Draw("colz");
  */
  
  
  
  // --------------------------------------------------------------------
  //
  // 2011 calibration, B=4T   muon-HV, split by months 

  /*
  //const TH2D* hCalib_2011_4T_Mar_muHV = (TH2D*)hCalib_2011_4T_Mar->Clone("hCalib_2011_4T_Mar_muHV");
  const TH2D* hCalib_2011_4T_Apr_muHV = (TH2D*)hCalib_2011_4T_Apr->Clone("hCalib_2011_4T_Apr_muHV");
  const TH2D* hCalib_2011_4T_May_muHV = (TH2D*)hCalib_2011_4T_May->Clone("hCalib_2011_4T_May_muHV");
  const TH2D* hCalib_2011_4T_Jun_muHV = (TH2D*)hCalib_2011_4T_June->Clone("hCalib_2011_4T_Jun_muHV");
  */
  
  const TH2D* hCalib_2011_4T_Apr_muHV2 = (TH2D*)hCalib_2011_4T_Apr->Clone("hCalib_2011_4T_Apr_muHV2");
  const TH2D* hCalib_2011_4T_May_muHV2 = (TH2D*)hCalib_2011_4T_May->Clone("hCalib_2011_4T_May_muHV2");
  const TH2D* hCalib_2011_4T_Jun_muHV2 = (TH2D*)hCalib_2011_4T_June->Clone("hCalib_2011_4T_Jun_muHV2");
  
  const TH2D* hCalib_2011_4T_Apr_muHV = InverseCorrectionHV(hCalib_2011_4T_Apr_muHV2, factors_2011_muons);
  const TH2D* hCalib_2011_4T_May_muHV = InverseCorrectionHV(hCalib_2011_4T_May_muHV2, factors_2011_muons);
  const TH2D* hCalib_2011_4T_Jun_muHV = InverseCorrectionHV(hCalib_2011_4T_Jun_muHV2, factors_2011_muons);
  
  
  // ------------------------------------------------------------------------------
  // relative relative relative relative relative relative relative relative 
  
  
  //const TH2D* ref = hCalib_2013_4T_muHV; // hCalib_2015_0T_muHV;
  const TH2D* ref = hCalib_2013_4T_HV; // hCalib_2015_0T_muHV;
  
  BadChannelList refBad = bad2015_0T;
  refBad.Combine(bad2015_4T);
  refBad.Combine(bad2013);
  refBad.Combine(bad2011);
  
  
  TCanvas* cRel = new TCanvas("cRel", "mu stability");
  cRel->Divide(2);
  cRel->cd(1);
  cRel->SetLogy(1);

  int relBin = 0;
  map<int, string> bLabel;

  double markerSitze = 1.6;
  int markerStyleMuHV = 21;
  int markerStylePhyHV = 20;
  int markerStyleMuHV2011 = 23;

  TGraphErrors* gRelative = new TGraphErrors(); // run II, p-hv
  gRelative->SetMarkerStyle(markerStylePhyHV);
  gRelative->SetMarkerSize(markerSitze);
  gRelative->SetMarkerColor(kRed+1);
  gRelative->SetLineColor(kRed+1);
  gRelative->SetLineWidth(3); 

  TGraphErrors* gRelative4 = new TGraphErrors(); // run II, mu-hv
  gRelative4->SetMarkerStyle(markerStyleMuHV);
  gRelative4->SetMarkerSize(markerSitze);
  gRelative4->SetMarkerColor(kRed+1);
  gRelative4->SetLineColor(kRed+1);
  gRelative4->SetLineWidth(3); 

  TGraphErrors* gRelative2 = new TGraphErrors(); // 2015 june, mu-hv, 0T
  gRelative2->SetMarkerStyle(markerStyleMuHV+4);
  gRelative2->SetMarkerSize(markerSitze*1.1);
  gRelative2->SetMarkerColor(kRed);
  gRelative2->SetLineColor(kRed);
  gRelative2->SetLineWidth(3); 
  
  TGraphErrors* gRelative3 = new TGraphErrors(); // 2011, mu-hv, 4T
  gRelative3->SetMarkerStyle(markerStyleMuHV);
  gRelative3->SetMarkerSize(markerSitze);
  gRelative3->SetMarkerColor(kBlue);
  gRelative3->SetLineColor(kBlue);
  gRelative3->SetLineWidth(3); 

  TGraphErrors* gRelative5 = new TGraphErrors(); // 2011, mu-hv-2011, 4T
  gRelative5->SetMarkerStyle(markerStyleMuHV2011);
  gRelative5->SetMarkerSize(markerSitze);
  gRelative5->SetMarkerColor(kBlue);
  gRelative5->SetLineColor(kBlue);
  gRelative5->SetLineWidth(3); 

  
  /*
  TH1D* hCalib_2011_0T_relative = Relative(hCalib_2011_0T_muHV, ref, refBad);
  hCalib_2011_0T_relative->Draw("same");
  gRelative->SetPoint(relBin, relBin+1, hCalib_2011_0T_relative->GetMean());
  gRelative->SetPointError(relBin, 0 , hCalib_2011_0T_relative->GetMeanError());
  bLabel[relBin] = "2011, 0T";
  ++relBin;

  TH1D* hCalib_2011_2T_relative = Relative(hCalib_2011_2T_muHV, ref, refBad);
  hCalib_2011_2T_relative->Draw("same");
  gRelative->SetPoint(relBin, relBin+1, hCalib_2011_2T_relative->GetMean());
  gRelative->SetPointError(relBin, 0 , hCalib_2011_2T_relative->GetMeanError());
  bLabel[relBin] = "2011, 2T";
  ++relBin;

  TH1D* hCalib_2011_4T_relative = Relative(hCalib_2011_4T_muHV, ref, refBad);
  hCalib_2011_4T_relative->Draw("same");
  gRelative->SetPoint(relBin, relBin+1, hCalib_2011_4T_relative->GetMean());
  gRelative->SetPointError(relBin, 0 , hCalib_2011_4T_relative->GetMeanError());
  bLabel[relBin] = "2011, 4T";
  ++relBin;
  */

  /*
  TH1D* hCalib_2011_4T_Mar_relative = Relative(hCalib_2011_4T_Mar_muHV, ref, refBad);
  hCalib_2011_4T_Mar_relative->Draw("same");
  gRelative3->SetPoint(gRelative3->GetN(), relBin+1, hCalib_2011_4T_Mar_relative->GetMean());
  gRelative3->SetPointError(gRelative3->GetN()-1, 0 , hCalib_2011_4T_Mar_relative->GetMeanError());
  bLabel[relBin] = "2011, 4T, Mar";
  ++relBin;
  */
  
  TH1D* hCalib_2011_4T_Apr_relative = Relative(hCalib_2011_4T_Apr_muHV, ref, refBad);
  hCalib_2011_4T_Apr_relative->Draw("same");
  gRelative3->SetPoint(gRelative3->GetN(), relBin+1, hCalib_2011_4T_Apr_relative->GetMean());
  gRelative3->SetPointError(gRelative3->GetN()-1, 0 , hCalib_2011_4T_Apr_relative->GetMeanError());
  bLabel[relBin] = "2011 Apr, pp";
  ++relBin;

  TH1D* hCalib_2011_4T_May_relative = Relative(hCalib_2011_4T_May_muHV, ref, refBad);
  hCalib_2011_4T_May_relative->Draw("same");
  gRelative3->SetPoint(gRelative3->GetN(), relBin+1, hCalib_2011_4T_May_relative->GetMean());
  gRelative3->SetPointError(gRelative3->GetN()-1, 0 , hCalib_2011_4T_May_relative->GetMeanError());
  bLabel[relBin] = "2011 May, pp";
  ++relBin;

  TH1D* hCalib_2011_4T_Jun_relative = Relative(hCalib_2011_4T_Jun_muHV, ref, refBad);
  hCalib_2011_4T_Jun_relative->Draw("same");
  gRelative3->SetPoint(gRelative3->GetN(), relBin+1, hCalib_2011_4T_Jun_relative->GetMean());
  gRelative3->SetPointError(gRelative3->GetN()-1, 0 , hCalib_2011_4T_Jun_relative->GetMeanError());
  bLabel[relBin] = "2011 June, pp";
  ++relBin;
  
  {
    TH1D* hCalib_2011_4T_Apr_relative2 = Relative(hCalib_2011_4T_Apr_muHV2, ref, refBad);
    hCalib_2011_4T_Apr_relative2->Draw("same");
    gRelative5->SetPoint(gRelative5->GetN(), relBin+1-3, hCalib_2011_4T_Apr_relative2->GetMean());
    gRelative5->SetPointError(gRelative5->GetN()-1, 0 , hCalib_2011_4T_Apr_relative2->GetMeanError());
    //bLabel[relBin] = "2011 Apr, pp";
    //++relBin;
    
    TH1D* hCalib_2011_4T_May_relative2 = Relative(hCalib_2011_4T_May_muHV2, ref, refBad);
    hCalib_2011_4T_May_relative2->Draw("same");
    gRelative5->SetPoint(gRelative5->GetN(), relBin+1-2, hCalib_2011_4T_May_relative2->GetMean());
    gRelative5->SetPointError(gRelative5->GetN()-1, 0 , hCalib_2011_4T_May_relative2->GetMeanError());
    //bLabel[relBin] = "2011 May, pp";
    //++relBin;
    
    TH1D* hCalib_2011_4T_Jun_relative2 = Relative(hCalib_2011_4T_Jun_muHV2, ref, refBad);
    hCalib_2011_4T_Jun_relative2->Draw("same");
    gRelative5->SetPoint(gRelative5->GetN(), relBin+1-1, hCalib_2011_4T_Jun_relative2->GetMean());
    gRelative5->SetPointError(gRelative5->GetN()-1, 0 , hCalib_2011_4T_Jun_relative2->GetMeanError());
    //bLabel[relBin] = "2011 June, pp";
    //++relBin;
  }
  
  //TH1D* hCalib_2013_4T_relative = Relative(hCalib_2013_4T_muHV, ref, refBad);
  TH1D* hCalib_2013_4T_relative = Relative(hCalib_2013_4T_HV, ref, refBad);
  hCalib_2013_4T_relative->Draw("same");
  gRelative->SetPoint(gRelative->GetN(), relBin+1, hCalib_2013_4T_relative->GetMean());
  gRelative->SetPointError(gRelative->GetN()-1, 0 , hCalib_2013_4T_relative->GetMeanError());
  bLabel[relBin] = "2013, pPb";
  ++relBin;
  
  //TH1D* hCalib_2015_June_0T_relative = Relative(hCalib_2015_0T_HV, ref, refBad);
  TH1D* hCalib_2015_June_0T_relative = Relative(hCalib_2015_0T_muHV, ref, refBad);
  hCalib_2015_June_0T_relative->Draw("same");
  gRelative2->SetPoint(gRelative2->GetN(), relBin+1, hCalib_2015_June_0T_relative->GetMean());
  gRelative2->SetPointError(gRelative2->GetN()-1, 0, hCalib_2015_June_0T_relative->GetMeanError());
  bLabel[relBin] = "2015 June, pp";
  ++relBin;

  /*
  TH2D* hCalib_2015_0T_muHV_magnet = (TH2D*) hCalib_2015_0T_muHV->Clone("hCalib_2015_0T_muHV_magnet");
  for (int isec=0;isec<16;++isec) {
    for (int imod=0;imod<14;++imod) {
      hCalib_2015_0T_muHV_magnet->SetBinContent(imod+1, isec+1, hCalib_2015_0T_muHV_magnet->GetBinContent(imod+1, isec+1) / Getcorr4Tto0TKaterina2013(isec, imod));
    }
  }
  TH1D* hCalib_2015_June_0T_mag_relative = Relative(hCalib_2015_0T_muHV_magnet, ref, refBad);
  hCalib_2015_June_0T_mag_relative->Draw("same");
  gRelative->SetPoint(relBin, relBin+1, hCalib_2015_June_0T_mag_relative->GetMean());
  gRelative->SetPointError(relBin, 0, hCalib_2015_June_0T_mag_relative->GetMeanError());
  bLabel[relBin] = "pp 0T, 2015 June, magnet";
  ++relBin;
  */
  

  // hCalib_2015_0T_relative->Draw();
  /*
  TH1D* hCalib_2015_Nov_0T_relative =  Relative(hCalib_2015_Nov_0T_muHV, ref);
  hCalib_2015_Nov_0T_relative->Draw();
  gRelative->SetPoint(relBin, relBin+1, hCalib_2015_Nov_0T_relative->GetMean());
  gRelative->SetPointError(relBin, 0, hCalib_2015_Nov_0T_relative->GetMeanError());
  bLabel[relBin] = "Nov 2015, 4T";
  ++relBin;
  */
  
  /*
  TH1D* hCalib_2015_Nov_4T_relative = Relative(hCalib_2015_Nov_4T, ref, refBad);
  hCalib_2015_Nov_4T_relative->Draw("same");
  gRelative->SetPoint(relBin, relBin+1, hCalib_2015_Nov_4T_relative->GetMean());
  gRelative->SetPointError(relBin, 0 , hCalib_2015_Nov_4T_relative->GetMeanError());
  bLabel[relBin] = "Nov 2015, 4T";
  ++relBin;
  */
  
  TH1D* hCalib_2015_HI_4T_relative = Relative(hCalib_2015_HI_4T_muHV, ref, refBad);
  hCalib_2015_HI_4T_relative->Draw("same");
  gRelative4->SetPoint(gRelative4->GetN(), relBin+1, hCalib_2015_HI_4T_relative->GetMean());
  gRelative4->SetPointError(gRelative4->GetN()-1, 0 , hCalib_2015_HI_4T_relative->GetMeanError());
  bLabel[relBin] = "2015 Nov, PbPb";
  ++relBin;
  
  //TH1D* hCalib_2015_Nov_ppHV_relative = Relative(hCalib_2015_Nov_ppHV_muHV, ref, refBad);
  TH1D* hCalib_2015_Nov_ppHV_relative = Relative(hCalib_2015_Nov_ppHV_HV, ref, refBad);
  hCalib_2015_Nov_ppHV_relative->Draw("same");
  gRelative->SetPoint(gRelative->GetN(), relBin+1, hCalib_2015_Nov_ppHV_relative->GetMean());
  gRelative->SetPointError(gRelative->GetN()-1, 0 , hCalib_2015_Nov_ppHV_relative->GetMeanError());
  bLabel[relBin] = "2015 Nov, PbPb";
  ++relBin;
  
  //TH1D* hCalib_2016B_4T_relative = Relative(hCalib_2016B_4T_muHV, ref, refBad);
  TH1D* hCalib_2016B_4T_relative = Relative(hCalib_2016B_4T_HV, ref, refBad);
  hCalib_2016B_4T_relative->Draw("same");
  gRelative->SetPoint(gRelative->GetN(), relBin+1, hCalib_2016B_4T_relative->GetMean());
  gRelative->SetPointError(gRelative->GetN()-1, 0 , hCalib_2016B_4T_relative->GetMeanError());
  bLabel[relBin] = "2016, pPb"; // Run2016B
  ++relBin;
  
  //TH1D* hCalib_2016C_4T_relative = Relative(hCalib_2016C_4T_muHV, ref, refBad);
  TH1D* hCalib_2016C_4T_relative = Relative(hCalib_2016C_4T_HV, ref, refBad);
  hCalib_2016C_4T_relative->Draw("same");
  gRelative->SetPoint(gRelative->GetN(), relBin+1, hCalib_2016C_4T_relative->GetMean());
  gRelative->SetPointError(gRelative->GetN()-1, 0 , hCalib_2016C_4T_relative->GetMeanError());
  bLabel[relBin] = "2016, Pbp"; // Run2016C
  ++relBin;
  
  cRel->cd(2);
  gPad->SetGridy();
  
  gStyle->SetOptStat(0);


  // -------------------------------------------------------------------------------- muon stability, relative
  
  TCanvas* canvasRelative = new TCanvas("realative", "relative", 1600*3/4, 1000*3/4);
  canvasRelative->SetBottomMargin(0.15);
  canvasRelative->SetTopMargin(0.1);
  canvasRelative->SetRightMargin(0.05);
  canvasRelative->SetLeftMargin(0.1);
  canvasRelative->SetGridy(1);
  //TH2D* relFrame = new TH2D("MuonStability", ";;Signal relative to 2013", relBin, 0.5, 0.5+relBin, 10, 0., 1.3);  
  TH2D* relFrame = new TH2D("MuonStability", ";;Signal relative to 2013", relBin, 0.5, 0.5+relBin, 10, 0.5, 2.1);  
  relFrame->GetXaxis()->SetLabelSize(0.05);
  //relFrame->GetXaxis()->SetTextAngle(80);
  relFrame->GetYaxis()->SetTitleSize(0.05);
  relFrame->GetYaxis()->SetTitleOffset(1);
  relFrame->GetXaxis()->SetLabelSize(0.055);
  relFrame->GetXaxis()->SetLabelOffset(0.01);
  for (int i=0; i<relBin; ++i) {
    relFrame->GetXaxis()->SetBinLabel(i+1, bLabel[i].c_str());
  }
  relFrame->Draw();
  
  //gRelative3->Draw("p");  // scaled up 2011 data
  gRelative2->Draw("p");
  gRelative->Draw("p");
  gRelative4->Draw("p");
  gRelative5->Draw("p");
  
  gPad->Update();
  gPad->Modified();
  
  TLine* lRun2 = new TLine(4.5, gPad->GetUymin(), 4.5, gPad->GetUymax());
  lRun2->SetLineWidth(3);
  lRun2->SetLineColor(kBlack);
  lRun2->SetLineStyle(7);
  lRun2->Draw();
  
  TLatex* lText = new TLatex();
  lText->SetTextFont(42);
  lText->SetTextSize(0.035);
  lText->SetTextColor(kBlack);
  lText->SetTextAlign(31);
  lText->DrawText(4.4, gPad->GetUymax()-(gPad->GetUymax()-gPad->GetUymin())*0.05, "LHC Run 1");
  lText->SetTextAlign(11);
  lText->DrawText(4.6, gPad->GetUymax()-(gPad->GetUymax()-gPad->GetUymin())*0.05, "LHC Run 2");
  
  TLegend* legRel= new TLegend(0.6,0.25,0.89,0.55);
  legRel->SetTextFont(42);
  legRel->SetHeader("CASTOR interfill data");
  legRel->AddEntry(ForLegend(markerStyleMuHV, 1.9, 3, kBlue), "Pre-upgrade PMTs", "f");
  legRel->AddEntry(ForLegend(markerStyleMuHV, 1.9, 3, kRed+1), "Post-upgrade PMTs", "f");
  legRel->AddEntry(ForLegend(markerStyleMuHV, 1.9, 3, kBlack), "Muon-HV menu (2015)", "p");
  legRel->AddEntry(ForLegend(markerStylePhyHV, 1.9, 3, kBlack), "Physics-HV menu", "p");
  legRel->AddEntry(ForLegend(markerStyleMuHV2011, 1.9, 3, kBlack), "Muon-HV menu (2011)", "p");
  legRel->AddEntry(ForLegend(markerStyleMuHV+4, 1.9, 3, kBlack), "CMS magnet B=0T", "p");
  legRel->Draw();
  
  TLatex* cms = new TLatex(0.1, 0.925, "CMS"); // , CASTOR interfill data
  cms->SetNDC();
  cms->SetTextFont(62);
  cms->SetTextSize(0.065);
  cms->Draw("same");
  
  canvasRelative->SaveAs("CastorCal_relative.pdf");
  
  
  
  // -------------------------------------------------
  
  TH2D* hIntercalReReco2015 = new TH2D("hIntercalReReco2015", "hIntercalReReco2015", 14, 0.5, 14.5, 16, 0.5, 16.5);
  ReadIntercal(hIntercalReReco2015, "gain_MelikeMuon_InterCalib_AbsCalib_AdjustToMeanFiveModInterCalibValues_20151105.txt");
  
  MinimumBiasResponse mb2015_data("plotsCastorSignals_Data.root", kBlack);  
  MinimumBiasResponse mb2015_epos("plotsCastorSignals_MC.root", "MinBias_EPOS_13TeV_MagnetOff_CASTORmeasured_newNoise", kBlue);
  MinimumBiasResponse mb2015_qgs("plotsCastorSignals_MC.root", "MinBias_CUETP8M1_13TeV-pythia8_MagnetOff_CASTORmeasured_newNoise", kRed);
  
  TH2D* castor2015_data = HRotate(mb2015_data.GetP2D("CastorSignals", "2015_data"));
  castor2015_data->SetMarkerStyle(20);
  TH2D* castor2015_epos = HRotate(mb2015_epos.GetP2D("CastorSignals", "2015_epos"));
  TH2D* castor2015_qgs = HRotate(mb2015_qgs.GetP2D("CastorSignals", "2015_qgsjet"));
  castor2015_epos->SetLineWidth(3);
  castor2015_qgs->SetLineWidth(3);
  castor2015_qgs->SetLineStyle(7);
  
  TH2D* castor2015_data_new = new TH2D("castor2015_data_new", "castor2015_data_new", 14, 0.5, 14.5, 16, 0.5, 16.5);
  castor2015_data_new->Sumw2();
  castor2015_data_new->SetMarkerStyle(20);
  
  TH2D* castor2015_data_input = new TH2D("castor2015_data_input", "castor2015_data_input", 14, 0.5, 14.5, 16, 0.5, 16.5);
  castor2015_data_input->Sumw2();
  castor2015_data_input->SetMarkerStyle(24);

  const bool showReReco = false;
  TH2D* castor2015_data_rereco = new TH2D("castor2015_data_rereco", "castor2015_data_rereco", 14, 0.5, 14.5, 16, 0.5, 16.5);
  castor2015_data_rereco->Sumw2();
  castor2015_data_rereco->SetMarkerStyle(23);
  
  /*
  TH2D* castor2015_data = new TH2D("castor2015_data_new", "castor2015_data", 14, 0.5, 14.5, 16, 0.5, 16.5);
  castor2015_data->SetMarkerStyle(25);
  TH2D* castor2015_qgs = new TH2D("castor2015_qgs", "castor2015_qgs", 14, 0.5, 14.5, 16, 0.5, 16.5);
  castor2015_qgs->SetLineColor(kBlue);
  castor2015_qgs->SetLineWidth(2);
  TH2D* castor2015_epos = new TH2D("castor2015_epos", "castor2015_epos", 14, 0.5, 14.5, 16, 0.5, 16.5);
  castor2015_epos->SetLineColor(kRed);
  castor2015_epos->SetLineWidth(2);
  
  for (int imod=0; imod<14; ++imod) {
    for (int isec=0; isec<16; ++isec) {
      if (!bad2015_0T.Is(isec+1, imod+1)) {
        castor2015_data->SetBinContent(imod+1, isec+1, castor2015_data_input->GetBinContent(isec+1, imod+1));
        castor2015_data_new->SetBinContent(imod+1, isec+1, castor2015_data_input->GetBinContent(isec+1, imod+1));
        castor2015_data_new->SetBinError(imod+1, isec+1, castor2015_data_input->GetBinContent(isec+1, imod+1)*0.16); 
        castor2015_qgs->SetBinContent(imod+1, isec+1, castor2015_qgs_input->GetBinContent(isec+1, imod+1));
        castor2015_epos->SetBinContent(imod+1, isec+1, castor2015_epos_input->GetBinContent(isec+1, imod+1));
      }
    }
  }
  */
  //TH2D* castor2015_data_input = (TProfile2D*) castor2015_data->Clone("castor2015_data_input");
  
  for (int imod=0; imod<14; ++imod) {
    for (int isec=0; isec<16; ++isec) {
      castor2015_data_input->SetBinContent(imod+1, isec+1, castor2015_data->GetBinContent(imod+1, isec+1));
      castor2015_data_input->SetBinError  (imod+1, isec+1, castor2015_data->GetBinError  (imod+1, isec+1));
      castor2015_data_new  ->SetBinContent(imod+1, isec+1, castor2015_data->GetBinContent(imod+1, isec+1));
      castor2015_data_new  ->SetBinError  (imod+1, isec+1, castor2015_data->GetBinError  (imod+1, isec+1));
      castor2015_data_rereco->SetBinContent(imod+1, isec+1, castor2015_data->GetBinContent(imod+1, isec+1));
      castor2015_data_rereco->SetBinError  (imod+1, isec+1, castor2015_data->GetBinError  (imod+1, isec+1));
    }
  }
  castor2015_data_input->Divide(hIntercalReReco2015);  
  castor2015_data_new->Divide(hIntercalReReco2015);
  castor2015_data_new->Multiply(hCalib_2015_0T_cal);
  
  bad2015_0T.Remove(castor2015_epos);
  bad2015_0T.Remove(castor2015_qgs);
  bad2015_0T.Remove(castor2015_data);
  bad2015_0T.Remove(castor2015_data_new);
  
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  
  TCanvas *cCas2015 = new TCanvas("cCas2015_validation","cCas2015_validation", 2000, 1300);
  cCas2015->Divide(4,2);
  cCas2015->cd(1);
  castor2015_data->Draw("colz");
  cCas2015->cd(2);
  castor2015_data_new->Draw("colz");
  cCas2015->cd(3);
  castor2015_qgs->Draw("colz");
  cCas2015->cd(4);
  castor2015_epos->Draw("colz");
  
  //cCas2015->cd(5);
  TCanvas* canvasPhi2015 = new TCanvas("canvasPhi2015");
  const bool norm = true;
  const TH1* proj2015_phi = ProjectPhi(castor2015_data_new, norm);
  const TH1* proj_2015_phi_qgs =  ProjectPhi(castor2015_qgs, norm);
  //ProjectPhi(castor2015_data, norm)->Draw("p");
  ProjectPhi(castor2015_data_input, norm)->Draw("p");
  if (showReReco) {
    ProjectPhi(castor2015_data_rereco, norm)->Draw("same,p");
  }
  ProjectPhi(castor2015_epos, norm)->Draw("same,hist");
  const_cast<TH1*>(proj_2015_phi_qgs)->Draw("same,hist");
  const_cast<TH1*>(proj2015_phi)->Draw("p,same");
  const double ErrEst = DrawErrorBand(proj2015_phi, 0.26, proj_2015_phi_qgs, true);
  
  const double xCMS = 0.2;
  const double yCMS = 0.87;
  {
    TText* cms = new TText(xCMS, yCMS, "CMS");
    cms->SetNDC(1);
    cms->SetTextSize(0.07);
    cms->Draw();
  }
  canvasPhi2015->SaveAs("CastorCal2015_phi.pdf");
  
  
  //cCas2015->cd(6);
  TCanvas* canvasDepth2015 = new TCanvas("canvasDepth2015");
  gPad->SetLogy(1);
  const TH1* proj2015_z = ProjectDepth(castor2015_data_new, norm);
  //ProjectDepth(castor2015_data, norm)->Draw("p");
  const TH1* proj2015_z_uncal = ProjectDepth(castor2015_data_input, norm);
  const_cast<TH1*>(proj2015_z_uncal)->Draw("p");
  if (showReReco) {
    ProjectDepth(castor2015_data_rereco, norm)->Draw("same,p");
  }
  ProjectDepth(castor2015_epos, norm)->Draw("same,hist");
  ProjectDepth(castor2015_qgs, norm)->Draw("same,hist");
  const_cast<TH1*>(proj2015_z)->Draw("p,same");
  DrawErrorBand(proj2015_z, ErrEst);

  TLegend* leg2015 = new TLegend(0.55, 0.62, 0.88, 0.93);
  leg2015->SetTextFont(42);
  leg2015->SetTextSize(0.042);
  leg2015->SetBorderSize(0);
  leg2015->AddEntry(proj2015_z, "Data, 2015", "lp");
  leg2015->AddEntry(proj2015_z_uncal, "(uncalibrated)", "lp");  
  leg2015->AddEntry(castor2015_qgs, "QGSJetII.4", "lp");
  leg2015->AddEntry(castor2015_epos, "EPOS-LHC", "lp");
  leg2015->Draw();

  {
    TText* cms = new TText(xCMS, yCMS, "CMS");
    cms->SetNDC(1);
    cms->SetTextSize(0.07);
    cms->Draw();
  }
  
  canvasDepth2015->SaveAs("CastorCal2015_depth.pdf");
  
  
  
  
  // -------------------------------------------------
  
  MinimumBiasResponse mb2013("../AnalyseCFF/MinBiasResponseOutput_data_PAMinBiasUPC_HIRun2013_PromptReco_v1.root", kBlack);
  MinimumBiasResponse mb2016("../AnalyseCFF/MinBiasResponseOutput_data_ZeroBias_PARun2016B_PromptReco_v1.root", kRed);
  //MinimumBiasResponse mb2016("../AnalyseCFF/MinBiasResponseOutput_data_ZeroBias_PARun2016B-PromptReco-v1.285244.root", kRed);
  MinimumBiasResponse pA_epos_nominal("../AnalyseCFF/MinBiasResponseOutput_data_ReggeGribovMCfix_EposLHC_5TeV_pPb.root", kBlue);
  //MinimumBiasResponse pA_epos("../AnalyseCFF/MinBiasResponseOutput_data_Merijn_pPb_EposLHC_5TeV_measured.root", kBlack);
  //MinimumBiasResponse pA_epos("../AnalyseCFF/MinBiasResponseOutput_ReggeGribovPartonMC_EposLHC_pPb_4080_4080_DataBS.root", kBlack);
  //MinimumBiasResponse pA_epos("../AnalyseCFF/MinBiasResponseOutput_ReggeGribovPartonMC_EposLHC_PbP_4080_4080_DataBS.root", kBlack);  

  
  //TH2D* hCasMB_epos = pA_epos.Get2D("castor_channel_2d");
  TH2D* hCasMB2013 = mb2013.Get2D("castor_channel_2d");
  TH2D* hCasMB2016 = mb2016.Get2D("castor_channel_2d");

  // hCasMB_epos->SetLineWidth(1);
  //  hCasMB_epos->SetLineColor(kBlue);
  
  //bad2013.Remove(hCasMB_epos);
  bad2013.Remove(hCasMB2013);
  bad2016.Remove(hCasMB2016);

  // conservative 
  //bad2016.Remove(hCasMB_epos);
  bad2016.Remove(hCasMB2013);
  bad2013.Remove(hCasMB2016);  

  TH2D* hCasMB_epos_nominal = pA_epos_nominal.Get2D("castor_channel_2d");
  hCasMB_epos_nominal->SetLineWidth(2);
  hCasMB_epos_nominal->SetLineStyle(1);
  hCasMB_epos_nominal->SetLineColor(kBlue-2);
  bad2013.Remove(hCasMB_epos_nominal);
  bad2016.Remove(hCasMB_epos_nominal);

  /*TH1D* hEvnts_epos = pA_epos.Get1D("EventCounter");
  double count_epos = 0;
  for (int iBin=1; iBin<=hEvnts_epos->GetNbinsX(); ++iBin) {
    if (string(hEvnts_epos->GetXaxis()->GetBinLabel(iBin)) == "Vertex")
      count_epos = hEvnts_epos->GetBinContent(iBin);
  }
  cout << "count_epos " << count_epos << endl;
  */
  
  TH1D* hEvnts_2013 = mb2013.Get1D("EventCounter");
  double count_2013 = 0;
  for (int iBin=1; iBin<=hEvnts_2013->GetNbinsX(); ++iBin) {
    if (string(hEvnts_2013->GetXaxis()->GetBinLabel(iBin)) == "Vertex")
      count_2013 = hEvnts_2013->GetBinContent(iBin);
  }
  cout << "count_2013 " << count_2013 << endl;

  TH1D* hEvnts_2016 = mb2016.Get1D("EventCounter");
  double count_2016 = 0;
  for (int iBin=1; iBin<=hEvnts_2016->GetNbinsX(); ++iBin) {
    if (string(hEvnts_2016->GetXaxis()->GetBinLabel(iBin)) == "Vertex")
      count_2016 = hEvnts_2016->GetBinContent(iBin);
  }
  cout << "count_2016 " << count_2016 << endl;

  
  TCanvas *cHF = new TCanvas("cCas2013_validation","cCas2013/16_validation", 1500, 1500);
  cHF->Divide(3,4);
  
  cHF->cd(1);
  gPad->SetLogy(1);
  TH1D* hf_2013 = mb2013.Get1D("hfm_pf_tower");
  TH1D* hf_2016 = mb2016.Get1D("hfm_pf_tower");
  //TH1D* hf_epos = pA_epos.Get1D("hfm_pf_tower");
  hf_2013->Scale(1./count_2013);
  hf_2016->Scale(1./count_2016);
  //hf_epos->Scale(1./count_epos);
  hf_2013->Draw();
  hf_2016->Draw("same");
  //hf_epos->Draw("same");
  
  cHF->cd(2);
  gPad->SetLogy(1);
  TH1D* hfp_2013 = mb2013.Get1D("hfp_pf_tower");
  TH1D* hfp_2016 = mb2016.Get1D("hfp_pf_tower");
  //TH1D* hfp_epos = pA_epos.Get1D("hfp_pf_tower");
  hfp_2013->Scale(1./count_2013);
  hfp_2016->Scale(1./count_2016);
  //hfp_epos->Scale(1./count_epos);
  hfp_2013->Draw();
  hfp_2016->Draw("same");
  //hfp_epos->Draw("same");
  
  cHF->cd(3);
  gPad->SetLogz(1);
  //hCasMB_epos->Draw("colz");
  if (hCasMB_epos_nominal) {
    hCasMB_epos_nominal->Draw("colz");
  }
  
  cHF->cd(4);
  //gPad->SetLogy(1);
  hCasMB2013->Draw("colz");
  
  cHF->cd(5);
  //gPad->SetLogy(1);
  hCasMB2016->Draw("colz");
  
  cHF->cd(6);
  {
    TH1D* hCasRatio = new TH1D("hCasRatio", "hCasRatio", 100,0,2);
    hCasRatio->Sumw2();
    for (int isec=0; isec<16; ++isec) {
      for (int imod=0; imod<16; ++imod) {
        const double a = hCasMB2016->GetBinContent(imod+1,isec+1);
        const double b = hCasMB2013->GetBinContent(imod+1,isec+1);
        if (a!=0 and b!=0) {
          hCasRatio->Fill(a/b);
        }
      }
    }
    hCasRatio->Draw();
  }
  
  hCasMB2013->Scale(1./count_2013);
  hCasMB2016->Scale(1./count_2016);
  //hCasMB_epos->Scale(1./count_epos);
  if (hCasMB_epos_nominal) {
    //hCasMB_epos_nominal->Scale(1./count_epos);
  }
  
  cHF->cd(7);
  //gPad->SetLogy(1);
  TH2D* hCasMB2013_mucal = (TH2D*)hCasMB2013->Clone();
  hCasMB2013_mucal->Divide(hCalib_2013_4T);
  hCasMB2013_mucal->Draw("colz");
  
  cHF->cd(8);
  //gPad->SetLogy(1);
  TH2D* hCasMB2016_mucal = (TH2D*)hCasMB2016->Clone();
  hCasMB2016_mucal->Divide(hCalib_2016C_4T);
  hCasMB2016_mucal->Draw("colz");

  cHF->cd(9);
  {
    TH1D* hCasRatio2 = new TH1D("hCasRatio2", "hCasRatio2", 100,0,2);
    hCasRatio2->Sumw2();
    for (int isec=0; isec<16; ++isec) {
      for (int imod=0; imod<16; ++imod) {
        const double a = hCasMB2016_mucal->GetBinContent(imod+1,isec+1);
        const double b = hCasMB2013_mucal->GetBinContent(imod+1,isec+1);
        if (a!=0 and b!=0) {
          hCasRatio2->Fill(a/b);
        }
      }
    }
    hCasRatio2->Draw();
  }  


  //cHF->cd(10);
  TCanvas* canvasPhi2013 = new TCanvas("canvasPhi2013");

  const bool norm2013 = true;
  hCasMB2013_mucal->SetMarkerStyle(20);
  hCasMB2016_mucal->SetMarkerStyle(21);
  hCasMB2016_mucal->SetMarkerSize(0.9);
  hCasMB2016_mucal->SetMarkerColor(kGreen+1);
  hCasMB2016_mucal->SetLineColor(kGreen+1);
  hCasMB2016_mucal->SetLineWidth(2);
  hCasMB2016_mucal->SetLineStyle(1);
  hCasMB2013->SetMarkerStyle(24);
  hCasMB2016->SetMarkerColor(kBlue);
  hCasMB2016->SetLineColor(kBlue);
  hCasMB2016->SetMarkerColor(kGreen+1);
  hCasMB2016->SetLineColor(kGreen+1);
  hCasMB2016->SetMarkerStyle(25);
  hCasMB2016->SetMarkerSize(0.9);
  TH1D* hCasMB2013_phi = ProjectPhi(hCasMB2013_mucal, norm2013);
  TH1D* hCasMB2016_phi = ProjectPhi(hCasMB2016_mucal, norm2013);
  //TH1D* hCasMBepos_phi = ProjectPhi(hCasMB_epos, norm2013);  
  TH1D* hCasMBepos_phi = ProjectPhi(hCasMB_epos_nominal, norm2013);  
  hCasMBepos_phi->Draw("hist");
  if (hCasMB_epos_nominal) {
    ProjectPhi(hCasMB_epos_nominal, norm2013)->Draw("hist,same");
  }
  ProjectPhi(hCasMB2013, norm2013)->Draw("p,same");
  ProjectPhi(hCasMB2016, norm2013)->Draw("p,same");
  const_cast<TH1D*>(hCasMB2016_phi)->Draw("p,same");
  const_cast<TH1D*>(hCasMB2013_phi)->Draw("p,same");
  double ErrEst2013 = 0.26, ErrEst2016 = 0.26;
  ErrEst2016 = DrawErrorBand(hCasMB2016_phi, ErrEst2016, hCasMBepos_phi, true);
  ErrEst2013 = DrawErrorBand(hCasMB2013_phi, ErrEst2013, hCasMBepos_phi, true);

  {
    TText* cms = new TText(xCMS, yCMS, "CMS");
    cms->SetNDC(1);
    cms->SetTextSize(0.07);
    cms->Draw();
  }

  canvasPhi2013->SaveAs("CastorCal2013_phi.pdf");

  
  //cHF->cd(11);
  TCanvas* canvasDepth2013 = new TCanvas("canvasDepth2013");    
  gPad->SetLogy(1);
  cout << "-> 2013 data: " << endl;
  TH1D* hCasMB2013_z = ProjectDepth(hCasMB2013_mucal, norm2013);
  cout << "-> 2016 data: " << endl;
  TH1D* hCasMB2016_z = ProjectDepth(hCasMB2016_mucal, norm2013);
  //ProjectDepth(hCasMB_epos, norm2013)->Draw("hist");
  ProjectDepth(hCasMB_epos_nominal, norm2013)->Draw("hist");
  TH1* hCasMB2013_z_uncal = ProjectDepth(hCasMB2013, norm2013);
  const_cast<TH1*>(hCasMB2013_z_uncal)->Draw("p,same");
  TH1* hCasMB2016_z_uncal = ProjectDepth(hCasMB2016, norm2013);
  const_cast<TH1*>(hCasMB2016_z_uncal)->Draw("p,same");
  const_cast<TH1D*>(hCasMB2016_z)->Draw("p,same");
  const_cast<TH1D*>(hCasMB2013_z)->Draw("p,same");
  cout << "-> epos MC (2016) " << endl;
  DrawErrorBand(hCasMB2016_z, ErrEst2016);
  cout << "-> epos MC (2013) " << endl;
  DrawErrorBand(hCasMB2013_z, ErrEst2013);
  cout << ". " << endl;
  
  TLegend* leg2013 = new TLegend(0.55, 0.62, 0.88, 0.93);
  leg2013->SetTextFont(42);
  leg2013->SetTextSize(0.042);
  leg2013->SetBorderSize(0);
  leg2013->AddEntry(hCasMB2013_z, "Data, 2013", "lp");
  leg2013->AddEntry(hCasMB2013_z_uncal, "(uncalibrated)", "lp");  
  leg2013->AddEntry(hCasMB2016_z, "Data, 2016", "lp");
  leg2013->AddEntry(hCasMB2016_z_uncal, "(uncalibrated)", "lp");  
  //leg2013->AddEntry(hCasMB_epos, "EPOS-LHC (measured)", "l");
  leg2013->AddEntry(hCasMB_epos_nominal, "EPOS-LHC (nominal)", "l");
  leg2013->Draw();

  {
    TText* cms = new TText(xCMS, yCMS, "CMS");
    cms->SetNDC(1);
    cms->SetTextSize(0.07);
    cms->Draw();
  }
  canvasDepth2013->SaveAs("CastorCal2013_depth.pdf");

  
  // --------------------------------------------------
  /*
    TCanvas* cMag = new TCanvas("cMag", "cMag");   
    TH2D* magEffect = (TH2D*) hCalib_2015_HI_4T->Clone();
    //TH2D* magEffect = (TH2D*) hCalib_2015_Nov_4T->Clone();
    //hCalib_2015_HI_4T
    magEffect->Divide(hCalib_2015_0T);
    //magEffect->Multiply(hHV);
    magEffect->SetMinimum(0);
    magEffect->SetMaximum(2);
    magEffect->Draw("colz");
  */  
}





int
main(int argc, char** argv)
{
  gStyle->SetOptStat(0);

  setTDRStyle();
  gStyle->SetPalette(1);


  /*
  if (argc<2) {
    cout << "Please use: " << argv[0] << " input-file " << endl;
    return 1;
  }
  TFile* muons = TFile::Open(argv[1]);
  */
  TApplication app("runner", 0, 0);    
  calibrate();
  app.Run();
  
  return 0;
}
