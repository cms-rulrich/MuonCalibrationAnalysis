#include <fitNoiseAna.h>

#include <TProfile2D.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TH1D.h>
#include <TFile.h>
#include <TTree.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TH2D.h>
#include <TApplication.h>
#include <TDirectory.h>
#include <TKey.h>
#include <TList.h>
#include <TGaxis.h>

#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include <TGraph.h>
#include <TGraphErrors.h>

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cmath>

using namespace std;





 
NoiseFitHelper::NoiseFitHelper(TFile* input, const bool drawChannels)
{
  fInput = input;//TFile::Open(name.c_str());
  fDrawChannels = drawChannels;
  string dirname;
  {
    TIter next(fInput->GetListOfKeys());
    TKey* obj = 0;
    while (obj = (TKey*) next()) {
      if (string(obj->GetClassName()).find("TDirectory") == 0)
	dirname = obj->GetName();
    }
  }
  
  // get bad channels
  TH2D* hBadCh = (TH2D*) fInput->Get((dirname+"/ch_bad_2d").c_str());
  
  // get muon response
  TProfile2D* hMuonSignals = (TProfile2D*) fInput->Get((dirname+"/ch_mu_signals_2d").c_str());
  
  // get background response
  TProfile2D* hNoiseSignals = (TProfile2D*) fInput->Get((dirname+"/ch_bkg_signals_2d").c_str());
  
  if (!hBadCh || !hMuonSignals || !hNoiseSignals) {
    cout << "error reading data" << endl;
    exit (3);
  }
  
  
  ROOT::Math::Minimizer* min = 
    ROOT::Math::Factory::CreateMinimizer("Minuit2", "Migrad");
  /*
  //  Minuit2                     Fumili2
  //  Fumili
  //  GSLMultiMin                ConjugateFR, ConjugatePR, BFGS, 
  //                              BFGS2, SteepestDescent
  //  GSLMultiFit
  //   GSLSimAn
  //   Genetic
  */
  
  // set tolerance , etc...
  min->SetMaxFunctionCalls(1000000); // for Minuit/Minuit2 
  min->SetMaxIterations(10000);  // for GSL 
  min->SetTolerance(0.001);
  min->SetPrintLevel(1);
  
  hFitFail = new TH2D("hFitFail", "hFitFail", 14, 0.5, 14.5, 16, 0.5, 16.5);
  
  h2dChi2 = new TH2D("h2dChi2", "h2dChi2", 14, 0.5, 14.5, 16, 0.5, 16.5);
  h2dNorm = new TH2D("h2dNorm", "h2dNorm", 14, 0.5, 14.5, 16, 0.5, 16.5);
  h2dRange = new TH2D("h2dRange", "h2dRange", 14, 0.5, 14.5, 16, 0.5, 16.5);
  h2Shift = new TH2D("h2Shift", "h2Shift", 14, 0.5, 14.5, 16, 0.5, 16.5);
  h2Zprob = new TH2D("h2Zprob", "h2Zprob", 14, 0.5, 14.5, 16, 0.5, 16.5);
  
  
  for (int iCh=0; iCh<224; ++iCh) {
    // for (int iCh=0; iCh<20; ++iCh) {
    
    const int iSec = iCh/14 + 1;
    const int iMod = iCh%14 + 1;
    
    bool drawChannel = false;
    
    /*
      if (iSec==9 && iMod==4) { // reference channel
      drawChannel = true;
      }
    */
    
    ostringstream name_noise;
    //name_noise << dirname_noise << "/ChannelNoise_sec_" << iSec << "_mod_" << iMod;
    name_noise << dirname << "/NoiseSignal_sec_" << iSec << "_mod_" << iMod;
    TH1D* hNoise = (TH1D*) fInput->Get(name_noise.str().c_str());
    cout << "reading " << name_noise.str().c_str() << " " << hNoise << endl;
    hNoise->SetFillColor(kBlue);
    //hNoise->SetFillStyle(3050);
    hNoise->SetLineStyle(0);
    hNoise->SetLineColor(kWhite);
    
    ostringstream name_muon;
    //name_muon << dirname << "/MuonSignalSecCh_Excl8_mod_" << iMod << "_sec_" << iSec;
    name_muon << dirname << "/MuonSignal_sec_" << iSec << "_mod_" << iMod;
    TH1D* hMuon = (TH1D*) fInput->Get(name_muon.str().c_str());
    /*if (!hMuon) { // really old stuff
      name_muon.str("");
      name_muon << dirname << "/MuonSignalSecCh_mod_" << iMod << "_sec_" << iSec;
      hMuon = (TH1D*) fInput->Get(name_muon.str().c_str());
      }*/
    cout << "reading " << name_muon.str().c_str() << " " << hMuon << endl;
    hMuon->SetXTitle("Signal / fC");
    hMuon->SetYTitle("Entries");
    hMuon->SetMarkerStyle(20);
    hMuon->SetMarkerSize(1);
    
    hNoise->Scale(hMuon->GetMaximum()/hNoise->GetMaximum());
    
    ostringstream can_name;
    can_name << "Channel: sec=" << iSec << " mod=" << iMod;
    
    // do the fit
    min->SetVariable(0, "Norm", 1, 0.1);
    min->SetVariable(1, "Shift", 0, 1);
    min->SetVariable(2, "Range", 100, 0.1);
    
    MuonNoiseFit minData(hNoise, hMuon);
    ROOT::Math::Functor func(minData, 3);
    min->SetFunction(func);
    min->FixVariable(1);
    min->FixVariable(2);
    cout << "RU prefit" << endl;
    min->Minimize(); 
    
    {
      const double* pars = min->X();
      const int npars = min->NDim();      
      std::cout << "Minimum 1: f(";
      for (int ipar=0; ipar<npars; ++ipar) cout << (ipar==0?"":",") <<  pars[ipar];
      cout << "): " << min->MinValue()  << std::endl;
    }
    
    // scan noise range
    vector<vector<double> > vpars;
    vector<double> vchi2red;
    // vector<double> vKS;
    vector<double> vrange;
    double minchi2 = 0;
    int mini = 0;
    bool first = true;
    for (int iextra=0; iextra<40; ++iextra) {
      cout << " ===================== iextra= " << iextra << endl;
      const double rangeLimit = hNoise->GetXaxis()->GetBinUpEdge(hNoise->GetMaximumBin()-2+iextra);
      //min->ReleaseVariable(1);
      //min->SetVariable(0, "Norm", 1, 0.1);
      //min->SetVariable(1, "Shift", 1, 1);
      min->SetVariable(2, "Range", rangeLimit, 0);
      //min->FixVariable(0);
      //min->FixVariable(1);
      min->FixVariable(2);
      cout << "RU final fit" << endl;
      min->Minimize();
      
      if (min->Status() != 0 ) {
        cout << "RU fit failed status=" << min->Status() << endl;
	continue;
      }
      
      const double* pars = min->X();
      const int npars = min->NDim();
      const int nfree = min->NFree();
      const int nbins = minData.getN(pars);
      
      vector<double> vecPars;
      for (int ipar=0; ipar<npars; ++ipar)
        vecPars.push_back(pars[ipar]);
      
      const double chi2red = minData(pars)/(nbins-nfree);
      
      vpars.push_back(vecPars);
      vchi2red.push_back(chi2red);
      vrange.push_back(rangeLimit);
      if (first || (minchi2 > chi2red)) {
	first = false;
	mini = vchi2red.size()-1;
	minchi2 = chi2red;
      }
    }
    
    vector<double> vecPars;
    string failMsg;
    
    if (first) { // FAILURE
      
      hFitFail->Fill(iMod, iSec);
      drawChannel = true;
      failMsg += "fail";
      
    } else {
      // pick best one, smalles chi2/ndf
      
      cout << " mini " << mini << " " << vpars.size() << endl;
      for (; mini<vpars.size()-1; ++mini) {
        if (vchi2red[mini]>1)
          break;	
      }
      cout << "mini " << mini << "  " << vchi2red[mini] << endl;
      //std::cout << "Minimum 2: f(";
      //for (int ipar=0; ipar<npars; ++ipar) cout << (ipar==0?"":",") <<  pars[ipar];
      //cout << "): " << min->MinValue()  << std::endl;
      vecPars = vpars[mini];
      // vecPars[2] = 100;
      
      // zero prop
      const double pZero = hNoise->Integral() * vecPars[0] / hMuon->Integral();
      cout << "pZero=" << pZero << endl;
      
      if (pZero>1) {
        drawChannel = true;
        if (!failMsg.empty()) failMsg += ", ";
        failMsg += "pZero";
      }
      if (vchi2red[mini] >10) {
        drawChannel = true;
        if (!failMsg.empty()) failMsg += ", ";
        failMsg += "chi2";
      }
      if (abs(vecPars[1])>30) { // shift
        drawChannel = true;
        if (!failMsg.empty()) failMsg += ", ";
        failMsg += "shift";
      }
      if (vecPars[0]>3 && vecPars[0]<0.1) { // norm
        drawChannel = true;
        if (!failMsg.empty()) failMsg += ", ";
        failMsg += "norm";
      }
      
      if (hBadCh->GetBinContent(iMod, iSec)==0) {
        
        h2dChi2->Fill(iMod, iSec, vchi2red[mini]);
        h2dNorm->Fill(iMod, iSec, vecPars[0]);
        h2dRange->Fill(iMod, iSec, vecPars[2]);
        h2Shift->Fill(iMod, iSec, vecPars[1]);
        h2Zprob->Fill(iMod, iSec, -log(pZero));      
        
        //h2rms->Fill(iMod, iSec, hMuon->GetRMS()*hMuon->GetRMS() - hNoise->GetRMS()*hNoise->GetRMS());
        //h2mean->Fill(iMod, iSec, hMuon->GetMean() - hNoise->GetMean()*vecPars[0]*hNoise->Integral()/hMuon->Integral()  );
        /*
        const double var = pow(hMuonSignals->GetBinError(iMod, iSec),2) - pow(hNoiseSignals->GetBinError(iMod, iSec),2);
        if (var>=0) {
          h2rms->Fill(iMod, iSec, sqrt(var));
        }
        h2mean->Fill(iMod, iSec, hMuonSignals->GetBinContent(iMod, iSec) - hNoiseSignals->GetBinContent(iMod, iSec));
        */
      }
    }
    
    
    
    if (fDrawChannels && drawChannel) {
      TCanvas* can = 0;
      can = new TCanvas((can_name.str()).c_str(), (can_name.str()+": "+failMsg).c_str(), 500, 1000);
      can->Divide(1,2);
      can->cd(1);
      gPad->SetLogy(1);
      
      hMuon->Draw("p");
      hNoise->Draw("same, hist, fill");
      hMuon->Draw("same");
      
      if (!first) {
        TGraph* fitR = minData.getNoise(&vecPars.front());
        fitR->SetLineWidth(3);
        fitR->SetLineColor(kRed);
        fitR->SetMarkerStyle(21);
        fitR->SetMarkerColor(kRed);
        fitR->Draw("lp");
      }
     
      can->cd(2);
      TGraph* gchi2red = new TGraph(vrange.size(), &vrange.front(), &vchi2red.front());
      gchi2red->SetTitle("Reduced chi2;Range_max / fC; #Chi^{2}/Ndf");
      gchi2red->SetMarkerStyle(25);
      gchi2red->SetMarkerColor(kRed);
      gchi2red->SetLineColor(kRed);
      gchi2red->Draw("alp");
      /*
        for (int ig=0; ig<vpars.size(); ++ig) {
	TGraph* gp = new TGraph(vrange.size(), &vrange.front(), &vpars[ig].front());
	gp->SetMarkerStyle(27);
	gp->SetMarkerColor(kBlue);
	gp->SetLineColor(kBlue);
	gp->Draw("lp");
        }
      */
      
    } else {
      
      delete hMuon;
      delete hNoise;
    }
    
    
  } // end loop channels
  
}


