#include <TCanvas.h>
#include <TLegend.h>
#include <TH1D.h>
#include <TFile.h>
#include <TTree.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TH2D.h>
#include <TApplication.h>
#include <TDirectory.h>
#include <TKey.h>
#include <TList.h>
#include <TProfile.h>
#include <TLine.h>
#include <TLatex.h>
#include <TText.h>

#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include <TGraph.h>

#include <iostream>
#include <string>
#include <sstream>
#include <iostream>
#include <map>
#include <utility>

using namespace std;


 

void
plotEvents(TFile* muons, TFile* noise, int nSigma, bool plot) 
{ 
  gStyle->SetOptStat(0);
  gROOT->ProcessLine("#include <vector>");
  
  // first find dataset names
  string dirname_muons;
  {
    TIter next(muons->GetListOfKeys());
    TKey* obj = 0;
    while (obj = (TKey*) next()) {
      if (string(obj->GetClassName()).find("TDirectory") == 0) {
	dirname_muons = obj->GetName();
        cout << "found dir: " << dirname_muons << endl;
      }
    }
  }
  
  string dirname_noise;
  {
    TIter next(noise->GetListOfKeys());
    TKey* obj = 0;
    while (obj = (TKey*) next()) {
      if (string(obj->GetClassName()).find("TDirectory") == 0) {
	dirname_noise = obj->GetName();
        cout << "found dir: " << dirname_noise << endl;
      }
    }
  }

  // read noise mean/RMS
  ostringstream rms_name, mean_name, rms2d_name, mean2d_name;
  rms_name << dirname_noise << "/hist_ch_RMS";
  mean_name << dirname_noise << "/hist_ch_Mean";
  rms2d_name << dirname_noise << "/ch_input_rms_2d";
  mean2d_name << dirname_noise << "/ch_input_mean_2d";
  TProfile* hist_ch_RMS = (TProfile*) noise->Get(rms_name.str().c_str());
  TProfile* hist_ch_mean = (TProfile*) noise->Get(mean_name.str().c_str());
  TH2D* ch_input_rms_2d = (TH2D*) noise->Get(rms2d_name.str().c_str());
  TH2D* ch_input_mean_2d = (TH2D*) noise->Get(mean2d_name.str().c_str());
  if (!hist_ch_RMS && !ch_input_rms_2d) {
    cout << " cannot read hist_ch_RMS nor ch_input_rms_2d" << endl;
    exit(1);
  }
  if (!hist_ch_mean && ! ch_input_mean_2d) {
    cout << " cannot read hist_ch_mean nor ch_input_mean_2d" << endl;
    exit(1);
  }
  // bad channels: hist_ch_good
  
  
  ostringstream tname;
  //tname << dirname_muons << "/muons";
  tname << dirname_muons << "/data";
  TTree* data = (TTree*) muons->Get(tname.str().c_str());
  if (!data) {
    cout << "no event data found " << endl;
    return;
  }
  
  int trgl1L1GTAlgo[328];
  // int trgCastorHaloMuon; // old tree format
  // double CastorRecHitEnergy[224]; // old tree format
  vector<float>* CastorRecHitEnergy = 0;
  
  //data->SetBranchAddress("trgl1L1GTAlgo", trgl1L1GTAlgo);
  //data->SetBranchAddress("trgCastorHaloMuon", &trgCastorHaloMuon);
  data->SetBranchAddress("CastorRecHitEnergy", &CastorRecHitEnergy);
  
  TText *text = 0;
  TCanvas* can = 0;
  TCanvas* canOne = 0;

  const int nPlots = 8;
  
  if (plot) {
    can = new TCanvas("events", "events", 1200, 1200);
    can->Divide(nPlots,nPlots,0.001,0.001);
    
    text = new TText();
    text->SetTextSize(0.14);
    text->SetTextFont(42);
    text->SetNDC();  
  }
  
  TFile* fileOut = TFile::Open("re_analysis.root", "recreate");
  TDirectory* dirOut = fileOut->mkdir(dirname_muons.c_str());
  dirOut->cd();
  //fileOut->ChangeDirectory(dirname_muons.c_str());
  
  TH2D* hAvg = new TH2D("avera", "muon data", 14, 0.5, 14.5, 16, 0.5, 16.5);
  
  TH2D* hcorrMax = new TH2D("hcorrMax", "hcorrMax", 300, -5, 200, 100, -5, 200);
  hcorrMax->SetXTitle("Average");
  hcorrMax->SetYTitle("Max");
  
  TH2D* hcorr = new TH2D("hcorr", "hcorr", 300, -5, 200, 100, -5, 200);
  hcorr->SetXTitle("Average");
  hcorr->SetYTitle("Max");
  
  TH1I* countExtra = new TH1I("countExtra", "countExtra", 20, 0, 20);
  TH1I* countExtraCluster = new TH1I("countExtraCluster", "countExtra", 20, 0, 20);
  
  TH2I* hChannelNZS = new TH2I("channelNZS", "channelNZS", 14, 0.5, 14.5, 16, 0.5, 16.5);
  
  TH1I* muonPerSector = new TH1I("muonPerSector", "muonPerSector", 16, 0.5, 16.5);
  muonPerSector->SetLineColor(kBlue);
  TH1I* muonPerSector0 = new TH1I("muonPerSector0", "muonPerSector", 16, 0.5, 16.5);
  muonPerSector0->SetLineColor(kRed);
  
  vector<TH1D*> chSpectrum(224, 0);
  for (int iCh=0; iCh<224; ++iCh) {
    const int iSec = iCh/14 + 1;
    const int iMod = iCh%14 + 1;
    ostringstream hist_name;
    hist_name << "MuonSignal_sec_" << iSec << "_mod_" << iMod;
    //hist_name << "MuonSignalSecCh_Excl8_mod_" << iMod << "_sec_" << iSec;
    TH1D* hist = new TH1D(hist_name.str().c_str(), "", 420, -50, 1000);
    chSpectrum[iCh] = hist;
  }
    
  const int bitMuon = 261;
  const int bitBPTXplus = 10;

  vector<double> vec_elec_rms(224,0);

  cout << "nMuons=" << data->GetEntries() << endl;
  
  int iStart = 0;
  do {
    
    int iEvRead = 0;
    int iEvPlot = 0;
    
    if (iEvRead+iStart>=data->GetEntries())
      break;
    
    while (iEvRead+iStart<data->GetEntries() && iEvPlot<nPlots*nPlots) {
      
      data->GetEntry(iEvRead+iStart);
      ++iEvRead;

      /*
      cout << "read " << iEvRead << " " << CastorRecHitEnergy->size() << endl;
      for (int p=0; p<224; ++p) {cout << " " << (*CastorRecHitEnergy)[p]; }
      cout << endl;*/
      
      if ((iEvRead+iStart)%1000==0) {
	cout << "Read " << iEvRead+iStart << " from " << data->GetEntries() << endl;
      }
      
      /*
      if (!trgl1L1GTAlgo[bitMuon])
	continue; // skip
        
      if (!trgl1L1GTAlgo[bitBPTXplus])
	continue; // skip
      */
      
      TH2D* hMuon = 0;
      if (plot) {	
	ostringstream hname;
	hname << "event_" << iEvPlot;
        
	hMuon = new TH2D(hname.str().c_str(), "", 14, 0.5, 14.5, 16, 0.5, 16.5);
	hMuon->SetXTitle("Module");
	hMuon->SetYTitle("Sector");
      }
      
      vector<double> avgSec(16,0), maxSec(16,0);
      int iMaxSec = -1;
      double sMaxSec = 0;
      
      vector<pair<int,int>> channelNZS;
      vector<int> sectorNZS(16, 0);
      int maxNZS = 0;
      int iMaxNZS = 0;

      int countCluster = 0;

      for (int iCh=0; iCh<224; ++iCh) {
	
	const int iSec = iCh/14 + 1;
	const int iMod = iCh%14 + 1;

	const double Ecas = (*CastorRecHitEnergy)[iCh];

        double elec_rms = 0;
        double threshold = 0;
        if (hist_ch_RMS) {
          elec_rms = hist_ch_RMS->GetBinContent(iCh+1);
          threshold = hist_ch_mean->GetBinContent(iCh+1) + nSigma*elec_rms;
        } else {
          elec_rms = ch_input_rms_2d->GetBinContent(iMod, iSec);
          threshold = ch_input_mean_2d->GetBinContent(iMod, iSec) + nSigma*elec_rms;
        }
        vec_elec_rms[iCh] = elec_rms;

	if (Ecas > threshold) {
	  hChannelNZS->Fill(iMod, iSec);
	  if (hMuon)
	    hMuon->Fill(iMod, iSec, Ecas);
          
	  /*
	  if (iCh+1==140 || iCh+1==65 || iCh+1==68) {
	    continue; 
	  }
	  */
	  
	  channelNZS.push_back(std::make_pair(iSec, iMod));	
	  sectorNZS[iSec-1]++;
	  if (sectorNZS[iSec-1] > maxNZS) {
	    maxNZS = sectorNZS[iSec-1];
	    iMaxNZS = iSec-1;
	  }	  
	} else {
	  if (hMuon)
	    hMuon->Fill(iMod, iSec, 0);
	}
	hAvg->Fill(iMod, iSec, (*CastorRecHitEnergy)[iCh]);
        
	for (vector<pair<int,int>>::iterator iNZS = channelNZS.begin(); iNZS!=channelNZS.end(); ++iNZS) {
	  if (iNZS->second == iMaxNZS+1)
	    continue;
	  for (int dM=-2; dM<=2; ++dM) {
	    for (int dS=-2; dS<=2; ++dS) {
	      int moduleTest = iNZS->first + dM;
	      int sectorTest = iNZS->second + dS;
	      if (moduleTest<1 || moduleTest>14)
		continue;
	      if (sectorTest<1) sectorTest += 16;
	      if (sectorTest>16) sectorTest -= 16;
	      if (sectorTest == iMaxNZS+1)
		continue;	      
	      for (vector<pair<int,int>>::iterator iNZStest = channelNZS.begin(); iNZStest!=channelNZS.end(); ++iNZStest) {
		if (iNZStest->first == moduleTest && iNZStest->second == sectorTest) {
		  ++countCluster;
		}
	      }
	    }
	  }
	}
	
	avgSec[iSec-1] += (*CastorRecHitEnergy)[iCh];
	maxSec[iSec-1] = max(maxSec[iSec-1], (double)(*CastorRecHitEnergy)[iCh]);
        
	if (avgSec[iSec-1] > sMaxSec) {
	  sMaxSec = avgSec[iSec-1];
	  iMaxSec = iSec-1;
	}
	
	hcorrMax->Fill(avgSec[iMaxSec]/14, maxSec[iMaxSec]);
	for (int is=0;is<16;++is) {
	  if (is != iMaxSec)
	    hcorr->Fill(avgSec[is]/14, maxSec[is]);
	}
        
      } // end loop channels
      
      // count non-muon NZS
      int noiseNZS = 0;
      for (int iS=0; iS<16; ++iS) {
	if (iS != iMaxNZS)
	  noiseNZS += sectorNZS[iS];
      }
      
      muonPerSector0->Fill(iMaxNZS+1);
      countExtra->Fill(noiseNZS);
      countExtraCluster->Fill(countCluster);
      
      if (noiseNZS>5) {
	if (hMuon) {
	  delete hMuon;
	  hMuon = 0;
	}
	continue; // skip event
      }
      
      muonPerSector->Fill(iMaxNZS+1);

      for (int iMod=0; iMod<14; ++iMod) {
	const int iCh = iMaxNZS*14 + iMod;
	chSpectrum[iCh]->Fill((*CastorRecHitEnergy)[iCh]);
      }
      
      if (plot) {
	hMuon->SetMaximum(60);
	hMuon->SetMinimum(10);
        
        bool plotOne = iEvPlot==2;
        if (plotOne) {
          if (canOne)
            plotOne = false;
          else {
            canOne = new TCanvas("event", "event", 680, 600);
            canOne->SetLogz(1);
            canOne->SetFillColor(kWhite);
            gPad->SetTopMargin(0.1);
            gPad->SetBottomMargin(0.12);
            gPad->SetLeftMargin(0.15);
            gPad->SetRightMargin(0.18);
            
            canOne->cd();
            hMuon->GetXaxis()->SetLabelSize(0.05);
            hMuon->GetYaxis()->SetLabelSize(0.05);
            hMuon->GetXaxis()->SetTitleSize(0.06);
            hMuon->GetYaxis()->SetTitleSize(0.06);
            hMuon->GetZaxis()->SetMoreLogLabels(1);
            hMuon->GetZaxis()->SetLabelSize(0.05);
            hMuon->GetZaxis()->SetTitleSize(0.06);
            hMuon->GetZaxis()->SetTitle("Signal [fC]");
            hMuon->GetYaxis()->SetTitle("Tower");
            hMuon->Draw("col,z");
          }
        }
                
        can->cd(iEvPlot+1);
	gPad->SetLogz(1);
        gPad->SetFillColor(kWhite);
        /*
	if (!trgl1L1GTAlgo[bitMuon] && !trgl1L1GTAlgo[bitBPTXplus])
	  gPad->SetFillColor(kRed);
	else if (!trgl1L1GTAlgo[bitMuon])
	  gPad->SetFillColor(kOrange);
	else if (!trgl1L1GTAlgo[bitBPTXplus])
	  gPad->SetFillColor(kMagenta);
        */
        
        // margins
	gPad->SetTopMargin(0.12);
	gPad->SetBottomMargin(0.01);
	gPad->SetLeftMargin(0.015);
	gPad->SetRightMargin(0.015);
	
	hMuon->Draw("col");

        static int countMuon = 1;
        
	ostringstream txt;
	// txt << "Event #" << iEvRead+iStart; 
	// text->DrawText(0.1, 0.8, txt.str().c_str());
        txt << "Muon #" << (countMuon++);
        text->SetTextAlign(22);
        text->DrawText(0.5, 0.94, txt.str().c_str());
        
        gPad->Modified();
        gPad->Update();
        
        // grid
        double gxmin = gPad->GetUxmin();
        double gxmax = gPad->GetUxmax();
        double gymin = gPad->GetUymin();
        double gymax = gPad->GetUymax();
        TLine* lgrid = new TLine();
        lgrid->SetLineWidth(1);
        lgrid->SetLineStyle(3);
        lgrid->SetLineColor(kGray);
        for (int ig=0; ig<15; ++ig) {
          lgrid->DrawLine(gxmin, gymin+(ig+1)*(gymax-gymin)/16, gxmax, gymin+(ig+1)*(gymax-gymin)/16);
        }
        for (int ig=0; ig<13; ++ig) {
          lgrid->DrawLine(gxmin+(ig+1)*(gxmax-gxmin)/14, gymin, gxmin+(ig+1)*(gxmax-gxmin)/14, gymax);
        }
        if (plotOne) {
          canOne->cd();
          for (int ig=0; ig<15; ++ig) {
            lgrid->DrawLine(gxmin, gymin+(ig+1)*(gymax-gymin)/16, gxmax, gymin+(ig+1)*(gymax-gymin)/16);
          }
          for (int ig=0; ig<13; ++ig) {
            lgrid->DrawLine(gxmin+(ig+1)*(gxmax-gxmin)/14, gymin, gxmin+(ig+1)*(gxmax-gxmin)/14, gymax);
          }
          TLatex* cms = new TLatex(0.17, 0.925, "CMS"); // , CASTOR interfill data
          cms->SetNDC();
          cms->SetTextFont(62);
          cms->SetTextSize(0.065);
          cms->Draw("same");
          canOne->SaveAs("muon_example.pdf");          
        }
        
      }
      
      iEvPlot++;
      
    } // end while loop, read events from tree

    if (plot) {
      can->Modified();
      can->Update();

      string input;
      cout << "continue? " << endl;
      input = cin.get();
      if (input.find("n") == 0)
	break;
    }
    
    iStart += iEvRead;
    
  } while (true);

  cout << " done reading" << endl;
  
  TCanvas* cavg = new TCanvas("average", "a", 1200, 800);
  cavg->Divide(3,2);
  cavg->cd(1);
  gPad->SetLogz(1);
  hAvg->Draw("colz");
  
  cavg->cd(2);
  //gPad->SetLogz(1);
  hcorr->Draw("colz");
  
  cavg->cd(3);
  //gPad->SetLogz(1);
  hcorrMax->Draw("colz");
  
  cavg->cd(4);
  gPad->SetLogy(1);
  muonPerSector0->Draw("");
  muonPerSector->Draw("same");

  cavg->cd(5);
  hChannelNZS->Draw("colz");

  cavg->cd(6);
  countExtra->Draw();
  countExtraCluster->Draw("same");

  TCanvas* cSpect = new TCanvas("cSpect", "cSpect", 1200, 1200);
  TH2D* hMean = new TH2D("hMean", "mean", 14, 0.5, 14.5, 16, 0.5, 16.5);
  TH2D* hRMS = new TH2D("hRMS", "variance", 14, 0.5, 14.5, 16, 0.5, 16.5);
  cSpect->Divide(14, 16, 0, 0);
  for (int iCh=0; iCh<224; ++iCh) {
    cSpect->cd(iCh+1);
    gPad->SetLogy(1);
    gPad->SetTopMargin(0);
    gPad->SetBottomMargin(0);
    gPad->SetLeftMargin(0);
    gPad->SetRightMargin(0);
    chSpectrum[iCh]->Draw();
    const int iSec = iCh/14 + 1;
    const int iMod = iCh%14 + 1;
    hMean->Fill(iMod, iSec, chSpectrum[iCh]->GetMean());
    hRMS->Fill(iMod, iSec, pow(chSpectrum[iCh]->GetRMS(),2) - pow(vec_elec_rms[iCh],2));
  }

  TCanvas* cSumm = new TCanvas("cSumm", "cSumm", 1000, 500);
  cSumm->Divide(2);
  cSumm->cd(1);
  gPad->SetLogz(1);
  hMean->Draw("colz");

  cSumm->cd(2);
  gPad->SetLogz(1);
  hRMS->Draw("colz");

  fileOut->Flush();
  fileOut->Write();
  // fileOut->Close();
}


int
main(int argc, char** argv)
{
  if (argc<3) {
    cout << "Please use: " << argv[0] << " muons_8_9.root mean_rms.root [sigmaZS=2] [plotYes=1] " << endl;
    return 1;
  }
  
  TFile* muons = TFile::Open(argv[1]);
  TFile* noise = TFile::Open(argv[2]); 
  const bool plot = (argc>=5 ? atoi(argv[4])!=0 : true);
  const double nSigma = (argc>=4 ? atof(argv[3]) : 2);
  
  TApplication app("runner", 0, 0);  

  cout << "plot: " << plot << " sigmaZS=" << nSigma << endl;
  
  plotEvents(muons, noise, nSigma, plot);
  
  app.Run();
  
  return 0;
}
