#include <TH1D.h>
#include <TH2D.h>
#include <TGaxis.h>
#include <TGraphErrors.h>
#include <TFile.h>

#include <vector>


class MuonNoiseFit {  
public:
  MuonNoiseFit(TH1D* hNoise, TH1D* hMuon) : fNoise(hNoise), fMuon(hMuon) {
    fNoise->Scale(fMuon->GetMaximum()/fNoise->GetMaximum());
  }
  
  TGraph* getNoise(const double* x) const {    
    const double norm = x[0];
    const double shift = x[1];
    const double fitEnd = x[2]; // fNoise    
    
    std::vector<double> vecx, vecy;
    
    for (int iBin=0; iBin<fMuon->GetNbinsX(); ++iBin) {
      
      const double y = fMuon->GetBinCenter(iBin+1);
      
      if (y-shift > fitEnd)
	continue;
      
      const int i = fNoise->GetXaxis()->FindBin(y-shift);
      double noise = 0;
      if (y-shift > fNoise->GetBinCenter(i)) {
	const double fraction = 1. - (y-shift-fNoise->GetBinCenter(i)) / fNoise->GetBinWidth(i);
	noise = fNoise->GetBinContent(i) * fraction + (1.-fraction) * 
	  (i<fNoise->GetNbinsX()-2 ? fNoise->GetBinContent(i+1) : 0);
      } else {
	const double fraction = 1. - (fNoise->GetBinCenter(i)-y+shift) / fNoise->GetBinWidth(i);
	noise = fNoise->GetBinContent(i) * fraction + (1.-fraction) * 
	  (i>2 ? fNoise->GetBinContent(i-1) : 0);
      }

      if (noise>0) {
	vecx.push_back(y);
	vecy.push_back(noise*norm);
      }
    }
    return new TGraph(vecx.size(), &vecx.front(), &vecy.front());
  }

  
  int getN(const double* x) const {
    TGraph* g = getNoise(x);
    int n = g->GetN();
    delete g;
    return n;
  }

  
  double operator()(const double *x) const {
    const double norm = x[0];
    const double shift = x[1];
    const double fitEnd = x[2]; // fNoise

    double chi2 = 0;
    for (int iBin=0; iBin<fMuon->GetNbinsX(); ++iBin) {
      
      const double y = fMuon->GetBinCenter(iBin+1);
      
      if (y-shift > fitEnd)
	continue;
      
      const int i = fNoise->GetXaxis()->FindBin(y-shift);
      double noise = 0;
      double noise_err = 0;
      if (y-shift > fNoise->GetBinCenter(i)) {
	const double fraction = 1. - (y-shift-fNoise->GetBinCenter(i)) / fNoise->GetBinWidth(i);
	noise = fNoise->GetBinContent(i) * fraction + (1.-fraction) * 
	  (i<fNoise->GetNbinsX()-2 ? fNoise->GetBinContent(i+1) : 0);
	noise_err = fNoise->GetBinError(i) * fraction + (1.-fraction) * 
	  (i<fNoise->GetNbinsX()-2 ? fNoise->GetBinError(i+1) : 0);
      } else {
	const double fraction = 1. - (fNoise->GetBinCenter(i)-y+shift) / fNoise->GetBinWidth(i);
	noise = fNoise->GetBinContent(i) * fraction + (1.-fraction) * 
	  (i>2 ? fNoise->GetBinContent(i-1) : 0);
	noise_err = fNoise->GetBinError(i) * fraction + (1.-fraction) * 
	  (i>2 ? fNoise->GetBinError(i-1) : 0);
      }
      
      if (noise>0 && fMuon->GetBinContent(iBin+1)>0) {
	//const double err2 = (noise + fMuon->GetBinContent(iBin+1));  // poisson V1 + V2
	const double err2 = (pow(noise_err,2) + pow(fMuon->GetBinError(iBin+1),2));  // poisson V1 + V2
	//const double err2 = pow(fMuon->GetBinError(iBin+1),2) * fMuon->GetBinWidth(iBin+1);  // poisson V1 + V2
	//const double err2 = pow(fMuon->GetBinError(iBin+1), 2);  // poisson V1 + V2
	chi2 += pow(noise*norm - fMuon->GetBinContent(iBin+1), 2) / err2;
      }
    }
    return chi2;
  } 
  
private:
  TH1D* fNoise;
  TH1D* fMuon;
};


class NoiseFitHelper {

 public:
  NoiseFitHelper() : fDrawChannels(false), fInput(0) {}
  ~NoiseFitHelper() {
    //delete fInput;
    //delete hFitFail;
    //delete h2dChi2;
    //delete h2dNorm;
    //delete h2dRange;
    //delete h2Shift;
    //delete h2Zprob;
  }
  NoiseFitHelper(TFile* input, const bool drawChannels=false);
  
  void SetDrawChannels(const bool v) { fDrawChannels=v; }
  
private:
  bool fDrawChannels;
  
  TFile* fInput;

 public:
  TH2D* hFitFail;
  
  TH2D* h2dChi2;
  TH2D* h2dNorm;
  TH2D* h2dRange;
  TH2D* h2Shift;
  TH2D* h2Zprob;  
};
