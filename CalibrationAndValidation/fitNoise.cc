#include <fitNoiseAna.h>

#include <TProfile2D.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TH1D.h>
#include <TFile.h>
#include <TTree.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TH2D.h>
#include <TApplication.h>
#include <TDirectory.h>
#include <TKey.h>
#include <TList.h>
#include <TGaxis.h>

#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include <TGraph.h>
#include <TGraphErrors.h>

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cmath>

using namespace std;


TH1D*
spread(TGraphErrors* g1, TGraphErrors* g2)
{
  static int iSpread = 0;
  iSpread++;
  ostringstream name;
  name << "spread_" << iSpread;
  TH1D* h = new TH1D(name.str().c_str(), name.str().c_str(), 100, -100, 100);
  for (int i=0; i<224; ++i) {
    if (g2->GetY()[i]!=0) {
      h->Fill(((g1->GetY()[i] / g2->GetY()[i]) - 1) * 100);
    }
  }
  return h;
}


TGraphErrors*
makeCal(TH2D* h)
{
  const int refMod = 4; // absolute numbering
  const int refSec = 9; // absolute numbering
  // const int refChannel = (9-1)*14 + (4-1);

  const double ref = h->GetBinContent(refMod, refSec);
  const double refErr = h->GetBinError(refMod, refSec);

  vector<double> vecx, vecy, vecerr;
  
  for(int iMod=0; iMod<14; ++iMod) {
    for(int iSec=0; iSec<16; ++iSec) {
      
      const int iCh = iSec*14 + iMod;
      
      const double v = h->GetBinContent(iMod+1, iSec+1);
      const double verr = h->GetBinError(iMod+1, iSec+1);
      
      vecx.push_back(iCh+1);
      vecy.push_back(v/ref);
      vecerr.push_back(sqrt(refErr*refErr + verr*verr));  
    }
  }

  return new TGraphErrors(224, &vecx.front(), &vecy.front(), 0, &vecerr.front());
}





void
fitNoise(const string& name)
{ 
  TFile* input = TFile::Open(name.c_str());
  NoiseFitHelper nfh(input);
  
  TCanvas* c2d = new TCanvas("c2d", "c2d", 1500, 300);
  c2d->Divide(4,2);
  
  c2d->cd(1);
  nfh.h2Zprob->Draw("colz");
  c2d->cd(2);
  nfh.h2Shift->Draw("colz");
  c2d->cd(3);
  nfh.h2dRange->Draw("colz");
  c2d->cd(4);
  nfh.h2dNorm->Draw("colz");
  c2d->cd(5);
  gPad->SetLogz(1);
  nfh.h2dChi2->Draw("colz");
  c2d->cd(6);
  gPad->SetLogz(1);
  //nfh.h2rms->Draw("colz");
  c2d->cd(7);
  gPad->SetLogz(1);
  //nfh.h2mean->Draw("colz");
  c2d->cd(8);
  nfh.hFitFail->Draw("colz");
  
  TCanvas* cCal = new TCanvas("ccal");

  /*
  TGraphErrors* calMean = makeCal(h2mean);
  calMean->SetTitle("mu-mean;Channel;Div by Ref");
  calMean->SetMarkerStyle(20);
  calMean->SetLineColor(kBlue);
  calMean->SetFillStyle(0);
  calMean->SetMarkerColor(kBlue);

  TGraphErrors* calRMS = makeCal(h2rms);
  calRMS->SetTitle("mu-rms;Channel;Div by Ref");
  calRMS->SetMarkerStyle(21);
  calRMS->SetLineColor(kRed);
  calRMS->SetFillStyle(0);
  calRMS->SetMarkerColor(kRed);
  */
  
  TGraphErrors* calZero = makeCal(nfh.h2Zprob);
  calZero->SetTitle("-log(p_{zero});Channel;Div by Ref");
  calZero->SetMarkerStyle(23);
  calZero->SetLineColor(kGreen+2);
  calZero->SetMarkerColor(kGreen+2);
  calZero->SetFillStyle(0);
  
  calZero->Draw("ap");
  //calMean->Draw("p");
  //calRMS->Draw("p");

  cCal->BuildLegend();


  /*
  TCanvas* cCalSpread = new TCanvas("ccalSpread");
  TH1D* hS1 = spread(calMean, calRMS);
  hS1->SetTitle("mean-rms; percent; #");
  hS1->SetLineColor(kRed);
  TH1D* hS2 = spread(calMean, calZero);
  hS2->SetLineColor(kBlue);
  hS2->SetTitle("mean-pzero; percent; #");
  TH1D* hS3 = spread(calZero, calRMS);
  hS3->SetLineColor(kGreen+2);
  hS3->SetTitle("pzero-rms; percent; #");
  
  
  hS3->Draw("");
  hS1->Draw("same");
  hS2->Draw("same");
  
  cCalSpread->BuildLegend();
  */
  
  // save output
  //TFile* output = TFile::Open(("_pzero.root").c_str(), "update");  
  //h2Zprob->Write();  
  //output->Flush();  
}


int
main(int argc, char** argv)
{
  if (argc<2) {
    cout << "Please use: " << argv[0] << " input-file " << endl;
    return 1;
  }
  
  TApplication app("runner", 0, 0);  
  fitNoise(argv[1]);
  app.Run();
  
  return 0;
}
