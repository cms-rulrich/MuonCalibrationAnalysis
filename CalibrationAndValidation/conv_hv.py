#!/usr/bin/python

import sys


def hyphen_range(s):
    """ yield each integer from a complex range string like "1-9,12, 15-20,23"

    >>> list(hyphen_range('1-9,12, 15-20,23'))
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 15, 16, 17, 18, 19, 20, 23]

    >>> list(hyphen_range('1-9,12, 15-20,2-3-4'))
    Traceback (most recent call last):
        ...
    ValueError: format error in 2-3-4
    """
    for x in s.split(','):
        elem = x.split('-')
        if len(elem) == 1: # a number
            yield int(elem[0])
        elif len(elem) == 2: # a range inclusive
            start, end = map(int, elem)
            for i in xrange(start, end+1):
                yield i
        else: # more than one hyphen
            raise ValueError('format error in %s' % x)


fin=open('hvhistory2011.csv')
fout=open('CorrectionFactors_hvhistory2011.txt', 'w')

fout.write('sector      mod      transp fact      transp fact error   \n')

for lin in fin.readlines():
    
    lins=lin.split(';')
    if len(lins) != 32:
        print ("error in reading " + str(len(lins)))
        sys.exit(1)

    mods = '['
    for mod in hyphen_range(lins[2]):
        mods += str(mod) + ' '
        fout.write( str(mod) + ' ' + lins[1] + ' ' + lins[30].replace(',','.') + '  0.0\n')
    mods += ']'

    print lins[1] + ' ' + lins[2] + ' ' + ' ' + mods  + ' ' + lins[30].replace(',','.')
        
fout.close()
fin.close()


