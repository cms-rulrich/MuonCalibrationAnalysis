#include <TROOT.h>
#include <TF1.h>
#include <TKey.h>
#include <TTree.h>
#include <TChain.h>
#include <TLorentzVector.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TFile.h>
#include "Math/GenVector/LorentzVector.h"
#include <TText.h>

#include <TApplication.h>

#include <iostream>
#include <sstream>
#include <string>
#include <stdexcept>
#include <fstream>
#include <cstdio>

#include "tdr_macro.h"

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;



void
ToLegend(TLegend* leg, TH1* h, bool scaleIsGeV)
{
  ostringstream info;
  info << h->GetTitle() << ", RMS=" << fixed << setprecision(2) << h->GetRMS() << (scaleIsGeV ? "GeV" : "fC");
  leg->AddEntry(h, info.str().c_str(), "l");
}


void
plotThis(TH1* h, TLegend* leg, const unsigned int norm, bool& first, bool const scaleIsGeV)
{
  if (h->GetBinWidth(1)*norm <= 0)
    return;
  h->Scale(1./(h->GetBinWidth(1)*norm));
  h->Draw( (first ? "" : "same") );
  if (leg)
    ToLegend(leg, h, scaleIsGeV);
  first = false;
}


class Data {

public:
  Data() {}

  void Pick() { fTree->GetEntry( int(gRandom->Rndm()*fTree->GetEntries()) ); }

  void LoadData(const string& dir) 
  {
    /*
      This is the EmptyBX data based on BPTX only
    */
    //int nFiles = tree->Add("/lsdf/castor/CFFData/CastorNoise/EmptyBX/NoiseAna_Castor_data_MinimumBias_2015_JuneNoise/170902_232917/0000//trees_*.root");
    
    // browse for CFF trees
    const std::string target_path(dir);
    std::vector< std::string > all_matching_files;
    
    boost::filesystem::recursive_directory_iterator end_itr; // Default ctor yields past-the-end
    for( boost::filesystem::recursive_directory_iterator i( target_path ); i != end_itr; ++i ) {
      // Skip if not a file
      if( !boost::filesystem::is_regular_file( i->status() ) )
	continue;
      const string filename = i->path().string();
      if (filename.rfind(".root") != filename.length()-5)
	continue;
      if (filename.rfind("/tree") == string::npos)
	continue;
      all_matching_files.push_back( filename );
    }
    int nFiles = 0;
    for (const string& filename : all_matching_files) {
      // find treename
      {
	TFile* fTest = new TFile(filename.c_str(), "READ");
	TIter next(fTest->GetListOfKeys());
	TKey *key;
	while ((key = (TKey*)next())) {
	  TClass *cl = gROOT->GetClass(key->GetClassName());
	  if (!cl->InheritsFrom("TDirectory"))
	    continue;
	  TDirectory* tdir = (TDirectory*)key->ReadObj();
	  TIter next2(tdir->GetListOfKeys());
	  TKey *key2;
	  while ((key2 = (TKey*)next2())) {
	    if (string(key2->GetClassName()).find("TTree") != 0)
	      continue;
	    TTree* tree2 = (TTree*)key2->ReadObj();
	    if (!tree2->FindBranch("cmenergy") ||
		!tree2->FindBranch("bx") ||
		!tree2->FindBranch("PUNumInteractions") ||
		!tree2->FindBranch("CastorRecHitEnergy")) {
	      // this does not look like CFF!
	      continue;          
	    }
	    const string fullname = string(tdir->GetName()) + "/" + tree2->GetName();
	    if (!fTree) {
	      fTree = new TChain(fullname.c_str());
	      if (fTree->AddFile(filename.c_str())) {
		cout << "found CFF: " << filename << " " << fullname << endl;
		nFiles++;
		goto nextFile;
	      } else {
		cout << "error opening CFF files" << endl;
		exit(2);
	      }
	    }
	    
	    if (fTree->GetName() == fullname) {
	      // cout << "added CFF: " << fTree->GetName() << endl;
	      if (fTree->AddFile(filename.c_str())) {
		nFiles++;
		goto nextFile;
	      }
	    } else {
	      cout << "no CFF: " << tree2->GetName() << endl;
	    }
	  }
	}
      nextFile:
	;
	fTest->Close();
	delete fTest;
      }
    }
    // -------------------------
    
    const unsigned int nEvtsTree = fTree->GetEntries();
    cout << "Found " << nFiles << " files with " << nEvtsTree << " events" << endl;
    
    // tree->Show(0);
    // try to read rechits
    const bool haveTowers = fTree->GetBranch("CastorTowerp4");
    if (haveTowers) {
      fTree->SetBranchAddress("CastorTowerp4", &CastorTowerp4);
    }
    const bool haveJets = fTree->GetBranch("ak5CastorJetsP4");
    if (haveJets) {
      fTree->SetBranchAddress("ak5CastorJetsP4", &CastorJetsP4);
    }
    const bool haveHits = fTree->GetBranch("CastorRecHitEnergy");
    if (haveHits) {
      fTree->SetBranchAddress("CastorRecHitEnergy", &CastorRecHitEnergy);
      fTree->SetBranchAddress("CastorRecHitSector", &CastorRecHitSector);
      fTree->SetBranchAddress("CastorRecHitModule", &CastorRecHitModule);
      fTree->SetBranchAddress("CastorRecHitisBad", &CastorRecHitBad);
    }
    if (fTree->GetBranch("trgl1L1GTTech")) {
      fTree->SetBranchAddress("trgl1L1GTTech", &trgl1L1GTTech); // >=2016 data does not have tech any more...
    }
    // mandatory  (!!!) :
    fTree->SetBranchAddress("trgl1L1GTAlgo", &trgl1L1GTAlgo);
    fTree->SetBranchAddress("bx", &bxCms);
    fTree->SetBranchAddress("run", &run);    
  } // end LoadData

  void SaveTo(const string& to, const int nMax) {

    fTo = to;
    fNmax = nMax;

    
    
  }

  
  TChain* fTree = 0;
  
  //vector<TLorentzVector> CastorToweremp4;
  vector<ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > >* CastorTowerp4 = 0;
  vector<ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > >* CastorJetsP4 = 0;
  vector<float>* CastorRecHitEnergy = 0;
  vector<int>* CastorRecHitSector = 0;
  vector<int>* CastorRecHitModule = 0;
  vector<int>* CastorRecHitBad = 0;
  vector<int>* trgl1L1GTTech = 0;
  vector<int>* trgl1L1GTAlgo = 0;
  int bxCms = 0;
  int run = 0;

  
}; // end class Data


void MakePU(const string& dirSim, const string& dirNoise, const double lambda, const int nEvts)
{

  Data sim;
  sim.LoadData(dirSim);
  Data noise;
  noise.LoadData(dirNoise);

  Data output;
  output.SaveTo("./output", 1000);

  for (int i=0; i<nEvts; ++i) {

    const int n = gRandom::PoissonI(lambda);
    
    output.Clear();
    output.SetN(n);
    if (n==0) {
      noise.Pick();
      output.Add(noise);
    } else {
      for (int j=0; j<n; ++j) {
	sim.Pick();
	output.Add(sim);      
      }
    }
    output.Write();
  }
  
  output.Close();
}



void
help(const string cmd)
{
  cout << "usage: " << cmd << " input-sample-directory [GeV/fC] [max-events]" << endl;
}


int
main(int argc, char** argv)
{
  if (argc < 2) {
    help(argv[0]);
    return 1;
  }
  
  const string dir(argv[1]);

  const unsigned int maxEvt = argc>3 ? atof(argv[3]) : 1e9;
  bool scaleIsGeV = false; // default is fC
  if (argc>2) {
    if (string(argv[2]) == "GeV")
      scaleIsGeV = true;
    else if (string(argv[2]) == "fC")
      scaleIsGeV = false;
    else {
      help(argv[0]);
      return 2;
    }
  }

  cout << "Maximum number of events to read is: " << maxEvt << " unit is " << (scaleIsGeV ? "GeV" : "fC") << endl;
  
  setTDRStyle();
  gStyle->SetPalette(1);
  TApplication run("holder",0,0);
  CastorNoise(dir, maxEvt, scaleIsGeV);
  run.Run();
  return 0;
}
