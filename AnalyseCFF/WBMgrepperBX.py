#!/usr/bin/python
# coding: utf-8

from subprocess import call
import sys
from collections import namedtuple
import datetime
import json
import urllib
import gzip
import shutil
from os import listdir, path
import os
import os.path

from bs4 import BeautifulSoup

Debug = True
READONLY = False

# -----------------------------------------------------------------------
BadPagesFound = [] # these pages don't load properly. Don't try again


# -----------------------------------------------------------------------
# read config file 
if len(sys.argv) <= 1:
    print "usage: %s run " % os.path.basename(sys.argv[0])
    sys.exit(2)

run = sys.argv[1]


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'



# ==========================================================
# read the file BadQueries.txt and store it local memory
# ==========================================================
def read_bad_query_list(filename="BadQueries.txt"):
    f = None

    if not os.path.isfile(filename):
        return

    try:
        f = open(filename, "r")
        for line in f.readlines():
            BadPagesFound.append(line.rstrip('\n'))

    except:
        print ('could not read file ' + filename + '. FATAL!')
        sys.exit(1)    

    finally:
        if f is not None:
            f.close()


# ==========================================================
# read the file BadQueries.txt and store it local memory
# ==========================================================
def add_bad_query(query, filename="BadQueries.txt"):

    print
    print ("**************************")
    print ("INFO: adding: " + query + " to list of bad queries: " + filename)
    print ("**************************")
    print

    f = None
    try:
        mode = "w"
        if os.path.isfile(filename):
            mode = "a"
        
        f = open(filename, mode)
        f.write(query + '\n')

    except:
        print ('could not write/append file ' + filename + '. FATAL!')
        sys.exit(1)    

    finally:
        if f is not None:
            f.close()

    BadPagesFound.append(query)



# ================================================================
# check the structure and completeness of one a html file on disk
# ================================================================
def check_open_file(file, requires, alsoExit=True) :
    ok = False
    for line in file.readlines():

        if "Cern Authentication" in line:
            print
            print "************************************"
            print ("NO CERN AUTHENTIFICATION. LOGIN AND SAVE cookies.txt with \'Cookie Exporter\' in firefo")
            print "************************************"
            print
            print ("removing: " + str(file))
            file.close()
            os.remove(file.name)
            if alsoExit:
                sys.exit(1)
            
        if "cmswbm.web.cern.ch/Shibboleth.sso/ADFS" in line:
            print
            print "************************************"
            print ("NO CERN AUTHENTIFICATION/shibboleth. LOGIN AND SAVE cookies.txt with \'Cookie Exporter\' in firefo")
            print "************************************"
            print
            print ("removing: " + str(file))
            file.close()
            os.remove(file.name)
            if alsoExit:
                sys.exit(1)
            
        if alsoExit and "HTTP Status 500" in line:
            print
            print "************************************"
            print("Page cannot be loaded")
            print "************************************"
            print
            ok = False
            break

        if alsoExit and "was not found on this server" in line:
            print
            print "************************************"
            print("Webpage was not found on server")
            print "************************************"
            print
            ok = False
            break

        for test in requires:
            if test in line:
                requires.remove(test)

        if len(requires) == 0:
            ok = True
            break

    # if not ok:
    #     print
    #     print ("**************************")
    #     print ("INFO: check file: " + filename + " -> not ok. ")
    #     print ("      missing: " + str(requires))
    #     print ("**************************")
    #     print

    return ok


# ================================================================
# check the structure and completeness of one a html file on disk
# ================================================================
def check_html_file(filename, requires, gz=True, alsoExit=True) :
    ok = False
    if os.path.isfile(filename):
        file = None
        try:
            if gz:
                file = gzip.open(filename, 'rb')
            else:
                file = open(filename, "r")
            ok = check_open_file(file, requires, alsoExit)

        except SystemExit:
            sys.exit(1) # cern authentification error

        except:
            print
            print ("**************************")
            print ("ERROR in file " + filename)
            print ("**************************")
            print
            ok = False
        
        finally:
            if file is not None:
                file.close()


        if not ok:
            print
            print ("**************************")
            print ("INFO: check file: " + filename + " -> not ok. ")
            print ("      missing: " + str(requires))
            print ("**************************")
            print

    else:
        print
        print ("**************************")
        print ("INFO: check file: " + filename + " -> does not (yet) exist. ")
        print ("**************************")
        print
    
    return ok


# ==========================================================
# check if cmswbm.web.cern.ch page corresponding to "query" exists on disk, 
# and that it contains the information "requires" and that it does not
# contain a "CERN Authorization" error. Download the page with wget if 
# necessary.
# ==========================================================
def get_WBM_page(query, requires, file_name=None):

    # error status
    isgood = True
    
    # this is the local path of the wget data
    theWbmPath = path.join('cmswbm.web.cern.ch','cmswbm','cmsdb','servlet')

    # some of the very complex queries, that contain slashes, 
    # must be handled with care:
    if file_name == None:
        file_name = query
    
    file_path = path.join(theWbmPath, file_name).strip()
    file_path_gz = file_path + '.gz'
    
    need_wget = False
    need_gz = True

    if Debug:
        print ("get_WBM_page: " + query)
        print ("get_WBM_page: " + file_path_gz)
    
    if not check_html_file(file_path_gz, requires, alsoExit=False):
        if not check_html_file(file_path, requires, gz=False, alsoExit=False):
            need_wget = True
    else:
        need_gz = False

    if need_wget:
        if READONLY:
            print ("cannot download WBM page in read-only mode")
            sys.exit(1)
        ret = call(["wget", "--no-check-certificate", "--load-cookies", "cookies.txt", "-p", "https://cmswbm.web.cern.ch/cmswbm/cmsdb/servlet/" + query])
        if not check_html_file(file_path, requires, gz=False):
            print
            print "***********************************"
            print ("ERRORL: HTML file is still corrupt, also after wget")
            print "***********************************"
            print
            isgood = False
    
    if need_gz:
        with open(file_path, 'rb') as f_in, gzip.open(file_path_gz, 'wb', compresslevel=9) as f_out:
            shutil.copyfileobj(f_in, f_out)
        print ("removing unzipped: " + str(file_path))
        os.remove(file_path)
        
    if not check_html_file(file_path_gz, requires):
        print
        print "***********************************"
        print ("ERRORL:  HTML file is corrupt after gzip")
        print "***********************************"
        print
        isgood = False
        
    return gzip.open(file_path_gz, "r"), isgood



# ==========================================================
# analyse the RunSummary page for one run
# ==========================================================
def check_runsummary(run):

    query = "RunSummary?RUN="+str(run)+"&DB=default"

    fill = 0
    l1trigger = long(0)

    if query in BadPagesFound:
        return fill, l1trigger
    
    file = None
    try:
        file, isgood = get_WBM_page(query, ["<A HREF=RunSummary>Run Summary</A>", "<TITLE>CMS RunSummary Run "+str(run)+"</TITLE>"])
        
        if isgood:

            for line in file.readlines():

                if "<TR><TH ALIGN=RIGHT>LHC Fill" in line:
                    snip = ""
                    if "<A HREF=FillReport?FILL=" in line:
                        snip = line.split('>')[5].split('<')[0]
                    else:
                        snip = line.split('>')[4].split('<')[0]
                    if snip =="n/a":
                        fill = 0
                    else:
                        fill = int(snip)

                if "<TR><TH ALIGN=RIGHT>L1 Triggers</TH>" in line:
                    snip = line.split('<TD>')[1].split('</TD>')[0]
                    if (snip=="n/a"):
                        l1trigger = 0
                    else:
                        l1trigger = long(snip)

        else:
            print ("ERROR in downloaded data (runsummary)!! " + query)
            add_bad_query(query)

    except SystemExit:
        sys.exit(1) # pass on fatal errors
    
    except:
        print ("ERROR in downloaded data (runsummary)!! " + query)
        add_bad_query(query)

    finally:
        if file is not None:
            file.close()

    return fill, l1trigger



# ==========================================================
# Analyse the FillReport page for one fill
# ==========================================================
def check_fill(fill):

    bx_name = "none"
    bx_link = "none"
    
    if fill == 0:
        return (bx_name, bx_link)

    query="FillReport?FILL="+str(fill)

    if query in BadPagesFound:
        return (bx_name, bx_link)

    file = None
    try:
        file,isgood = get_WBM_page(query, ["CMS Fill "+str(fill)+" Report</B></FONT></TD>"]) #, "</BODY></HTML>"]) sometimes web pages are broken...
        if isgood:
            for line in file.readlines():
                if "InjectionScheme</A></TH><TD>" in line:
                    if ">n/a<" in line or "null" in line or len(line.split('HREF'))!=3:
                        bx_name = "none"
                        bx_link = "none"
                    else:
                        bx_link=line.split('HREF')[2].split('"')[1]
                        bx_name=line.split('HREF')[2].split('"')[2].split('</A>')[0].lstrip('>')
        else:
            print ("ERROR in opening downloaded data!! FindFile " + query)
            add_bad_query(query)
            

    except SystemExit:
        sys.exit(1)  # pass on fatal errors

    except:
        print ("EXCEPTION in downloaded data!! FindFile " + query)
        print "Unexpected error:", sys.exc_info()[0]
        add_bad_query(query)

    finally:
        if file is not None:
            file.close()

    return (bx_name, bx_link)



# ==========================================================
# Analyse the FillReport page for one fill
# ==========================================================
def check_bunchfill(fill):

    if fill == 0:
        return 'none'

    query="BunchFill?FILL=" + str(fill)

    if query in BadPagesFound:
        return "none"

    file = None
    try:
        file,isgood = get_WBM_page(query, ["Bunch Fill"]) #, "</BODY></HTML>"]) sometimes web pages are broken...
                
        if isgood:

            html_doc = "<html><header></header><body><table>"
            switch = False
            for line in file.readlines():
                if "TBODY" in line:
                    switch = not switch
                elif switch:
                    html_doc += line
            html_doc += "</table></body></html>\n"

            soup = BeautifulSoup(html_doc, 'lxml')
            #print str(soup.prettify())

            beam1 = []
            beam2 = []

            rows = soup.find_all("tr")
            for row in rows: 
                cols = row.find_all("td")
                if len(cols) != 6:
                    print ("column error")
                    sys.exit(1)    
                bx = int(cols[0].text.strip()) # read: https://vocms0186.cern.ch/bunchNumber.html
                # bx = bx - 1 # CMS -> LHC nope !!!! only difference between cms and lhc is CMS[0] == LHC[3564]
                if cols[3].find("b") != None:
                    beam1.append(bx)
                if cols[4].find("b") != None:
                    beam2.append(bx)
            # print "bx=", bx, str(cols[3]), str(cols[4]), str(cols[3].find("b")).strip(), str(cols[4].find("b")).strip()
#            print str(beam1), str(beam2)

            bx_name = "LHC_fillingscheme_fill"+str(fill)
            bx_file = bx_name + ".txt"
            scheme_out = open(bx_file, "w")
            
            scheme_out.write("BEAM 1\n")
            scheme_out.write("RFbucket,Slot,Head-On IP1,Head-On IP2,Head-On IP5,Head-On IP8,Head-On Tot,LR IP1,LR IP2,LR IP5,LR IP8,LR Tot\n");
            for b1 in beam1:
                collide_at_ip5 = "0"
                if b1 in beam2:
                    collide_at_ip5 = "1"
                scheme_out.write("0,"+str(b1)+",0,0,"+collide_at_ip5+",0,0,0,0,0,0,0\n")
            scheme_out.write("\n\nBEAM 2\n")
            scheme_out.write("RFbucket,Slot,Head-On IP1,Head-On IP2,Head-On IP5,Head-On IP8,Head-On Tot,LR IP1,LR IP2,LR IP5,LR IP8,LR Tot\n");
            for b2 in beam2:
                collide_at_ip5 = "0"
                if b2 in beam1:
                    collide_at_ip5 = "1"
                scheme_out.write("0,"+str(b2)+",0,0,"+collide_at_ip5+",0,0,0,0,0,0,0\n")
            
            scheme_out.close();
            
        else:
            print ("ERROR in downloaded data!! FindFile " + query)
            add_bad_query(query)
            

    except SystemExit:
        sys.exit(1)  # pass on fatal errors

    except:
        print ("Exception in downloaded data!! FindFile " + query)
        print "Unexpected error:", sys.exc_info()[0]
        add_bad_query(query)

    finally:
        if file is not None:
            file.close()

    return bx_file




# ==========================================================
# Download and save the bunch filling scheme
# ==========================================================
def getBXscheme(bx_name, bx_link):

    print ("name: " + bx_name)
    print ("link: " + bx_link)
    
    query = "../" + bx_link

    if query in BadPagesFound:
        return False
    
    isgood = True
    file = None
    try:
        file,isgood = get_WBM_page(query, ["INJECTION SCHEME"]) #, "</BODY></HTML>"]) sometimes web pages are broken...
        
        if isgood:
            bx_out = open(bx_name + ".txt", "w")
            for line in file.readlines():
                bx_out.write(line)
            bx_out.close()
                
        else:
            print ("ERROR in downloaded data!! FindFile " + query)
            add_bad_query(query)        
            
    except SystemExit:
        sys.exit(1)  # pass on fatal errors

    except:
        isgood = False
        print ("Exception in downloaded data !!! FindFile " + query)
        print "Unexpected error:", sys.exc_info()[0]
        add_bad_query(query)

    finally:
        if file is not None:
            file.close()
    
    return isgood
    


# =================================================================
# Start main program 
# =================================================================


already = False
try:
    with open("lhc_filling.txt", "r") as liste:
        for line in liste.readlines():
            if run in line:
                print ("already in lhc_filling.txt")
                already = True
                break
    liste.close()
except:
    alread = False

    
if not already:
    fill, l1trig = check_runsummary(run)
    bx_name,bx_link = check_fill(fill)
    have_bx = False
    file_bx = "none"
    if bx_link != "none":
        have_bx = getBXscheme(bx_name, bx_link)
        file_bx = bx_name + ".txt"

    if not have_bx:
        file_bx = check_bunchfill(fill)
        
        liste = open("lhc_filling.txt", "a")
        liste.write(str(run) + "\t "  + str(fill) + "\t " + file_bx + "\t " + bx_name + " \n")
        liste.close()
