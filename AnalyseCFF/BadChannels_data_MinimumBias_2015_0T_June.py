
# badChannelsSecMod = [[2,10],[3,11],[12,12],[5,4],[3,8],[7,5],[9,4],[8,8],[4,11],[16,6],[16,4],[15,4]] 
# badChannelsSecMod = [[2,5],[2,10],[3,8],[4,9],[5,9]]
#[1,7],[2,7],[3,7],[4,7],[5,7],[6,7],[7,7],[8,7],[9,7],[10,7],[11,7],[12,7],[13,7],[14,7],[15,7],[16,7],
#                     [1,8],[2,8],[3,8],[4,8],[5,8],[6,8],[7,8],[8,8],[9,8],[10,8],[11,8],[12,8],[13,8],[14,8],[15,8],[16,8],
badChannelsSecMod = [[3,11], [2,10], # bad mean/rms performance
                     [7,5], [12,12], # slightly poor, a bit noisy
                     [10,14],[2,4],[3,8],[3,14],[13,14],[14,5],[16,5], # even less noisy
                     [7,3],[9,4]] # a bit over-frequent in ZS

# this is muon-HV and 0T: a bit noisy and nExtra high
zeroSuppressionThreshold = 2.5

maxExtraChannels = 6
minChannelsSector = 4

# muon penetrating selection:
minChannelsPerSection = 1
minSections = 3

