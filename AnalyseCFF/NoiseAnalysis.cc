#include <TROOT.h>
#include <TF1.h>
#include <TKey.h>
#include <TTree.h>
#include <TChain.h>
#include <TLorentzVector.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TFile.h>
#include "Math/GenVector/LorentzVector.h"
#include <TText.h>

#include <TApplication.h>

#include <iostream>
#include <sstream>
#include <string>
#include <stdexcept>
#include <fstream>
#include <cstdio>

#include "tdr_macro.h"

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>

// added by Koller //
#include <sys/stat.h>    // for mkdir
#include <sys/types.h>   // ""
#include <cstring>       // for string to char

using namespace std;

const bool skipDenseLHCregions = true;   // skip LHC beam if BX-delta is <3
const bool skipFillWithCloseBX = false;  // skip runs with any BX closer than 3 
const bool useBadChannelFlag = true;    // wether or not to ignore bad channels
const bool useBPTX_alternatively = true; // if NO filling scheme found on WBM, use BPTX
const bool checkBPTXalways = true;      // in all cases: use PBTX, too
const bool runsIndividually = false;      // split output on run-level
const bool useIntercal = true;           // Intercalibrates all Data through a specific file
const int deltaBX_exclude = 50;         // BX window around colliding bunches to exclude from noise study

// added by koller //
double intercalfactor[224]= {}; // for the calibration
string newFolder;               // to create a new Folder which contains all plots

/*
  L1 tech trigger menu:
  (this seem to be the same over all times of CMS, but triggers are not always activated or working properly)

  https://cmswbm.cern.ch/cmsdb/servlet/L1Summary?RUN=247920&KEY=TSC_20150610_003250_collisions_BASE
  0     bptx-and
  1     bptx-plus
  2     bptx-minus
  5	L1Tech_BPTX_plus_AND_NOT_minus.v0	3950091	219.79	219.79	3838215	214.68	214.90	307	0
  6	L1Tech_BPTX_minus_AND_not_plus.v0	4002237	222.69	222.69	3902351	218.26	218.49	101	0
  7	L1Tech_BPTX_quiet.v0

  However, in Run2 (2016) the tech triggers disappeared. In Algo it is:
  0  L1_ZeroBias
  1  L1_BptxPlus_NotBptxMinus
  2  L1_BptxMinus_NotBptxPlus
  9  L1_NotBptxOR
  10 L1_BptxPlus
  11 L1_BptxMinus
*/



////////////// added by Koller to convert it to GEV with factors from file ///////////
/*
ReadIntercalHist(TH2* h, const char* fname)
{
  ifstream cal(fname);
  int n = 0;
  while (cal.good()) {
    if (cal.peek() == '#') { cal.ignore(999, '\n');}
    int auxI = 0;
    int isec =0, imod = 0;
    string name = "";
    double auxD = 0;
    string ID = "";
    cal >> auxI >> isec >> imod >> name >> auxD >> auxD >> auxD >> auxD >> ID;
    if (!cal.good()) break;
    h->SetBinContent(imod, isec, auxD);
    h->SetBinError(imod, isec, 0);
    n++;
  }
  cal.close();
  cout << "Read " << n << " entries from " << fname << endl;
}
*/
void
ReadIntercalfactor(const char* fname)
{
  ifstream cal(fname);
  int n = 0;
  while (cal.good()) {
    if (cal.peek() == '#') { cal.ignore(999, '\n');}
    int auxI = 0;
    int isec =0, imod = 0;
    string name = "";
    double auxD = 0;
    string ID = "";
    cal >> auxI >> isec >> imod >> name >> auxD >> auxD >> auxD >> auxD >> ID;
    if (!cal.good()) break;
    intercalfactor[n]=auxD;
    n++;
  }
  cal.close();
  cout << "Read " << n << " entries from " << fname << endl;
}
////////////-----------------------------------/////////////////



void
ToLegend(TLegend* leg, TH1* h, bool scaleIsGeV)
{
  ostringstream info;
  info << h->GetTitle() << ", RMS=" << fixed << setprecision(2) << h->GetRMS() << (scaleIsGeV ? "GeV" : "fC");
  leg->AddEntry(h, info.str().c_str(), "l");
}


void
plotThis(TH1* h, TLegend* leg, const unsigned int norm, bool& first, bool const scaleIsGeV)
{
  if (h->GetBinWidth(1)*norm <= 0)
    return;
  h->Scale(1./(h->GetBinWidth(1)*norm));
  h->Draw( (first ? "" : "same") );
  if (leg)
    ToLegend(leg, h, scaleIsGeV);
  first = false;
}


map<int,bool>
read_bad_channel_data(const string& fname)
{
  map<int,bool> bad;
  ifstream data(fname);
  while (data.good()) {
    string line;
    getline(data, line);
    istringstream lines(line);
    if (lines.peek() == '#')
      continue;
    string dummys;
    int eta, phi, depth;
    lines >> eta >> phi >> depth >> dummys;
    depth -= 1;
    phi -= 1;
    bad[depth + phi*14] = true;
  }
  data.close();
  cout << "Use bad-channel list " << fname << " with " << bad.size() << " entries" << endl;
  return bad;
}


bool reportFills = true;
static map<int,bool> rememberFills;

bool
read_bx_pattern(const int run, vector<int>& plus, vector<int>& minus, vector<int>& both,
                int& fill, int& minDeltaBX,
                vector<pair<int, int> >& denseBeam)
{
  denseBeam.clear();
  plus.clear();
  minus.clear();
  both.clear();
  
  string bx_file = "";
  for (int i=0; i<2; ++i) {
    ifstream liste("lhc_filling.txt");
    while (liste.good()) {
      string line;
      getline(liste, line);
      istringstream lines(line);
      if (lines.peek() == '#')
        continue;
      int theRun = 0;
      lines >> theRun;
      if (theRun == run) {
        fill = 0;
        lines >> fill >> bx_file;
        if (rememberFills.count(fill)==0) {
          if (reportFills) {
            cout << "New fill: " << fill << endl;
          }
          rememberFills[fill] = true;
        }
      }    
    }
    liste.close();
    
    if (bx_file.find("none")==0) {
      cout << "BX scheme for run " << run << " NOT FOUND " << endl;
      return false;
    }
    /*
    if (bx_file.find("Single_20p_20Pb_10_10_9_1non_coll.txt")==string::npos) { // WARNING: for debugging
      cout << "BX scheme for run " << run << " excluded " << endl;
      return false;
      } */
    if (bx_file == "") {
      if (i==2) {
        cout << "coud not get bx scheme" << endl;
        exit(4);
      }
      ostringstream cmd;
      cmd << "./WBMgrepperBX.py " << run;
      std::shared_ptr<FILE> pipe(popen(cmd.str().c_str(), "r"), pclose);
      if (!pipe) throw std::runtime_error("popen() failed!");           
    } else {
      break;
    }
  }
  
  
  vector<int> plus_coll_ip5;
  vector<int> minus_coll_ip5;
  ifstream data(bx_file);
  int isBeam = 0;
  while (data.good()) {
    string line;
    getline(data, line);
    boost::trim(line);
    istringstream lines(line);
    if (line == "") {
      isBeam = 0;
      continue;
    }
    if (isBeam != 0) {
      vector<string> strs;
      boost::split(strs, line, boost::is_any_of(", \t"));
      if (strs.size() != 12) {
        cout << "error in lhc bunch filling scheme " << endl;
        exit(6);
      }
      istringstream col2(strs[1]), col5(strs[4]);
      int ip5=0, bxnum=0;
      col2 >> bxnum;
      col5 >> ip5;
      if (isBeam == 1) {
        if (ip5 != 0) { // collide at IP5
          plus_coll_ip5.push_back(bxnum);
        }
        plus.push_back(bxnum);
      } else if (isBeam == 2) {
        if (ip5 != 0) { // collide at IP5
          minus_coll_ip5.push_back(bxnum);
        }
        minus.push_back(bxnum);
      }      
      continue;
    }
    isBeam = 0;
    if (lines.peek() == '#')
      continue;    
    if (line.find("BEAM ") != string::npos) {
      string dummy;
      int beam = 0;
      istringstream beams(line.substr(line.find("BEAM "), 6)); 
      beams >> dummy >> beam;
      getline(data, line);
      if (line.find("RFbucket,Slot,Head-On IP1,Head-On IP2,Head-On IP5,Head-On IP8,Head-On Tot,LR IP1,LR IP2,LR IP5,LR IP8,LR Tot") == 0) { 
        isBeam = beam;
      }      
    }
  } 
  data.close();

  for (int bx1 : plus) { // all plus bunches
    for (int bx2 : minus) { // all minus bunches
      if (bx1 == bx2) {
        bool check1 = false;
        for (int bx1_check : plus_coll_ip5) // all plus bunches colliding at ip5
          if (bx1_check == bx1)
            check1 = true;
        bool check2 = false;
        for (int bx2_check : minus_coll_ip5) // all minus bunches colliding at ip5
          if (bx2_check == bx2)
            check2 = true;
        if (!check1 or !check2) {
          cout << "LHC bunch filling scheme incomplete" << endl;
          exit(6);
        }
        both.push_back(bx1);
      }
    }
  }

  // determine minimum delta-BX
  minDeltaBX = 4000;
  for (int bx1 : both) {
    for (int bx2 : both) {
      if (bx1!=bx2)
        minDeltaBX = min(abs(bx1-bx2), minDeltaBX);
    }
  }

  // vector<pair<int, int> > denseBeam;
  const int checkDeltaBX = 2;
  for (const int bx1 : both) {
    for (auto ii : denseBeam)
      if (bx1>=ii.first and bx1<=ii.second) // bx1 already covered
        goto nextBX1;    
    for (int bx2 : both) {
      for (const auto ii : denseBeam)
        if (bx2>=ii.first and bx2<=ii.second) // bx2 already covered
          goto nextBX2;
      if (bx1==bx2) // ignore same
        continue;
      for (auto& ii : denseBeam)
        if (abs(bx2-ii.first)<=checkDeltaBX) {
          ii.first = bx2;
          goto nextBX2;
        }
      for (auto& ii : denseBeam)
        if (abs(bx2-ii.second)<=checkDeltaBX) {
          ii.second = bx2;
          goto nextBX2;
        }
      if (abs(bx1-bx2)<=checkDeltaBX)
        denseBeam.push_back(make_pair(min(bx1,bx2), max(bx1,bx2)));
    nextBX2:
      ;
    }
  nextBX1:
    ;
  }
  
  for (auto ii : denseBeam) {
    cout << "Fill " << fill << " has dense LHC beam (deltaBX<=" << minDeltaBX << "): "
         << ii.first << " to " << ii.second << endl;
  }
    
  if (plus_coll_ip5.size()!=both.size() or minus_coll_ip5.size()!=both.size() or
      both.size()>plus.size() or both.size()>minus.size() or
      (plus.size()==0 and minus.size()==0 and both.size()==0)) {
    cout << "Reading LHC bunch filling scheme failed! run=" << run << " " << bx_file 
         << "plus: " << plus.size() << " minus: " << minus.size() << " both: " << both.size()
         << "plus@ip5: " << plus_coll_ip5.size() << " minus@ip5: " << minus_coll_ip5.size() 
         << endl;
    return false;
  }
  
  cout << "BX scheme for run " << run << " in fill " << fill << " is " << bx_file << ", beam1: " << plus.size() << " beam2: " << minus.size() << " both: " << both.size()
       << " minDeltaBX=" << minDeltaBX
       << endl;

  if (skipFillWithCloseBX && minDeltaBX<=3)
    return false;
  return true;
}



void
CastorNoise(const string& dir, const unsigned int maxEvt, const bool scaleIsGeV)
{
  TChain* tree = 0;
  
  bool report_runs = true;

  if (report_runs) {
    cout << "Report all run numbers " << endl;   
  } else {
    cout << "Do not report run numbers " << endl;
  }  
  if (useBadChannelFlag) {
    cout << "Use bad channel flag information" << endl;
  } else {
    cout << "Ignore bad channel flag information" << endl;
  }
  if (useBPTX_alternatively) {
    cout << "Try to use BPTX triggers alterantively, IF bx-filling scheme did not work (e.g. 2011 data)" << endl;
  } else {
    cout << "Do not use BPTX triggers! Only LHC bx filling schemes!" << endl;
  }
  cout << "Exclude all BX closer than " << deltaBX_exclude << " to collisions from noise analysis  " << endl;
  
  int techBitQuiet = 7; // 2011, 2015
  int techBitPlus  = 5; // 2011, 2015
  int techBitMinus = 6; // 2011, 2015
  int algoBitQuiet = 1;  // 2016 zero bias
  int algoBitPlus  = 10; // 2016 
  int algoBitMinus = 11; // 2016
  
  /*
    The zero bias skim was needed to find the BX scheme. The scheme from https://cmswbm.cern.ch/cmsdb/servlet/BunchFill?FILL=3855 is shifted by 206 bunch crossings! 
   */
  //int nFiles = tree->Add("/lsdf/castor/CFFData/CastorNoise/ZeroBias/NoiseAna_Castor_data_MinimumBias_2015_JuneZB/170903_234710/0000/trees_*.root");

  // --------------------------------
  // The BX-shift includes the CMS (start at 1) -> LHC (start at 0) translation. Careful: the shift is subtracted from BMX BX number !!!!
  // 
  // --------------------------------
  
  const int bx_shift_2013 = 0; // from L1_SingleMuBeamHalo and similar
  const int bx_shift_2016 = 0; // from L1_BptxPlus PAMinimumBias1+HLTRandom
  const int bx_shift_2015Nov = 0; // from 	L1_CastorMuon_BptxAND
  
  const int bx_shift_fill3855 = 208; // from ZB data..  -->   whole  June 2015 "LHCF-Run" !!!
  // the orginal LHC bx filling scheme
  // const vector<int> bx_pattern_fill3855 = {1, 81, 161, 241, 321, 401, 481, 561, 641, 721, 801, 1215, 1295, 1375, 1455, 1535, 1615, 1695, 1786, 1866, 1946, 2026, 2106, 2186, 2266, 2346, 2426, 2506, 2586};
  
  bool do_bx_filter = false; // ...

  /*
    This is the EmptyBX data based on BPTX only
   */
  //int nFiles = tree->Add("/lsdf/castor/CFFData/CastorNoise/EmptyBX/NoiseAna_Castor_data_MinimumBias_2015_JuneNoise/170902_232917/0000//trees_*.root");

  // added by Koller //
  
  ReadIntercalfactor("intercalibration_2016B_4T_Nov.txt");



  map<int,bool> bad_channels;
  

  // browse for CFF trees
  const std::string target_path(dir);
  std::vector< std::string > all_matching_files;
  
  boost::filesystem::recursive_directory_iterator end_itr; // Default ctor yields past-the-end
  for( boost::filesystem::recursive_directory_iterator i( target_path ); i != end_itr; ++i ) {
    // Skip if not a file
    if( !boost::filesystem::is_regular_file( i->status() ) )
      continue;
    const string filename = i->path().string();
    if (filename.rfind(".root") != filename.length()-5)
      continue;
    if (filename.rfind("/tree") == string::npos)
      continue;
    all_matching_files.push_back( filename );
  }
  int nFiles = 0;
  for (const string& filename : all_matching_files) {
    // find treename
    {
      TFile* fTest = new TFile(filename.c_str(), "READ");
      TIter next(fTest->GetListOfKeys());
      TKey *key;
      while ((key = (TKey*)next())) {
	TClass *cl = gROOT->GetClass(key->GetClassName());
	if (!cl->InheritsFrom("TDirectory"))
          continue;
	TDirectory* tdir = (TDirectory*)key->ReadObj();
	TIter next2(tdir->GetListOfKeys());
	TKey *key2;
	while ((key2 = (TKey*)next2())) {
	  if (string(key2->GetClassName()).find("TTree") != 0)
            continue;
	  TTree* tree2 = (TTree*)key2->ReadObj();
	  if (!tree2->FindBranch("cmenergy") ||
	      !tree2->FindBranch("bx") ||
	      !tree2->FindBranch("PUNumInteractions") ||
	      !tree2->FindBranch("CastorRecHitEnergy")) {
	    // this does not look like CFF!
	    continue;          
	  }
	  const string fullname = string(tdir->GetName()) + "/" + tree2->GetName();
	  if (!tree) {
	    tree = new TChain(fullname.c_str());
	    if (tree->AddFile(filename.c_str())) {
	      cout << "found CFF: " << filename << " " << fullname << endl;
	      nFiles++;
	      goto nextFile;
	    } else {
	      cout << "error opening CFF files" << endl;
	      exit(2);
	    }
	  }
	  
	  if (tree->GetName() == fullname) {
	    // cout << "added CFF: " << tree->GetName() << endl;
	    if (tree->AddFile(filename.c_str())) {
	      nFiles++;
	      goto nextFile;
	    }
	  } else {
	    cout << "no CFF: " << tree2->GetName() << endl;
	  }
	}
      }
    nextFile:
      ;
      fTest->Close();
      delete fTest;
    }
  }

  // -------------------------
  
  vector<int> bx_pattern_plus, bx_pattern_minus, bx_pattern_both;
  int bx_shift = 0; // also in 2011
  int previous_run = 0;
  int skip_run = 0;
  bool failedToReadBXScheme = false;
  
  const unsigned int nEvtsTree = tree->GetEntries();
  cout << "Found " << nFiles << " files with " << nEvtsTree << " events" << endl;
  
  //vector<TLorentzVector> CastorToweremp4;
  vector<ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > >* CastorTowerp4 = 0;
  vector<ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > >* CastorJetsP4 = 0;
  vector<float>* CastorRecHitEnergy = 0;
  vector<int>* CastorRecHitSector = 0;
  vector<int>* CastorRecHitModule = 0;
  vector<int>* CastorRecHitBad = 0;
  vector<int>* trgl1L1GTTech = 0;
  vector<int>* trgl1L1GTAlgo = 0;
  int bxCms = 0;
  int run = 0;
  
  // tree->Show(0);
  // try to read rechits
  const bool haveTowers = tree->GetBranch("CastorTowerp4");
  if (haveTowers) {
    tree->SetBranchAddress("CastorTowerp4", &CastorTowerp4);
  }
  const bool haveJets = tree->GetBranch("ak5CastorJetsP4");
  if (haveJets) {
    tree->SetBranchAddress("ak5CastorJetsP4", &CastorJetsP4);
  }
  const bool haveHits = tree->GetBranch("CastorRecHitEnergy");
  if (haveHits) {
    tree->SetBranchAddress("CastorRecHitEnergy", &CastorRecHitEnergy);
    tree->SetBranchAddress("CastorRecHitSector", &CastorRecHitSector);
    tree->SetBranchAddress("CastorRecHitModule", &CastorRecHitModule);
    tree->SetBranchAddress("CastorRecHitisBad", &CastorRecHitBad);
  }
  if (tree->GetBranch("trgl1L1GTTech")) {
    tree->SetBranchAddress("trgl1L1GTTech", &trgl1L1GTTech); // >=2016 data does not have tech any more...
  }
  // mandatory  (!!!) :
  tree->SetBranchAddress("trgl1L1GTAlgo", &trgl1L1GTAlgo);
  tree->SetBranchAddress("bx", &bxCms);
  tree->SetBranchAddress("run", &run);
  
  TH1D* hBX = new TH1D("hbx", "EnergyCAS vs. BX", 4000, -0.5, 3999.5);

  TH1D* hBX_count = new TH1D("hbx_count", "hbx_count", 4000, -0.5, 3999.5);
  hBX_count->SetLineColor(kOrange+1);
  hBX_count->SetMarkerColor(kOrange+1);
  hBX_count->SetMarkerStyle(20);
  TH1D* hLHC_P_count = new TH1D("hlhc_p_count", "hlhc_p_count", 4000, -0.5, 3999.5);
  hLHC_P_count->SetLineColor(kBlue);
  TH1D* hLHC_M_count = new TH1D("hlhc_m_count", "hlhc_m_count", 4000, -0.5, 3999.5);
  hLHC_M_count->SetLineColor(kGreen+2);
  TH1D* hLHC_B_count = new TH1D("hlhc_b_count", "hlhc_b_count", 4000, -0.5, 3999.5);
  hLHC_B_count->SetLineColor(kRed);
  hLHC_B_count->SetMarkerColor(kRed);
  hLHC_B_count->SetMarkerStyle(20);

  TH2D* h2d_castor = new TH2D("h2d_castor", "h2d_castor", 14, 0, 14, 16, 0, 16);
  h2d_castor->SetXTitle("Module");
  h2d_castor->SetYTitle("Sector");
  
  TH1D* hAutoCorr = new TH1D("hAutoCorr", "hAutoCorr", 1001, -500.5, 500.5);
  TH1D* hAutoCorrTech = new TH1D("hAutoCorrTech", "hAutoCorr", 1001, -500.5, 500.5);
  TH1D* hAutoCorrAlgo = new TH1D("hAutoCorrAlgo", "hAutoCorr", 1001, -500.5, 500.5);
  hAutoCorrAlgo->SetLineColor(kBlue);
  hAutoCorrTech->SetLineColor(kRed);
  hAutoCorr->SetXTitle("#DeltaBX(LHC-GT)");
  hAutoCorr->SetYTitle("Number");
  // ...
  map<int,TH1D*> vecAutoTech, vecAutoAlgo;
  
  const int nBins = 200;
  const double Emin = scaleIsGeV ? -10 : -10 / 0.02;
  const double Emax = scaleIsGeV ? 129 : 129 / 0.02;

  TH1D* hEtower_A = new TH1D("hEtower_A", "All", nBins, Emin, Emax);
  hEtower_A->SetLineColor(kBlack);
  hEtower_A->SetLineStyle(2);
  hEtower_A->SetLineWidth(3);
  hEtower_A->SetXTitle(scaleIsGeV ? "Energy tower [GeV]" : "Charge tower [fC]");
  hEtower_A->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
  TH1D* hEtower_Q = new TH1D("hEtower_Q", "No beam", nBins, Emin, Emax);
  hEtower_Q->SetLineColor(kRed);
  hEtower_Q->SetLineStyle(2);
  hEtower_Q->SetLineWidth(3);
  hEtower_Q->SetXTitle(scaleIsGeV ? "Energy tower [GeV]" : "Charge tower [fC]");
  hEtower_Q->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
  TH1D* hEtower_P = new TH1D("hEtower_P", "Beam 1 only", nBins, Emin, Emax);
  hEtower_P->SetLineColor(kBlue);
  hEtower_P->SetLineStyle(2);
  hEtower_P->SetLineWidth(3);
  hEtower_P->SetXTitle(scaleIsGeV ? "Energy tower [GeV]" : "Charge tower [fC]");
  hEtower_P->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
  TH1D* hEtower_M = new TH1D("hEtower_M", "Beam 2 only", nBins, Emin, Emax);
  hEtower_M->SetLineColor(kGreen+2);
  hEtower_M->SetLineStyle(2);
  hEtower_M->SetLineWidth(3);
  hEtower_M->SetXTitle(scaleIsGeV ? "Energy tower [GeV]" : "Charge tower [fC]");
  hEtower_M->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
  
  TH1D* hEtowerNZS_Q = new TH1D("hEtowerNZS_Q", "No beam", nBins, Emin, Emax);
  hEtowerNZS_Q->SetLineColor(kRed);
  hEtowerNZS_Q->SetLineWidth(3);
  hEtowerNZS_Q->SetXTitle(scaleIsGeV ? "Energy tower [GeV]" : "Charge tower [fC]");
  hEtowerNZS_Q->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
  TH1D* hEtowerNZS_P = new TH1D("hEtowerNZS_P", "Beam 1 only", nBins, Emin, Emax);
  hEtowerNZS_P->SetLineColor(kBlue);
  hEtowerNZS_P->SetLineWidth(3);
  hEtowerNZS_P->SetXTitle(scaleIsGeV ? "Energy tower [GeV]" : "Charge tower [fC]");
  hEtowerNZS_P->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
  TH1D* hEtowerNZS_M = new TH1D("hEtowerNZS_M", "Beam 2 only", nBins, Emin, Emax);
  hEtowerNZS_M->SetLineColor(kGreen+2);
  hEtowerNZS_M->SetLineWidth(3);
  hEtowerNZS_M->SetXTitle(scaleIsGeV ? "Energy tower [GeV]" : "Charge tower [fC]");
  hEtowerNZS_M->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");

  TH1D* hEjet_Q = new TH1D("hEjet_Q", "No beam", nBins, Emin, Emax*14);
  hEjet_Q->SetLineColor(kRed);
  hEjet_Q->SetLineWidth(3);
  hEjet_Q->SetXTitle(scaleIsGeV ? "Energy jet [GeV]" : "Charge jet [fC]" );
  hEjet_Q->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
  TH1D* hEjet_P = new TH1D("hEjet_P", "Beam 1 only", nBins, Emin, Emax*14);
  hEjet_P->SetLineColor(kBlue);
  hEjet_P->SetLineWidth(3);
  hEjet_P->SetXTitle(scaleIsGeV ? "Energy jet [GeV]" : "Charge jet [fC]" );
  hEjet_P->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
  TH1D* hEjet_M = new TH1D("hEjet_M", "Beam 2 only", nBins, Emin, Emax*14);
  hEjet_M->SetLineColor(kGreen+2);
  hEjet_M->SetLineWidth(3);
  hEjet_M->SetXTitle(scaleIsGeV ? "Energy jet [GeV]" : "Charge jet [fC]");
  hEjet_M->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");

  TH1D* hEchannel_A = new TH1D("hEchannel_A", "All", nBins, Emin, Emax);
  hEchannel_A->SetLineColor(kBlack);
  hEchannel_A->SetLineStyle(2);
  hEchannel_A->SetLineWidth(3);
  hEchannel_A->SetXTitle(scaleIsGeV ? "Energy channel [GeV]" : "Charge channel [fC]");
  hEchannel_A->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
  TH1D* hEchannel_Q = new TH1D("hEchannel_Q", "No beam", nBins, Emin, Emax);
  hEchannel_Q->SetLineColor(kRed);
  hEchannel_Q->SetLineWidth(3);
  hEchannel_Q->SetXTitle(scaleIsGeV ? "Energy channel [GeV]" : "Charge channel [fC]");
  hEchannel_Q->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
  TH1D* hEchannel_P = new TH1D("hEchannel_P", "Beam 1 only", nBins, Emin, Emax);
  hEchannel_P->SetLineColor(kBlue);
  hEchannel_P->SetLineWidth(3);
  hEchannel_P->SetXTitle(scaleIsGeV ? "Energy channel [GeV]" : "Charge channel [fC]");
  hEchannel_P->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
  TH1D* hEchannel_M = new TH1D("hEchannel_M", "Beam 2 only", nBins, Emin, Emax);
  hEchannel_M->SetLineColor(kGreen+2);
  hEchannel_M->SetLineWidth(3);
  hEchannel_M->SetXTitle(scaleIsGeV ? "Energy channel [GeV]" : "Charge channel [fC]");
  hEchannel_M->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");

  TH1D* hEtot_A = new TH1D("hEtot_A", "All", nBins, Emin, Emax);
  hEtot_A->SetLineColor(kBlack);
  hEtot_A->SetLineWidth(3);
  hEtot_A->SetLineStyle(2);
  hEtot_A->SetXTitle(scaleIsGeV ? "Energy total [GeV]" : "Charge total [fC]");
  hEtot_A->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
  TH1D* hEtot_Q = new TH1D("hEtot_Q", "No beam", nBins, Emin, Emax);
  hEtot_Q->SetLineColor(kRed);
  hEtot_Q->SetLineWidth(3);
  hEtot_Q->SetXTitle(scaleIsGeV ? "Energy total [GeV]" : "Charge total [fC]");
  hEtot_Q->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
  TH1D* hEtot_P = new TH1D("hEtot_P", "Beam 1 only", nBins, Emin, Emax);  
  hEtot_P->SetLineColor(kBlue);
  hEtot_P->SetLineWidth(3);
  hEtot_P->SetXTitle(scaleIsGeV ? "Energy total [GeV]" : "Charge total [fC]");
  hEtot_P->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
  TH1D* hEtot_M = new TH1D("hEtot_M", "Beam 2 only", nBins, Emin, Emax);
  hEtot_M->SetLineColor(kGreen+2);
  hEtot_M->SetLineWidth(3);
  hEtot_M->SetXTitle(scaleIsGeV ? "Energy total [GeV]" : "Charge total [fC]");
  hEtot_M->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
  
  map<int, vector<TH1D*> > vecEchannel_Q;
  map<int, double> vecChannelMean, vecChannelVar;
  map<int, int> vecChannelCount;
  {
    const int run = 0; // gneric sum of all runs
    vecEchannel_Q[run] = vector<TH1D*>(224,0);            
    for (int iHist=0; iHist<224; ++iHist) {
      ostringstream hname, hname2, htitle;
      hname << "hEchannel_Q_" << iHist << "_run_" << run;
      htitle << "Quiet, PMT=" << iHist << ", run=" << run;
      vecEchannel_Q[run][iHist] = new TH1D(hname.str().c_str(), htitle.str().c_str(), nBins, Emin, Emax);
      vecEchannel_Q[run][iHist]->SetLineColor(kRed);
      vecEchannel_Q[run][iHist]->SetLineWidth(3);
      vecEchannel_Q[run][iHist]->SetXTitle(scaleIsGeV ? "Energy channel [GeV]" : "Charge channel [fC]");
      vecEchannel_Q[run][iHist]->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
    }
  }
  map<int, double> vecRunMean, vecRunVar;
  map<int, int> vecRunCount;
  map<int,TH1D*> vecERun_Q, vecERun_P, vecERun_M;
  {
    const int run = 0; // generic sum of all runs
    {
      ostringstream vecERunName, vecERunTitle;
      vecERunName << "Echannel_Run" << run << "_Q";
      vecERunTitle << "run: " << run << " Q";
      vecERun_Q[run] = new TH1D(vecERunName.str().c_str(), vecERunTitle.str().c_str(), nBins, Emin, Emax);
      vecERun_Q[run]->SetLineColor(kRed);
      vecERun_Q[run]->SetLineWidth(3);
      vecERun_Q[run]->SetXTitle(scaleIsGeV ? "Energy channel [GeV]" : "Charge channel [fC]");
      vecERun_Q[run]->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
    }
    {
      ostringstream vecERunName, vecERunTitle;
      vecERunName << "Echannel_Run" << run << "_P";
      vecERunTitle << "run: " << run << " P";
      vecERun_P[run] = new TH1D(vecERunName.str().c_str(), vecERunTitle.str().c_str(), nBins, Emin, Emax);
      vecERun_P[run]->SetLineColor(kBlue);
      vecERun_P[run]->SetLineWidth(3);
      vecERun_P[run]->SetXTitle(scaleIsGeV ? "Energy channel [GeV]" : "Charge channel [fC]");
      vecERun_P[run]->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
    }
    {
      ostringstream vecERunName, vecERunTitle;
      vecERunName << "Echannel_Run" << run << "_M";
      vecERunTitle << "run: " << run << " M";
      vecERun_M[run] = new TH1D(vecERunName.str().c_str(), vecERunTitle.str().c_str(), nBins, Emin, Emax);
      vecERun_M[run]->SetLineColor(kGreen+2);
      vecERun_M[run]->SetLineWidth(3);
      vecERun_M[run]->SetXTitle(scaleIsGeV ? "Energy channel [GeV]" : "Charge channel [fC]");
      vecERun_M[run]->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
    }
  }
  
  unsigned int countQuiet = 0;
  unsigned int countPlus = 0;
  unsigned int countMinus = 0;
  unsigned int countAll = 0;
  
  int fill = 0;
  int minDeltaBX = 0;
  vector<pair<int, int> > denseBeam;
  
  //
  // start loop events ####################################################################
  //
  const unsigned int nEvt = min(maxEvt, nEvtsTree);
  for (unsigned int iEvt=0; iEvt<nEvt; ++iEvt) {
    
    if (!(iEvt%100000)) {
      cout << "...processing event #" << iEvt << "/" << nEvt << " -> " << iEvt*100/nEvt << "%" << endl;
    }
    
    tree->GetEntry(iEvt);

    // see if run has known BX scheme
    if (run != previous_run) {

      previous_run = run;
      bool did_report = false; // do-it-once flag
      
      static bool readChanQual1 = false; // do-it-once flag
      if (run>=286500 && run<=286520) {       // for Run_2016D
        if (!readChanQual1) {
          readChanQual1 = true;
          bad_channels = read_bad_channel_data("BadChannels2016.txt");
        }
  /*if (run>=247000 && run<=248100) {       //for Run Empty BX Run_2015A
	  if (!readChanQual1) {
	    readChanQual1 = true;
	    bad_channels = read_bad_channel_data("CastorChannelQuality_v3.00_offline.txt");
	  }
  */
  } else {
	if (readChanQual1) {
	  cout << "Clear bad channel list" << endl;
	  readChanQual1 = false;
	  bad_channels.clear();
	}
      }

      // check BX scheme
      if (!read_bx_pattern(run, bx_pattern_plus, bx_pattern_minus, bx_pattern_both,
                           fill, minDeltaBX, denseBeam)) {
        failedToReadBXScheme = true;
        if (!useBPTX_alternatively)
          skip_run = run; // problem in reading bx_pattern, skip run! Or, alternatively, use BPTX....
      }
      failedToReadBXScheme = false;
      do_bx_filter = true;
      did_report = true;
      
      // during 2013 running
      if (run>=210100 && run<=212300) {
        if (bx_shift != bx_shift_2013) {
          bx_shift = bx_shift_2013;
          cout << "Switching to bx_shift=" << bx_shift << endl;
        }
      }
      // during LHCf-runs June 2015, and in particular 247920
      else if (run>=247000 && run<=248100) { // June 2015, LHCf run
        if (bx_shift != bx_shift_fill3855) {
          cout << "Switching to bx_shift=" << bx_shift_fill3855 << endl;
          bx_shift = bx_shift_fill3855;
        }
      } else if (run>=260858 && run<=263800) { // Nov 2015
        if (bx_shift != bx_shift_2015Nov) {
          cout << "Switching to bx_shift=" << bx_shift_2015Nov << endl;
          bx_shift = bx_shift_2015Nov;
        }
      } else if (run>284000 && run<286495) { // 2016, including ALICE run
        if (bx_shift != bx_shift_2016) {
          cout << "Switching to bx_shift=" << bx_shift_2016 << endl;
          bx_shift = bx_shift_2016;
        }
      }      
      else {
	/*if (do_bx_filter) {
	  did_report = true;
	  cout << "New run " << run << " : BX filter is OFF" << endl;
	}
	do_bx_filter = false;
        */
      }      
      if (report_runs && !did_report) {
	cout << "new run " << run << endl;
      }
    } // end run != previous_run
    
    // in case bunch crossing scheme cannot be determined:
    if (skip_run == run)
      continue;

    for (int iterBX : bx_pattern_plus)
      hLHC_P_count->Fill(iterBX + bx_shift);
    for (int iterBX : bx_pattern_minus)
      hLHC_M_count->Fill(iterBX + bx_shift);
    for (int iterBX : bx_pattern_both)
      hLHC_B_count->Fill(iterBX + bx_shift);

    
    // this is just for bunch crossing statistics and delta-BX:
    bool anyTrigger = false;
    bool anyAlgoTrigger = false;
    bool anyTechTrigger = false;
    if (trgl1L1GTAlgo) {
      for (int k=0; k<trgl1L1GTAlgo->size(); ++k) {
        if ((*trgl1L1GTAlgo)[k]) {
          if (!vecAutoAlgo.count(k)) {
            ostringstream name_act;
            name_act << "hAutoCorrAlgo_" << k;
            vecAutoAlgo[k] = new TH1D(name_act.str().c_str(), name_act.str().c_str(), 1001., -500.5, 500.5);
            vecAutoAlgo[k]->SetLineColor(kCyan+1);
          }
          for (int lhcBX : bx_pattern_both) {
            const int deltaBX = bxCms - lhcBX;
            vecAutoAlgo[k]->Fill(deltaBX);
          }
        }
        if ((run>=170000 && run <247000) || // 2013
            (run>=247036 && run<=248075) || // 2015
            (run>=250000)) {                // 2016
          if (k==1 || // L1_AlwaysTrue (2015+2013), L1_BptxPlus_NotBptxMinus (2016),
              k>10)   // L1_AlwaysTrue and stuff like beam halo muons etc.
            continue;
        } else if (run<170000) { // 2011
          if (k>1) //  	L1_PreCollisions L1_BeamGas_Bsc 	L1_BeamGas_Hf L1_InterBunch_Bsc L1_InterBunch_Hf (2011)
            continue;
        }
        if ((*trgl1L1GTAlgo)[k]) {
          anyTrigger = true;
          anyAlgoTrigger = true;
          //break; ... don't want this
        }
      }
    }
    if (trgl1L1GTTech) {
      for (int k=0; k<trgl1L1GTTech->size(); ++k) {
        if ((*trgl1L1GTTech)[k]) {
          if (!vecAutoTech.count(k)) {
            ostringstream name_act;
            name_act << "hAutoCorrTech_" << k;
            vecAutoTech[k] = new TH1D(name_act.str().c_str(), name_act.str().c_str(), 1001., -500.5, 500.5);
            vecAutoTech[k]->SetLineColor(kOrange+1);
          }
          for (int lhcBX : bx_pattern_both) {
            const int deltaBX = bxCms - lhcBX;          
            vecAutoTech[k]->Fill(deltaBX);
          }
        }
        if (k != 0 || // only BPTX-AND
            k != 9)   //  L1Tech_HCAL_HF_coincidence_PM.v2 (2013)
          continue;
        if (k==7 && k>12) // L1Tech_BPTX_quiet and all beyond HF minimum bias
          continue;
        if ((*trgl1L1GTTech)[k]) {
          anyTrigger = true;
          anyTechTrigger = true;
          // break; don't want this
        }
      }
    }
    for (int lhcBX : bx_pattern_both) {
      const int deltaBX = bxCms - lhcBX;
      //if (run <= 247900 ) continue; // warning: only for checks and development only !
      if (anyTrigger)
        hAutoCorr->Fill(deltaBX);
      if (anyAlgoTrigger)
        hAutoCorrAlgo->Fill(deltaBX);
      if (anyTechTrigger)
        hAutoCorrTech->Fill(deltaBX);
    }
    // ^^^ end BX statistics ^^^^

    
    // BX filter: exclude colliding BX
    if (do_bx_filter) {
      bool skip = failedToReadBXScheme; // can't do cut w/o BX scheme
      for (const int iBX : bx_pattern_both)
	if (abs(bxCms - bx_shift - iBX) < deltaBX_exclude) {
	  skip = true;
	}
      if (skip) {
	continue; // next event
      }
    } // end do_bx_filter   

    
    // trigger decision
    bool bxQuiet = true;
    bool bxPlus = false;
    bool bxMinus = false;
    for (const int iBX : bx_pattern_plus) {
      if (bxCms - bx_shift - iBX == 0) {
        bxPlus = true;
        bxQuiet = false;
      }
    }
    for (const int iBX : bx_pattern_minus) {
      if (bxCms - bx_shift - iBX == 0) {
        bxMinus = true;
        bxQuiet = false;
      }
    }
    /*
    if (checkBPTXalways || (failedToReadBXScheme && useBPTX_alternatively)) { 
      if (trgl1L1GTTech && trgl1L1GTTech->size()>0) {
        //bxQuiet = (*trgl1L1GTTech)[techBitQuiet];
        bxPlus = bxPlus || (*trgl1L1GTTech)[techBitPlus];
        bxMinus = bxMinus || (*trgl1L1GTTech)[
          techBitMinus];        
      } else {
        cout << "ERROR: failure to use trgl1L1GTTech" << endl;
        continue;
      }
      bxQuiet = !(bxPlus || bxMinus);
    }
    */
    
    if (checkBPTXalways || (failedToReadBXScheme && useBPTX_alternatively)) { 
      if (trgl1L1GTAlgo && trgl1L1GTAlgo->size()>0) {
        //bxQuiet = (*trgl1L1GTAlgo)[techBitQuiet];
        bxPlus = bxPlus || (*trgl1L1GTAlgo)[algoBitPlus];
        bxMinus = bxMinus || (*trgl1L1GTAlgo)[algoBitMinus];        
      } else {
        cout << "ERROR: failure to use trgl1L1Algo" << endl;
        continue;
      }
      bxQuiet = !(bxPlus || bxMinus);
    }
    
    
    //if (bxMinus && bxPlus) // -> ZeroBias, collision data, everything
    //continue;
    
    int count = 0;
    countAll++;
    if (bxQuiet) {
      count++;
      countQuiet++;
    }
    if (bxPlus) {
      count++;
      countPlus++;
    }
    if (bxMinus) {
      count++;
      countMinus++;
    }
    if (count>1) {
      cout << "ERROR in triggers... " << bxQuiet << " " << bxPlus << " " << bxMinus << endl;
      continue;
    }

    bool thisIsABadFill = false;
    /*
    switch(fill) {
    case 0:
    case 3834:
    case 3835:      
    case 3857:
    case 3858:
    case 3962:
    case 3965:
    case 3971:
      thisIsABadFill = true;
      //cout << "This is a bad fill: " << fill << endl;
    }
    */
    bool thisIsABadRun = false;
    /*    switch (run) {
    case 247066:
    case 247674:
    case 247350:
    case 247562:
    case 247568:
    case 247578:
    case 247667:
    case 247619:
    case 247669:
    case 247675:
    case 247699:
    case 248075:
    case 247976:
    case 247902:
      thisIsABadRun = true;
    }
    */
    
    bool thisIsABadBX = false;
    if (bxCms<=bx_shift)
      thisIsABadBX = true;      // otherwise we can end up in the abort gap
    if (skipDenseLHCregions) {
      for (const auto idense : denseBeam) {
        if ((bxCms - bx_shift) >= idense.first-50 &&
            (bxCms - bx_shift) <= idense.second+50) {
          thisIsABadBX = true;
        }
      }
    }
    /*
      if (bxCms<500 && bxCms>1000)
      thisIsABadBX = false;
    */
    
    // Channels and NZS-towers
    if (haveHits) {
      vector<double> towerNZS(16, 0.0);
      double sumTot = 0.0;
      for (unsigned int j=0; j<CastorRecHitEnergy->size(); ++j) {
	
  double Echannel;
	if (useIntercal){
     Echannel = (*CastorRecHitEnergy)[j]*intercalfactor[j]; 
  } else {
     Echannel = (*CastorRecHitEnergy)[j];
  }
	towerNZS[(((*CastorRecHitSector)[j]-1)%16)] += Echannel; // numbering is irrelevant
        // cout << j << " " << (*CastorRecHitSector)[j] << " " << (*CastorRecHitModule)[j] << endl;
        // ---->    j = module-1 + (sector-1)*14
        
	if (bxQuiet) {
          
          if (runsIndividually && vecEchannel_Q.count(run)==0) {
            vecEchannel_Q[run] = vector<TH1D*>(224,0);            
            for (int iHist=0; iHist<224; ++iHist) {
              ostringstream hname, htitle;
              hname << "hEchannel_Q_" << iHist << "_run_" << run;
              htitle << "Quiet, PMT=" << iHist << ", run=" << run;
              vecEchannel_Q[run][iHist] = new TH1D(hname.str().c_str(), htitle.str().c_str(), nBins, Emin, Emax);
              vecEchannel_Q[run][iHist]->SetLineColor(kRed);
              vecEchannel_Q[run][iHist]->SetLineWidth(3);
              vecEchannel_Q[run][iHist]->SetXTitle(scaleIsGeV ? "Energy channel [GeV]" : "Charge channel [fC]");
              vecEchannel_Q[run][iHist]->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
            }
          }
          if (!thisIsABadBX) {
            vecEchannel_Q[0][j]->Fill(Echannel);
            if (runsIndividually)
              vecEchannel_Q[run][j]->Fill(Echannel);
            if (vecChannelMean.count(j)==0) {
              vecChannelMean[j] = 0;
              vecChannelVar[j] = 0;
              vecChannelCount[j] = 0;
            }
            vecChannelMean[j] += Echannel;
            vecChannelVar[j] += Echannel*Echannel;
            vecChannelCount[j]++;
          }
        } // end quiet

        // ------------------------------- BAD CHANNEL CUT ----------------
        if (useBadChannelFlag) {
          if (bad_channels.count(j))
            continue;        
          if ((*CastorRecHitBad)[j])
            continue;
        }
        
        sumTot += Echannel;
        
	//h2d_castor->Fill((*CastorRecHitSector)[j]-1, (*CastorRecHitModule)[j]-1, Echannel);
        if (!thisIsABadRun && !thisIsABadFill  && !thisIsABadBX) 
          h2d_castor->Fill((*CastorRecHitModule)[j]-1, (*CastorRecHitSector)[j]-1, Echannel);
        
	hEchannel_A->Fill(Echannel);
        
	if (bxQuiet) {
          if (runsIndividually && vecERun_Q.count(run)==0) {
            ostringstream vecERunName, vecERunTitle;
            vecERunName << "Echannel_Run" << run << "_Q";
            vecERunTitle << "run: " << run << " Q";
            vecERun_Q[run] = new TH1D(vecERunName.str().c_str(), vecERunTitle.str().c_str(), nBins, Emin, Emax);
            vecERun_Q[run]->SetLineColor(kRed);
            vecERun_Q[run]->SetLineWidth(3);
            vecERun_Q[run]->SetXTitle(scaleIsGeV ? "Energy channel [GeV]" : "Charge channel [fC]");
            vecERun_Q[run]->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
          }
          // for run-based mean, rms
          if (!thisIsABadBX) {
            if (vecRunMean.count(run)==0) {
              vecRunMean[run] = 0;
              vecRunVar[run] = 0;
              vecRunCount[run] = 0;
            }
            vecRunMean[run] += Echannel;
            vecRunVar[run] += Echannel*Echannel;
            vecRunCount[run]++;
            vecERun_Q[0]->Fill(Echannel); // sum of all runs
            if (runsIndividually) {
              vecERun_Q[run]->Fill(Echannel);            
              if (thisIsABadRun) 
                vecERun_Q[run]->SetLineStyle(2);            
              if (thisIsABadFill)
                vecERun_Q[run]->SetLineStyle(3);
            }
          } // end bad BX
          if (!thisIsABadRun && !thisIsABadFill  && !thisIsABadBX)
            hEchannel_Q->Fill(Echannel);         
	} // end quiet
	if (bxPlus) {
          if (runsIndividually && vecERun_P.count(run)==0) {
            ostringstream vecERunName, vecERunTitle;
            vecERunName << "Echannel_Run" << run << "_P";
            vecERunTitle << "run: " << run << " P";
            vecERun_P[run] = new TH1D(vecERunName.str().c_str(), vecERunTitle.str().c_str(), nBins, Emin, Emax);
            vecERun_P[run]->SetLineColor(kBlue);
            vecERun_P[run]->SetLineWidth(3);
            vecERun_P[run]->SetXTitle(scaleIsGeV ? "Energy channel [GeV]" : "Charge channel [fC]");
            vecERun_P[run]->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
          }
          vecERun_P[0]->Fill(Echannel);          
          if (runsIndividually) {
            vecERun_P[run]->Fill(Echannel);          
            if (thisIsABadRun)
              vecERun_P[run]->SetLineStyle(2);
            if (thisIsABadFill)
              vecERun_P[run]->SetLineStyle(3);
          }
          if (!thisIsABadRun && !thisIsABadFill  && !thisIsABadBX) 
            hEchannel_P->Fill(Echannel);
	} // end plus        
	if (bxMinus) {
          if (runsIndividually && vecERun_M.count(run)==0) {
            ostringstream vecERunName, vecERunTitle;
            vecERunName << "Echannel_Run" << run << "_M";
            vecERunTitle << "run: " << run << " M";
            vecERun_M[run] = new TH1D(vecERunName.str().c_str(), vecERunTitle.str().c_str(), nBins, Emin, Emax);
            vecERun_M[run]->SetLineColor(kGreen+2);
            vecERun_M[run]->SetLineWidth(3);
            vecERun_M[run]->SetXTitle(scaleIsGeV ? "Energy channel [GeV]" : "Charge channel [fC]");
            vecERun_M[run]->SetYTitle(scaleIsGeV ? "1/N_{evts} dN/dE [1/GeV]" : "1/N_{evts} dN/dQ [1/fC]");
          }
          vecERun_M[0]->Fill(Echannel);
          if (runsIndividually) {
            vecERun_M[run]->Fill(Echannel);
            if (thisIsABadRun)
              vecERun_M[run]->SetLineStyle(2);
            if (thisIsABadFill)
              vecERun_M[run]->SetLineStyle(3);
          }
          if (!thisIsABadRun && !thisIsABadFill  && !thisIsABadBX) {
            hEchannel_M->Fill(Echannel);
          }
	} // end minus
      } // end CHANNELS
      
      hBX->Fill(bxCms, sumTot);
      hBX_count->Fill(bxCms);
      
      if (thisIsABadRun || thisIsABadFill)
        continue;
      
      if (thisIsABadBX)
        continue;

      // TOWERS-NZS
      for (unsigned int j=0; j<towerNZS.size(); ++j) {
	const double Etower = towerNZS[j];
	//hEtowerNZS_A->Fill(Etower);
	if (bxQuiet) {
	  hEtowerNZS_Q->Fill(Etower);
	}
	if (bxPlus) {
	  hEtowerNZS_P->Fill(Etower);
	}
	if (bxMinus) {
	  hEtowerNZS_M->Fill(Etower);
	}
      }// end TOWERS-NZS
      
      
      { // TOTAL CASTOR
	hEtot_A->Fill(sumTot);
	if (bxQuiet) {
	  hEtot_Q->Fill(sumTot);
	}
	if (bxPlus) {
	  hEtot_P->Fill(sumTot);
	}
	if (bxMinus) {
	  hEtot_M->Fill(sumTot);
	}

      } // end TOTAL CASTOR

    } // have hits

    if (thisIsABadRun || thisIsABadFill || thisIsABadBX)
      continue;

        
    // TOWERS
    if (haveTowers) {
      for (unsigned int j=0; j<CastorTowerp4->size(); ++j) {
	const double Etower = (*CastorTowerp4)[j].E();
	hEtower_A->Fill(Etower);
	if (bxQuiet) {
	  hEtower_Q->Fill(Etower);
	}
	if (bxPlus) {
	  hEtower_P->Fill(Etower);
	}
	if (bxMinus) {
	  hEtower_M->Fill(Etower);
	}
      }// end TOWERS
    }
    
    // JETS
    if (haveJets) {
      for (unsigned int j=0; j<CastorJetsP4->size(); ++j) {
	//hEjet_A->Fill((*CastorJetsP4)[j].E());
	if (bxQuiet) {
	  hEjet_Q->Fill((*CastorJetsP4)[j].E());
	}
	if (bxPlus) {
	  hEjet_P->Fill((*CastorJetsP4)[j].E());
	}
	if (bxMinus) {
	  hEjet_M->Fill((*CastorJetsP4)[j].E());
	}
      }
    }// end JETS
            
  } // end loop events ###############################################



  
  // find max(deltaBX)
  list<pair<int, double>> sortedBins;
  for (int i=1; i<hAutoCorr->GetNbinsX()+1; ++i) {
    const double binC = hAutoCorr->GetBinContent(i);
    list<pair<int,double>>::iterator iL = sortedBins.begin();
    for (;iL != sortedBins.end(); ++iL) {
      if (binC > iL->second)
        break;
    }
    sortedBins.insert(iL, make_pair(hAutoCorr->GetBinCenter(i), binC));
  }
  
  list<pair<int,double>>::iterator iL = sortedBins.begin();
  int cbins = 0;
  double threshold = 0;
  bool fat_warning = false;
  for (;iL != sortedBins.end(); ++iL) {
    if (threshold==0) {
      threshold = iL->second - 4*sqrt(iL->second);
      if (iL->second == 0)
        fat_warning = true; 
      else if (iL->first != bx_shift) {
        cout << "\n\n"
             << "************************************************************\n"
             << "************************************************************\n\n"
             << " WARNING: bx_shift=" << bx_shift << ", but maximum deltaBX correlation=" << iL->first << " (check trigger mix)" << endl
             << "\n If there is a BX-shift the classification in QUIET/PLUS/MINUS is wrong !!!!" << endl << endl
             << "************************************************************\n"
             << "************************************************************\n\n"
             << endl;
      }
    }
    cout << " sorted bins: deltaBX=" << iL->first << " Ncount=" << iL->second
         << (iL->second>threshold ? "" : "  (below 4sigma)")
         << endl;
    if (cbins++ > 20)
      break;
  }  
  if (fat_warning) {
    cout << "\n************************************************************\n"
         << "************************************************************\n"
         << " BIG FAT WARNING: No triggers algo/tech found! Maybe this is EmptyBX? \n"
         << "************************************************************\n"
         << "************************************************************\n\n"
         << endl;
  }
  
  TText* cms = new TText(0.8, 0.87, "CMS");
  cms->SetNDC(1);
  cms->SetTextSize(0.07);

  ostringstream cnameTFile;
  cnameTFile << newFolder << "/NoiseAnalysisOut.root";
  TFile* fOut = TFile::Open(cnameTFile.str().c_str(), "recreate");
  
  TCanvas* canBX = new TCanvas("canBX", "castor signal vs. BX");
  hBX->Draw();
  hBX->Write();
  hBX_count->Scale(hBX->GetMaximum()/hLHC_B_count->GetMaximum()/2);
  hBX_count->Draw("same,p");
  hBX_count->Write();
  hLHC_M_count->Scale(hBX->GetMaximum()/hLHC_B_count->GetMaximum()/2);
  hLHC_P_count->Scale(hBX->GetMaximum()/hLHC_B_count->GetMaximum()/2);
  hLHC_B_count->Scale(hBX->GetMaximum()/hLHC_B_count->GetMaximum()/2);
  hLHC_M_count->Draw("same");
  hLHC_M_count->Write();
  hLHC_P_count->Draw("same");
  hLHC_P_count->Write();
  hLHC_B_count->Draw("same,p");
  hLHC_B_count->Write();
  cms->Draw();

  {  
    TCanvas* canAuto = new TCanvas("canAuto", "deltaBX auto correlation");
    canAuto->SetLogy(1);
    bool first = true;
    plotThis(hAutoCorr, 0, 1., first, scaleIsGeV);
    plotThis(hAutoCorrTech, 0, 1., first, scaleIsGeV);
    plotThis(hAutoCorrAlgo, 0, 1., first, scaleIsGeV);
    hAutoCorr->Write();
    hAutoCorrTech->Write();
    hAutoCorrAlgo->Write();
    for (auto h : vecAutoTech) {
      plotThis(h.second, 0, 1., first, scaleIsGeV);
      h.second->Write();
    }
    for (auto h:vecAutoAlgo) {
      plotThis(h.second, 0, 1., first, scaleIsGeV);
      h.second->Write();
    }
    cms->Draw();
  }
  ostringstream cname01;
  TCanvas* can2D = new TCanvas("can2D", "castor 2d");
  can2D->SetLogz(1);
  h2d_castor->Draw("colz");
  h2d_castor->Write();
  cms->Draw();
  cname01 << newFolder << "/Energy2D.pdf";
  can2D->SaveAs(cname01.str().c_str());
  
  // Gaussian noise fit
  TF1* fit = new TF1("fit", "gaus", -1e5, 1e6);

  TH1D* hFitMuPMT = new TH1D("hFitMuPMT", "hFitMuPMT", 224, 0, 224);
  hFitMuPMT->SetXTitle("Channel");
  hFitMuPMT->SetYTitle(scaleIsGeV ? "Mean [GeV]" : "Mean [fC]");
  
  TH1D* hFitSigmaPMT = new TH1D("hFitSigmaPMT", "hFitSigmaPMT", 224, 0, 224);
  hFitSigmaPMT->SetXTitle("Channel");
  hFitSigmaPMT->SetYTitle(scaleIsGeV ? "RMS [GeV]" : "RMS [fC]");
  
  for (const auto& it : vecChannelMean) {
    const int ich = it.first;
    const double mu = vecChannelMean[ich] / vecChannelCount[ich];
    const double rms = sqrt(vecChannelVar[ich] / vecChannelCount[ich] - mu*mu);
    // fit
    // for (const auto& iRun : vecEchannel_Q) {
    vecEchannel_Q[0][ich]->Fit(fit, "L");    
    cout << " Channel dependence: " << ich << " n=" << vecChannelCount[ich] << " mean=" << mu << " rms=" << rms
         << " mu="<< fit->GetParameter(1) << "+-" << fit->GetParError(1)
         << " sigma="<< fit->GetParameter(2) << "+-" << fit->GetParError(2)
         << endl;
    hFitMuPMT->Fill(ich, fit->GetParameter(1));
    hFitSigmaPMT->Fill(ich, fit->GetParameter(2));
  }
  ostringstream cname0;
  TCanvas* canFit = new TCanvas("canFit", "canFit");
  canFit->Divide(2);
  canFit->cd(1);
  hFitMuPMT->Draw("");
  hFitMuPMT->Write();
  canFit->cd(2);
  hFitSigmaPMT->Draw("");
  hFitSigmaPMT->Write();
  cname0 << newFolder << "/GausFitNoise.pdf";
  canFit->SaveAs(cname0.str().c_str());
  
  for (const auto& iRun : vecEchannel_Q) {
    
    const int run = iRun.first;
    
    ostringstream hn1, hn2, hn3, hn4;
    hn1 << "hMeanPMT";
    if (run>0)
      hn1 << "_" << run;
    TH1D* hMeanPMT = new TH1D(hn1.str().c_str(), hn1.str().c_str(), 224, 0, 224);
    hMeanPMT->SetXTitle("Channel");
    hMeanPMT->SetYTitle(scaleIsGeV ? "Mean [GeV]" : "Mean [fC]");
    
    hn2 << "hRmsPMT";
    if (run>0)
      hn2 << "_" << run;
    TH1D* hRmsPMT = new TH1D(hn2.str().c_str(), hn2.str().c_str(), 224, 0, 224);
    hRmsPMT->SetXTitle("Channel");
    hRmsPMT->SetYTitle(scaleIsGeV ? "RMS [GeV]" : "RMS [fC]");
    
    hn3 << "hRmsPMT2D";
    if (run>0)
      hn3 << "_" << run;
    TH2D* hRmsPMT2D = new TH2D(hn3.str().c_str(), hn3.str().c_str(), 14, 0, 14, 16, 0, 16);
    hRmsPMT2D->SetXTitle("Module");
    hRmsPMT2D->SetYTitle("Sector");
    hRmsPMT2D->SetZTitle(scaleIsGeV ? "RMS [GeV]" : "RMS [fC]");
    
    hn4 << "hMeanPMT2D";
    if (run>0)
      hn4 << "_" << run;
    TH2D* hMeanPMT2D = new TH2D(hn4.str().c_str(), hn4.str().c_str(), 14, 0, 14, 16, 0, 16);
    hMeanPMT2D->SetXTitle("Module");
    hMeanPMT2D->SetYTitle("Sector");
    hMeanPMT2D->SetZTitle(scaleIsGeV ? "MEAN [GeV]" : "MEAN [fC]");   
    
    ostringstream cname;

    cname << newFolder << "/AllPMTs";
    if (run>0)
      cname << "_run_" << run;
    TCanvas* canAllPMT = new TCanvas(cname.str().c_str(), cname.str().c_str());
    canAllPMT->Divide(14,16);
    for (int iHist=0; iHist<224; ++iHist) {
      canAllPMT->cd(iHist+1);
      gPad->SetLogy(1);
      vecEchannel_Q[run][iHist]->Draw();
      vecEchannel_Q[run][iHist]->Write();
      hMeanPMT->Fill(iHist, vecEchannel_Q[run][iHist]->GetMean());
      hRmsPMT->Fill(iHist, vecEchannel_Q[run][iHist]->GetRMS());
      hMeanPMT2D->Fill(iHist%14, iHist/14, vecEchannel_Q[run][iHist]->GetMean());
      hRmsPMT2D->Fill(iHist%14, iHist/14, vecEchannel_Q[run][iHist]->GetRMS());
    }
    cname << ".pdf";
    
    canAllPMT->SaveAs(cname.str().c_str());
    
    ostringstream cname2;
    cname2 << newFolder << "/mean_rms";
    if (run>0)
      cname2 << "_run_" << run;
    TCanvas* canMeanRMS = new TCanvas(cname2.str().c_str(), cname2.str().c_str(), 1000, 1000);
    canMeanRMS->Divide(2,2);
    canMeanRMS->cd(1); //-
    hMeanPMT->Draw();
    hMeanPMT->Write();
    canMeanRMS->cd(2); //-
    gPad->SetLogy(1);
    hRmsPMT->Draw();
    hRmsPMT->Write();
    canMeanRMS->cd(3); //-
    hMeanPMT2D->Draw("colz");
    hMeanPMT2D->Write();
    canMeanRMS->cd(4); //-
    gPad->SetLogz(1);
    hRmsPMT2D->Draw("colz");
    hRmsPMT2D->Write();
    cname2 << ".pdf";

    canMeanRMS->SaveAs(cname2.str().c_str());    
  }

  
  for (const auto& it : vecRunMean) {
    const int run = it.first;
    const double mu = vecRunMean[run] / vecRunCount[run];
    const double rms = sqrt(vecRunVar[run] / vecRunCount[run] - mu*mu);
    cout << " Run dependence: " << run << " n=" << vecRunCount[run] << " mu=" << mu << " rms=" << rms << endl;
  }
  ostringstream cname3;
  TCanvas* canAllRuns = new TCanvas("canAllRuns", "Run summary");
  const int nRuns = max(max(vecERun_Q.size(), vecERun_P.size()), vecERun_M.size());
  canAllRuns->Divide(int(sqrt(nRuns)), int(sqrt(nRuns))+1);
  int irun = 0;
  for (auto h:vecERun_Q) {    
    canAllRuns->cd(irun+1);
    cout << "----------------------" << endl;
    gPad->SetLogy(1);
    {
      if (h.second->GetEntries()) {
        h.second->Scale(1./h.second->GetEntries());
      }
      ostringstream tit;
      tit << h.second->GetTitle() << " m=" << h.second->GetMean() << " r=" << h.second->GetRMS();
      cout << tit.str() << endl;
      h.second->SetTitle(tit.str().c_str());
      h.second->Draw();
      h.second->Write();
    }
    if (vecERun_P.count(h.first)) {
      if (vecERun_P[h.first]->GetEntries()) {
        vecERun_P[h.first]->Scale(1./vecERun_P[h.first]->GetEntries());
      }
      ostringstream tit;
      tit << vecERun_P[h.first]->GetTitle() << " m=" << vecERun_P[h.first]->GetMean() << " r=" << vecERun_P[h.first]->GetRMS();
      cout << tit.str() << endl;
      vecERun_P[h.first]->SetTitle(tit.str().c_str());
      vecERun_P[h.first]->Draw("same");
      vecERun_P[h.first]->Write();
    } else {
      //cout << "ERROR 1 in vecERun_P" << endl;
    }
    if (vecERun_M.count(h.first)) {
      if (vecERun_M[h.first]->GetEntries()) {
        vecERun_M[h.first]->Scale(1./vecERun_M[h.first]->GetEntries());
      }
      ostringstream tit;
      tit << vecERun_M[h.first]->GetTitle() << " m=" << vecERun_M[h.first]->GetMean() << " r=" << vecERun_M[h.first]->GetRMS();
      cout << tit.str() << endl;
      vecERun_M[h.first]->SetTitle(tit.str().c_str());
      vecERun_M[h.first]->Draw("same");
      vecERun_M[h.first]->Write();
    } else {
      //cout << "ERROR 1 in vecERun_M" << endl;
    }
    gPad->BuildLegend();
    ++irun;    
  }
  cname3 << newFolder << "/SummaryRuns.pdf";
  canAllRuns->SaveAs(cname3.str().c_str());
  
    
  if (haveTowers) {
    TCanvas* canTower = new TCanvas("canTower", "towers");
    canTower->SetLogy(1);
    hEtowerNZS_M->Scale(1./(hEtowerNZS_M->GetBinWidth(1)*countMinus));
    hEtowerNZS_P->Scale(1./(hEtowerNZS_P->GetBinWidth(1)*countPlus));
    hEtowerNZS_Q->Scale(1./(hEtowerNZS_Q->GetBinWidth(1)*countQuiet));
    hEtowerNZS_M->Draw("");
    hEtowerNZS_P->Draw("same");
    hEtowerNZS_Q->Draw("same");
    hEtowerNZS_Q->Write();
    hEtowerNZS_P->Write();
    hEtowerNZS_M->Write();
    hEtower_M->Scale(1./(hEtower_M->GetBinWidth(1)*countMinus));
    hEtower_P->Scale(1./(hEtower_P->GetBinWidth(1)*countPlus));
    hEtower_Q->Scale(1./(hEtower_Q->GetBinWidth(1)*countQuiet));
    hEtower_M->Draw("same");
    hEtower_P->Draw("same");
    hEtower_Q->Draw("same");
    hEtower_Q->Write();
    hEtower_P->Write();
    hEtower_M->Write();

    //hEtower_M->SetMinimum(hEtower_P->GetMinimum());
    //canTower->Modified();
    //canTower->Update();
    ostringstream cname4;
    TLegend* legTower = new TLegend(0.35,0.55,0.6,0.8);
    legTower->SetTextFont(42);
    legTower->SetTextSize(0.045);
    legTower->SetBorderSize(0);
    ToLegend(legTower, hEtowerNZS_Q, scaleIsGeV);
    ToLegend(legTower, hEtowerNZS_P, scaleIsGeV);
    ToLegend(legTower, hEtowerNZS_M, scaleIsGeV);
    TGraph* gDummy = new TGraph();
    gDummy->SetLineColor(kBlack);
    gDummy->SetLineStyle(2);
    gDummy->SetLineWidth(3);
    legTower->AddEntry(gDummy, "with zero suppression", "l");
    legTower->Draw();
    cms->Draw();
    cname4 << newFolder << "/CastorNoiseTower.pdf";
    canTower->SaveAs(cname4.str().c_str());
  }

  if (haveJets) {
    ostringstream cname5;
    TCanvas* canJet = new TCanvas("canJet", "jets");
    canJet->SetLogy(1);
    hEjet_M->Scale(1./(hEjet_M->GetBinWidth(1)*countMinus));
    hEjet_P->Scale(1./(hEjet_P->GetBinWidth(1)*countPlus));
    hEjet_Q->Scale(1./(hEjet_Q->GetBinWidth(1)*countQuiet));
    hEjet_P->Draw(""); // to get right scale ...
    hEjet_M->Draw("same");
    hEjet_P->Draw("same");
    hEjet_Q->Draw("same"); 
    hEjet_Q->Write();
    hEjet_P->Write();
    hEjet_M->Write();
    //hEjet_M->SetMinimum(hEjet_P->GetMinimum());
    //canJet->Modified();
    //canJet->Update();
    TLegend* legJet = new TLegend(0.55,0.6,0.9,0.8);
    legJet->SetTextFont(42);
    legJet->SetTextSize(0.045);
    legJet->SetBorderSize(0);
    //ToLegend(legJet, hEjet_Q);
    //ToLegend(legJet, hEjet_P);
    //ToLegend(legJet, hEjet_M);->AddEntry(hEjet_M, "Beam 2 only", "l");
    legJet->AddEntry(hEjet_Q, "No beams", "l");
    legJet->AddEntry(hEjet_P, "Beam 1 only", "l");
    legJet->AddEntry(hEjet_M, "Beam 2 only", "l");
    legJet->Draw();
    cms->Draw();
    cname5 << newFolder << "/CastorNoiseJet.pdf";
    canJet->SaveAs(cname5.str().c_str());
  }
  
  if (haveHits) {
    TCanvas* canChannel = new TCanvas("canChannel", "channels");
    canChannel->SetLogy(1);
    TLegend* legChannel = new TLegend(0.35,0.6,0.6,0.8);
    legChannel->SetTextFont(42);
    legChannel->SetTextSize(0.045);
    legChannel->SetBorderSize(0);
    {
      bool first = true;
      plotThis(hEchannel_Q, legChannel, countQuiet, first, scaleIsGeV);
      plotThis(hEchannel_M, legChannel, countMinus, first, scaleIsGeV);
      plotThis(hEchannel_P, legChannel, countPlus, first, scaleIsGeV);
      plotThis(hEchannel_A, legChannel, countAll, first, scaleIsGeV);
      hEchannel_A->Write();
      hEchannel_M->Write();
      hEchannel_P->Write();
      hEchannel_Q->Write();
    }
    ostringstream cname6;
    //hEchannel_M->SetMinimum(hEchannel_P->GetMinimum());
    //canChannel->Modified();
    //canChannel->Update();
    legChannel->Draw();
    cms->Draw();
    cname6 << newFolder << "/CastorNoiseChannel.pdf";
    canChannel->SaveAs(cname6.str().c_str());

    ostringstream cname7;
    TCanvas* canTot = new TCanvas("canTot", "total");
    canTot->SetLogy(1);
    TLegend* legTot = new TLegend(0.35,0.6,0.6,0.8);
    legTot->SetTextFont(42);
    legTot->SetTextSize(0.045);
    legTot->SetBorderSize(0);
    {
      bool first = true;
      plotThis(hEtot_Q, legTot, countQuiet, first, scaleIsGeV);
      plotThis(hEtot_M, legTot, countMinus, first, scaleIsGeV);
      plotThis(hEtot_P, legTot, countPlus, first, scaleIsGeV);
      plotThis(hEtot_A, legTot, countAll, first, scaleIsGeV);
      hEtot_A->Write();
      hEtot_M->Write();
      hEtot_P->Write();
      hEtot_Q->Write();
    }
    //hEtot_M->SetMinimum(hEtot_P->GetMinimum());
    //canTot->Modified();
    //canTot->Update();
    legTot->Draw();
    cms->Draw();
    cname7 << newFolder << "/CastorNoiseTot.pdf";
    canTot->SaveAs(cname7.str().c_str());
  }

  fOut->Flush();
}

void
help(const string cmd)
{
  cout << "usage: " << cmd << " input-sample-directory [GeV/fC] [max-events]" << endl;
}


int
main(int argc, char** argv)
{
  if (argc < 2) {
    help(argv[0]);
    return 1;
  }
  
  const string dir(argv[1]);

  const unsigned int maxEvt = argc>3 ? atof(argv[3]) : 1e9;
  bool scaleIsGeV = false; // default is fC
  if (argc>2) {
    if (string(argv[2]) == "GeV")
      scaleIsGeV = true;
    else if (string(argv[2]) == "fC")
      scaleIsGeV = false;
    else {
      help(argv[0]);
      return 2;
    }

  // added by Koller //
  newFolder=argv[4];

  
  const int dir_err = mkdir(newFolder.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  if (-1 == dir_err)
  {
      printf("Error creating directory \n");
      printf("This is input: %s",argv[4]);
      exit(1);
  }
  
  }

  cout << "Maximum number of events to read is: " << maxEvt << " unit is " << (scaleIsGeV ? "GeV" : "fC") << endl;
  
  setTDRStyle();
  gStyle->SetPalette(1);
  TApplication run("holder",0,0);
  CastorNoise(dir, maxEvt, scaleIsGeV);
  run.Run();
  return 0;
}
