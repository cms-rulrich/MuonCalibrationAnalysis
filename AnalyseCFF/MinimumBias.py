#!/usr/bin/env python
#
#

import CommonFSQFramework.Core.ExampleProofReader

import sys, os, time
sys.path.append(os.path.dirname(__file__))
from os import listdir
from os.path import isfile, join
import ROOT
from math import sqrt, log10, log
from array import *

import numpy as np


class MinimumBias(CommonFSQFramework.Core.ExampleProofReader.ExampleProofReader):
    def init( self, maxEvents = None):

        self.maxEvents = maxEvents

        self.didCheckPF = False
        self.hasPF = False
        self.hasTower = False

        self.runs = {}
        
        self.hist = {}
        self.hist["EventCounter"] = ROOT.TH1D("EventCounter","EventCounter",10,0,20)

        self.tree_run = array('i', [0] )
        self.tree_bx = array('i', [0] )
        self.tree_LS = array('i', [0] )
        self.tree_hfM_pf = array('f', [0.] )
        self.tree_hfP_pf = array('f', [0.] )
        self.tree_hfM = array('f', [0.] )
        self.tree_hfP = array('f', [0.] )
        self.tree_cas = array('f', [0.] )
        self.tree_nVertex = array('i', [0] )
        #d = array( 'f', maxn*[ 0. ] )
        self.hist["tree"] = ROOT.TTree("tree", "tree")
        self.hist["tree"].Branch( 'run', self.tree_run, 'run/I' )
        self.hist["tree"].Branch( 'bx', self.tree_bx, 'bx/I' )
        self.hist["tree"].Branch( 'LS', self.tree_LS, 'LS/I' )
        self.hist["tree"].Branch( 'hfM_pf', self.tree_hfM_pf, 'hfM_pf/F' )
        self.hist["tree"].Branch( 'hfP_pf', self.tree_hfP_pf, 'hfP_pf/F' )
        self.hist["tree"].Branch( 'hfM', self.tree_hfM, 'hfM/F' )
        self.hist["tree"].Branch( 'hfP', self.tree_hfP, 'hfP/F' )
        self.hist["tree"].Branch( 'cas', self.tree_cas, 'cas/F' )
        self.hist["tree"].Branch( 'nVertex', self.tree_nVertex, 'nVertex/I' )
        # t.Branch( 'myval', d, 'myval[mynum]/F' )

        self.hist["nVertex"] = ROOT.TH1D("nVertex", "nVertex", 10, 0, 10)

        self.hist["dEdEta"] = ROOT.TH1D("dEdEta", "dEdEta", 50, -7, 7)
        self.hist["dNdEta"] = ROOT.TH1D("dNdEta", "dNdEta", 50, -7, 7)
        #        self.hist["hfp_tower"] = ROOT.TH1D("hfp_tower", "hfp_tower", 200, -5, 500)

        # the input noise/rms levels in HF
        self.hist["hfm_pf_tower"] = ROOT.TH1D("hfm_pf_tower", "hfm_pf_tower", 300, -5, 150)
        self.hist["hfp_pf_tower"] = ROOT.TH1D("hfp_pf_tower", "hfp_pf_tower", 300, -5, 150)
        self.hist["hfm_pf_total"] = ROOT.TH1D("hfm_pf_total", "hfm_pf_total", 300, -5, 150)
        self.hist["hfp_pf_total"] = ROOT.TH1D("hfp_pf_total", "hfp_pf_total", 300, -5, 150)

        self.hist["hfm_tow_tower"] = ROOT.TH1D("hfm_tow_tower", "hfm_tow_tower", 300, -5, 150)
        self.hist["hfp_tow_tower"] = ROOT.TH1D("hfp_tow_tower", "hfp_tow_tower", 300, -5, 150)
        self.hist["hfm_tow_total"] = ROOT.TH1D("hfm_tow_total", "hfm_tow_total", 300, -5, 150)
        self.hist["hfp_tow_total"] = ROOT.TH1D("hfp_tow_total", "hfp_tow_total", 300, -5, 150)

        # the castor data
        self.hist["castor_channel_2d_hfand"] =  ROOT.TH2D("castor_channel_2d_hfand", "castor_channel_2d_hfand", 14, 0.5, 14.5, 16, 0.5, 16.5)
        self.hist["castor_channel_2d_hfp"] =  ROOT.TH2D("castor_channel_2d_hfp", "castor_channel_2d_hfp", 14, 0.5, 14.5, 16, 0.5, 16.5)
        self.hist["castor_channel_2d_hfm"] =  ROOT.TH2D("castor_channel_2d_hfm", "castor_channel_2d_hfm", 14, 0.5, 14.5, 16, 0.5, 16.5)
        self.hist["castor_channel_2d"] =  ROOT.TH2D("castor_channel_2d", "castor_channel_2d", 14, 0.5, 14.5, 16, 0.5, 16.5)
        self.hist["castor_tower"] =  ROOT.TH1D("castor_tower", "castor_tower", 300, -5, 3000)
        self.hist["castor_total"] =  ROOT.TH1D("castor_total", "castor_total", 300, -5, 3000)

#        for isec in xrange(0,16):
#            henergy = 'MuonSignal_sec_{sec}'.format(sec=str(isec+1))
#            self.hist[henergy] = ROOT.TH1D(henergy, henergy, 200, -50, 6000)
#            
#            for imod in xrange(0,14):
#                hname1 = 'MuonSignal_sec_{sec}_mod_{mod}'.format(sec=str(isec+1), mod=str(imod+1))
#                hname2 = 'NoiseSignal_sec_{sec}_mod_{mod}'.format(sec=str(isec+1), mod=str(imod+1))
#                self.hist[hname1] = ROOT.TH1D(hname1, hname1, len(binsMuon)-1, array('d',binsMuon)) # muons
#                self.hist[hname2] = ROOT.TH1D(hname2, hname2, len(binsNoise)-1, array('d',binsNoise))   # noise

        # store all histograms
        for h in self.hist:
            if not "TTree" in self.hist[h].ClassName():
                self.hist[h].Sumw2()
            self.GetOutputList().Add(self.hist[h])


            
    def analyze(self):

#        if (self.fChain.run != 285244):
#        if (self.fChain.run != 285216):
#            return 0
        
#        if (self.theSample == "data_ForwardTriggers_2011_Mar") :
#            if (self.fChain.run<160575 or self.fChain.run>161230) :
#                return 0            
            
        self.hist["EventCounter"].Fill("all", 1)

        #if (self.hist["EventCounter"].GetBinContent(1) % 100 != 0):
        #    return 0

        if self.fChain.CastorRecHitEnergy.size() != 224:
            return 0

        self.hist["EventCounter"].Fill("Data Valid",1)

        # read CASTOR RecHits
        energy_ch = [[0 for _ in xrange(14)] for _ in xrange(16)]
        for ich in range(self.fChain.CastorRecHitEnergy.size()):
            isec = ich//14
            imod = ich%14
            energy_ch[isec][imod] = self.fChain.CastorRecHitEnergy.at(ich)
            
            
        ###########################
        # do the HF-AND           #
        ###########################

        hfp_pf_energy_tot = 0
        hfm_pf_energy_tot = 0
        hfp_pf_energy_max = 0
        hfm_pf_energy_max = 0

        hfp_tow_energy_tot = 0
        hfm_tow_energy_tot = 0
        hfp_tow_energy_max = 0
        hfm_tow_energy_max = 0

        # first check for PF
        if (not self.didCheckPF):
            self.didCheckPF = True
            for branch in self.fChain.GetListOfBranches():
                if( branch.GetName() == "PFCandidatesp4"):
                    self.hasPF = True
                    print "--------------------------------------------------- DATA has ParticleFlow content "
                if( branch.GetName() == "CaloTowersp4"):
                    self.hasTower = True
                    print "--------------------------------------------------- DATA has CaloTower content "


        if (self.hasPF):

            for ipf in range(self.fChain.PFCandidatesp4.size()):
            
                eta = self.fChain.PFCandidatesp4[ipf].eta()
                energy = self.fChain.PFCandidatesp4[ipf].energy()
                #energy = self.fChain.PFCandidatesrawHcalEnergy[ipf] + self.fChain.PFCandidatesrawEcalEnergy[ipf]

                if (abs(eta)>3.2 and abs(eta)<4.9) :

                    if (eta>0) :
                        self.hist["hfp_pf_tower"].Fill(energy)
                        hfp_pf_energy_tot += energy
                        hfp_pf_energy_max = max(hfp_pf_energy_max, energy)
                    else :
                        self.hist["hfm_pf_tower"].Fill(energy)
                        hfm_pf_energy_tot += energy
                        hfm_pf_energy_max = max(hfm_pf_energy_max, energy)
                
                    #PFCandidatesp4
                    #PFCandidatesrawEcalEnergy
                    #PFCandidatesrawHcalEnergy
                    #PFCandidatesparticleId
                    

        if (self.hasTower):
            
            for itow in range(self.fChain.CaloTowersp4.size()):

                eta = self.fChain.CaloTowersp4[itow].eta()
                energy = self.fChain.CaloTowersemEnergy[itow] + self.fChain.CaloTowershadEnergy[itow]

                if (abs(eta)>3.2 and abs(eta)<4.9) :
                
                    if (eta>0) :
                        self.hist["hfp_tow_tower"].Fill(energy)
                        hfp_tow_energy_tot += energy
                        hfp_tow_energy_max = max(hfp_tow_energy_max, energy)
                    else :
                        self.hist["hfm_tow_tower"].Fill(energy)
                        hfm_tow_energy_tot += energy
                        hfm_tow_energy_max = max(hfm_tow_energy_max, energy)
                

        nVertex = 0
        for ivtx in range(self.fChain.vtxx.size()) :
            if (self.fChain.vtxndof[ivtx]>5):
                nVertex += 1
        
        self.hist["hfp_pf_total"].Fill(hfp_pf_energy_max)
        self.hist["hfm_pf_total"].Fill(hfm_pf_energy_max)

        self.hist["hfp_tow_total"].Fill(hfp_tow_energy_max)
        self.hist["hfm_tow_total"].Fill(hfm_tow_energy_max)

        self.hist["nVertex"].Fill(nVertex)

        self.tree_run[0] = self.fChain.run
        self.tree_bx[0] = self.fChain.bx
        self.tree_LS[0] = self.fChain.lumi
        self.tree_cas[0] = 0. # self.fChain.run
        self.tree_hfM_pf[0] = hfm_pf_energy_max
        self.tree_hfP_pf[0] = hfp_pf_energy_max
        self.tree_hfM[0] = hfm_tow_energy_max
        self.tree_hfP[0] = hfp_tow_energy_max
        self.tree_nVertex[0] = nVertex
        self.hist["tree"].Fill()
    
        #self.hist["run_plus"].Fill(self.fChain.run, hfp_energy_max)
        #self.hist["run_minus"].Fill(self.fChain.run, hfm_energy_max)

        isHfAND = True
        isHfP = False
        isHfM = False

        isVertex = False
        if (nVertex==1):
            isVertex = True        
        
        hf_threshold = 10
        if (self.hasTower):
#            if (hfp_tow_energy_max<hf_threshold and hfm_tow_energy_max<hf_threshold):
#                return 1
            if (hfp_tow_energy_max<hf_threshold):
                isHfM = True
                isHfAND = False
            elif (hfm_tow_energy_max<hf_threshold):
                isHfP = True
                isHfAND = False
        else:
#            if (hfp_pf_energy_max<hf_threshold and hfm_pf_energy_max<hf_threshold):
#                return 1
            if (hfp_pf_energy_max<hf_threshold):
                isHfM = True
                isHfAND = False
            elif (hfm_pf_energy_max<hf_threshold):
                isHfP = True
                isHfAND = False
            
                        
        #####################################
        # now we have a collision candidate #
        #####################################

        
        if (isHfAND):
            self.hist["EventCounter"].Fill("HF-AND", 1)
        if (isHfM):
            self.hist["EventCounter"].Fill("HF-Minus", 1)
        if (isHfP):
            self.hist["EventCounter"].Fill("HF-Plus", 1)
        if (isVertex):
            self.hist["EventCounter"].Fill("Vertex", 1)

            
        if not isVertex:
            return 1

        
        if not (self.fChain.run in self.runs):
            self.runs[self.fChain.run] = 0
        self.runs[self.fChain.run] += 1

        # the HF dndeta and dedeta, if event selected
        if (self.hasPF):

            for ipf in range(self.fChain.PFCandidatesp4.size()):
            
                eta = self.fChain.PFCandidatesp4[ipf].eta()
                energy = self.fChain.PFCandidatesp4[ipf].energy()

                self.hist["dEdEta"].Fill(eta, energy)
                if (energy>1):
                    self.hist["dNdEta"].Fill(eta)
                    
        if (self.hasTower):
            for itow in range(self.fChain.CaloTowersp4.size()):

                eta = self.fChain.CaloTowersp4[itow].eta()
                energy = self.fChain.CaloTowersemEnergy[itow] + self.fChain.CaloTowershadEnergy[itow]
                
                self.hist["dEdEta"].Fill(eta, energy)
                if (energy>1):
                    self.hist["dNdEta"].Fill(eta)
                    

        # CASTOR data is last
                    
        energy_tot = 0
        energy_secsum = [0.0] * 16
        for isec in xrange(16):
            for imod in xrange(14):
#                if [isec+1,imod+1] in self.bad_ch:
#                    continue
                energy_secsum[isec] += energy_ch[isec][imod]                

                self.hist["castor_channel_2d"].Fill(imod+1, isec+1, energy_ch[isec][imod])
                
                if (isHfAND):
                    self.hist["castor_channel_2d_hfand"].Fill(imod+1, isec+1, energy_ch[isec][imod])
                if (isHfM):
                    self.hist["castor_channel_2d_hfm"].Fill(imod+1, isec+1, energy_ch[isec][imod])
                if (isHfP):
                    self.hist["castor_channel_2d_hfp"].Fill(imod+1, isec+1, energy_ch[isec][imod])
                
            self.hist["castor_tower"].Fill(energy_secsum[isec]) # HF OR
            energy_tot += energy_secsum[isec]
            
        self.hist["castor_total"].Fill(energy_tot)
        
        #henergy = 'MuonSignal_sec_{sec}'.format(sec=str(iSectorMax+1))
        #hnameAllsec ='MuonSignalAllSec'
        #self.hist[henergy].Fill(energy_secsum[iSectorMax])
        #self.hist[hnameAllsec].Fill(energy_secsum[iSectorMax])
        #self.hist["GoodMuonCountPerSec"].Fill(iSectorMax+1)
        
        return 1
    

    
    
    
    def finalize(self):

        print str(self.runs)

        ntot = self.hist["EventCounter"].GetBinContent(2)
        n0 = ntot - self.hist["EventCounter"].GetBinContent(3)
        print "ntrig: " + str(ntot)
        print "n0 : " + str(n0)
        print "interaction probability: " + str(-log(n0/ntot))
        
        print "Finalize:"

        
        
    def finalizeWhenMerged(self):
        print "finalizeWhenMerged"

        olist = self.GetOutputList()
        histos = {}
        for o in olist:
            if not "TH1" in o.ClassName():
                if not "TH2" in o.ClassName():
                    if not "TProfile2D" in o.ClassName():
                        continue
            histos[o.GetName()] = o
        
        




##################################################################################
##################################################################################
##################################################################################
##################################################################################

if __name__ == "__main__":

    sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)

    if (len(sys.argv) < 2 or len(sys.argv) > 3):
        print "Please specify exaclty one CFF sample to run over"
        print "use printTree.py to see list of available samples"
        print "add \"force\" to force run even if no improvement is expected"
        sys.exit(1)
            
    sampleList = []
    sampleList.append(sys.argv[1])

    if (len(sampleList)!=1):
        print "You must specify exactly one CFF sample to run over"
        print "Use printTree.py to see available samples"
        sys.exit(1)
        
    outfolder = './'
    outFileName = "MinBiasResponseOutput_" + sampleList[0] + ".root"
    slaveParams = {"theSample": sampleList[0]}
    
    MinimumBias.runAll(slaveParameters = slaveParams,
                       sampleList = sampleList, 
                       maxFilesMC = None,
                       maxFilesData = None,
                       maxNevents = 100000000,
#                       maxNevents = 100000,
                       nWorkers = 8,
                       outFile = outFileName,
                       verbosity = 2)
    
    print ("Output file name: " + outFileName )


    
