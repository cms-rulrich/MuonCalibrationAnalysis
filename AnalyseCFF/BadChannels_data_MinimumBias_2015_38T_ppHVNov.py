
# badChannelsSecMod = [[2,10],[3,11],[12,12],[5,4],[3,8],[7,5],[9,4],[8,8],[4,11],[16,6],[16,4],[15,4]] 
# badChannelsSecMod = [[2,5],[2,10],[3,8],[4,9],[5,9]]
badChannelsSecMod = [#[1,7],[2,7],[3,7],[4,7],[5,7],[6,7],[7,7],[8,7],[9,7],[10,7],[11,7],[12,7],[13,7],[14,7],[15,7],[16,7], # keep center part
                     #[1,8],[2,8],[3,8],[4,8],[5,8],[6,8],[7,8],[8,8],[9,8],[10,8],[11,8],[12,8],[13,8],[14,8],[15,8],[16,8], # keep center part
                     [1,5],[7,5],[10,5],[13,5],[16,5], # over-abundent in ZS
                     [3,11], [2,10],[16,6]]

# default
zeroSuppressionThreshold = 2.2

maxExtraChannels = 6
minChannelsSector = 4

# muon penetrating selection:
minChannelsPerSection = 1
minSections = 3

