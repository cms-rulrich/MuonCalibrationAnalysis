#!/bin/bash

#python MuonSelection.py data_MinimumBias_2015_0T_June force
#python MuonSelection.py data_PAMinBiasUPC_Run2013 force
#python MuonSelection.py data_MinimumBias_2015_38T_ppHVNov force 

python MuonSelection.py data_ForwardTriggers_2011_Mar force
python MuonSelection.py data_ForwardTriggers_2011_Apr force
python MuonSelection.py data_ForwardTriggers_2011_May force
python MuonSelection.py data_ForwardTriggers_2011_Jun force
python MuonSelection.py data_ForwardTriggers_2011_Jul force

python MuonSelection.py data_ForwardTriggers_2011_0T force
python MuonSelection.py data_ForwardTriggers_2011_2T force
python MuonSelection.py data_ForwardTriggers_2011 force
#python MuonSelection.py data_MinimumBias_2015_38T_HIRunNov force
#python MuonSelection.py data_MinimumBias_2015_0T_Nov force 
python MuonSelection.py data_MinimumBias_2015_38T_Nov force 
#python MuonSelection.py data_PACastor_Muon_2016B force 
#python MuonSelection.py data_PACastor_Muon_2016C force
