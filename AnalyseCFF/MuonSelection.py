#!/usr/bin/env python

#
# formerly this was developed by Melike and called "Step2_Selection_3.py"
#
# RU Do 7. Sep 21:06:07 CEST 2017
# - remove toy-MC stuff. Evntually add it later again...
#
# RU Di 12. Sep 11:27:35 CEST 2017
# - complete rewrite
#

import CommonFSQFramework.Core.ExampleProofReader

import sys, os, time
sys.path.append(os.path.dirname(__file__))
from os import listdir
from os.path import isfile, join
import ROOT
from math import sqrt, log10
from array import *

import numpy as np

saveCFF = True 

class MuonSelection(CommonFSQFramework.Core.ExampleProofReader.ExampleProofReader):
    def init( self, maxEvents = None):

        self.saveCFF_maxEvents = 100
        # self.saveCFF_maxEvents = 10000000
        self.maxEvents = maxEvents
        
        self.hist = {}
        self.hist["EventCounter"] = ROOT.TH1D("EventCounter","EventCounter",10,0,20)
            
        # the input noise/rms levels
        self.hist["ch_input_mean_2d"] =  ROOT.TH2D("ch_input_mean_2d", "ch_input_mean_2d", 14, 0.5, 14.5, 16, 0.5, 16.5)
        self.hist["ch_input_meanErr_2d"] =  ROOT.TH2D("ch_input_meanErr_2d", "ch_input_meanErr_2d", 14, 0.5, 14.5, 16, 0.5, 16.5)
        self.hist["ch_input_rms_2d"] =  ROOT.TH2D("ch_input_rms_2d", "ch_input_rms_2d", 14, 0.5, 14.5, 16, 0.5, 16.5)
        self.hist["ch_bad_2d"] =  ROOT.TH2D("ch_bad_2d", "ch_bad_2d", 14, 0.5, 14.5, 16, 0.5, 16.5)

        # the observed noise/rms levels
        self.hist["ch_bkg_count_2d"] =  ROOT.TH2D("ch_bkg_count_2d", "ch_bkg_count_2d", 14, 0.5, 14.5, 16, 0.5, 16.5)
        self.hist["ch_bkg_mean_2d"] =  ROOT.TH2D("ch_bkg_mean_2d", "ch_bkg_mean_2d", 14, 0.5, 14.5, 16, 0.5, 16.5)
        self.hist["ch_bkg_rms_2d"] =  ROOT.TH2D("ch_bkg_rms_2d", "ch_bkg_rms_2d", 14, 0.5, 14.5, 16, 0.5, 16.5)
        self.hist["ch_bkg_signals_2d"] =  ROOT.TProfile2D("ch_bkg_signals_2d", "ch_bkg_signals_2d", 14, 0.5, 14.5, 16, 0.5, 16.5, "S")
        self.hist["ch_bkg_signals2_2d"] =  ROOT.TProfile2D("ch_bkg_signals2_2d", "ch_bkg_signals2_2d", 14, 0.5, 14.5, 16, 0.5, 16.5, "S")

        # the observed signal levels
        self.hist["ch_mu_count_2d"] =  ROOT.TH2D("ch_mu_count_2d", "ch_mu_count_2d", 14, 0.5, 14.5, 16, 0.5, 16.5)
        self.hist["ch_mu_mean_2d"] =  ROOT.TH2D("ch_mu_mean_2d", "ch_mu_mean_2d", 14, 0.5, 14.5, 16, 0.5, 16.5)
        self.hist["ch_mu_rms_2d"] =  ROOT.TH2D("ch_mu_rms_2d", "ch_mu_rms_2d", 14, 0.5, 14.5, 16, 0.5, 16.5)
        self.hist["ch_mu_signals_2d"] = ROOT.TProfile2D("ch_mu_signals_2d", "ch_mu_signals_2d", 14, 0.5, 14.5, 16, 0.5, 16.5, "S")
        self.hist["ch_mu_signals2_2d"] = ROOT.TProfile2D("ch_mu_signals2_2d", "ch_mu_signals2_2d", 14, 0.5, 14.5, 16, 0.5, 16.5, "S")
        
        self.hist["ch_sigma"] = ROOT.TH1D("ch_sigma","ch_sigma", 100, 0, 10)
        
        self.hist["ch_zs_2d"] =  ROOT.TH2D("ch_zs_2d", "ch_zs_2d", 14, 0.5, 14.5, 16, 0.5, 16.5)
        self.hist["zs_ntot"] = ROOT.TH1D("zs_ntot","zs_ntot", 224, 0, 224)
        self.hist["zs_ntot_cand"] = ROOT.TH1D("zs_ntot_cand","zs_ntot_cand", 224, 0, 224)
        
        self.hist["zs_nsecmax"] = ROOT.TH1D("zs_nsecmax","zs_nsecmax", 17, 0, 17)
        self.hist["zs_secmax"] = ROOT.TH1D("zs_secmax","zs_secmax", 16, .5, 16.5)
        self.hist["zs_nextra"] = ROOT.TH1D("zs_nextra","zs_nextra", 224, 0, 224)
        self.hist["zs_nneighbor"] = ROOT.TH1D("zs_nneighbor","zs_nneighbor", 29, 0, 29)
        
        self.hist["mucand_sec"] = ROOT.TH1D("mucand_sec","mucand_sec", 16, .5, 16.5)
        
        #self.hist["runs_mucand"] =  ROOT.TH1D("runs_mucand","runs_mucand", 100, 1, 0)
        #self.hist["runs_all"] =  ROOT.TH1D("runs_all","runs_all", 100, 1, 0)

        
        self.hist["bx_mucand"] =  ROOT.TH1D("bx_mucand","bx_mucand", 4000, 0, 4000);
        self.hist["bx_all"] =  ROOT.TH1D("bx_all","bx_all", 4000, 0, 4000);

        self.hist["GoodMuonCountPerSec"] = ROOT.TH1D("GoodMuonCountPerSec", "GoodMuonCountPerSec", 16, 0.5, 16.5)

        self.hist['MuonSignalAllSec'] = ROOT.TH1D("MuonSignalAllSec", "MuonSignalAllSec", 200, -50, 3000)

        # n-minis-one plots
        self.hist["nminusone_penetrating_front"] = ROOT.TH1D("nminusone_penetrating_front", "nminusone_penetrating_front", 5, 0, 5)
        self.hist["nminusone_penetrating_mid1"] = ROOT.TH1D("nminusone_penetrating_mid1", "nminusone_penetrating_mid1", 5, 0, 5)
        self.hist["nminusone_penetrating_mid2"] = ROOT.TH1D("nminusone_penetrating_mid2", "nminusone_penetrating_mid2", 5, 0, 5)
        self.hist["nminusone_penetrating_rear"] = ROOT.TH1D("nminusone_penetrating_rear", "nminusone_penetrating_rear", 5, 0, 5)
        self.hist["nminusone_nextra"] = ROOT.TH1D("nminusone_nextra", "nminusone_nextra", 224, 0, 224)
        self.hist["nminusone_nsectormax"] = ROOT.TH1D("nminusone_nsectormax", "nminusone_nsectormax", 14, 0, 14)

        # spectra for signal and background
        # consider QIE conversion table
        # max fC : fC/bin
        # 36 : 2.6
        # 73 : 5.2
        # 104: 7.8
        # 135: 10.4
        # (174: 13)
        # 343: 13
        # 525: 25
        # 681: 39
        # 837: 52
        # ( 1032: 65)
        # 1877: 65
        # 2787: 130
        # 3567: 195
        # 4347: 260
        # (5322: 325)
        # 9547: 325
        # 14097: 650
        # 17997: 975
        # 21897: 1300
        # 26772: 1625
        #
        # but consider: signals are "TS1-BASE1 + TS2-BASE2", each in units of fC
        QIE = {36 : 2.6,
               73 : 5.2,
               104: 7.8,
               135: 10.4,
               343: 13,
               525: 25,
               681: 39,
               837: 52,
               1877: 65,
               2787: 130,
               3567: 195,
               4347: 260,
               9547: 325,
               14097: 650,
               17997: 975,
               21897: 1300,
               26772: 1625}
        binWidening = 1.0
        maxRangeNoise = 150
        rangeCurr = -75
        binsNoise = []
        while rangeCurr < maxRangeNoise:
            binsNoise.append(rangeCurr)
            for qie_max, qie_bin in sorted(QIE.iteritems()):
                if rangeCurr < qie_max:
                    rangeCurr += qie_bin*binWidening
                    break

        maxRangeMuon = 1450
        rangeCurr = -75
        binsMuon = []
        while rangeCurr < maxRangeMuon:
            binsMuon.append(rangeCurr)
            for qie_max, qie_bin in sorted(QIE.iteritems()):
                if rangeCurr < qie_max:
                    rangeCurr += qie_bin*binWidening
                    break

        self.treeEnergy = array( 'd', [0.] )
        
        for isec in xrange(0,16):
            henergy = 'MuonSignal_sec_{sec}'.format(sec=str(isec+1))
            self.hist[henergy] = ROOT.TH1D(henergy, henergy, 200, -50, 6000)
            
            for imod in xrange(0,14):
                hname1 = 'MuonSignal_sec_{sec}_mod_{mod}'.format(sec=str(isec+1), mod=str(imod+1))
                hname2 = 'NoiseSignal_sec_{sec}_mod_{mod}'.format(sec=str(isec+1), mod=str(imod+1))
                self.hist[hname1] = ROOT.TH1D(hname1, hname1, len(binsMuon)-1, array('d',binsMuon)) # muons
                self.hist[hname2] = ROOT.TH1D(hname2, hname2, len(binsNoise)-1, array('d',binsNoise))   # noise

                treeName = 'ChannelTree_sec_{sec}_mod_{mod}'.format(sec=str(isec+1), mod=str(imod+1))
                self.hist[treeName] = ROOT.TTree(treeName, treeName)
                self.hist[treeName].Branch( 'energy', self.treeEnergy, 'energy/D' )
        
        self.tree_run = array('i', [0] )
        self.tree_bx = array('i', [0] )
        self.tree_mucand = array('i', [0] )
        self.hist["tree"] = ROOT.TTree("tree", "tree")
        self.hist["tree"].Branch( 'run', self.tree_run, 'run/I' )
        self.hist["tree"].Branch( 'bx', self.tree_bx, 'bx/I' )
        self.hist["tree"].Branch( 'mucand', self.tree_mucand, 'mucand/I' )

                
        # store all histograms
        for h in self.hist:
            if not "TTree" in self.hist[h].ClassName():
                self.hist[h].Sumw2()
            self.GetOutputList().Add(self.hist[h])


            
        #############################################################
        # also write a ttree with the final muon events!            #
        # in same format at CFF, but reduced content                #
        # create the branches and assign the fill-variables to them #
        #############################################################

        if saveCFF:
            self.outTree = None
            self.muSector = array('i', [0] )

            
    
    def getListSigmaChannel(self, energy_ch):
        listSigmaChannels = [[0 for _ in xrange(14)] for _ in xrange(16)]
        for isec in xrange(16):
            for imod in xrange(14):
                energy = energy_ch[isec][imod]
                if [isec+1,imod+1] in self.bad_ch:
                    listSigmaChannels[isec][imod] = 0
                    continue
                sigma = 0
                if self.input_rms_ch[isec][imod] > 0:
                    sigma = (energy - self.input_mean_ch[isec][imod]) / self.input_rms_ch[isec][imod]
                    self.hist["ch_sigma"].Fill(sigma)
                listSigmaChannels[isec][imod] = sigma
        return listSigmaChannels



    def isPenetrating(self, listZSChannels, minSections, threshold) :
        self.Front_Module = 0
        self.Mid1_Module  = 0
        self.Mid2_Module  = 0
        self.Rear_Module  = 0
        for imod in listZSChannels:
            # if imod <= 3: # [0,1,2,3]
            #     Front_Module += 1
            # elif imod <= 9: #[4,5,6,7,8,9]
            #     Mid_Module += 1
            # else: # [10,11,12,13]:
            #     Rear_Module += 1
            if imod <= 2: # [0,1,2]
                self.Front_Module += 1
            elif imod <= 6: #[3,4,5,6]
                self.Mid1_Module += 1
            elif imod <= 10: #[7,8,9,10]
                self.Mid2_Module += 1
            else: # [11,12,13]:
                self.Rear_Module += 1
        countSection = 0
        if (self.Front_Module>=threshold): countSection += 1
        if (self.Mid1_Module>=threshold): countSection += 1
        if (self.Mid2_Module>=threshold): countSection += 1
        if (self.Rear_Module>=threshold): countSection += 1
        if (countSection>=minSections): return True
        return False

    
    
    def analyze(self):

        if (self.theSample == "data_ForwardTriggers_2011_Mar") :
            if (self.fChain.run<160575 or self.fChain.run>161230) :
                return 0
        elif (self.theSample == "data_ForwardTriggers_2011_Apr") :
            if (self.fChain.run<=161230 or self.fChain.run>163735) :
                return 0
        elif (self.theSample == "data_ForwardTriggers_2011_May") :
            if (self.fChain.run<=163735 or self.fChain.run>166242) :
                return 0
        elif (self.theSample == "data_ForwardTriggers_2011_Jun") :
            if (self.fChain.run<=166242 or self.fCain.run>168262) :
                return 0
        elif (self.theSample == "data_ForwardTriggers_2011_Jul") :
            if (self.fChain.run<=168262 or self.fCain.run>172317) :
                return 0
        elif (self.theSample == "data_ForwardTriggers_2011_Aug") : # EMPTY
            if (self.fChain.run<=172317 or self.fCain.run>175117) :
                return 0
        elif (self.theSample == "data_ForwardTriggers_2011_Sep") : # EMPTY
            if (self.fChain.run<=175117 or self.fCain.run>177621) :
                return 0
            
        self.hist["EventCounter"].Fill("all", 1)
        #self.hist["runs_all"].Fill(self.fChain.run)
        self.hist["bx_all"].Fill(self.fChain.bx)

        # triggerL1_bptxPlus = False
        # triggerL1_bptxMinus = False
        # triggerL1_bptxQuiet = False
        # if hasattr(self.fChain, 'trgl1L1GTTech') and self.fChain.trgl1L1GTTech.size()>10:
        #     triggerL1_bptxPlus    = self.fChain.trgl1L1GTTech[1] # BPTX plus
        #     triggerL1_bptxMinus    = self.fChain.trgl1L1GTTech[2] # bptx minus
        #     # trgl1L1GTTech6    = self.fChain.trgl1L1GTTech[6] # minus and not plus
        #     triggerL1_bptxQuiet    = self.fChain.trgl1L1GTTech[7] # quiet
        # elif hasattr(self.fChain, 'trgl1L1GTAlgo') and self.fChain.trgl1L1GTAlgo.size()>10:
        #     triggerL1_bptxPlus    = self.fChain.trgl1L1GTAlgo[1] # BPTX plus
        #     triggerL1_bptxMinus    = self.fChain.trgl1L1GTAlgo[2] # bptx minus
        #     triggerL1_bptxQuiet    = not triggerL1_bptxMinus and not triggerL1_bptxPlus
        # else:
        #     print "no trigger readout found"
        #     sys.exit(1)
            
        # if (triggerL1_bptxPlus) and not (triggerL1_bptxMinus):
        #    self.hist["EventCounter"].Fill("BPTX+",1)

        # if (triggerL1_bptxMinus) and not (triggerL1_bptxPlus):
        #    self.hist["EventCounter"].Fill("BPTX-",1)
 
        if self.fChain.CastorRecHitEnergy.size() != 224:
            return 0

        self.hist["EventCounter"].Fill("Data Valid",1)

        # read CASTOR RecHits
        energy_ch = [[0 for _ in xrange(14)] for _ in xrange(16)]
        for ich in range(self.fChain.CastorRecHitEnergy.size()):
            isec = ich//14
            imod = ich%14
            energy_ch[isec][imod] = self.fChain.CastorRecHitEnergy.at(ich)
            
            
        ###########################
        # do the zero suppression #
        ###########################

        zs_threshold = self.zeroSuppressionThreshold # [sigma] and default is 2.0
        
        listSigmaChannels = self.getListSigmaChannel(energy_ch)
        listZSChannelsPerSector = [[] for _ in xrange(16)]

        iSectorMax = 0
        nSectorMax = 0
        nTotZS = 0
        for isec in xrange(16):
            for imod in xrange(14):
                sigma = listSigmaChannels[isec][imod]
                if sigma > zs_threshold:
                    listZSChannelsPerSector[isec].append(imod)
                    self.hist["ch_zs_2d"].Fill(imod+1, isec+1)
            nTotZS += len(listZSChannelsPerSector[isec])
            if (len(listZSChannelsPerSector[isec]) > nSectorMax):
                nSectorMax = len(listZSChannelsPerSector[isec])
                iSectorMax = isec

        # there might still be some signal in the neighbors... check
        nNeighbor = len(listZSChannelsPerSector[(isec+1)%16]) + len(listZSChannelsPerSector[(isec-1)%16])
                
        nExtra = nTotZS - nSectorMax
        
        self.hist["zs_ntot"].Fill(nTotZS)
        self.hist["zs_nsecmax"].Fill(nSectorMax)
        self.hist["zs_secmax"].Fill(iSectorMax+1)

        self.hist["zs_nextra"].Fill(nExtra)
        self.hist["zs_nneighbor"].Fill(nNeighbor)
        
        ################################
        # cut on muon candidate sector #
        ################################

        cutOnSectorMax = (nSectorMax >= self.nSectorMaxCut)   # very loose cut
        cutOnNextra = (nExtra <= self.nExtraCut)
        cutOnPenetrating = self.isPenetrating(listZSChannelsPerSector[iSectorMax], self.minSections, self.minChanPerSection)

        muonCandidate = cutOnPenetrating and cutOnNextra and cutOnPenetrating        
        
        # N-1 plots
        if cutOnSectorMax and cutOnNextra:
            self.hist["nminusone_penetrating_front"].Fill(self.Front_Module)
            self.hist["nminusone_penetrating_mid1"].Fill(self.Mid1_Module)
            self.hist["nminusone_penetrating_mid2"].Fill(self.Mid2_Module)
            self.hist["nminusone_penetrating_rear"].Fill(self.Rear_Module)
            
        if cutOnSectorMax and cutOnPenetrating:
            self.hist["nminusone_nextra"].Fill(nExtra)

        if cutOnNextra and cutOnPenetrating:
            self.hist["nminusone_nsectormax"].Fill(nSectorMax)

        if cutOnSectorMax: 
            self.hist["EventCounter"].Fill("Sector Nch_max",1)
            if cutOnNextra:
                self.hist["EventCounter"].Fill("Extra channels",1)
                if (cutOnPenetrating):
                    self.hist["EventCounter"].Fill("Muon cand",1)


        self.tree_run[0] = self.fChain.run
        self.tree_bx[0] = self.fChain.bx
        self.tree_mucand[0] = muonCandidate
        self.hist["tree"].Fill()

                    
        if not muonCandidate:
            return 0 
        
        ################################
        # now we have a muon candidate #
        ################################

        self.hist["zs_ntot_cand"].Fill(nTotZS)
        self.hist["mucand_sec"].Fill(iSectorMax)
        #self.hist["runs_mucand"].Fill(self.fChain.run)
        self.hist["bx_mucand"].Fill(self.fChain.bx)
        # if (triggerL1_bptxPlus):
        #     self.hist["EventCounter"].Fill("Muon cand (BPTX+)",1)
        # if (triggerL1_bptxMinus):
        #     self.hist["EventCounter"].Fill("Muon cand (BPTX-)",1)
        # if (not triggerL1_bptxPlus and not triggerL1_bptxMinus):
        #     self.hist["EventCounter"].Fill("Muon cand (BPTX0)",1)
        
        
        # sector-level signals (w/o intercalibration...):
        energy_secsum = [0.0] * 16
        for isec in xrange(16):
            for imod in xrange(14):
                if [isec+1,imod+1] in self.bad_ch:
                    continue
                energy_secsum[isec] += energy_ch[isec][imod]
                
        henergy = 'MuonSignal_sec_{sec}'.format(sec=str(iSectorMax+1))
        hnameAllsec ='MuonSignalAllSec'
        self.hist[henergy].Fill(energy_secsum[iSectorMax])
        self.hist[hnameAllsec].Fill(energy_secsum[iSectorMax])
        self.hist["GoodMuonCountPerSec"].Fill(iSectorMax+1)
        
        
        # PMT-level distributions
        for imod in xrange(0,14):
            TEST = []
            for iNZS in listZSChannelsPerSector[iSectorMax]:
                if (imod != iNZS):
                    TEST.append(iNZS)
            if (self.isPenetrating(TEST, self.minSections, self.minChanPerSection)):
                hname = 'MuonSignal_sec_{sec}_mod_{mod}'.format(sec=str(iSectorMax+1), mod=str(imod+1))
                treeName = 'ChannelTree_sec_{sec}_mod_{mod}'.format(sec=str(iSectorMax+1), mod=str(imod+1))
                chE = energy_ch[iSectorMax][imod]
                self.treeEnergy[0] = chE
                self.hist[hname].Fill( chE )
                self.hist[treeName].Fill()
                self.hist["ch_mu_count_2d"].Fill( imod+1, iSectorMax+1, 1 )
                self.hist["ch_mu_signals_2d"].Fill(imod+1, iSectorMax+1, chE)
                self.hist["ch_mu_signals2_2d"].Fill(imod+1, iSectorMax+1, chE*chE)
                
        # and finally the noise (oposite site of muon, +-X sectors)
        noiseRange = 3 # +- noiseRange around opposite muon candidate for noise collection
        for inoiseSec in range(iSectorMax-noiseRange+8, iSectorMax+noiseRange+1+8):
            for imod in range(0,14):
                theSector = inoiseSec % 16
                hname = 'NoiseSignal_sec_{sec}_mod_{mod}'.format(sec=str(theSector+1), mod=str(imod+1))
                chE = energy_ch[theSector][imod]
                self.hist[hname].Fill(chE)
                self.hist["ch_bkg_count_2d"].Fill( imod+1, theSector+1, 1 )
                self.hist["ch_bkg_signals_2d"].Fill(imod+1, theSector+1, chE)
                self.hist["ch_bkg_signals2_2d"].Fill(imod+1, theSector+1, chE*chE)

        if saveCFF and self.saveCFF_maxEvents>0:
            
            self.saveCFF_maxEvents -= 1

            if self.outTree == None:
                self.outTree = self.fChain.CloneTree(0)
                self.outTree.Branch('muon_sector', self.muSector, 'muon_sector/I' )
                self.addToOutput(self.outTree)

            for ich in range(self.fChain.CastorRecHitEnergy.size()):
                isec = ich//14
                imod = ich%14
                # update bad channel list
                self.fChain.CastorRecHitisBad[ich] = 0
                if [isec+1,imod+1] in self.bad_ch:
                    self.fChain.CastorRecHitisBad[ich] = 1
            self.muSector[0] = iSectorMax+1
            self.outTree.Fill()
                            
        return 1
    

    
    
    
    def finalize(self):
        print "Finalize:"
        if saveCFF:
            if hasattr(self, 'outTree'):
                if self.outTree != None:
                    self.outTree.AutoSave()


        
        
    def finalizeWhenMerged(self):
        print "finalizeWhenMerged"

        olist = self.GetOutputList()
        histos = {}
        for o in olist:
            if not "TH1" in o.ClassName():
                if not "TH2" in o.ClassName():
                    if not "TProfile2D" in o.ClassName():
                        continue
            histos[o.GetName()] = o
        
        for isec in xrange(0,16):
            for imod in xrange(0,14):
                histos["ch_input_mean_2d"].Fill(imod+1, isec+1, self.input_mean_ch[isec][imod]) # do not exclude bad channels..
                histos["ch_input_meanErr_2d"].Fill(imod+1, isec+1, self.meanErr_ch[isec][imod]) # ...
                histos["ch_input_rms_2d"].Fill(imod+1, isec+1, self.input_rms_ch[isec][imod]) # ..need those in next iteration

                #hname = 'NoiseSignal_sec_{sec}_mod_{mod}'.format(sec=str(isec+1), mod=str(imod+1))
                #self.hist[hname]
                #histos["ch_bkg_mean_2d"].Fill(imod+1, isec+1, histos[hname].GetMean())
                #histos["ch_bkg_rms_2d"].Fill(imod+1, isec+1, histos[hname].GetRMS())
                
                #histos["ch_bkg_mean_2d"].Fill(imod+1, isec+1, histos["ch_bkg_signals_2d"].GetBinContent(imod+1, isec+1))
                #histos["ch_bkg_rms_2d"].Fill(imod+1, isec+1, histos["ch_bkg_signals_2d"].GetBinError(imod+1, isec+1))

                bg_mean = histos["ch_bkg_signals_2d"].GetBinContent(imod+1, isec+1) 
                bg_rms = 0
                if (bg_mean>0 and histos["ch_bkg_signals2_2d"].GetBinContent(imod+1, isec+1) - bg_mean*bg_mean > 0 ):
                    bg_rms = sqrt(histos["ch_bkg_signals2_2d"].GetBinContent(imod+1, isec+1) - bg_mean*bg_mean)
                histos["ch_bkg_mean_2d"].Fill(imod+1, isec+1, bg_mean)
                histos["ch_bkg_rms_2d"].Fill(imod+1, isec+1, bg_rms)
                #histos["ch_bkg_mean_2d"].Fill(imod+1, isec+1, bg_mean)
                #histos["ch_bkg_rms_2d"].Fill(imod+1, isec+1, histos["ch_bkg_signals_2d"].GetBinError(imod+1, isec+1))

                sig_mean = histos["ch_mu_signals_2d"].GetBinContent(imod+1, isec+1)
                sig_rms = 0
                if (sig_mean>0 and histos["ch_mu_signals2_2d"].GetBinContent(imod+1, isec+1) - sig_mean*sig_mean > 0 ) :
                    sig_rms = sqrt(histos["ch_mu_signals2_2d"].GetBinContent(imod+1, isec+1) - sig_mean*sig_mean)
                histos["ch_mu_mean_2d"].Fill(imod+1, isec+1, sig_mean)
                histos["ch_mu_rms_2d"].Fill(imod+1, isec+1, sig_rms)
                #histos["ch_mu_mean_2d"].Fill(imod+1, isec+1, histos["ch_mu_signals_2d"].GetBinContent(imod+1, isec+1))
                #histos["ch_mu_rms_2d"].Fill(imod+1, isec+1, histos["ch_mu_signals_2d"].GetBinError(imod+1, isec+1))

                if [isec+1,imod+1] in self.bad_ch:
                    histos["ch_bad_2d"].Fill(imod+1, isec+1, 1)


        # produce some number printout
        list_zs_2d = {}
        for isec in xrange(0,16):
            for imod in xrange(0,14):
                list_zs_2d[histos["ch_zs_2d"].GetBinContent(imod+1, isec+1)] = [imod+1, isec+1]
        for n_zs, chan in sorted(list_zs_2d.iteritems()):
            print ("Sorted channels ZS sec=" + str(chan[0]) + ", mod=" + str(chan[1]) + ", Nzs=" + str(n_zs))

        
        




##################################################################################
##################################################################################
##################################################################################
##################################################################################

if __name__ == "__main__":

    sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)

    if (len(sys.argv) < 2 or len(sys.argv) > 3):
        print "Please specify exaclty one CFF sample to run over"
        print "use printTree.py to see list of available samples"
        print "add \"force\" to force run even if no improvement is expected"
        sys.exit(1)
            
    sampleList = []
    sampleList.append(sys.argv[1])

    forceRun = False
    if len(sys.argv) == 3 and sys.argv[2]=="force":
        forceRun = True
        
    if (len(sampleList)!=1):
        print "You must specify exactly one CFF sample to run over"
        print "Use printTree.py to see available samples"
        sys.exit(1)
        
    outfolder = './'
    outFileNameTrunk = "MuonSelectionOutput_" + sampleList[0] + "_iteration"
        
    #check which output files already exist
    currIteration = -1
    filenames = [ f for f in listdir(outfolder) if isfile(join(outfolder, f)) ]
    for ifilename in filenames:
        if ifilename.find(outFileNameTrunk) < 0:
            continue
        testFile = ROOT.TFile.Open(ifilename)
        testDir = testFile.Get(sampleList[0])
        hasMuonOutput = testDir != None
        testFile.Close()
        if hasMuonOutput:
            currIteration = max(currIteration, int(ifilename.strip(".root").split("_")[-1]))

    print "Found " + str(currIteration) + " previous iterations of output file"

    currIteration += 1
    outFileName = outFileNameTrunk + "_{iter}.root".format(iter=str(currIteration))

    out = ROOT.TFile.Open(outFileName, "recreate") # open to save noise fits
    dir = out.mkdir("NoiseFits")
    dir.cd()
    
    # ###################################
    #
    # Determine noise and background levels.
    # Bad channels
    #

    input_mean_ch = [[0 for _ in xrange(14)] for _ in xrange(16)]
    meanErr_ch = [[0 for _ in xrange(14)] for _ in xrange(16)]
    input_rms_ch = [[0 for _ in xrange(14)] for _ in xrange(16)]

    initialFlatNoise = False
    excludeFailed = []
    countFailed = 0
    updateAnyChannel = False
    countUpdates = 0 # how many channels get new noise estimates
    prevBad = [] # for cross-checks !
    if currIteration>0: # use existing output to re-define noise levels
    
        # for Gaussian noise level fit
        fit = ROOT.TF1("fit", "gaus", -1e5, 1e6);

        print ("Reading previous output from " + outFileNameTrunk + "_{iter}.root".format(iter=str(currIteration-1)))
        noiseInputFile = ROOT.TFile(outFileNameTrunk + "_{iter}.root".format(iter=str(currIteration-1)), "read")
        hPrevMean = noiseInputFile.Get(sampleList[0] + "/ch_input_mean_2d")
        hPrevErr = noiseInputFile.Get(sampleList[0] + "/ch_input_meanErr_2d")
        hPrevRMS = noiseInputFile.Get(sampleList[0] + "/ch_input_rms_2d")
        
        for ich in range(224):
            isec = ich//14
            imod = ich%14
            input_mean_ch[isec][imod] = 0
            meanErr_ch[isec][imod] = 100
            input_rms_ch[isec][imod] = 0
            hNoise = noiseInputFile.Get(sampleList[0] + "/NoiseSignal_sec_{sec}_mod_{mod}".format(sec=str(isec+1),mod=str(imod+1)))
            #  fit
            hNoise.Scale(1, "width")
            #print ("Fit " + str(isec+1) + " " + str(imod+1))
            #fitResult = hNoise.Fit(fit, "QWLIS") # S->FitResults, L->ML, I->Integral in bin, W because of Scale
            fitResult = hNoise.Fit(fit, "QLEIS+") # WL # E -> MINOS
            lineStyle = 2
            fit.SetLineStyle(1)
            prevErr = 100 # big default ... (same behaviour as w/o err check)
            if hPrevErr != None:
                prevErr = hPrevErr.GetBinContent(imod+1, isec+1)
            #if fitResult.IsValid() and fitResult.Status()==0 and fitResult.HasMinosError(1): # this shit all does not work. unbelievable:
            if str(ROOT.gMinuit.fCstatu) == "SUCCESSFUL":
                if fit.GetParError(1) < prevErr*0.999:
                    updateAnyChannel = True
                    countUpdates += 1
                    lineStyle = 2
                    input_mean_ch[isec][imod] = fit.GetParameter(1)
                    meanErr_ch[isec][imod] = fit.GetParError(1)
                    input_rms_ch[isec][imod] = fit.GetParameter(2)
                else:
                    fit.SetLineStyle(4)
                    lineStyle = 1
                    input_mean_ch[isec][imod] = hPrevMean.GetBinContent(imod+1, isec+1)
                    meanErr_ch[isec][imod] = prevErr
                    input_rms_ch[isec][imod] = hPrevRMS.GetBinContent(imod+1, isec+1)
            else:
                print ("Info: noise fit failed for sec=" + str(isec+1) + " mod=" + str(imod+1))                
                countFailed += 1
                excludeFailed.append( [isec+1,imod+1] ) # sec-mod
                fit.SetLineStyle(4) # also set fit to dash-dot
                if hNoise.GetMeanError()!=0 and hNoise.GetRMS()!=0 and hNoise.GetMeanError() <= prevErr:
                    lineStyle = 3
                    input_mean_ch[isec][imod] = hNoise.GetMean()
                    meanErr_ch[isec][imod] = hNoise.GetMeanError()
                    input_rms_ch[isec][imod] = hNoise.GetRMS()
                else:
                    lineStyle = 4
                    input_mean_ch[isec][imod] = hPrevMean.GetBinContent(imod+1, isec+1)
                    meanErr_ch[isec][imod] = prevErr
                    input_rms_ch[isec][imod] = hPrevRMS.GetBinContent(imod+1, isec+1)
            # save plot for visual inspection !        
            ROOT.gStyle.SetOptFit(1)
            c = ROOT.TCanvas("Fit_" + str(isec+1) + "_" + str(imod+1))
            c.SetLogy(1)
            hNoise.Draw("hist");
            fit.SetLineColor(ROOT.kRed)
            fit.SetLineWidth(3)
            fit.DrawCopy("same,l")
            fit.SetLineStyle(lineStyle)
            fit.SetLineColor(ROOT.kBlue)
            fit.SetParameter(1, hPrevMean.GetBinContent(imod+1, isec+1))
            fit.SetParameter(2, hPrevRMS.GetBinContent(imod+1, isec+1))
            fit.DrawCopy("same,l")
            dir.cd()
            c.Write("Fit_" + str(isec+1) + "_" + str(imod+1))

        print ("Noise fit failed " + str(countFailed) + " times")
        print ("Number of channels with improved noise estimates is " + str(countUpdates))
        
        # finally read the prvious bad channel list for later below
        hPrevBad = noiseInputFile.Get(sampleList[0] + "/ch_bad_2d")
        for ich in range(224):
            isec = ich//14
            imod = ich%14
            if hPrevBad.GetBinContent(imod+1, isec+1) != 0:
                prevBad.append( [isec+1,imod+1] ) # sec mod

        # close input file
        noiseInputFile.Close()
            
    else: # currIteration == 0
        for ich in range(224):
            isec = ich//14
            imod = ich%14
            input_mean_ch[isec][imod] = 0 # reasonable starting point
            meanErr_ch[isec][imod] = 100 # big
            input_rms_ch[isec][imod] = 1.5  # reasonable starting point
            initialFlatNoise = True
        

    
    ###################################################################
    # next we identify bad channels based on noise properties:
    # 

    # cut channels if:
    #  - they are cutLevel (*2) from average channel RMS (or MEAN)
    #  - the RMS is identical to zero (QIE error?)
    #  - the RMS is larger than a value of 10
    # 
    
    cutLevel = 10   # this is the noise-level threshold in [sigma] for RMS and cutLevel*2 for MEAN
    
    
    
    # read external bad-channel file
    badChannelFile = "BadChannels_" + sampleList[0]
    if not isfile(badChannelFile +".py"):
        print "No bad-channel file \"" + badChannelFile + ".py\" found. Create one!"
        sys.exit(1)
    print ("Reading: " + badChannelFile + ".py" )
    exec("from " + badChannelFile + " import badChannelsSecMod")
    exec("from " + badChannelFile + " import zeroSuppressionThreshold")
    exec("from " + badChannelFile + " import minChannelsSector")
    exec("from " + badChannelFile + " import maxExtraChannels")
    exec("from " + badChannelFile + " import minChannelsPerSection")
    exec("from " + badChannelFile + " import minSections")
    
    bad_ch = []
    for bad in badChannelsSecMod:
        bad_ch.append(bad) # sec, mod
    for failed in excludeFailed: # noise fits
        bad_ch.append(failed) # sec, mod
        
    loopKiller = 0
    while True:
        loopKiller += 1
        if (loopKiller>15):
            print ("Could not determine stable noise levels and bad-channel data")
            print ("requires intervention")
            sys.exit(1)
            
        print ("Noise level loop " + str(loopKiller))
        
        averageBgk = 0
        average2Bgk = 0
        averageBgkRms = 0
        average2BgkRms = 0
        countCh = 0
        for ich in range(224):
            isec = ich//14
            imod = ich%14
            if ([isec+1, imod+1] in bad_ch):
                continue
            averageBgk += input_mean_ch[isec][imod]
            average2Bgk += input_mean_ch[isec][imod]**2
            averageBgkRms += input_rms_ch[isec][imod]
            average2BgkRms += input_rms_ch[isec][imod]**2
            countCh += 1
        
        averageBgk /= countCh
        average2Bgk /= countCh
        averageBgkRms /= countCh
        average2BgkRms /= countCh
        errBgk = sqrt(average2Bgk - averageBgk**2) 
        errBgkRms = sqrt(average2BgkRms - averageBgkRms**2) 
        
        print("Number bad channels: " +str(len(bad_ch)))
        print("Exclude Avg Bgk    >"+str(cutLevel*2)+"rms: "+str(averageBgk)+" + "+str(cutLevel*2*errBgk))
        print("Exclude Avg BgkRms >"+str(cutLevel)+"rms: "+str(averageBgkRms)+" + "+str(cutLevel*errBgkRms))
        
        # complement bad-channels by checking mean/rms of noise
        bad_ch_now = list(bad_ch)
        bad_ch = list()
        countMeanExtra = 0
        countRmsExtra = 0
        for ich in range(224):
            isec = ich//14
            imod = ich%14
            theCh = [isec+1,imod+1]
            if theCh in badChannelsSecMod or theCh in excludeFailed: # noise fits
                bad_ch.append( theCh )
                continue
            if not initialFlatNoise:
                if (input_rms_ch[isec][imod]==0 or
                    abs(input_rms_ch[isec][imod]-averageBgkRms)/errBgkRms > cutLevel or
                    input_rms_ch[isec][imod]>10) :
                    countRmsExtra += 1
                    bad_ch.append( [isec+1, imod+1] )
                    print ("From RMS: sec=" + str(isec+1) + " mod=" + str(imod+1))
                elif (abs(input_mean_ch[isec][imod]-averageBgk)/errBgk > cutLevel*2): # don't want to be strict on mean
                    countMeanExtra += 1
                    bad_ch.append( [isec+1, imod+1] )
                    print ("From mean: sec=" + str(isec+1) + " mod=" + str(imod+1))
                    
        if (bad_ch == bad_ch_now):
            print ("Bad channel search: success")            
            break
        print "Bad channel list temporary: " + str(bad_ch) + " (from mean: " + str(countMeanExtra) + ", rms: " + str(countRmsExtra) + ")"
        
    print "Number of bad channels: " + str(len(bad_ch))
    
    out.Close() # close output file before we start PROOF
    
    
    # now we check if there is any use to start PROOF, or if the last run was already final
    if currIteration > 0:
        if not updateAnyChannel and bad_ch == prevBad:
            print ("")
            print ("No further improvement expected. Bad channels and noise levels are constant. Quitting!")
            print ("")
            print ("Congratulations!")
            print ("")
            if not forceRun:
                sys.exit(0)
            print ("---> forced to run anyway ....")
            print ("")
            
    slaveParams = {"input_mean_ch" : input_mean_ch,
                   "input_rms_ch" : input_rms_ch,
                   "meanErr_ch": meanErr_ch ,
                   "bad_ch" : bad_ch,
                   "zeroSuppressionThreshold": zeroSuppressionThreshold,
                   "nSectorMaxCut": minChannelsSector, # 4, # 6, [larger or equal]
                   "nExtraCut": maxExtraChannels, # 10, # 20, # 5 [smaller or equal]
                   "minChanPerSection" : minChannelsPerSection, # 2 or 1 [larger or equal]
                   "minSections" : minSections, # 3 or 4 [larger or equal]
                   "theSample": sampleList[0]}
    
    MuonSelection.runAll(slaveParameters = slaveParams,
                         sampleList = sampleList, 
                         maxFilesMC = None,
                         maxFilesData = None,
                         nWorkers = 8,
                         outFile = outFileName,
                         verbosity = 2)
    
    print ("Output file name: " + outFileName + " at iteration " + str(currIteration))


    
