
# badChannelsSecMod = [[2,10],[3,11],[12,12],[5,4],[3,8],[7,5],[9,4],[8,8],[4,11],[16,6],[16,4],[15,4]] 
# badChannelsSecMod = [[2,5],[2,10],[3,8],[4,9],[5,9]]
badChannelsSecMod = [[2,10],
                     [9,5],[12,5]] # over-frequent in ZS

# default
zeroSuppressionThreshold = 2.2 # 2.0 was very strong cut

maxExtraChannels = 6
minChannelsSector = 4

# muon penetrating selection:
minChannelsPerSection = 1
minSections = 3

